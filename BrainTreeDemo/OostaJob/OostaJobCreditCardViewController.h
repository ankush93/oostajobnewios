//
//  OostaJobCreditCardViewController.h
//  OostaJob
//
//  Created by Apple on 09/02/16.
//  Copyright © 2016 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BraintreeUI/BraintreeUI.h>
@interface OostaJobCreditCardViewController : UIViewController
- (IBAction)btnBack:(id)sender;
- (IBAction)btnHome:(id)sender;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBg;
@property (weak, nonatomic) IBOutlet BTUICardFormView *cardFormView;
@property (weak, nonatomic) IBOutlet UIButton *btnPay;
- (IBAction)btnPayPressed:(id)sender;
@property (strong, nonatomic) NSString * strToken;
@end
