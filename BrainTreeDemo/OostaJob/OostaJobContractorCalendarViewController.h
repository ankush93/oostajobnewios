//
//  OostaJobContractorCalendarViewController.h
//  OostaJob
//
//  Created by Apple on 21/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"
@interface OostaJobContractorCalendarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnFulldayNotAvailable;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;
- (IBAction)btnFulldayNotAvailablePressed:(id)sender;
- (IBAction)btnResetPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblSchedule;
@property (weak, nonatomic) IBOutlet CKCalendarView *calendar;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblTimings;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnTimings;
- (IBAction)btnTimingPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
- (IBAction)btnMenuTapped:(id)sender;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UIView *viewOtherContents;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
- (IBAction)btnDonePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrlVw;

@end
