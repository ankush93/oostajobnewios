//
//  OostaJobContractorBidNowTableViewCell.h
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "HPTextViewInternal.h"

@interface OostaJobCustomerJobDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContentAll;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispName;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispTime;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispMsg;
@property(weak,nonatomic) IBOutlet AsyncImageView *imgTbl;

@property (weak, nonatomic) IBOutlet UIView *viewCustomer;
@property (weak, nonatomic) IBOutlet UIButton *btnReply;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;

@property (weak, nonatomic) IBOutlet UILabel *lblReply;
@property (weak, nonatomic) IBOutlet UIView *viewSend;
@property (weak, nonatomic) IBOutlet UITextView *txtViewReply;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerMsg;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgReply;

@property (weak, nonatomic) IBOutlet UILabel *lblCustomerMsgTime;

@end
