//
//  ContractorHistoryController.m
//  OostaJob
//
//  Created by Ankush on 22/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import "ContractorHistoryController.h"
#import "ImageCache.h"
@interface ContractorHistoryController ()
{
    NSMutableDictionary *dictJobList;
}
@end

@implementation ContractorHistoryController
@synthesize historytable,img;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = YES;
//    SWRevealViewController *revealController = [self revealViewController];
//    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
//    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self CreateBlurEffectOnBackgroundImage];
    [self ContractorPaymentHistory:^(bool resultJobList){
        NSLog(@"data fetched");
    }];
    self.isPresented = YES; // Set it to true, when controller is initialised
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    CGAffineTransform newTransform = CGAffineTransformMake(0.0,1.0,-1.0,0.0,0.0,0.0);
    self.view.transform = newTransform;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    self.isPresented = NO; // Set it to true, when controller is initialised
[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [super viewWillDisappear:animated];
}


- (NSUInteger)supportedInterfaceOrientations
{return (UIInterfaceOrientationMaskPortrait);}

- (BOOL) shouldAutorotate{
    return YES;
}


#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"less than min ago";
}

-(void)CustomerPaymentHistory:(void (^)(bool resultJobList))completionHandler{
#pragma mark getJobList
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading....";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@myPaymentHistory/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];
        
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getcustomerjoblist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     dictJobList=[[NSMutableDictionary alloc]init];
                     [dictJobList setObject:[result valueForKeyPath:@"response"] forKey:@"response"];
                     
                     NSLog(@"dictJobList %@",dictJobList);
                     
                     NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"response"] count]]];
                     
                     //                         NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                     //
                     //                         if ([[sortedArr lastObject] intValue]>=1)
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"CUSTOMERJOBS" forKey:@"SCREEN"];
                     //                         }
                     //                         else
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     //                         }
                     //                         [self reloadTableViews];
                     //
                     //                         if (isHireTab == YES){
                     //                             [self.tblHire reloadData];
                     //                         }
                     [historytable reloadData];
                     
                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"FaildStatus"] isEqualToString:@"No Jobs Found"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"No History found";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[dictJobList valueForKeyPath:@"response"] count]!=0)
    {
        return 1;
    }
    return 0;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([[dictJobList valueForKeyPath:@"response"] count]!=0)
    {
        return [[dictJobList valueForKeyPath:@"response"] count];
    }
    else
    {
        return 1;
    }
    
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"CELL";
    tableView.backgroundColor=[UIColor clearColor];
    
    
    
    if ([[dictJobList valueForKeyPath:@"response"] count]==0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.text = @"No Jobs in Closed";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
        return cell;
    }
    else
    {
        ContractorHistoryTableViewCell *cell=(ContractorHistoryTableViewCell*)[historytable dequeueReusableCellWithIdentifier:tblIdentifier];
        if (cell==nil)
        {
            NSArray *nib;
            UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            flowLayout.itemSize = CGSizeMake(120, 120);
            cell.collectionView.collectionViewLayout = flowLayout;
            cell.collectionView.backgroundColor=[UIColor clearColor];
            cell.collectionView.tag=501;
            cell.collectionView.hidden=YES;
            cell.collectionView.delegate=self;
            cell.collectionView.dataSource=self;
            
            
            
            nib= [[NSBundle mainBundle] loadNibNamed:@"ContractorHistoryTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            // Disable Cell highlight
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            //Font name annd size
            
            
            [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
            
            
            cell.pinlbl.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Zipcode"]];
            
            
            NSArray *status = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.payment_status"];
            
            NSLog(@"payment status :%@",status);
            NSLog(@"payment status :%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.amount"]);
            
            NSArray *amountarr = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.amount"];
            
            
            NSArray *amountdeduct = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.amount_after_deduction"];
            
            
            NSMutableString * amount2 = [[NSMutableString alloc] init];
            for (NSObject * obj in amountdeduct)
            {
                [amount2 appendString:[obj description]];
            }
            
            
            
            
            NSMutableString * amount1 = [[NSMutableString alloc] init];
            for (NSObject * obj in amountarr)
            {
                [amount1 appendString:[obj description]];
            }
            
            NSArray *posttime = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobpostTime"];
            
            NSMutableString * post = [[NSMutableString alloc] init];
            for (NSObject * obj in posttime)
            {
                [post appendString:[obj description]];
            }
            
            NSArray *jobnamearr = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobtypeName"];
            
            NSMutableString * jobname = [[NSMutableString alloc] init];
            for (NSObject * obj in jobnamearr)
            {
                [jobname appendString:[obj description]];
            }
            
//            NSArray *customernamearr = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Name"];
//
//            NSMutableString * cusname = [[NSMutableString alloc] init];
//            for (NSObject * obj in customernamearr)
//            {
//                [cusname appendString:[obj description]];
//            }
            
//            NSArray *addressarr = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Address"];
//
//            NSMutableString * address = [[NSMutableString alloc] init];
//            for (NSObject * obj in addressarr)
//            {
//                [address appendString:[obj description]];
//            }
            
            
            cell.jobcustomer.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Name"]];
            
            cell.joblocation.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Address"]];
            NSArray *statusarr = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.payment_status"];
            
            NSMutableString * stat = [[NSMutableString alloc] init];
            for (NSObject * obj in statusarr)
            {
                [stat appendString:[obj description]];
            }
            
            if (indexPath.row%2) {
                [cell.cellview setBackgroundColor:[UIColor whiteColor]];
            }
            else{
                [cell.cellview setBackgroundColor:[UIColor lightGrayColor]];

            }
            
            NSArray *earning = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.earning_available_date"];
            
            NSMutableString * ear = [[NSMutableString alloc] init];
            for (NSObject * obj in earning)
            {
                [ear appendString:[obj description]];
            }
            
            
            NSLog(@"payment status :%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobpostTime"]);
            
            cell.jobdate.text = post;
            
            cell.jobcategory.text = jobname;
            
           // cell.jobcustomer.text = cusname;
            
           //  cell.joblocation.text = address;
            
            
            cell.jobvalue.text = [@"$" stringByAppendingString:amount1];
            
            cell.jobearning.text = [@"$" stringByAppendingString:amount2];
            
            cell.earningsavailability.text = ear;
            
            cell.pending.text = stat;
            
            
            NSMutableString * status1 = [[NSMutableString alloc] init];

            for (NSObject * obj in status)
            {
                [status1 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", status1);
            
            cell.paidamount.text=amount1;
            
            
            cell.paidstatus.text=status1;
            
            cell.customername.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Name"]];
            
            cell.address.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerdetails.Address"]];
            
            
            NSLog(@"job list in history:%@",[NSString stringWithFormat:@"%@",[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row]]);
            
       
            
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.jobpostTime"]];
            
            
            
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
            NSString *str1 =[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",dateString]];
            NSString* strName = [[str1 componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString *ct_title = [strName stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
            ct_title = [ct_title stringByReplacingOccurrencesOfString:@",    " withString:@", "];
            
            
            
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 
                 cell.lbltime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            //            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            
           
            
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"answer"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            
            
            [cell.taglist setAutomaticResize:YES];
            [cell.taglist setTags:arrTags];
            [cell.taglist setTagDelegate:self];
            cell.taglist.userInteractionEnabled = NO;
            
           
            //            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"media"] mutableCopy];
            CGFloat origin = cell.taglist.frame.origin.y+cell.taglist.frame.size.height;
            
            
            //        NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            //        arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            //        if (arrContractor.count>0)
            //        {
            
            //        }
            //        else
            //        {
            //            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
            //            cell.lblContratorCount.textColor = [UIColor lightGrayColor];
            //        }
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"media"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSArray *typeofjob = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobtypeName"];
            NSMutableString * jobtypeName1 = [[NSMutableString alloc]init];
            
            for (NSObject * obj in typeofjob)
            {
                [jobtypeName1 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", jobtypeName1);
            
            
            
            cell.labelname.text=jobtypeName1;
            
            NSArray *earn = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.earning_available_date"];
            
            NSMutableString * earnings1 = [[NSMutableString alloc]init];
            
            for (NSObject * obj in earn)
            {
                [earnings1 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", earnings1);
            
            cell.earningslbl.text =earnings1;
            
            
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.job_icon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                               });
                
            }
            
            
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.viewContent.frame.size.height);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:10));
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            origin = origin+(IS_IPAD?8:5);
            
            
            
            //
            //            [cell CollectionData:[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.media"] andtheIndexPath:indexPath andTheTableView:tableView];
            //            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobicon"] stringByDeletingPathExtension];
            //            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            //            {
            //                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            //            }
            //            else
            //            {
            //                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
            //                               {
            //                                   dispatch_async(dispatch_get_main_queue(), ^{
            //                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
            //                                       UIImage *img=[UIImage imageWithData:data];
            //                                       if (img)
            //                                       {
            //                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
            //                                           cell.imgIcon.image=img;
            //                                       }
            //                                   });
            //
            //
            //                               });
            //
            //            }
            //            cell.btnMessage.tag = indexPath.row;
            //            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            //            cell.btnContractor.tag = indexPath.row;
            //            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            //            cell.backgroundColor=[UIColor clearColor];
            //
            //            cell.imgArrow.hidden = YES;
            //            cell.btnTblBidding.userInteractionEnabled = NO;
            //
            //
            //            cell.viewStars.frame=CGRectMake(cell.viewStars.frame.origin.x, cell.viewContractor.frame.origin.y, cell.viewStars.frame.size.width, cell.viewStars.frame.size.height);
            //            float totalRating = [[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.totalRating"]] floatValue];
            //            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            //
            //            NSLog(@"check star rating :%f",totalRating);
            //            if ( totalRating == 0) {
            //                cell.viewStars.hidden =YES;
            //                cell.ratenowbtn.hidden = NO;
            //            }
            //            else{
            //                cell.viewStars.hidden =NO;
            //                cell.ratenowbtn.hidden = YES;
            //
            //            }
            //            for (int i=1; i<=5; i++)
            //            {
            //                for (UIImageView *img in cell.imgStars)
            //                {
            //                    if (img.tag == 500+i)
            //                    {
            //                        if (i<=totalRating)
            //                        {
            //                            img.image = [UIImage imageNamed:@"star-fill.png"];
            //                        }
            //                        else if ((i-0.5)==totalRating)
            //                        {
            //                            img.image = [UIImage imageNamed:@"half.png"];
            //                        }
            //                        else
            //                        {
            //                            img.image =[UIImage imageNamed:@"empty.png"];
            //                        }
            //                    }
            //                }
            //            }
            //        }
        }
        
        return cell;
        
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(void)CreateBlurEffectOnBackgroundImage
{
    [self.img assignBlur];
}


- (IBAction)OnclickMenu:(id)sender {
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)ContractorPaymentHistory:(void (^)(bool resultJobList))completionHandler{
#pragma mark getJobList
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading....";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@contractorPaymentHistory/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];
        
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getcustomerjoblist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     dictJobList=[[NSMutableDictionary alloc]init];
                     [dictJobList setObject:[result valueForKeyPath:@"response"] forKey:@"response"];
                     
                     NSLog(@"dictJobList %@",dictJobList);
                     
                     NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"response"] count]]];
                     
                     //                         NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                     //
                     //                         if ([[sortedArr lastObject] intValue]>=1)
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"CUSTOMERJOBS" forKey:@"SCREEN"];
                     //                         }
                     //                         else
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     //                         }
                     //                         [self reloadTableViews];
                     //
                     //                         if (isHireTab == YES){
                     //                             [self.tblHire reloadData];
                     //                         }
                     [historytable reloadData];
                     
                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"FaildStatus"] isEqualToString:@"No Jobs Found"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"No jobs found";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
    
}
- (IBAction)OnclickBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
