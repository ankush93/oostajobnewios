//
//  OostaJobContractorBillingViewController.h
//  OostaJob
//
//  Created by Armor on 24/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BraintreeUI/BraintreeUI.h>
#import "OostaJobFeedbackViewController.h"
#import "OostaJobCustomerJobsViewController.h"
@interface OostaJobContractorBillingViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UILabel *lblBilling;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDollar;
@property (weak, nonatomic) IBOutlet UILabel *lbl30;
@property (weak, nonatomic) IBOutlet UILabel *lblSubscription;
@property (weak, nonatomic) IBOutlet UILabel *lblPayUsing;
@property (weak, nonatomic) IBOutlet UILabel *lblPermonth;
@property (weak, nonatomic) IBOutlet UIButton *btnUnsubscribe;
- (IBAction)btnMenuTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewUnsubscribe;
@property (weak,nonatomic) NSDate *selecteddate1;
@property (weak,nonatomic) NSString *selectedtime1;
@property (weak,nonatomic) NSString *struserid1;
@property (weak,nonatomic) NSString *strpostid1;
@property (weak,nonatomic) NSString *fromcomplete;
@property (strong,nonatomic) NSMutableArray *arraydetails;

@end
