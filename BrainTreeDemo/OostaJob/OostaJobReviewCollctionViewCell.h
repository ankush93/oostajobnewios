//
//  OostaJobReviewCollctionViewCell.h
//  OostaJob
//
//  Created by Apple on 13/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface OostaJobReviewCollctionViewCell : UICollectionViewCell
@property(nonatomic,strong)IBOutlet AsyncImageView *imgProfile;
@end
