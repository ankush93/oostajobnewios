//
//  OostaJobAppDelegate.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobAppDelegate.h"
#import "OostaJobWelcomeViewController.h"
#import "SWRevealViewController.h"
#import "RearViewController.h"
#import "OostaJobEmailVerificationViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "OostaJobContractorStatusViewController.h"
#import "ContractorRearViewController.h"
#import "OostaJobMapViewController.h"
#import <BraintreeCore/BraintreeCore.h>
#import "Flurry.h"
#import "TestFairy.h"
#import "AGPushNoteView.h"
#import "Reachability.h"
#import "OostaJobCustomerJobsViewController.h"
#import "OostaJobContractorJobsViewController.h"
#import "OostaJobBuildJobViewController.h"
#import "ContractorHistoryController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@interface OostaJobAppDelegate()<SWRevealViewControllerDelegate>
{
    
}
@property (nonatomic) Reachability *internetReachability;
@end

@implementation OostaJobAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self getTheServiceURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
    [Fabric with:@[[Crashlytics class]]];
#ifdef __IPHONE_8_0
    //Right, that is the point
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                         |UIRemoteNotificationTypeSound
                                                                                         |UIRemoteNotificationTypeAlert) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
#else
    //register to receive notifications
    UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
#endif
    
    
    // Override point for customization after application launch.
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"REGISTEREDCUSTOMER"])
    {
        [self gotoListOfJobs];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"REGISTEREDCONTRACTOR"])
    {
        [self gotoContractorJobList];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"CUSTOMERJOBS"])
    {
        [self gotoJobsCustomer];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"CONTRACTORJOBS"])
    {
        [self gotoContractorJobs];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"EMAIL"])
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController IPad" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:emailVerificationObj];
            self.window.rootViewController =naviObj;
        }
        else if (IS_IPHONE5)
        {
            OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:emailVerificationObj];
            self.window.rootViewController =naviObj;
        }
        else
        {
            OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:emailVerificationObj];
            self.window.rootViewController =naviObj;
        }
        
        
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"PROFILE"])
    {
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobContractorStatusViewController *profileVerificationObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController IPad" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:profileVerificationObj];
            self.window.rootViewController =naviObj;
        }
        else if (IS_IPHONE5)
        {
            OostaJobContractorStatusViewController *profileVerificationObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:profileVerificationObj];
            self.window.rootViewController =naviObj;
        }
        else
        {
            OostaJobContractorStatusViewController *profileVerificationObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController Small" bundle:nil];
            UINavigationController *naviObj=[[UINavigationController alloc]initWithRootViewController:profileVerificationObj];
            self.window.rootViewController =naviObj;
        }
        
        
    }
    else
    {
        OostaJobWelcomeViewController *frontViewController;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
        }
        else if (IS_IPHONE5)
        {
            frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
        }
        else
        {
            frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
        }
        
        RearViewController *rearViewController;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController IPad" bundle:nil];
        }
        else if (IS_IPHONE5)
        {
            rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController" bundle:nil];
        }
        else
        {
            rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController Small" bundle:nil];
        }
        
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                        initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        
        mainRevealController.delegate = self;
        
        self.viewController = mainRevealController;
        
        self.window.rootViewController = self.viewController;
    }
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [TestFairy begin:@"b11627bd4f48e3b315654f1579e412eb9e206485"];
    
    
    //PayPal
    if (!TARGET_IPHONE_SIMULATOR)
    [BTAppSwitch setReturnURLScheme:BraintreeDemoAppDelegatePaymentsURLScheme];
    [self FlurryIntegration];
    return YES;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([self.window.rootViewController.presentedViewController isKindOfClass:[ContractorHistoryController class]])
    {
        ContractorHistoryController *secondController = (ContractorHistoryController *) self.window.rootViewController.presentedViewController;
        
        if (secondController.isPresented) // Check current controller state
        {
            return UIInterfaceOrientationMaskAll;
        }
        else return UIInterfaceOrientationMaskPortrait;
    }
    else return UIInterfaceOrientationMaskPortrait;
}


#pragma mark Flurry
-(void)FlurryIntegration
{
    [Flurry startSession:@"Y7GQF2K4GM85NZSSCNXF"];
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setPulseEnabled:YES];
    CLLocationManager *locationManager = [[CLLocationManager alloc] init]; [locationManager startUpdatingLocation];
    
    CLLocation *location = locationManager.location;
    [Flurry setLatitude:location.coordinate.latitude longitude:location.coordinate.longitude
     horizontalAccuracy:location.horizontalAccuracy verticalAccuracy:location.verticalAccuracy];
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
- (BOOL)application:(__unused UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    if ([[url.scheme lowercaseString] isEqualToString:[BraintreeDemoAppDelegatePaymentsURLScheme lowercaseString]]) {
        if (!TARGET_IPHONE_SIMULATOR)
        return [BTAppSwitch handleOpenURL:url options:options];
    }
    return YES;
}
#endif

// Deprecated in iOS 9, but necessary to support < versions
- (BOOL)application:(__unused UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(__unused id)annotation {
    if ([[url.scheme lowercaseString] isEqualToString:[BraintreeDemoAppDelegatePaymentsURLScheme lowercaseString]]) {
        if (!TARGET_IPHONE_SIMULATOR)
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    }
    return YES;
}

void uncaughtExceptionHandler(NSException *exception)
{
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

- (BOOL) prefersStatusBarHidden
{
    return YES;
}
-(void)gotoListOfJobs
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    UINavigationController *viewControllerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.window.rootViewController = viewControllerNavigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
//    [self goToBuildYourJob];
    /*if ([[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"SKIPLOGIN"] isEqualToString:@"SKIPLOGIN"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SKIPLOGIN"];
        NSString *savedClassName = [[NSUserDefaults standardUserDefaults] objectForKey:@"OostaJobBuildJobViewController"];
        
        Class screenClass = NSClassFromString(savedClassName);
        
        id newClass = [[screenClass alloc] init];
        
        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
        [navigationController pushViewController:newClass animated:YES];
    }*/
}
-(void)gotoJobsCustomer
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    OostaJobCustomerJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
    }
    CustomerfrontViewController.strHome = @"YES";
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


-(void)gotoContractorJobList
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    OostaJobMapViewController *ContractorfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    else
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    
    ContractorRearViewController *ContraRearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ContractorfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:ContraRearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}
-(void)gotoContractorJobs
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    OostaJobContractorJobsViewController *ContractorfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContractorfrontViewController = [[OostaJobContractorJobsViewController alloc] initWithNibName:@"OostaJobContractorJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContractorfrontViewController = [[OostaJobContractorJobsViewController alloc] initWithNibName:@"OostaJobContractorJobsViewController" bundle:nil];
    }
    else
    {
        ContractorfrontViewController = [[OostaJobContractorJobsViewController alloc] initWithNibName:@"OostaJobContractorJobsViewController" bundle:nil];
    }
    ContractorfrontViewController.strHome = @"YES";
    
    ContractorRearViewController *ContraRearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ContractorfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:ContraRearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


-(void)getTheServiceURL
{
    NSString* urlStr1 =[NSString stringWithFormat:@"%@urllinks",BaseURL];
    NSLog(@"GetTheServiceURL Url %@",urlStr1);
    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
    [NSURLConnection sendAsynchronousRequest:request1
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSArray *statusArr =[result valueForKeyPath:@"Result"];
             NSLog(@"Status %@",statusArr);
             NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
             if ([resultStr isEqualToString:@"Success"])
             {
                 NSLog(@"result:%@",result);
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Aboutus"] forKey:@"Aboutus"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"FAQ"] forKey:@"FAQ"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"ContractorTerms"] forKey:@"ContractorTerms"];
                [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerTerms"] forKey:@"CustomerTerms"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerStories"] forKey:@"CustomerStories"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Contractorpayments"] forKey:@"Contractorpayments"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Customerpaymentsterms"] forKey:@"Customerpaymentsterms"];
             }
             else
             {
                 NSLog(@"result:%@",result);
             }
         }
         else
         {
             NSLog(@"error %@",connectionError);
         }
     }];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //<ff57b7ed 68331bd0 c1467d93 e61e671e 670bc85b 44bb8144 ec33ef92 69445c44>
    NSLog(@"My token is: %@", deviceToken);
    NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
    NSString *strToken = [[[[deviceToken description]
                            stringByReplacingOccurrencesOfString: @"<" withString: @""]
                           stringByReplacingOccurrencesOfString: @">" withString: @""]
                          stringByReplacingOccurrencesOfString: @" " withString: @""] ;
    NSLog(@"Device_Token %@\n",strToken);
    if (strToken.length==0)
    {
        strToken=@"28d1e4a814db45b0cdabf929106d5f9dd48d966900f1c591dcff70ecdd6b5e6d";
    }
    [userValue setObject:strToken forKey:@"DEVICE_TOKEN"];
    
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"OOstaJob" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"OostaJob.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - SWRevealViewController Delegate
- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}


- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController willRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willShowFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didShowFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideFrontViewController:(UIViewController *)rearViewController

{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

#pragma mark didReceieve Remote Notification

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
#if !TARGET_IPHONE_SIMULATOR
    
    NSLog(@"User_Info:%@",userInfo);
    
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        
        AGPushNote* pushNote = [[AGPushNote alloc] init];
        
        pushNote.message = [NSString stringWithFormat:@"%@", [apsInfo valueForKeyPath:@"alert"]];
        [AGPushNoteView showNotification:pushNote];
        
        [AGPushNoteView setMessageAction:^(AGPushNote *pushNote) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                            message:pushNote.message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Close"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
        if ([[apsInfo valueForKeyPath:@"key"] isEqualToString:@"Message"])
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"NotificationReload" object:apsInfo];
        }
        
    }
    else
    {
        
    }

#endif
}

#pragma mark Reachability
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}
- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable)
    {
    }
    else
    {
        NSLog(@"No Internet Access");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"No Internet Access"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)goToBuildYourJob
{
    OostaJobBuildJobViewController *BuildObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController" bundle:nil];
    }
    else
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController" bundle:nil];
    }
    BuildObj.strBuildJob = @"YES";
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    [navigationController pushViewController:BuildObj animated:YES];
}
@end
