//
//  OostaJobMapViewController.m
//  OostaJob
//
//  Created by Apple on 21/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobMapViewController.h"
#import "OostaJobContractorTableViewCell.h"
#import "SWRevealViewController.h"
#import "OostaJobCustomAnnotation.h"
#import "DWTagList.h"
#import <AVFoundation/AVFoundation.h>
#import "ImageCache.h"
#import "OostaJobContractorBidNowViewController.h"


@interface OostaJobMapViewController ()<DWTagListDelegate,UICollectionViewDataSource,UICollectionViewDelegate,SWRevealViewControllerDelegate,UIActionSheetDelegate,MXLMediaViewDelegate,OostaJobContractorBidNowViewControllerDelegate>
{
    NSMutableArray *arrListOfJobTypes;
    int page;
    NSMutableArray *arrJobList;
    CGFloat heightCell;
    NSMutableArray *arrHeights;
    
}
@end

@implementation OostaJobMapViewController
@synthesize table;
@synthesize btnSender;
@synthesize list;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    
    self.navigationController.navigationBarHidden=YES;
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self getJobType];
    [self settingScrollView];
    page = 0;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    [self numberToolBar];
    
    _tblViewJobList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self getTheServiceURL];

}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
-(void)numberToolBar
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    _txtSearch.inputAccessoryView = numberToolbar;
}

-(void)doneWithNumberPad
{
    self.strZipcode = _txtSearch.text;
    
    [_txtSearch resignFirstResponder];
    
    [self getJobListForContractor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblTotalJobsAvailable.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        _txtJobCategory.font=[UIFont fontWithName:FONT_THIN size:26.0f];
        _txtSearch.font=[UIFont fontWithName:FONT_THIN size:26.0f];
        _txtJobCategory.layer.cornerRadius = 5.0;
        _txtJobCategory.layer.masksToBounds = YES;
        _txtSearch.layer.cornerRadius = 5.0;
        _txtSearch.layer.masksToBounds = YES;
        
        _mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
        _mapViewController.delegate = self;
        [_mapViewController.view setFrame:CGRectMake(0, 0, 768, 844)];
        [self.scrollView addSubview:_mapViewController.view];
        
        

        
    }else
    {
        self.lblTotalJobsAvailable.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        _txtJobCategory.font=[UIFont fontWithName:FONT_THIN size:18.0f];
        _txtSearch.font=[UIFont fontWithName:FONT_THIN size:18.0f];
        
        _mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
        _mapViewController.delegate = self;
        [self.scrollView addSubview:_mapViewController.view];
        [_mapViewController.view setFrame:CGRectMake(0, 0, 320, _scrollView.frame.size.height)];
    }
   }

- (IBAction)MenubtnTapped:(id)sender
{
    [self hideDropDown:sender];
    [self rel];
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

- (IBAction)RightMenubtnTapped:(id)sender
{
    UIButton* RightMenuBtn = (UIButton*)sender;
    if (page==0)
    {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * 1;
        [self.scrollView scrollRectToVisible:frame animated:YES];
        [RightMenuBtn setImage:[UIImage imageNamed:@"map_toggle.png"] forState:UIControlStateNormal];
        
    }
    else if (page==1)
    {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * 0;
        [self.scrollView scrollRectToVisible:frame animated:YES];
        [RightMenuBtn setImage:[UIImage imageNamed:@"menu-toggle.png"] forState:UIControlStateNormal];
        
    }
    
}
-(void)getJobListForContractor
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Jobs...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    arrJobList = [[NSMutableArray alloc]init];
    arrHeights = [[NSMutableArray alloc]init];
    
    NSString * key1 =@"zipcode";
    NSString * obj1 =self.strZipcode?self.strZipcode:@"";;
    NSString * key2 =@"expertise";
    NSString * obj2 =self.strExpertise?self.strExpertise:@"";
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2]
                                   forKeys:@[key1,key2]];
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcontractorjobs/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getcontractorjobs:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 
                 arrJobList = [[result valueForKeyPath:@"Contractor_joblist"] mutableCopy];
                 if ([[result valueForKeyPath:@"Contractor_joblist"] count]==1)
                 {
                    _lblTotalJobsAvailable.text=[NSString stringWithFormat:@"%u job around you Bid now !",[[result valueForKeyPath:@"Contractor_joblist"] count]];
                 }
                 else
                 {
                     _lblTotalJobsAvailable.text=[NSString stringWithFormat:@"%u jobs around you Bid now !",[[result valueForKeyPath:@"Contractor_joblist"] count]];
                 }
                 
                 for (int i=0; i<[[result valueForKeyPath:@"Contractor_joblist"] count]; i++)
                 {
                     [arrHeights addObject:[NSString stringWithFormat:@"%d",0]];
                 }
                 [self.tblViewJobList reloadData];
                 [_mapViewController resetAnnitations:arrJobList];
             }
             else if([[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"Contractor_joblist"]] isEqualToString:@""])
             {
                 
             }
             else if([[result valueForKeyPath:@"Contractor_joblist"] count] ==0)
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = @"No matching jobs found";
                 _lblTotalJobsAvailable.text =  @"No matching jobs found";;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 [self.tblViewJobList reloadData];
                 [_mapViewController resetAnnitations:nil];
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 [self.tblViewJobList reloadData];
             }
         }
         
     }];
    }
}
- (IBAction)btnSearchZipCode:(id)sender
{
    
}

- (IBAction)btnSelectJobs:(id)sender
{
    if (arrListOfJobTypes.count>1)
    {
        if(_dropDown == nil)
        {
            CGFloat f = 150;
            _dropDown = [[UIView alloc]init];
            [self showDropDown:sender andHeight:&f theContentArr:arrListOfJobTypes theDiection:@"down"];
//            [self.mapView bringSubviewToFront:_dropDown];
            
        }
        else {
            [self hideDropDown:sender];
            [self rel];
        }
    }
    
    
}


#pragma mark DropDown

- (void)showDropDown:(UIButton *)b andHeight:(CGFloat *)height theContentArr:(NSArray *)arr theDiection:(NSString *)direction
{
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    
    // Initialization code
    CGRect btn = b.frame;
    self.list = [NSArray arrayWithArray:arr];
    NSLog(@"arr:%@",arr);
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, -5);
    }else if ([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    self.dropDown.layer.masksToBounds = NO;
    self.dropDown.layer.cornerRadius = 8;
    self.dropDown.layer.shadowRadius = 5;
    self.dropDown.layer.shadowOpacity = 0.5;
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
    table.delegate = self;
    table.dataSource = self;
    table.layer.cornerRadius = 5;
    table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.separatorColor = [UIColor grayColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
    } else if([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, *height);
    [UIView commitAnimations];
    
    [self.dropDown addSubview:table];
    [self.viewContentHolder.superview addSubview:self.dropDown];
    [table reloadData];
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}
-(void)rel
{
    self.dropDown=nil;
}

#pragma mark -UitableView DataSource and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewJobList)
    {
        static NSString *tblIdentifier=@"CELL";
        OostaJobContractorTableViewCell *cell=(OostaJobContractorTableViewCell*)[self.tblViewJobList dequeueReusableCellWithIdentifier:tblIdentifier];
        if (cell==nil)
        {
            NSArray *nib;
            if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                
                nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell IPad" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
            }
            else
            {
                nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
        }
        
        NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
        arrAnswers = [[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
        NSMutableArray *arrTags=[[NSMutableArray alloc]init];
        for (int i=0; i<arrAnswers.count; i++)
        {
            if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
            {
                if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                {
                    [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                }
                else
                {
                    [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]]];
                }
                
            }
        }
        
        [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[arrJobList  objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
        
        NSString *str=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
        
        NSString *strRemining=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
        
        NSMutableArray *arr=[[NSMutableArray alloc]init];
        arr=[[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
        
        [cell.tagList setAutomaticResize:YES];
        [cell.tagList setTags:arrTags];
        [cell.tagList setTagDelegate:self];
        heightCell = 0;
        heightCell=cell.tagList.frame.size.height+cell.tagList.frame.origin.y;
        if (strRemining.length!=0)
        {
            heightCell = heightCell+23;
        }
        
        if (str.length!=0)
        {
            heightCell = heightCell+47;
        }
        
        if (arr.count!=0)
        {
            heightCell = heightCell+(IS_IPAD?133:63);
        }
        NSLog(@"height:%f",heightCell);
        [arrHeights replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%f",heightCell+10]];
        return heightCell+10;
    }
    else
    {
        return IS_IPAD?60:40;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblViewJobList)
    {
        return [arrJobList count];
    }
    else
    {
        return [self.list count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewJobList)
    {
        static NSString *tblIdentifier=@"CELL";
        OostaJobContractorTableViewCell *cell=(OostaJobContractorTableViewCell*)[self.tblViewJobList dequeueReusableCellWithIdentifier:tblIdentifier];
        if (cell==nil)
        {
            NSArray *nib;
            UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            flowLayout.itemSize = CGSizeMake(120, 120);
            cell.collectionView.collectionViewLayout = flowLayout;
            cell.collectionView.backgroundColor=[UIColor clearColor];
            cell.collectionView.tag=501;
            cell.collectionView.hidden=YES;
            cell.collectionView.delegate=self;
            cell.collectionView.dataSource=self;
            if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                
                
                nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell IPad" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                // Disable Cell highlight
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                //Font name annd size
                cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                
                cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                cell.viewbids.layer.cornerRadius=15.0;
                cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                cell.lblJobClosedTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                
                [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                
                cell.viewMessage.layer.cornerRadius=5.0;
                cell.viewContractor.layer.cornerRadius=5.0;
                cell.viewbids.layer.cornerRadius=14;
                cell.viewContent.layer.cornerRadius=5;
                cell.viewbids.tag=100;
                cell.btnTblBidding.tag=200;
                cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
            }
            else
            {
                nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                // Disable Cell highlight
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                //Font name annd size
                
                cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                cell.viewbids.layer.cornerRadius=10.0;
                cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                
                
                cell.viewMessage.layer.cornerRadius=5.0;
                cell.viewContractor.layer.cornerRadius=5.0;
                cell.viewContent.layer.cornerRadius=5;
                cell.lblJobClosedTime.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                
                [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                cell.viewbids.tag=100;
                cell.btnTblBidding.tag=200;
            }
        }
        
        NSLog(@"arrJobList==>>%@",arrJobList);
        
        cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
        cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
        cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
        NSString *milesRemining = [NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"Miles"]];
        if ([milesRemining integerValue]>1)
        {
            cell.lblTime.text = [NSString stringWithFormat:@"%@ miles from here",milesRemining];
        }
        else
        {
            cell.lblTime.text = cell.lblTime.text = [NSString stringWithFormat:@"< 1 mile from here"];
        }
        cell.lblJobClosedTime.text=[NSString stringWithFormat:@"Job closes in %@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
        
        
        cell.btnTblBidding.tag=indexPath.row;
        
        NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
        arrAnswers = [[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
        NSMutableArray *arrTags=[[NSMutableArray alloc]init];
        for (int i=0; i<arrAnswers.count; i++)
        {
            if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
            {
                if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                {
                    [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                }
                else
                {
                    [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]]];
                }
                
            }
        }
        
        
        if ([[arrJobList  objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"] == (NSString *)[NSNull null])
        {
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",@"0"]];
            cell.tagList.isHighlightLast = YES;
        }
        else
        {
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[arrJobList  objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
            cell.tagList.isHighlightLast = YES;
        }
        
        
        
        [cell.tagList setAutomaticResize:YES];
        [cell.tagList setTags:arrTags];
        [cell.tagList setTagDelegate:self];
        CGFloat individualHeight = [[arrHeights objectAtIndex:indexPath.row] floatValue];
        
        NSString *str=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
        NSString *strRemining=[NSString stringWithFormat:@"%@",[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
        
        
        NSMutableArray *arr=[[NSMutableArray alloc]init];
        arr=[[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
        CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
        
        cell.lblJobClosedTime.frame=CGRectMake(cell.lblJobClosedTime.frame.origin.x, origin+3, cell.lblJobClosedTime.frame.size.width, 20);
        
        if (strRemining.length!=0)
        {
            origin = origin+23;
        }
        else
        {
            cell.lblJobClosedTime.hidden=YES;
            origin=origin+0;
        }
        
        
        cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+3, cell.lblDesc.frame.size.width, 44);
        
        if (str.length!=0)
        {
            
            origin=origin+47;
        }
        else
        {
            cell.lblDesc.hidden=YES;
            origin=origin+0;
        }
        
        cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+3, cell.collectionView.frame.size.width, IS_IPAD?130:60);
        
        if (arr.count!=0)
        {
            origin = origin + (IS_IPAD?133:63);
        }
        else
        {
            cell.collectionView.hidden=YES;
            origin=origin+0;
        }
        cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+5, cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
        cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+5, cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
        origin = origin +5;
        
        cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, individualHeight-7);
        
        cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, individualHeight+7);
        
        [cell CollectionData:[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
        NSString *imageCacheKey = [[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"icon"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                           {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrJobList objectAtIndex:indexPath.row] valueForKeyPath:@"icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                   UIImage *img=[UIImage imageWithData:data];
                                   if (img)
                                   {
                                       [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                       cell.imgIcon.image=img;
                                   }
                                   else
                                   {
                                       NSLog(@"Not Found");
                                   }
                               });
                               
                               
                           });
            
        }
        cell.btnTblBidding.userInteractionEnabled=NO;
        cell.tagList.userInteractionEnabled=NO;
        cell.viewContent.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        cell.viewContent.layer.borderWidth = 1.0f;
        cell.viewContent.layer.cornerRadius=5.0f;
        cell.viewContent.layer.masksToBounds = YES;
        
        return cell;
        
        
        
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:13.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        if (indexPath.row==list.count-1)
        {
            cell.textLabel.text = [list objectAtIndex:indexPath.row];
        }
        else
        {
            cell.textLabel.text = [[list objectAtIndex:indexPath.row] valueForKeyPath:@"job_name"];
        }
        
        cell.textLabel.textColor = [UIColor blackColor];
        if ([indexPathSel isEqual: indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewJobList)
    {
        OostaJobContractorBidNowViewController *BidObj;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController IPad" bundle:nil];
        }
        else if (IS_IPHONE5)
        {
            BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
        }
        else
        {
            BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
        }
//        BidObj.delegate =self;
        BidObj.arrSelected = [arrJobList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:BidObj animated:YES];
    }
    else
    {
        NSLog(@"didselect");
        [self hideDropDown:btnSender];
        [self rel];
        
        if (indexPath.row==list.count-1)
        {
            _txtJobCategory.text = [list objectAtIndex:indexPath.row];
            self.strExpertise = @"";
        }
        else
        {
            _txtJobCategory.text = [[list objectAtIndex:indexPath.row] valueForKeyPath:@"job_name"];
            
            self.strExpertise = [[list objectAtIndex:indexPath.row] valueForKeyPath:@"jobtype_id"];
        }
        
        if(indexPathSel)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:indexPathSel];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([indexPathSel isEqual:indexPath])
        {
            indexPathSel = nil;
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            indexPathSel = indexPath;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            indexPathSel = indexPath;
        }
        [self getJobListForContractor];
    }
}



#pragma mark Job types
-(void)getJobType
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobtype",BaseURL]]];
    NSLog(@"getjobtype URL:%@",request.URL);
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 NSMutableArray * JobTypeArr=[[NSMutableArray alloc]init];
                 JobTypeArr=[result valueForKeyPath:@"JobtypeDetails"];
                 NSLog(@"Job types%@",JobTypeArr);
                 
                 arrListOfJobTypes = [[NSMutableArray alloc]init];
                 NSArray *arrSelectedExpertise=[[NSArray alloc]init];
                 arrSelectedExpertise = [[[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.expertiseAT"]] stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","] mutableCopy];
                 for (int i=0; i<JobTypeArr.count; i++)
                 {
                     if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[JobTypeArr objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
                     {
                         [arrListOfJobTypes addObject:[JobTypeArr objectAtIndex:i]];
                     }
                 }
                 if (arrListOfJobTypes.count>1)
                 {
                     [arrListOfJobTypes addObject:@"All Jobs"];
                     _txtJobCategory.text=@"All Jobs";
                 }
                 else
                 {
                     
                     _txtJobCategory.text=[NSString stringWithFormat:@"%@",[[arrListOfJobTypes objectAtIndex:arrListOfJobTypes.count-1] valueForKeyPath:@"job_name"]];
                 }
                 
                 
                 indexPathSel =[NSIndexPath indexPathForRow:arrListOfJobTypes.count-1 inSection:0];
                 NSLog(@"arrListOfJobTypes:%@",arrListOfJobTypes);
             }
             else
             {
                 
             }
         }
     }];
    
}
#pragma mark touch
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint location = [[touches anyObject] locationInView:self.view];
    CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
    
    for(UIView *view in self.view.subviews){
        CGRect subviewFrame = view.frame;
        
        if(CGRectIntersectsRect(fingerRect, subviewFrame)){
            //we found the finally touched view
            NSLog(@"Yeah !, i found it %@",view);
            if (view!=_dropDown)
            {
                [self hideDropDown:btnSender];
                [self rel];
            }
        }
        
    }
    
}

#pragma mark ScrollView
-(void)settingScrollView
{
    self.scrollView.pagingEnabled = YES;
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width * 2, self.scrollView.frame.size.height);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView==_scrollView)
    {
        //find the page number you are on
        CGFloat pageWidth = scrollView.frame.size.width;
        page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        NSLog(@"Dragging - You are now on page %i",page);
    }
}
//dragging ends, please switch off paging to listen for this event
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *) targetContentOffset
NS_AVAILABLE_IOS(5_0){
    if(scrollView==_scrollView)
    {
        //find the page number you are on
        CGFloat pageWidth = scrollView.frame.size.width;
        page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        NSLog(@"Dragging - You are now on page %i",page);
    }
    
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView==_scrollView)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    }
}

#pragma mark Location

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSLog(@"locations:%@",locations);
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error)
                       {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           [manager stopUpdatingLocation];
                           
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                           self.strZipcode=placemark.postalCode;
                           
                           if (_txtSearch.text.length==0)
                           {
                               _txtSearch.text = self.strZipcode;
                              [self getJobListForContractor];
                           }
                           
                       }
                       
                   }];
}
-(void)showVideoPreview:(NSMutableArray *)array
{
   /* if ([[array valueForKey:@"mediaType"] isEqualToString:@"1"])
    {
        MXLMediaView *mediaView = [[MXLMediaView alloc] init];
        
        [mediaView setDelegate:self];
        NSString *imageCacheKey = [[array valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            [mediaView showImage:[[ImageCache sharedImageCache]imageForKey:imageCacheKey] inParentView:self.view completion:^{
                NSLog(@"Done showing MXLMediaView");
            }];
        }
        else
        {
            NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[array valueForKey:@"mediaContent"]]]];
            UIImage *img=[UIImage imageWithData:data];
            [mediaView showImage:img inParentView:self.view completion:^{
                NSLog(@"Done showing MXLMediaView");
            }];
        }
        
        
    }
    else if ([[array valueForKey:@"mediaType"] isEqualToString:@"2"])
    {
        MXLMediaView *mediaView = [[MXLMediaView alloc] init];
        [mediaView setDelegate:self];
        
        // The best video on the Internet.
        NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[array valueForKey:@"mediaContent"]]];
        
        [mediaView showVideoWithURL:videoURL inParentView:self.view completion:^{
            NSLog(@"Complete");
            
        }];
    }*/
    MXLMediaView *mediaView = [[MXLMediaView alloc] init];
    mediaView.arrMedia =  array;
    mediaView.selectedIndex = 0;
    [mediaView setDelegate:self];
    [mediaView addingMediaInScrollViewinParentView:self.view completion:^{
        NSLog(@"Done showing MXLMediaView");
    }];
}
-(void)gotoDetailsPage:(NSMutableArray *)array
{
    OostaJobContractorBidNowViewController *BidObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
    }
    else
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
    }
    BidObj.arrSelected = [array mutableCopy];
//    BidObj.delegate =self;
    [self.navigationController pushViewController:BidObj animated:YES];
}
-(void)getreload
{
    [self getJobListForContractor];
}
- (IBAction)btnSearchPressed:(id)sender
{
    self.strZipcode = _txtSearch.text;
    
    [_txtSearch resignFirstResponder];
    
    [self getJobListForContractor];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
}


-(void)getTheServiceURL
{
    NSString* urlStr1 =[NSString stringWithFormat:@"%@urllinks",BaseURL];
    NSLog(@"GetTheServiceURL Url %@",urlStr1);
    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
    [NSURLConnection sendAsynchronousRequest:request1
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSArray *statusArr =[result valueForKeyPath:@"Result"];
             NSLog(@"Status %@",statusArr);
             NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
             if ([resultStr isEqualToString:@"Success"])
             {
                 NSLog(@"result:%@",result);
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Aboutus"] forKey:@"Aboutus"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"FAQ"] forKey:@"FAQ"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"ContractorTerms"] forKey:@"ContractorTerms"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerTerms"] forKey:@"CustomerTerms"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerStories"] forKey:@"CustomerStories"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Contractorpayments"] forKey:@"Contractorpayments"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Customerpaymentsterms"] forKey:@"Customerpaymentsterms"];
             }
             else
             {
                 NSLog(@"result:%@",result);
             }
         }
         else
         {
             NSLog(@"error %@",connectionError);
         }
     }];
}
@end
