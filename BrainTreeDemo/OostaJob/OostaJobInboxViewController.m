//
//  OostaJobInboxViewController.m
//  OostaJob
//
//  Created by Armor on 23/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobInboxViewController.h"
#import "OostaJobInboxTableViewCell.h"
@interface OostaJobInboxViewController ()
{
    NSMutableArray *arrInboxList;
}
@end

@implementation OostaJobInboxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.imgBlur assignBlur];
    [self setupUI];
    
    _tblInbox.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self getInbox];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupUI
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        
        self.lblInboxHeading.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
    }
    else
    {
        self.lblInboxHeading.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
}
#pragma mark-TableView DataSource and Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrInboxList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"Cellname";
    
    OostaJobInboxTableViewCell *cell=(OostaJobInboxTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tblIdentifier];
    
    if (cell==nil) {
        
        NSArray *nib;
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobInboxTableViewCell iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.lblTblTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
            cell.lblTblDate.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
            cell.lblTblDetails.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        }
        else
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobInboxTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.lblTblTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
            cell.lblTblDate.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
            cell.lblTblDetails.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
            
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?717:282, FLT_MAX);
    NSString *str = [NSString stringWithFormat:@"%@",[[arrInboxList objectAtIndex:indexPath.row] valueForKeyPath:@"message"]];
    CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?25.0f:16.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    
    
    cell.lblTblTitle.text = [NSString stringWithFormat:@"%@",[[arrInboxList objectAtIndex:indexPath.row] valueForKeyPath:@"subject"]];
    NSString *dateString = [NSString stringWithFormat:@"%@",[[arrInboxList objectAtIndex:indexPath.row] valueForKeyPath:@"createdTime"]];
    __block NSDate *detectedDate;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
    [detector enumerateMatchesInString:dateString
                               options:kNilOptions
                                 range:NSMakeRange(0, [dateString length])
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         detectedDate = result.date;
         NSDateFormatter *dateFor = [[NSDateFormatter alloc]init];
         [dateFor setDateFormat:@"dd MMM YYYY"];
         cell.lblTblDate.text = [dateFor stringFromDate:detectedDate];
     }];
    
    
    cell.lblTblDetails.text=[NSString stringWithFormat:@"%@",[[arrInboxList objectAtIndex:indexPath.row] valueForKeyPath:@"message"]];
    [cell.lblTblDetails sizeToFit];
    cell.lblTblDetails.numberOfLines = 0;
    cell.lblTblDetails.frame = CGRectMake(cell.lblTblDetails.frame.origin.x, cell.lblTblDetails.frame.origin.y, cell.lblTblDetails.frame.size.width, expectedLabelSize.height);
    cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.lblTblDetails.frame.origin.y+expectedLabelSize.height+5);
    
    cell.btnRemove.tag = indexPath.row;
    
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?717:282, FLT_MAX);
    NSString *str = [NSString stringWithFormat:@"%@",[[arrInboxList objectAtIndex:indexPath.row] valueForKeyPath:@"message"]];
    CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?25.0f:16.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    return (IS_IPAD?70:39)+expectedLabelSize.height+5;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self getRemoveTheInboxForTheIndex:indexPath.row andCompletionHandler:^(bool resultRemoveTheInbox)
         {
             NSLog(@"removed Success");
         }];
    }
}

- (IBAction)btnMenuPressed:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
-(void)getInbox
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading Inbox...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getinboxlist/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
        NSLog(@"getinboxlist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     arrInboxList=[[NSMutableArray alloc]init];
                     arrInboxList=[[result valueForKeyPath:@"inboxlist"] mutableCopy];
                     NSLog(@"arrInboxList%@",arrInboxList);
                     [self.tblInbox reloadData];
                     
                 }
                 else if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Failed"])
                 {
                     if ([[result valueForKeyPath:@"inboxlist"] count]==0)
                     {
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = @"Inbox is empty";
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                     }
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
             }
         }];
    }
}

-(void)getRemoveTheInboxForTheIndex:(int)theIndex andCompletionHandler:(void (^)(bool resultRemoveTheInbox))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Deleting...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@removeInbox/%@",BaseURL,[[arrInboxList objectAtIndex:theIndex] valueForKeyPath:@"notifiyID"]]]];
        NSLog(@"removeInbox:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"Deleted Successfully";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     [arrInboxList removeObjectAtIndex:theIndex];
                     [self.tblInbox reloadData];
                     completionHandler(true);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
}

@end
