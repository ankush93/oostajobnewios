//
//  OostaJobCustomerRegisterationViewController.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECropViewController.h"
#import "LocationPickerViewController.h"
@protocol OostaJobCustomerUpdateViewControllerDelegate <NSObject>
@optional
- (void)getUpdate;
@end

@interface OostaJobCustomerUpdateViewController : UIViewController<PECropViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,LocationPickerViewControllerDelegate,MBProgressHUDDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusName;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusPassword;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusAddress;
@property (weak, nonatomic) IBOutlet UIView *viewPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnPhotoCapture;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblIAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsConditions;
- (IBAction)btnBackTapped:(id)sender;
- (IBAction)btnRegisterTapped:(id)sender;
- (IBAction)btnCameraTapped:(id)sender;
- (IBAction)btnCheckBoxTapped:(id)sender;
- (IBAction)btnTermsandConditionTapped:(id)sender;
- (IBAction)btnAddressTapped:(id)sender;
- (IBAction)btnTermsAndConditionClosePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webViewTermsAndConditions;
@property (weak, nonatomic) IBOutlet UILabel *lblTermsAndConditions;
@property (strong, nonatomic) IBOutlet UIView *viewTermsAndConditions;
- (IBAction)btnMenuTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)btnChangePassword:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewChangePassword;
@property (weak, nonatomic) IBOutlet UIView *viewContentChangePasswod;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePasswordHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnClosePressed:(id)sender;

@property (nonatomic, assign)   id<OostaJobCustomerUpdateViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadYourPhoto;
@end
