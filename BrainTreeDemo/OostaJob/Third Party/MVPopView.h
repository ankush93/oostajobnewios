//
//  MVPopView.h
//  
//
//  Created by ximiao on 15/10/10.
//
//

#import <UIKit/UIKit.h>
#import "TNRadioButtonGroup.h"

@interface MVPopView : UIView
@property (nonatomic, strong) TNRadioButtonGroup *temperatureGroup;
@property (nonatomic, strong) MVPopView *view12;

-(void)showInView:(UIView*)view;
-(void)dismiss;
@end
