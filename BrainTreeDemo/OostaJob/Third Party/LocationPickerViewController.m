//
//  LocationPickerViewController.m
//  Oosta Jobs
//
//  Created by Apple on 19/09/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "LocationPickerViewController.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"

@interface LocationPickerViewController ()
{
    NSString *address;
    NSString *latitude;
    NSString *longitude;
    NSString *zipcode;
}
@end

@implementation LocationPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /* UIImageView *pinImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin"]];
     pinImage.frame=CGRectMake(0, 0, 40, 40);
     [pinImage setCenter:CGPointMake(self.mapView.frame.size.width/2, self.mapView.frame.size.height/2+40)];
     [self.mapView.superview addSubview:pinImage];*/
    
    searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
    searchQuery.radius = 100.0;
    shouldBeginEditing = YES;
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    if(_strLat == nil && _strLong == nil){
    [self.locationManager startUpdatingLocation];
    }
    else {
        [self setMapViewRegion];
    }
    self.mapView.delegate = self;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [_btnDone.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        _lblTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:FONT_THIN size:16]}];
        id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
        
        
        [barButtonAppearanceInSearchBar setTitleTextAttributes:@{UITextAttributeFont : [UIFont fontWithName:FONT_BOLD size:26],UITextAttributeTextColor : [UIColor whiteColor]} forState:UIControlStateNormal];
    }
    else
    {
        [_btnDone.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        _lblTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{                        NSFontAttributeName: [UIFont fontWithName:FONT_THIN size:16]}];
        
        id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
        
        
        [barButtonAppearanceInSearchBar setTitleTextAttributes:@{UITextAttributeFont : [UIFont fontWithName:FONT_THIN size:18],UITextAttributeTextColor : [UIColor whiteColor]} forState:UIControlStateNormal];
        
    }
    if (_strAddress.length!=0)
    {
        self.searchDisplayController.searchBar.text=_strAddress;
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    for (CLLocation *current in locations) {
        if (current.horizontalAccuracy < 150 && current.verticalAccuracy < 150) {
            [self.locationManager stopUpdatingLocation];
            [self setMapViewRegion];
            break;
        }
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
    self.strLat = @"33.6404592";
    self.strLong = @"-117.854151";
    self.strAddress = @"5141 California Ave, Irvine, CA 92617, USA";
    [self setMapViewRegion];
}


- (void)setMapViewRegion{
    
    CLLocationCoordinate2D zoomCenter;
    
    CLLocationCoordinate2D userLocation;
    userLocation.latitude=[_strLat doubleValue];
    userLocation.longitude=[_strLong doubleValue];
    if (_strLat.length!=0&&_strLat.length!=0)
    {
        zoomCenter = userLocation;
    }
    else
    {
        zoomCenter = self.locationManager.location.coordinate;
    }
    
    
    self.chosenLocation = zoomCenter;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 500, 500);
    [self.mapView setRegion:viewRegion animated:YES];
    [self reverseGeocode:self.chosenLocation];
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    self.chosenLocation = self.mapView.centerCoordinate;
    
    NSLog(@"Latitude: %f Longetude: %f",self.chosenLocation.latitude, self.chosenLocation.longitude);
}
- (void)reverseGeocode:(CLLocationCoordinate2D)locationCord
{
    CLGeocoder *geo = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:locationCord.latitude longitude:locationCord.longitude];
    NSLog(@"didUpdateToLocation: %@", location);
    CLLocation *currentLocation = location;
    
    if (currentLocation != nil)
        NSLog(@"longitude = %.8f\nlatitude = %.8f", currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);
    latitude=[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    longitude=[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    
    // stop updating location in order to save battery power
        [self.locationManager stopUpdatingLocation];
    
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    
    
    
    [geo reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark * placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
                 
                 zipcode=placemark.postalCode;
             }
             
             address=strAdd;
             self.searchDisplayController.searchBar.text = address;
             [self.mapView removeAnnotations:self.mapView.annotations];
             MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
             Pin.coordinate = locationCord;
             Pin.title = address;
             Pin.subtitle = [NSString stringWithFormat:@"%@",[placemark postalCode]];
             
             // add annotation to mapview
             [self.mapView addAnnotation:Pin];
             
             //             if(_delegate && [_delegate respondsToSelector:@selector(getAddress:andtheLat:andtheLong:andtheZipcode:)])
             //             {
             //                 [_delegate getAddress:address andtheLat:latitude andtheLong:longitude andtheZipcode:zipcode];
             //             }
             
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDone:(id)sender
{
    if (address.length!=0&&(![address isEqualToString:@"(null)"])&&(zipcode!=nil))
    {
        [self.navigationController popViewControllerAnimated:YES];
        
        if(_delegate && [_delegate respondsToSelector:@selector(getAddress:andtheLat:andtheLong:andtheZipcode:)])
        {
            
            [_delegate getAddress:address andtheLat:latitude andtheLong:longitude andtheZipcode:zipcode];
        }
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = @"Invalid address";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    
}


- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return [searchResultPlaces objectAtIndex:indexPath.row];
}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [searchResultPlaces count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNovaT-Thin" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    return cell;
}


- (void)recenterMapToPlacemark:(CLPlacemark *)placemark {
    /*MKCoordinateRegion region;
     MKCoordinateSpan span;
     
     span.latitudeDelta = 0.02;
     span.longitudeDelta = 0.02;
     
     region.span = span;
     region.center = placemark.location.coordinate;
     
     [self.mapView setRegion:region];*/
    
    CLLocationCoordinate2D zoomCenter;
    zoomCenter = placemark.location.coordinate;
    
    /*MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 500, 500);
    [self.mapView setRegion:viewRegion animated:YES];*/
    
    self.chosenLocation = zoomCenter;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 500, 500);
    [self.mapView setRegion:viewRegion animated:YES];
    [self reverseGeocode:self.chosenLocation];

    
    
}



- (void)dismissSearchControllerWhileStayingActive {
    // Animate out the table view.
    NSTimeInterval animationDuration = 0.3;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.searchDisplayController.searchResultsTableView.alpha = 0.0;
    [UIView commitAnimations];
    
    shouldBeginEditing = NO;
    [self.searchDisplayController setActive:NO];
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchDisplayController.searchBar resignFirstResponder];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not map selected Place");
        } else if (placemark) {
            
            [self recenterMapToPlacemark:placemark];
            [self dismissSearchControllerWhileStayingActive];
            self.searchDisplayController.searchBar.text=addressString;
            [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }];
}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    searchQuery.location = self.mapView.userLocation.coordinate;
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            
            searchResultPlaces = places;
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self handleSearchForSearchString:searchString];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (![searchBar isFirstResponder]) {
        // User tapped the 'clear' button.
        shouldBeginEditing = NO;
        [self.searchDisplayController setActive:NO];
        [self.mapView removeAnnotation:selectedPlaceAnnotation];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (shouldBeginEditing) {
        // Animate in the table view.
        NSTimeInterval animationDuration = 0.3;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        self.searchDisplayController.searchResultsTableView.alpha = 1.0;
        [UIView commitAnimations];
        
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:YES];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
}

/*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    if ([annotation isKindOfClass:[annotation class]]){
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView * pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.animatesDrop = YES;
            pinView.draggable = YES;
            pinView.image = [UIImage imageNamed:@"pin"];
            
        }
        
        return pinView;
    }
    return nil;
}*/

/*-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != _mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
        {
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        //pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.canShowCallout = YES;
        //pinView.animatesDrop = YES;
        
        pinView.image = [UIImage imageNamed:@"pin small.png"];    //as suggested by Squatch
        pinView.draggable = YES;
        }
        else
        {
            pinView.annotation = annotation;
            pinView.image = [UIImage imageNamed:@"pin small.png"];
        }

    }
    else {
        [_mapView.userLocation setTitle:@"Current Location"];
    }
    return pinView;
}
*/
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    
    if(annotation != _mapView.userLocation)
    {
        static NSString *reuseId = @"pin";
        MKPinAnnotationView *pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        if (pav == nil)
        {
            pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
            pav.draggable = YES; // Right here baby!
            pav.canShowCallout = YES;
            pav.animatesDrop = YES;
            
        }
        else
        {
            pav.annotation = annotation;
            pav.image = [UIImage imageNamed:@"pin small.png"];
        }
        
        return pav;
    }
    else
    {
        [_mapView.userLocation setTitle:@"Current Location"];
        return nil;
    }
    
}

- (void)deselectAnnotation:(id<MKAnnotation>)annotation
{
    [self.mapView deselectAnnotation:annotation animated:YES];
}

- (void)didselectAnnotation:(id<MKAnnotation>)annotation
{
    [self.mapView selectAnnotation:annotation animated:YES];
}
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [self.mapView selectAnnotation:view.annotation animated:YES];
}
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    [self.mapView deselectAnnotation:view.annotation animated:YES];
}
- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    
    
    if (newState == MKAnnotationViewDragStateEnding) // you can check out some more states by looking at the docs
    {
    [self.mapView deselectAnnotation:annotationView.annotation animated:YES];
        annotationView.dragState = MKAnnotationViewDragStateNone;
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        self.chosenLocation = droppedAt;
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(droppedAt, 500, 500);
        [self.mapView setRegion:viewRegion animated:YES];
        [self reverseGeocode:self.chosenLocation];
    }
}

@end
