//
//  LocationPickerViewController.h
//  Oosta Jobs
//
//  Created by Apple on 19/09/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class SPGooglePlacesAutocompleteQuery;
@protocol LocationPickerViewControllerDelegate <NSObject>
@optional
- (void)getAddress:(NSString*)address andtheLat:(NSString *)latitude andtheLong:(NSString *)longitude andtheZipcode:(NSString *)zipcode;
@end

@interface LocationPickerViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,UIAlertViewDelegate,UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate>
{
    NSArray *searchResultPlaces;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    MKPointAnnotation *selectedPlaceAnnotation;
    
    BOOL shouldBeginEditing;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property CLLocationManager *locationManager;
@property CLLocationCoordinate2D chosenLocation;
@property (strong, nonatomic)NSString *strLat;
@property (strong, nonatomic)NSString *strLong;
@property (strong, nonatomic)NSString *strAddress;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnDone:(id)sender;

@property (nonatomic, assign)   id<LocationPickerViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end


