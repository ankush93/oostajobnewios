#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BasicMapAnnotation : NSObject <MKAnnotation> {
	CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
	NSString *_title;
    NSUInteger tag;
}

@property (nonatomic, retain) NSString *title;


- (id)initWithLatitude:(CLLocationDegrees)latitude
          andLongitude:(CLLocationDegrees)longitude andTheTitle:(NSString *)Title;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
