//
//  NSURL+UIImage.m
//  OostaJob
//
//  Created by Apple on 05/03/16.
//  Copyright © 2016 Armor. All rights reserved.
//

#import "NSURL+UIImage.h"

@implementation NSURL_UIImage

+ (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage
{
    NSURL *url = [NSURL URLWithString:urlString];
    
    dispatch_queue_t callerQueue = dispatch_get_main_queue();
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("com.armor.OostaJob", NULL);
    dispatch_async(downloadQueue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(callerQueue, ^{
            processImage(imageData);
        });
    });
    
    downloadQueue=nil;
    
    
}

@end
