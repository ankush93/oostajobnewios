//
//  Popupcontroller.h
//  OostaJob
//
//  Created by Ankush on 31/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TNRadioButtonGroup.h"

@interface Popupcontroller : UIViewController
@property (nonatomic, strong) TNRadioButtonGroup *temperatureGroup;

@end
