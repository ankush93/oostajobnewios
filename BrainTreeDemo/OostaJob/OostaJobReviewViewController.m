//
//  OostaJobReviewViewController.m
//  OostaJob
//
//  Created by Apple on 13/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobReviewViewController.h"
#import "OostaJobReviewCollctionViewCell.h"
#import "OostaJobReviewTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <BraintreeDropIn/BraintreeDropIn.h>
#import <BraintreeCore/BraintreeCore.h>
#import "OostaJobCustomerJobsViewController.h"
@interface OostaJobReviewViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,MBProgressHUDDelegate>
{
    NSString *clientToken;

    int currentIndex;
    NSMutableArray *arrJobTypes;
    NSDateFormatter *dateFormatter;
    NSMutableArray *arrAvailableTimings;
    int selectedIndex;
    NSDate *selectedDate;
    NSString *selectedTime;
    NSMutableDictionary * dictSelectedTimings;
    BOOL isfirstTimeTransform;

}
#define TRANSFORM_CELL_VALUE CGAffineTransformMakeScale(0.8, 0.8)
#define ANIMATION_SPEED 0.2
@property (assign, nonatomic) NSUInteger animationsCount;
@end

@implementation OostaJobReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    [self CreateBlurEffectOnBackgroundImage];
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.ScrolView.contentSize=CGSizeMake(IS_IPAD?768:320,_tblView.frame.origin.y+_tblView.frame.size.height+5);
    self.ScrolView.scrollEnabled=YES;
    [self.cllView registerNib:[UINib nibWithNibName:@"OostaJobReviewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
    
    clientToken = @"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIxMmMxYmY0N2RkZTJkYTdhNjI1MmJmOThkMGJiMDQ1MmY5ZGViODhmZmJjMGVmNzU4ZjcyYzZlMWZkM2JhYzQ1fGNyZWF0ZWRfYXQ9MjAxOC0wMS0yMVQxNToxNDo1MC4xNjE2OTA5NDcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=";
    
   isfirstTimeTransform = YES;
    
    
    [self settingTheCalendarView];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self scrollToPage:[_indexofSelected intValue] animated:YES];
    [self setUpAllValuesforthe:[_indexofSelected intValue]];
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblAddress.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblRates.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblReviews.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblWork.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        [self.BtnbidAmount.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:25.0f]];
        self.lblSelectedDate.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        
        self.btnMeet.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        self.btnSkip.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
    }
    else
    {
        self.lblAddress.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblRates.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        self.lblReviews.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblWork.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        [self.BtnbidAmount.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
        
        self.lblSelectedDate.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        
        self.btnMeet.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:15.0f];
        
        self.btnSkip.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:15.0f];
    }
}
-(void)setUpAllValuesforthe:(int)index
{
    selectedIndex = index;
    [self scrollToPage:selectedIndex animated:YES];
    NSLog(@"selected Value:%@",_arrSelected[index]);
    _lblHeader.text = [NSString stringWithFormat:@"%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"Name"]];
    _lblName.text = [NSString stringWithFormat:@"%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"Name"]];
    NSMutableArray * arrSelectedExpertise = [[NSMutableArray alloc]init];
    arrSelectedExpertise = [[[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.expertiseAT"]] stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","] mutableCopy];
    NSMutableArray *arrListOfJobTypes = [[NSMutableArray alloc]init];
    if (arrJobTypes.count==0)
    {
        [self getJobTypeandCompletionHandler:^(bool resultJobType) {
            if (resultJobType==YES)
            {
                for (int i=0; i<arrJobTypes.count; i++)
                {
                    if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
                    {
                        [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
                    }
                }
                
                NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
                NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
                strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
                NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceCharacterSet]];
                trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
                _lblWork.text = trimmedStringAt;
            }
            else
            {
                
            }
        }];
    }
    else
    {
        for (int i=0; i<arrJobTypes.count; i++)
        {
            if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
            {
                [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
            }
        }
        
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
        strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
        _lblWork.text = trimmedStringAt;
    }
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"Lng"]] floatValue]];
    [self getAddressFromLatLon:location];
    
    _lblRates.text = [NSString stringWithFormat:@"Houly Rates: $%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"hourlyRate"]];
    float totalRating = [[NSString stringWithFormat:@"%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"totalRatingbycategory"]] floatValue];
    for (int i=1; i<=5; i++)
    {
        for (UIImageView *img in self.imgStars)
        {
            if (img.tag == 500+i)
            {
                if (i<=totalRating)
                {
                    img.image = [UIImage imageNamed:@"white-fill.png"];
                }
                else if ((i-0.5)==totalRating)
                {
                    img.image = [UIImage imageNamed:@"white-half.png"];
                }
                else
                {
                    img.image =[UIImage imageNamed:@"white-empty.png"];
                }
            }
        }
    }
    [_BtnbidAmount setTitle:[NSString stringWithFormat:@"Not to exceed bid amount $%@",[[_arrSelected objectAtIndex:index] valueForKeyPath:@"bidAmount"]] forState:UIControlStateNormal];
    _BtnbidAmount.layer.cornerRadius = 3.0f;
    _BtnbidAmount.layer.masksToBounds = YES;
    
    [_tblView reloadData];
}

- (void) getAddressFromLatLon:(CLLocation *)bestLocation
{
    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"locality %@",placemark.locality);
         NSLog(@"postalCode %@",placemark.postalCode);
         _lblAddress.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
     }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark -Table View

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] count]==0)
    {
        _tblView.backgroundColor = [UIColor clearColor];
        return 1;
        
    }
    else
    {
        _tblView.backgroundColor = [UIColor whiteColor];
        return [[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] count];
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] count]==0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.text = @"No Reviews Found";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
        return cell;
    }
    else
    {
        static NSString *identifier =@"OostaJobReviewTableViewCell";
    OostaJobReviewTableViewCell *cell=(OostaJobReviewTableViewCell *)[self.tblView dequeueReusableCellWithIdentifier:identifier];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if (cell == nil)
        {
            NSArray *nib;
            
            nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobReviewTableViewCell IPad" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
        cell.imgProfile.layer.masksToBounds=YES;
        cell.lblCellAddress.font=[UIFont fontWithName:FONT_THIN size:25];
        cell.lblCellDate.font=[UIFont fontWithName:FONT_BOLD size:25];
        cell.lblCellName.font=[UIFont fontWithName:FONT_BOLD size:25];
        cell.lblDescription.font=[UIFont fontWithName:FONT_THIN size:25];
    }else
    {
        
        if (cell == nil)
        {
            NSArray *nib;
            
            nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobReviewTableViewCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
        cell.imgProfile.layer.masksToBounds=YES;
        cell.lblCellAddress.font=[UIFont fontWithName:FONT_THIN size:12];
        cell.lblCellDate.font=[UIFont fontWithName:FONT_BOLD size:10];
        cell.lblCellName.font=[UIFont fontWithName:FONT_BOLD size:16];
        cell.lblDescription.font=[UIFont fontWithName:FONT_THIN size:14.0];
        
    }
    
    cell.imgProfile.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgProfile.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProfile];
    cell.imgProfile.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] objectAtIndex:0] valueForKeyPath:@"profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    cell.lblCellName.text=[NSString stringWithFormat:@"%@",[[[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"]objectAtIndex:0] valueForKeyPath:@"username"]];
    NSString *str=[NSString stringWithFormat:@"%@",[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"comment"]];
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?446:226, FLT_MAX);
    
    CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_THIN size:IS_IPAD?25.0f:14.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    
    CGRect framelbl = cell.lblDescription.frame;
    framelbl.size.height = expectedLabelSize.height;
    cell.lblDescription.frame = framelbl;
    
    CGRect framecontentView = cell.contentView.frame;
    framecontentView.size.height = cell.lblDescription.frame.origin.y+cell.lblDescription.frame.size.height;
    cell.contentView.frame = framecontentView;
    cell.lblDescription.text =str;
    [cell.lblDescription sizeToFit];
    cell.lblDescription.numberOfLines=0;
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[[[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row]  valueForKeyPath:@"customerDetails"]objectAtIndex:0] valueForKeyPath:@"Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[[[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row]  valueForKeyPath:@"customerDetails"]objectAtIndex:0] valueForKeyPath:@"Lng"]] floatValue]];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"locality %@",placemark.locality);
         NSLog(@"postalCode %@",placemark.postalCode);
         cell.lblCellAddress.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
     }];

    float totalRating = [[NSString stringWithFormat:@"%@",[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"ratingOverall"]] floatValue];
    for (int i=1; i<=5; i++)
    {
        for (UIImageView *img in cell.imgStars)
        {
            if (img.tag == 500+i)
            {
                if (i<=totalRating)
                {
                    img.image = [UIImage imageNamed:@"star-fill.png"];
                }
                else if ((i-0.5)==totalRating)
                {
                    img.image = [UIImage imageNamed:@"half.png"];
                }
                else
                {
                    img.image =[UIImage imageNamed:@"empty.png"];
                }
            }
        }
    }
    
    
    NSString *dateString = [NSString stringWithFormat:@"%@",[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"ratingTime"]];
    
    __block NSDate *detectedDate;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
    [detector enumerateMatchesInString:dateString
                               options:kNilOptions
                                 range:NSMakeRange(0, [dateString length])
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         detectedDate = result.date;
         NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
         NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
         
         NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
         NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
         NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
         
         NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
         NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
         [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
         cell.lblCellDate.text = [dateFormat stringFromDate:destinationDate];
         
         
     }];
    return cell;
    }

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] count]==0)
    {
        return IS_IPAD?60:40;
    }
    else
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[[[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"reviewlist"] objectAtIndex:indexPath.row] valueForKeyPath:@"comment"]];
        CGSize maximumLabelSize = CGSizeMake(IS_IPAD?446:226, FLT_MAX);
        
        CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_THIN size:IS_IPAD?25:14.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
        return (IS_IPAD?65:46)+expectedLabelSize.height+3;
    }
    
}
-(void)CreateBlurEffectOnBackgroundImage
{
    self.imgBlur.image = _imgBackGroundImage;
    [self.imgBlur assignBlur];
}
-(IBAction)UpArrowBtnTouched:(id)sender
{
    CGPoint point = CGPointMake(0,-20);
    [_ScrolView setContentOffset:point animated:YES];
    
}

- (IBAction)BackBtnTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -Collection View

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return _arrSelected.count;
}
- (OostaJobReviewCollctionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OostaJobReviewCollctionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
    cell.imgProfile.layer.masksToBounds=YES;
    
    cell.imgProfile.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgProfile.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProfile];
    cell.imgProfile.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_arrSelected objectAtIndex:indexPath.row] valueForKeyPath:@"profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if (indexPath.row == [_indexofSelected intValue] && isfirstTimeTransform) { // make a bool and set YES initially, this check will prevent fist load transform
        isfirstTimeTransform = NO;
    }else{
        cell.transform = TRANSFORM_CELL_VALUE; // the new cell will always be transform and without animation
    }
    
    return cell;
   
    
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView == _cllView)
    {
        float pageWidth = (IS_IPAD?170:150) + 5; // width + space
    
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = currentOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
    
    int index = newTargetOffset / pageWidth;
    
    if (index == [_indexofSelected intValue]) { // If first index
        UICollectionViewCell *cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }else{
        UICollectionViewCell *cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        index --; // left
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
        //index ++;
        index ++; // right
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }
        if (index>=0&&index<_arrSelected.count)
        {
            [self setUpAllValuesforthe:index];
        }
    
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking) return;
    
    NSUInteger selectedPage = indexPath.row;
    [self setUpAllValuesforthe:selectedPage];
    [self scrollToPage:selectedPage animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==self.ScrolView)
    {
        CGFloat totalScroll = scrollView.contentSize.height - scrollView.bounds.size.height;
        CGFloat offset =  scrollView.contentOffset.y;
        CGFloat percentage = offset / totalScroll;
        NSLog(@"percentage:%f",percentage);
        self.UpArrowBtn.alpha = (0.f + percentage);
    }
}

#pragma mark - UICollectionViewDelegate

- (void)scrollToPage:(NSUInteger)page animated:(BOOL)animated
{
    
    self.animationsCount++;
    CGFloat pageOffset = page * (IS_IPAD?175:155) - self.cllView.contentInset.left;
    [self.cllView setContentOffset:CGPointMake(pageOffset, 0) animated:animated];
    int index = pageOffset / (IS_IPAD?175:155);
    
    if (index == [_indexofSelected intValue]) { // If first index
        UICollectionViewCell *cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }else{
        UICollectionViewCell *cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        index --; // left
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
        index ++;
        index ++; // right
        cell = [self.cllView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }
    
    
}
#pragma mark - Convenience

- (CGFloat)contentOffset {
    return self.cllView.contentOffset.x + self.cllView.contentInset.left;
}

- (IBAction)btnReview:(id)sender
{
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0, 420);
                                 [self.ScrolView setContentOffset:point animated:YES];
                                 //CGPoint pointContent = CGPointMake(0, 0);
                                 //                                 [_scrollViewContent setContentOffset:pointContent animated:YES];
                                 //                                 _viewUnderLine.frame=CGRectMake(0, 0, 360, 2);
                             }
                             completion:nil];
            
        }
        else
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0, 220);
                                 [self.ScrolView setContentOffset:point animated:YES];
                                 //                                 CGPoint pointContent = CGPointMake(0, 0);
                                 //                                 [_scrollViewContent setContentOffset:pointContent animated:YES];
                                 //                                 _viewUnderLine.frame=CGRectMake(0, 0, 146, 2);
                             }
                             completion:nil];
            
        }
    }
}

#pragma mark Job types
-(void)getJobTypeandCompletionHandler:(void (^)(bool resultJobType))completionHandler
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobtype",BaseURL]]];
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 arrJobTypes=[[NSMutableArray alloc]init];
                 arrJobTypes=[result valueForKeyPath:@"JobtypeDetails"];
                 
                 completionHandler(true);
                 
             }
             else
             {
                 completionHandler(false);
             }
         }
     }];
    
}

#pragma mark Calendar
-(void)settingTheCalendarView
{
    arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
    selectedTime = arrAvailableTimings[0];
    [_pickerViewAvailableTimings reloadAllComponents];
    self.calendar.delegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.calendar.titleFont =[UIFont fontWithName:FONT_BOLD size:IS_IPAD?26.0f:18.0f];
    self.calendar.dateOfWeekFont =[UIFont fontWithName:FONT_BOLD size:IS_IPAD?22.0f:15.0f];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMMM YYYY"];
    _lblSelectedDate.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:60*60*24]]];
    selectedDate = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    self.calendar.onlyShowCurrentMonth = NO;
    self.calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    self.calendar.backgroundColor=[UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
}

- (void)localeDidChange
{
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    if ([date compare:[NSDate date]]== NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    // TODO: play with the coloring if we want to...
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    if ([self dateIsDisabled:date])
    {
        dateItem.backgroundColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor lightGrayColor];
        dateItem.disable = @"YES";
    }
    else if ([[dateFormat stringFromDate:date] isEqualToString:[dateFormat stringFromDate:selectedDate] ])
    {
        
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if (arr1.count==0&&[self containsKey:[dateFormat stringFromDate:date]]==YES)
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"SELECTED";
        
    }
    else
    {
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if (arr1.count==0&&[self containsKey:[dateFormat stringFromDate:date]]==YES)
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"NO";
    }
    
    
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    if([date compare:selectedDate]!=NSOrderedSame)
    {
        _lblSelectedDate.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
        selectedDate =date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        if ([self containsKey:[dateFormat stringFromDate:selectedDate]]==YES)
        {
            arrAvailableTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
            
        }
        else
        {
            arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
        }
        if(arrAvailableTimings.count>0)
        {
            selectedTime = [NSString stringWithFormat:@"%@",arrAvailableTimings[0]];
            [_pickerViewAvailableTimings reloadAllComponents];
            [self.pickerViewAvailableTimings selectRow:0 inComponent:0 animated:NO];
            [self pickerView:self.pickerViewAvailableTimings didSelectRow:0 inComponent:0];
        }
        else
        {
            [_pickerViewAvailableTimings reloadAllComponents];
        }
        
        
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date
{
    
    return YES;
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
    CGRect frameOther = _viewOtherContents.frame;
    frameOther.origin.y = IS_IPAD?(self.calendar.frame.size.height+14):self.calendar.frame.size.height;
    _viewOtherContents.frame = frameOther;
    _viewContentCalendar.frame = CGRectMake(_viewContentCalendar.frame.origin.x, _viewContentCalendar.frame.origin.y, _viewContentCalendar.frame.size.width, frameOther.origin.y+frameOther.size.height);
    _scrlCalendar.scrollEnabled=YES;
    _scrlCalendar.contentSize =CGSizeMake(_scrlCalendar.frame.size.width, frameOther.origin.y+frameOther.size.height+55);
}

#pragma matk PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if (arrAvailableTimings.count==0)
    {
        return 1;
    }
    else
    {
        return arrAvailableTimings.count;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (arrAvailableTimings.count==0)
    {
        return [NSString stringWithFormat:@"%@",@"No Timing Available"];
    }
    else
    {
        return [NSString stringWithFormat:@"%@",arrAvailableTimings[row]];
    }
    
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (arrAvailableTimings.count!=0)
    {
        NSLog(@" Index of selected color: %li",  (long)row);
        selectedTime = [NSString stringWithFormat:@"%@",arrAvailableTimings[row]];
    }
}


- (IBAction)btnMeetUpPressed:(id)sender
{
    [self CheckPayableAmountAPI:^(bool result){
        if (result == YES) {
            [self showDropIn:clientToken];
            
        }
    }];
//    [UIView animateWithDuration:0.3 animations:^{
//        _viewCalander.alpha = 0;
//    } completion: ^(BOOL finished)
//     {
//         _viewCalander.hidden = finished;
//         [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
//             if (resultJobHire == YES)
//             {
//                 NSLog(@"Hired");
//             }
//         }];
//     }];
}

- (IBAction)btnSkipPressed:(id)sender
{
    [self CheckPayableAmountAPI:^(bool result){
        if (result == YES) {
            [self showDropIn:clientToken];

        }
    }];
    
    

//    [UIView animateWithDuration:0.3 animations:^{
//        _viewCalander.alpha = 0;
//    } completion: ^(BOOL finished)
//     {
//         _viewCalander.hidden = finished;
//         selectedTime = @"";
//         selectedDate = nil;
//         [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
//             if (resultJobHire == YES)
//             {
//                 NSLog(@"Hired");
//             }
//         }];
//     }];
}
- (IBAction)btnCalendarClosedTapped:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewCalander.alpha = 0;
    } completion: ^(BOOL finished)
     {
         _viewCalander.hidden = finished;
     }];
}

- (IBAction)btnNitNow:(id)sender
{
    
    _viewCalander.alpha = 0;
    _viewCalander.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _viewCalander.alpha = 1;
        _strUserId = [[_arrSelected objectAtIndex:selectedIndex] valueForKeyPath:@"userID"];
        [self getCalendarDetailsndCompletionHandler:^(bool resultCalendarDetails) {
            NSLog(@"resultCalendarDetails called");
        }];
    }];
}

- (IBAction)btnHomePressed:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)CheckPayableAmountAPI:(void (^)(bool result))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"jobId";
        NSString * obj1 = _strJobpostID;
        
        NSString * key2 =@"contractor_id";
        NSString * obj2 = _ContractorID;

        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2]
                                       forKeys:@[key1,key2]];
        
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@payableAmount",BaseURL]]];
        NSLog(@"jobhired URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {
                     
                     [hud hide:YES];
                     MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:hud];
                     
                     hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     hud.mode = MBProgressHUDModeCustomView;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     hud.delegate = self;
                     hud.labelText = @"Success";
                     [hud show:YES];
                     [hud hide:YES afterDelay:2];
                    // [self gotoJobs];
                     
                     
                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
                 
                 completionHandler(true);
             }
         }];
    }
}
-(void)JobHiringContractorandCompletionHandler:(void (^)(bool resultJobHirre))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Hiring...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    
    NSString * key1 =@"userID";
    NSString * obj1 =_strUserId;
    
    NSString * key2 =@"jobid";
    NSString * obj2 =_strJobpostID;
    
    NSString * key3 =@"meetingTime";
    NSString * obj3 =selectedTime;
    
    NSString * key4 =@"meetingDate";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *date = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:selectedDate]];
    NSString * obj4 =date;
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3,obj4]
                                   forKeys:@[key1,key2,key3,key4]];
    
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@jobhired",BaseURL]]];
    NSLog(@"jobhired URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 
                 
                 
                 [hud hide:YES];
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Hired Successfully";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 [self gotoJobs];
                 
                 
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
             }
             
             completionHandler(true);
         }
     }];
    }
}

#pragma mark Get Calendar
-(void)getCalendarDetailsndCompletionHandler:(void (^)(bool resultCalendarDetails))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Timings...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcalender/%@",BaseURL,_strUserId]]];
    NSLog(@"getcalender:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 dictSelectedTimings = [[NSMutableDictionary alloc]init];
                 NSMutableArray *arrDate = [[NSMutableArray alloc]init];
                 NSMutableArray *arrTime = [[NSMutableArray alloc]init];
                 arrDate = [result valueForKeyPath:@"CalenderDetails.notAvailabledate"];
                 arrTime = [result valueForKeyPath:@"CalenderDetails.notAvailableTime"];
                 
                 NSMutableArray *arrCustomerDate = [[NSMutableArray alloc]init];
                 NSMutableArray *arrCustomerTime = [[NSMutableArray alloc]init];
                 arrCustomerDate = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailabledate"];
                 arrCustomerTime = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailableTime"];

                 
                 NSMutableArray *arrAvailable =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
                 //Customer time enhancement
                 
                 NSMutableDictionary * dictCustomerSelectedTimings = [[NSMutableDictionary alloc]init];
                 for (int i=0; i<arrCustomerDate.count; i++)
                 {
                     NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                     NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrCustomerTime[i]] componentsSeparatedByString:@","];
                     for (int j=0; j<arrAvailable.count; j++)
                     {
                         if ([arrServiceTimings containsObject:arrAvailable[j]])
                         {
                             [arrTimings addObject:arrAvailable[j]];
                         }
                         
                     }
                     [dictCustomerSelectedTimings setObject:arrTimings forKey:arrCustomerDate[i]];
                     
                     
                 }

                 
                 for (int i=0; i<arrDate.count; i++)
                 {
                     NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                     NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrTime[i]] componentsSeparatedByString:@","];
                     NSArray *arrServiceCustomerTimings = [[NSString stringWithFormat:@"%@",[dictCustomerSelectedTimings valueForKeyPath:arrDate[i]]] componentsSeparatedByString:@","];
                     
                     for (int j=0; j<arrAvailable.count; j++)
                     {
                         if (![arrServiceTimings containsObject:arrAvailable[j]]&&![arrServiceCustomerTimings containsObject:arrAvailable[j]])
                         {
                             [arrTimings addObject:arrAvailable[j]];
                         }
                         
                     }
                     [dictSelectedTimings setObject:arrTimings forKey:arrDate[i]];
                     
                 }
                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                 [dateFormat setDateFormat:@"YYYY-MM-dd"];
                 
                 for (int i=0; i<[[dictCustomerSelectedTimings allKeys] count]; i++)
                 {
                     if ([self containsKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]]==YES)
                     {
                         NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                         arrTimings = [[dictSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] mutableCopy];
                         
                         for (int j=0; j<[[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] count]; j++)
                         {
                             [arrTimings removeObject:[[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] objectAtIndex:j]];
                         }
                         
                         [dictSelectedTimings setObject:arrTimings forKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]];
                     }
                     else
                     {
                         NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                         NSMutableArray * arrServiceTimings = [[NSMutableArray alloc]init];
                         arrServiceTimings = [[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] mutableCopy];
                         
                         
                         for (int j=0; j<arrAvailable.count; j++)
                         {
                             if (![arrServiceTimings containsObject:arrAvailable[j]])
                             {
                                 [arrTimings addObject:arrAvailable[j]];
                             }
                             
                         }
                         [dictSelectedTimings setObject:arrTimings forKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]];
                     }
                 }
                 
                 
                 if ([self containsKey:[dateFormat stringFromDate:selectedDate]]==YES)
                 {
                     arrAvailableTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
                     if(arrAvailableTimings.count>0)
                     {
                         selectedTime = arrAvailableTimings[0];
                     }
                     [_pickerViewAvailableTimings reloadAllComponents];
                 }
                 NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
                 [self.calendar reloadData];
                 completionHandler(true);
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
         }
     }];
    }
}
- (BOOL)containsKey: (NSString *)key {
    BOOL retVal = 0;
    NSArray *allKeys = [dictSelectedTimings allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

-(void)gotoJobs
{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    
    [revealController pushFrontViewController:newFrontController animated:YES];
}


- (void)showDropIn:(NSString *)clientTokenOrTokenizationKey {
    BTDropInRequest *request = [[BTDropInRequest alloc] init];
    request.paypalDisabled = YES;
    request.applePayDisabled = YES;
    request.venmoDisabled = YES;
    request.threeDSecureVerification = YES;
    request.amount = @"1.00";
    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:clientTokenOrTokenizationKey request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
        NSLog(@"result grop in :%@",result);
        if (error != nil) {
            NSLog(@"ERROR");
        } else if (result.cancelled) {
            NSLog(@"CANCELLED");
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            // Use the BTDropInResult properties to update your UI
            //result.paymentOptionType
            // result.paymentMethod
            NSLog(@"Result payment method nonce :%@",result.paymentMethod.nonce);
            // result.paymentIcon
            // result.paymentDescription
        }
    }];
    [self presentViewController:dropIn animated:YES completion:nil];
}
+ (void)fetchExistingPaymentMethod:(NSString *)clientToken {
    [BTDropInResult fetchDropInResultForAuthorization:clientToken handler:^(BTDropInResult * _Nullable result, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"ERROR");
        } else {
            // Use the BTDropInResult properties to update your UI
            NSLog(@"Payment method%@", result.paymentMethod);
            NSLog(@"Payment Description :%@", result.paymentDescription);
            NSLog(@"Payment option type :%ld", (long)result.paymentOptionType);
        }
    }];
}
@end
