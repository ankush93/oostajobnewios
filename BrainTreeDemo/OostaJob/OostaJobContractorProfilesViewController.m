//
//  OostaJobContractorProfilesViewController.m
//  OostaJob
//
//  Created by Armor on 13/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContractorProfilesViewController.h"
#import "OostaJobReviewTableViewCell.h"
#import "OostaJobContractorUpdateViewController.h"
#import <CoreLocation/CoreLocation.h>
#define TRANSFORM_CELL_VALUE CGAffineTransformMakeScale(0.8, 0.8)
#define TRANSFORM_CELL_NORMAL_VALUE CGAffineTransformMakeScale(0.0, 0.0)
#define ANIMATION_SPEED 0.2
@interface OostaJobContractorProfilesViewController ()<OostaJobContractorUpdateViewControllerDelegate>
{
    BOOL isfirstTimeTransform;
    int currentIndex;
    NSMutableArray *arrJobTypes;
    NSMutableArray *arrReviews;
    NSMutableArray *arrListOfJobTypes;
    NSMutableArray *arrTotalReviews;
}
@end

@implementation OostaJobContractorProfilesViewController
@synthesize table;
@synthesize btnSender;
@synthesize list;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.imgBlur assignBlur];
    [self UISetup];
    isfirstTimeTransform = YES;
    self.navigationController.navigationBarHidden=YES;
    self.scrollView.contentSize=CGSizeMake(0,  _viewTableContent.frame.origin.y+_viewTableContent.frame.size.height);
    self.scrollView.scrollEnabled=YES;
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self setupValues];
    [self getReviews];
    _tblReview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
-(void)setupValues
{
    if ([[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS"])
    {
        _ProfPic.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.profilePhoto"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        _lblName.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Name"]];
        
        NSMutableArray * arrSelectedExpertise = [[NSMutableArray alloc]init];
        arrSelectedExpertise =
        [[[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.expertiseAT"]] stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","] mutableCopy];
        arrListOfJobTypes = [[NSMutableArray alloc]init];
        if (arrJobTypes.count==0)
        {
            [self getJobTypeandCompletionHandler:^(bool resultJobType) {
                if (resultJobType==YES)
                {
                    for (int i=0; i<arrJobTypes.count; i++)
                    {
                        if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
                        {
                            [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
                        }
                    }
                    
                    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
                    NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
                    strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
                    NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                                 [NSCharacterSet whitespaceCharacterSet]];
                    trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
                    _lblDesc.text = trimmedStringAt;
                    
                    arrListOfJobTypes = [[NSMutableArray alloc]initWithArray:[[_lblDesc.text componentsSeparatedByString:@","] mutableCopy]];
                    [arrListOfJobTypes addObject:@"All Jobs"];
                }
                else
                {
                    
                }
            }];
        }
        else
        {
            for (int i=0; i<arrJobTypes.count; i++)
            {
                if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
                {
                    [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
                }
            }
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
            NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
            strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceCharacterSet]];
            trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
            _lblDesc.text = trimmedStringAt;
            
            arrListOfJobTypes = [[NSMutableArray alloc]initWithArray:[[_lblDesc.text componentsSeparatedByString:@","] mutableCopy]];
            [arrListOfJobTypes addObject:@"All Jobs"];
        }
        _lblRate.text = [NSString stringWithFormat:@"Hourly Rates:$%@/hr",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.hourlyRate"]];
        float totalRating = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.totalRating"]] floatValue];
        for (int i=1; i<=5; i++)
        {
            for (UIImageView *img in self.imgStars)
            {
                if (img.tag == 500+i)
                {
                    if (i<=totalRating)
                    {
                        img.image = [UIImage imageNamed:@"white-fill.png"];
                    }
                    else if ((i-0.5)==totalRating)
                    {
                        img.image = [UIImage imageNamed:@"white-half.png"];
                    }
                    else
                    {
                        img.image =[UIImage imageNamed:@"white-empty.png"];
                    }
                }
            }
        }
        if ([[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.totalEarning"]] isEqualToString:@""])
        {
            _lblEarn.text = [NSString stringWithFormat:@"Your total earnings at Oostajob till now $%@",@"0"];
        }
        else
        {
            _lblEarn.text = [NSString stringWithFormat:@"Your total earnings at Oostajob till now $%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.totalEarning"]];
        }
        _lblAddr.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"]];
        _lblMail.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Email"]];
        _lblPhone.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Phone"]];
        
    }
    
    
    indexPathSel =[NSIndexPath indexPathForRow:arrListOfJobTypes.count-1 inSection:0];
    
}
#pragma mark Job types
-(void)getJobTypeandCompletionHandler:(void (^)(bool resultJobType))completionHandler
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobtype",BaseURL]]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 arrJobTypes=[[NSMutableArray alloc]init];
                 arrJobTypes=[result valueForKeyPath:@"JobtypeDetails"];
                 
                 completionHandler(true);
                 
             }
             else
             {
                 completionHandler(false);
             }
         }
     }];
    
}


-(void)UISetup
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.ProfPic.layer.cornerRadius=self.ProfPic.frame.size.width/2.0;
        self.ProfPic.layer.masksToBounds=YES;
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:35.0f];
        self.lblDesc.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblRate.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblAddr.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblAddr1.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblMail.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblPhone.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblEarn.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.btnAbout.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:28.0f];
        self.btnList.titleLabel.font=[UIFont fontWithName:FONT_THIN size:28.0f];
        self.btnReview.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:28.0f];
        self.lblAddr.textColor=[UIColor lightGrayColor];
        self.lblAddr1.textColor=[UIColor lightGrayColor];
        self.lblMail.textColor=[UIColor lightGrayColor];
        self.lblPhone.textColor=[UIColor lightGrayColor];
        self.imgView.layer.cornerRadius=self.ProfPic.frame.size.width/2.0;
        self.lblEarn.layer.cornerRadius=5.0;
        self.btnList.layer.borderWidth=1.0;
        self.btnList.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnList.layer.cornerRadius=2.0;
    }
    else
    {
        self.ProfPic.layer.cornerRadius=self.ProfPic.frame.size.width/2.0;
        self.ProfPic.layer.masksToBounds=YES;
        self.ProfPic.layer.borderWidth = 3.0f;
        self.ProfPic.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblDesc.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblRate.font=[UIFont fontWithName:FONT_BOLD size:17.0f];
        self.lblAddr.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblAddr1.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblMail.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblPhone.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblEarn.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.btnAbout.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.btnReview.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.btnList.titleLabel.font=[UIFont fontWithName:FONT_THIN size:18.0f];
        self.lblAddr.textColor=[UIColor lightGrayColor];
        self.lblAddr1.textColor=[UIColor lightGrayColor];
        self.lblMail.textColor=[UIColor lightGrayColor];
        self.lblPhone.textColor=[UIColor lightGrayColor];
        self.imgView.layer.cornerRadius=self.ProfPic.frame.size.width/2.0;
        self.lblEarn.layer.cornerRadius=5.0;
        self.btnList.layer.borderWidth=1.0;
        self.btnList.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnList.layer.cornerRadius=2.0;
        
    }
}
#pragma mark - set Button Actions
- (IBAction)btnBackTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnHome:(id)sender
{
    //coding for home view
}
-(IBAction)btnEdit:(id)sender
{
    //coding for editing
    [self goToUpdate];
}
-(IBAction)btnList:(id)sender
{
    //Shows Filter to table.
    
    if (arrListOfJobTypes.count>1)
    {
        if(_dropDown == nil)
        {
            CGFloat f = 150;
            
            _dropDown = [[UIView alloc]init];
            
            [self showDropDown:sender andHeight:&f theContentArr:arrListOfJobTypes theDiection:@"down"];
            //            [self.mapView bringSubviewToFront:_dropDown];
            
        }
        else {
            [self hideDropDown:sender];
            [self rel];
        }
    }
}
#pragma mark DropDown

- (void)showDropDown:(UIButton *)b andHeight:(CGFloat *)height theContentArr:(NSArray *)arr theDiection:(NSString *)direction
{
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    
    // Initialization code
    CGRect btn = b.frame;
    self.list = [NSArray arrayWithArray:arr];
    NSLog(@"arr:%@",arr);
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, -5);
    }else if ([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    self.dropDown.layer.masksToBounds = NO;
    self.dropDown.layer.cornerRadius = 8;
    self.dropDown.layer.shadowRadius = 5;
    self.dropDown.layer.shadowOpacity = 0.5;
    self.dropDown.backgroundColor =[UIColor whiteColor];
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
    table.delegate = self;
    table.dataSource = self;
    table.layer.cornerRadius = 5;
    table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.separatorColor = [UIColor grayColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
    } else if([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, *height);
    [UIView commitAnimations];
    
    [self.dropDown addSubview:table];
    table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tblReview.superview addSubview:self.dropDown];
    [table reloadData];
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}
-(void)rel
{
    self.dropDown=nil;
}


-(IBAction)btnAbout:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 300);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             _viewUnderLine.frame=CGRectMake(0, 0, 360, 2);
                         }
                         completion:nil];
        
    }
    else{
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 260);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             _viewUnderLine.frame=CGRectMake(0, 0, 146, 2);
                         }
                         completion:nil];
        
    }
}
-(IBAction)btnReview:(id)sender
{
    //Go to review page.
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 300);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(770, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(360, 0, 360, 2);
                             
                            _scrollViewContent.contentSize = CGSizeMake(0, [arrReviews count]*100 );
                         }
                         completion:nil];
        
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 260);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(300, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(146, 0, 146, 2);
                         }
                         completion:nil];
        
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_scrollView)
    {
        CGFloat totalScroll = scrollView.contentSize.height - scrollView.bounds.size.height;
        CGFloat offset =  scrollView.contentOffset.y;
        CGFloat percentage = offset / totalScroll;
        NSLog(@"percentage:%f",percentage);
        if (IS_IPHONE5||IS_IPAD)
        {
            self.btnUp.alpha = (0.f - percentage);
        }
        else
        {
            self.btnUp.alpha = (0.f + percentage);
        }
        
    }
    else
    {
        
    }
    
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidEndDecelerating");
    NSLog(@"scrollView.contentSize={%f,%f}",scrollView.contentSize.width,scrollView.contentSize.height);
    
}
- (IBAction)btnUp:(id)sender
{
    CGPoint point = CGPointMake(0,-20);
    [_scrollView setContentOffset:point animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -UITableView Datasource and Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblReview)
    {
        if ([arrReviews count]==0)
        {
            _tblReview.backgroundColor = [UIColor clearColor];
            return 1;
            
        }
        else
        {
            _tblReview.backgroundColor = [UIColor whiteColor];
            return [arrReviews count];
            
        }
        
    }
    else
    {
        return [self.list count];
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblReview)
    {
        if ([arrReviews count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Reviews Found";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
        
        static NSString *identifier =@"OostaJobReviewTableViewCell";
        OostaJobReviewTableViewCell *cell=(OostaJobReviewTableViewCell *)[self.tblReview dequeueReusableCellWithIdentifier:identifier];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            if (cell == nil)
            {
                NSArray *nib;
                
                nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobReviewTableViewCell IPad" owner:self options:nil];
                cell=[nib objectAtIndex:0];
            }
            
            cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
            cell.imgProfile.layer.masksToBounds=YES;
            cell.lblCellAddress.font=[UIFont fontWithName:FONT_THIN size:25];
            cell.lblCellDate.font=[UIFont fontWithName:FONT_BOLD size:25];
            cell.lblCellName.font=[UIFont fontWithName:FONT_BOLD size:25];
            cell.lblDescription.font=[UIFont fontWithName:FONT_THIN size:25];
            cell.lblJobTypes.font=[UIFont fontWithName:FONT_BOLD size:25];
        }else
        {
            
            if (cell == nil)
            {
                NSArray *nib;
                
                nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobReviewTableViewCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
            }
            
            cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
            cell.imgProfile.layer.masksToBounds=YES;
            cell.lblCellAddress.font=[UIFont fontWithName:FONT_THIN size:12];
            cell.lblCellDate.font=[UIFont fontWithName:FONT_BOLD size:10];
            cell.lblCellName.font=[UIFont fontWithName:FONT_BOLD size:16];
            cell.lblDescription.font=[UIFont fontWithName:FONT_THIN size:14.0];
            cell.lblJobTypes.font=[UIFont fontWithName:FONT_BOLD size:14];
            
        }
        cell.lblJobTypes.hidden = NO;
        cell.imgProfile.contentMode = UIViewContentModeScaleAspectFill;
        cell.imgProfile.clipsToBounds = YES;
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProfile];
        
        
        NSString *str=[NSString stringWithFormat:@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"comment"]];
        CGSize maximumLabelSize = CGSizeMake(IS_IPAD?446:200, FLT_MAX);
        
        CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_THIN size:IS_IPAD?25.0f:14.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
        cell.lblJobTypes.text = [NSString stringWithFormat:@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
        
        CGRect framelbl = cell.lblDescription.frame;
        framelbl.origin.y = cell.lblJobTypes.frame.origin.y+cell.lblJobTypes.frame.size.height+2;
        framelbl.size.height = expectedLabelSize.height;
        cell.lblDescription.frame = framelbl;
        
        
        
        CGRect framecontentView = cell.contentView.frame;
        framecontentView.size.height = cell.lblDescription.frame.origin.y+cell.lblDescription.frame.size.height;
        cell.contentView.frame = framecontentView;
        cell.lblDescription.text =str;
        [cell.lblDescription sizeToFit];
        cell.lblDescription.numberOfLines=0;
        
        if ([[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] count]!=0)
        {
            NSLog(@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"]);
            cell.imgProfile.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] objectAtIndex:0] valueForKeyPath:@"profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            cell.lblCellName.text=[NSString stringWithFormat:@"%@",[[[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] objectAtIndex:0] valueForKeyPath:@"username"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[[[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] objectAtIndex:0] valueForKeyPath:@"Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[[[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails"] objectAtIndex:0] valueForKeyPath:@"Lng"]] floatValue]];
            CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
            [geocoder reverseGeocodeLocation:location
                           completionHandler:^(NSArray *placemarks, NSError *error)
             {
                 if (error){
                     NSLog(@"Geocode failed with error: %@", error);
                     return;
                 }
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                 NSLog(@"locality %@",placemark.locality);
                 NSLog(@"postalCode %@",placemark.postalCode);
                 cell.lblCellAddress.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
             }];
        }
        float totalRating = [[NSString stringWithFormat:@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"ratingOverall"]] floatValue];
        for (int i=1; i<=5; i++)
        {
            for (UIImageView *img in cell.imgStars)
            {
                if (img.tag == 500+i)
                {
                    if (i<=totalRating)
                    {
                        img.image = [UIImage imageNamed:@"star-fill.png"];
                    }
                    else if ((i-0.5)==totalRating)
                    {
                        img.image = [UIImage imageNamed:@"half.png"];
                    }
                    else
                    {
                        img.image =[UIImage imageNamed:@"empty.png"];
                    }
                }
            }
        }
        
        
        
        NSString *dateString = [NSString stringWithFormat:@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"ratingTime"]];
        
        __block NSDate *detectedDate;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
        [detector enumerateMatchesInString:dateString
                                   options:kNilOptions
                                     range:NSMakeRange(0, [dateString length])
                                usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             detectedDate = result.date;
             NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
             NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
             
             NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
             NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
             NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
             
             NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
             [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
             cell.lblCellDate.text = [dateFormat stringFromDate:destinationDate];
             
             
         }];
        return cell;
        }
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        cell.textLabel.text = [list objectAtIndex:indexPath.row];
        
        
        cell.textLabel.textColor = [UIColor blackColor];
        if ([indexPathSel isEqual: indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        return cell;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblReview)
    {
        if ([arrReviews count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            NSString *str=[NSString stringWithFormat:@"%@",[[arrReviews objectAtIndex:indexPath.row] valueForKeyPath:@"comment"]];
            CGSize maximumLabelSize = CGSizeMake(IS_IPAD?446:200, FLT_MAX);
            
            CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_THIN size:IS_IPAD?25.0f:14.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
            return (IS_IPAD?66:46)+expectedLabelSize.height+6+21+2;
        }
        
    }
    else
    {
        return 40;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table)
    {
        
        
        NSLog(@"didselect");
        [self hideDropDown:btnSender];
        [self rel];
        [_btnList setTitle:[list objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        
        if(indexPathSel)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:indexPathSel];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([indexPathSel isEqual:indexPath])
        {
            indexPathSel = nil;
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            indexPathSel = indexPath;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            indexPathSel = indexPath;
        }
        [self filterTabletoParticularSkill:[list objectAtIndex:indexPath.row]];
    }
}
-(void)filterTabletoParticularSkill:(NSString *)skill
{
    arrReviews=[[NSMutableArray alloc]init];
    if ([skill isEqualToString:@"All Jobs"])
    {
        arrReviews= [arrTotalReviews mutableCopy];
    }
    else
    {
        for (int i=0; i<arrTotalReviews.count; i++)
        {
            if ([[NSString stringWithFormat:@"%@",[[arrTotalReviews objectAtIndex:i] valueForKeyPath:@"jobtypeName"]] isEqualToString:skill])
            {
                [arrReviews addObject:arrTotalReviews[i]];
            }
        }
    }
    [_tblReview reloadData];
}
- (IBAction)btnMenuTapped:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
-(void)getReviews
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Reviews...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getrating/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getrating:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 arrReviews=[[NSMutableArray alloc]init];
                 arrTotalReviews=[[NSMutableArray alloc]init];
                 arrReviews=[result valueForKeyPath:@"RatingDetails"];
                 arrTotalReviews=[result valueForKeyPath:@"RatingDetails"];
                 
                 float totalRating = [[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"EarningDetails.totalRating"]] floatValue];
                 for (int i=1; i<=5; i++)
                 {
                     for (UIImageView *img in self.imgStars)
                     {
                         if (img.tag == 500+i)
                         {
                             if (i<=totalRating)
                             {
                                 img.image = [UIImage imageNamed:@"white-fill.png"];
                             }
                             else if ((i-0.5)==totalRating)
                             {
                                 img.image = [UIImage imageNamed:@"white-half.png"];
                             }
                             else
                             {
                                 img.image =[UIImage imageNamed:@"white-empty.png"];
                             }
                         }
                     }
                 }
                 _lblEarn.text = [NSString stringWithFormat:@"Your total earnings at Oostajob till now $%@",[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"EarningDetails.totalEarning"]]];
                 
                 [self.tblReview reloadData];
                 
             }
             else if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Failed"])
             {
                 if ([[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"RatingDetails"]] isEqualToString:@"No Rating is found"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"No reviews found";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
         }
     }];
    }
}
-(void)goToUpdate
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobContractorUpdateViewController *ConUpObj=[[OostaJobContractorUpdateViewController alloc]initWithNibName:@"OostaJobContractorUpdateViewController IPad" bundle:nil];
        ConUpObj.strEx = _lblDesc.text;
        ConUpObj.delegate =self;
        [self.navigationController pushViewController:ConUpObj animated:YES];
        
    }
    else if (IS_IPHONE5)
    {
        OostaJobContractorUpdateViewController *ConUpObj=[[OostaJobContractorUpdateViewController alloc]initWithNibName:@"OostaJobContractorUpdateViewController" bundle:nil];
        ConUpObj.strEx = _lblDesc.text;
        ConUpObj.delegate =self;
        [self.navigationController pushViewController:ConUpObj animated:YES];
    }
    else
    {
        OostaJobContractorUpdateViewController *ConUpObj=[[OostaJobContractorUpdateViewController alloc]initWithNibName:@"OostaJobContractorUpdateViewController Small" bundle:nil];
        ConUpObj.strEx = _lblDesc.text;
        ConUpObj.delegate =self;
        [self.navigationController pushViewController:ConUpObj animated:YES];
    }
}
-(void)getUpdate
{
    [self setupValues];
    
    if(_delegate && [_delegate respondsToSelector:@selector(getUpdate)])
    {
        
        [_delegate getUpdate];
    }
}
@end
