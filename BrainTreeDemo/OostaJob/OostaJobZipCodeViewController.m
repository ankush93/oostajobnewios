//
//  OostaJobZipCodeViewController.m
//  OostaJob
//
//  Created by Armor on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobZipCodeViewController.h"

@interface OostaJobZipCodeViewController ()<UITextFieldDelegate>

@end

@implementation OostaJobZipCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.imgBlur assignBlur];
    [self setupUI];
    _lblZipcode.text = [NSString stringWithFormat:@"ZIP CODE %@",_strZipcode];
    _txtEmail.text = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Email"];
    [self registerForKeyboardNotifications];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-UIDesign
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        //lbl Font name and size
    self.lblZipcode.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
    self.lblShot.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    self.lblNotify.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        //Txt Font name and size
    self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        //Btn Font name,size and Corner Radius.
    self.btnSend.layer.cornerRadius=2.0;
    self.btnSend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
    }
    else
    {
        self.lblZipcode.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblShot.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblNotify.font=[UIFont fontWithName:FONT_BOLD size:17.0f];
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:17.0f];
        self.btnSend.layer.cornerRadius=2.0;
        self.btnSend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:17.0f];
    }
}
#pragma mark-TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.txtEmail resignFirstResponder];
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    return YES;
}
#pragma mark-Button Action
-(IBAction)btnSend:(id)sender
{
    if (![self IsValidEmail:_txtEmail.text])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = @"Enter Valid Email";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        [self EmailNotify];
    }
}
-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    /*UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+14, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;*/
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    
        CGPoint scrollPoint = CGPointMake(0.0, _txtEmail.frame.origin.y-30);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
}
-(void)EmailNotify
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Notify...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"id";
        NSString * obj1 =@"";
        NSString * key2 =@"zipcode";
        NSString * obj2 =self.strZipcode?self.strZipcode:@"";;
        NSString * key3 =@"email";
        NSString * obj3 =self.txtEmail.text;
        NSString * key4 =@"createdTime";
        NSString * obj4 =@"";
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getemailzipcode",BaseURL]]];
        NSLog(@"getemailzipcode:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     
                     MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:hud];
                     
                     hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     hud.mode = MBProgressHUDModeCustomView;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     hud.delegate = self;
                     hud.labelText = @"Done";
                     [hud show:YES];
                     [hud hide:YES afterDelay:2];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
             
         }];
    }
}
@end
