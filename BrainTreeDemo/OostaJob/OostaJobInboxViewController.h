//
//  OostaJobInboxViewController.h
//  OostaJob
//
//  Created by Armor on 23/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobInboxViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UILabel *lblInboxHeading;
@property (weak, nonatomic) IBOutlet UITableView *tblInbox;
- (IBAction)btnMenuPressed:(id)sender;
@end
