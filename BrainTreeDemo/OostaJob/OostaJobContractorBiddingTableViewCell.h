//
//  OostaJobContractorInvitesTableViewCell.h
//  OostaJob
//
//  Created by Armor on 20/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "MXLMediaView.h"
@interface OostaJobContractorBiddingTableViewCell : UITableViewCell<MXLMediaViewDelegate>
{
    NSMutableArray *arrCollectionViewData;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTblJobName;
@property (weak, nonatomic) IBOutlet UILabel *lblTblPin;



@property (weak, nonatomic) IBOutlet UIView *viewbids;
@property (weak, nonatomic) IBOutlet UIButton *btnTblBidding;
@property (strong, nonatomic) IBOutlet UIView *viewContractor;
@property (strong, nonatomic) IBOutlet UILabel *lblContratorCount;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UIView *viewMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblMessageCount;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIView *viewContent;
- (IBAction)Onclickcompletebtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *completedbtn;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DWTagList *tagList;
-(void)CollectionData:(NSMutableArray *)arr andtheIndexPath:(NSIndexPath *)indexPath andTheTableView:(UITableView *)tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIView *dropdown;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnContractor;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblCongrdz;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgCalendar;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UIView *viewStars;
@property (weak, nonatomic) IBOutlet UIImageView *imgMsg;
@property (weak, nonatomic) IBOutlet UIView *viewLowestBidAmount;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *lblLowestBidAmount;
@property (weak, nonatomic) IBOutlet DWTagList *reasonslist;
@end
