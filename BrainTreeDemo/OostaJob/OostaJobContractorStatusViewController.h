//
//  OostaJobContractorStatusViewController.h
//  OostaJob
//
//  Created by Armor on 25/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankDetailcontroller.h"
@interface OostaJobContractorStatusViewController : UIViewController<MBProgressHUDDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (strong, nonatomic) IBOutlet UILabel *lblVerifiction;
@property (strong, nonatomic) IBOutlet UILabel *lbl24hrs;
@property (strong, nonatomic) NSMutableArray *resultArr;
@property (strong, nonatomic) IBOutlet UIButton *backBtnTapped;
@property (strong, nonatomic) UIWindow *window;
- (IBAction)backBtnTappedContract:(id)sender;

@property (strong, nonatomic) SWRevealViewController *viewController;
@end
