//
//  OostaJobDescriptionViewController.h
//  OostaJob
//
//  Created by Apple on 05/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OostaJobDescriptionViewControllerDelegate <NSObject>
@optional
- (void)getDesc:(NSString*)description andthecurrentTappedIndex:(NSString *)currentTapIndex;
@end

@interface OostaJobDescriptionViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
- (IBAction)btnDonePressed:(id)sender;
- (IBAction)btnBackPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtVIewDesc;
@property (strong, nonatomic) NSString * currentTappedIndex;
@property (strong, nonatomic) NSString * currentValue;
@property (strong, nonatomic) NSMutableArray *arrContent;

@property (nonatomic, assign)   id<OostaJobDescriptionViewControllerDelegate> delegate;
@end
