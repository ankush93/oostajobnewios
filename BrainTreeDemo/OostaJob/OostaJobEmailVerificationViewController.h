//
//  OostaJobEmailVerificationViewController.h
//  OostaJob
//
//  Created by Apple on 28/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobEmailVerificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgblur;
@property (weak, nonatomic) IBOutlet UILabel *lblFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblThird;
@property (weak, nonatomic) IBOutlet UIButton *btnResend;
- (IBAction)btnResend:(id)sender;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@end
