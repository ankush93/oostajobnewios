//
//  CustomerHistoryController.h
//  OostaJob
//
//  Created by Ankush on 21/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerHistoryTableViewCell.h"
#import <BraintreeUI/BraintreeUI.h>
#import  <BraintreeDropIn/BraintreeDropIn.h>
@interface CustomerHistoryController : UIViewController <UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UITableView *historytable;
@property (weak, nonatomic) IBOutlet UIButton *menutapped;
- (IBAction)OnclickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *img;
- (IBAction)Onclickmenu:(id)sender;

@end
