//
//  NSURL+UIImage.h
//  OostaJob
//
//  Created by Apple on 05/03/16.
//  Copyright © 2016 Armor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL_UIImage : NSObject

+ (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage;

@end
