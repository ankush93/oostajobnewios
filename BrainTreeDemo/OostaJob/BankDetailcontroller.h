//
//  BankDetailcontroller.h
//  OostaJob
//
//  Created by Ankush on 17/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OostaJobBlurImageView.h"
#import "OostaJobEmailVerificationViewController.h"
#import "OostaJobMapViewController.h"
#import "ContractorRearViewController.h"
@interface BankDetailcontroller : UIViewController <UITextFieldDelegate,MBProgressHUDDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
- (IBAction)OnclickSkip:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *bankname;
@property (weak, nonatomic) IBOutlet UITextField *accountname;
@property (weak, nonatomic) IBOutlet UITextField *accountnumber;
@property (weak, nonatomic) IBOutlet UITextField *routing;
- (IBAction)OnclickSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submit;
@property (weak, nonatomic) IBOutlet UIButton *skipreference;
@property (weak, nonatomic) IBOutlet UIImageView *oostaiconreference;
@property (weak, nonatomic) IBOutlet UIButton *menureference;
- (IBAction)OnclickMenu:(id)sender;
@property (strong,nonatomic) NSString *passage;
- (IBAction)Onclickbarbuttonmenu:(id)sender;
- (IBAction)Onclickaddcard:(id)sender;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@end
