//
//  OostaJobContractorBidNowViewController.m
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//OostaJobContractorBidNowViewController
#import <BraintreeDropIn/BraintreeDropIn.h>
#import <BraintreeCore/BraintreeCore.h>
#import "OostaJobCustomerJobDetailsViewController.h"
#import "OostaJobCustomerJobDetailsTableViewCell.h"
#import "ALMoviePlayerController.h"
#import "OostaJobMediaCollectionViewCell.h"
#import "ImageCache.h"
#import <AVFoundation/AVFoundation.h>
#import "UITextView+Placeholder.h"
#import "OostaJobContractorsLIstTableViewCell.h"
#import "OostaJobReviewViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import "OostaJobFeedbackViewController.h"
#import "JSBadgeView.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "UIImage+ImageCompress.h"

@interface OostaJobCustomerJobDetailsViewController ()<ALMoviePlayerControllerDelegate,UITextViewDelegate,CKCalendarDelegate,MFMailComposeViewControllerDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>
{
    KLCPopup* popup;
    int val;
    KLCPopup *popupTermsAndConditions;
    MBProgressHUD *HUD;

    ALAssetsLibrary * assetsLibrary_;
    NSMutableDictionary *dictMedia;
    int selectedIndex;
    BOOL isClicked;
    BOOL isChatTextField;
    BOOL isConctractorClicked;
    BOOL isNotifyCalled;
    NSString *animationDirection;
    NSIndexPath *selectedReplyIndexPath;
    BOOL isClickedReply;
    UITextView *txtView;
    NSString *msg;
    NSMutableArray *arrContractors;
    NSDateFormatter *dateFormatter;
    NSMutableArray *arrAvailableTimings;
    NSDate *selectedDate;
    NSString *selectedTime;
    NSString *strPhone;
    NSString *strEmail;
    NSMutableDictionary * dictSelectedTimings;
    BOOL isBidding;
    JSBadgeView *badgeView;
    NSMutableArray * arrMediaList;
    NSUInteger mediaCount;
    NSString *strLastKey;
    NSString *clientToken;
    NSString *Jobid;
    NSString *Contractorid;
    NSString *clientToken1;
    UIAlertView *alertinitialstat;
    NSString *strClientToken;
    NSString *changetext;
}
@property (nonatomic, strong) BTAPIClient *braintreeClient;

@end

@implementation OostaJobCustomerJobDetailsViewController
@synthesize table;
@synthesize btnSender;
@synthesize list;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    clientToken = @"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIxMmMxYmY0N2RkZTJkYTdhNjI1MmJmOThkMGJiMDQ1MmY5ZGViODhmZmJjMGVmNzU4ZjcyYzZlMWZkM2JhYzQ1fGNyZWF0ZWRfYXQ9MjAxOC0wMS0yMVQxNToxNDo1MC4xNjE2OTA5NDcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=";
    
    

    mediaCount = 0;
    _scrlMsgContractor.hidden = YES;
    isNotifyCalled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedNotification:) name:@"Finished" object:nil];
    isConctractorClicked = NO;
    [super viewDidLoad];
    
    self.tblChatMsgs.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    arrMediaList =[[NSMutableArray alloc]init];
    dictMedia = [[NSMutableDictionary alloc]init];
    //    arrMediaList = [_arrSelected valueForKeyPath:@"jobmedialist"];
    
    
    for (int i=0; i<[[_arrSelected valueForKeyPath:@"jobmedialist"] count]; i++)
    {
        [arrMediaList addObject:[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:i]];
    }
    
    
    [self UISetup];
    // Do any additional setup after loading the view from its nib.
    
    
    
    self.resultAry=[[NSMutableArray alloc]init];
    self.resultDiscussionAry=[[NSMutableArray alloc]init];
    //Call Job discussion Function
    
    selectedIndex = 0;
    isClicked = YES;
    isChatTextField = NO;
    isClickedReply = NO;
    
    _tblChatMsgs.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblContractors.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self settingTheCalendarView];
//    [self registerForKeyboardNotifications];
    [self SetValueForSelectedArr];
    [self JobDiscussionandCompletionHandler:^(bool resultJobDiscussion) {
        if (resultJobDiscussion==YES) {
            NSLog(@"Completed Get Job Discussion Method");
        }
    }];
    
    NSInteger section = [self.collectionView numberOfSections] - 1;
    NSInteger item = [self.collectionView numberOfItemsInSection:section] - 1;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionLeft) animated:YES];
    
    NSLog(@"calendar skip_view_origin==>>%f,calendar skip_view_Height==>>%f",_viewForCalendarSkip.frame.origin.y,_viewForCalendarSkip.frame.size.height);
    NSLog(@"scroll_Calendar_view_origin==>>%f,scroll_Calendar_view_Height==>>%f",_scrlCalendar.frame.origin.y,_scrlCalendar.frame.size.height);
    
    
    [self fetchClientToken];
    [self getClientTokenalreadycheck];
}



- (void)fetchClientToken {
    NSURL *clientTokenURL = [NSURL URLWithString:@"http://52.41.134.73/api/index.php/getclientaccesstoken"];
    NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];

    NSString * key1 = @"Braintreeid";
    NSString * obj1 = [brain objectForKey:@"BrainTreeid"];
    NSLog(@"brain tree id in details:%@",[brain objectForKey:@"BrainTreeid"]);
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1]
                                   forKeys:@[key1]];
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
    [clientTokenRequest setHTTPBody:body];

    [clientTokenRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //[clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];

    [[[NSURLSession sharedSession] dataTaskWithRequest:clientTokenRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // TODO: Handle errors
        NSString *clientToken1 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"enter :%@",clientToken1);
        
        
        // As an example, you may wish to present Drop-in at this point.
        // Continue to the next section to learn more...
    }] resume];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReload:) name:@"NotificationReload" object:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   // [self HitApi];
}
//-(void)HitApi{
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if (internetStatus == NotReachable)
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//        hud.mode = MBProgressHUDModeText;
//        hud.color=[UIColor whiteColor];
//        hud.labelColor=kMBProgressHUDLabelColor;
//        hud.backgroundColor=kMBProgressHUDBackgroundColor;
//
//        hud.labelText = INTERNET_ERR;
//        hud.margin = 10.f;
//        hud.yOffset = 20.f;
//        hud.removeFromSuperViewOnHide = YES;
//        [hud hide:YES afterDelay:2];
//    }
//    else
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        hud.color=[UIColor whiteColor];
//        hud.labelText = @"Sending...";
//        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
//        hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
//
//        hud.margin = 10.f;
//        hud.yOffset = 20.f;
//        [hud show:YES];
//
//        NSString * key1 =@"jobId";
//        NSString * obj1 = userid;
//        NSString * key2 =@"contractor_id";
//        NSString * obj2 = self.bankname.text;
//
//
//        NSLog(@"cont id :%@",obj1);
//        NSLog(@"cont id :%@",obj2);
//
//
//
//        NSDictionary *jsonDictionary =[[NSDictionary alloc]
//                                       initWithObjects:@[obj1,obj2]
//                                       forKeys:@[key1,key2]];
//
//        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
//        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"DATA %@",jsonString);
//        NSMutableData *body = [NSMutableData data];
//
//        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@addBankDetails",BaseURL]]];
//        [request setHTTPBody:body];
//        [request setHTTPMethod:@"POST"];
//
//        [NSURLConnection sendAsynchronousRequest: request
//                                           queue: [NSOperationQueue mainQueue]
//                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
//         {
//             if (error || !data)
//             {
//                 NSLog(@"Server Error : %@", error);
//             }
//             else
//             {
//                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
//                                                                        options:0
//                                                                          error:NULL];
//                 NSLog(@"Result %@",result);
//                 if ([[result valueForKey:@"Result"] isEqualToString:@"Success"])
//                 {
//                     [hud hide:YES];
//                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//                     [self.navigationController.view addSubview:HUD];
//
//                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
//                     HUD.mode = MBProgressHUDModeCustomView;
//                     HUD.color=[UIColor whiteColor];
//                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
//                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
//                     HUD.delegate = self;
//                     HUD.labelText = @"success";
//                     [HUD show:YES];
//                     [HUD hide:YES afterDelay:2];
//                     [popup dismissPresentingPopup];
//
//                 }
//                 else
//                 {
//                     [hud hide:YES];
//                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//                     hud.mode = MBProgressHUDModeText;
//                     hud.color=[UIColor whiteColor];
//                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
//
//                     hud.labelText = @"Error";
//                     hud.margin = 10.f;
//                     hud.yOffset = 20.f;
//                     hud.removeFromSuperViewOnHide = YES;
//                     [hud hide:YES afterDelay:2];
//                 }
//
//
//             }
//         }];
//    }
//}

-(void)viewDidAppear:(BOOL)animated
{
    if ([_strGoto isEqualToString:@"Contractor"])
    {
        [self btnContractorPressed:nil];
    }
    [self performSelector:@selector(unHideScrollView) withObject:nil afterDelay:.2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)unHideScrollView{
    _scrlMsgContractor.hidden = NO;
}

-(void)SetValueForSelectedArr
{
    if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"1"])
    {
        [_btnBidding setTitle:@"Bidding" forState:UIControlStateNormal];
        _viewMsgContr.hidden=YES;
        _viewBiddingMessages.hidden=NO;
        _viewUnderLine.hidden=YES;
        _lblBiddingMemberCount.hidden = NO;
        _imgBiddingMember.hidden = NO;
        isBidding = YES;
        _lblBiddingMemberCount.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"Contractorcnt"]];
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"notificationCount"]] intValue]>0)
        {
            badgeView = [[JSBadgeView alloc] initWithParentView:self.viewNotification alignment:JSBadgeViewAlignmentTopCenter];
            [badgeView setBadgeBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
            [badgeView setBadgeTextFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?16.0f:12.0f]];
            badgeView.badgeText = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"notificationCount"]];
            badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
            badgeView.layer.masksToBounds = YES;
        }
    }
    else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"2"])
    {
        
        NSLog(@"data in Hire :%@",_arrSelected);
        [_btnBidding setTitle:@"Hire" forState:UIControlStateNormal];
        _viewMsgContr.hidden=NO;
        _viewBiddingMessages.hidden=YES;
        _viewUnderLine.hidden=NO;
        isBidding = NO;
    }
    else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"3"])
    {
        [_btnBidding setTitle:@"In Progress" forState:UIControlStateNormal];
        isBidding = NO;
        _viewSelectedContractor.hidden = NO;
        _viewMsgContr.hidden=YES;
        _viewBiddingMessages.hidden=NO;
        _viewUnderLine.hidden=YES;
        _imgArrow.hidden = YES;
        _btnBidding.userInteractionEnabled = NO;
        
        NSLog(@"Contractor Details :%@",_arrSelected);
        Jobid = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"jobpostID"]];
        Contractorid = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.contractorID"]];
        NSLog(@"Job id :%@",Jobid);
        NSLog(@"Contracor id :%@",Contractorid);
        
        self.imgSelected.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"ContractorDetails.profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        self.lblNameSelected.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.username"]];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Lng"]] floatValue]];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             _lblAddressSelected.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
         }];
        
        
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"<null>"])
        {
            self.lblMeetingTimeSelected.text = @"Meeting not scheduled";
        }
        else
        {
            NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]];
            __block NSDate *detectedDate;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                 [dateFormate setDateFormat:@"MM-dd-YYYY"];
                 self.lblMeetingTimeSelected.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[_arrSelected valueForKeyPath:@"ContractorDetails.meetingTime"]];
             }];
        }
        strPhone = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Phone"]];
        strEmail = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Email"]];
        [_btnEmail setTitle:strEmail forState:UIControlStateNormal];
        [_btnCall setTitle:strPhone forState:UIControlStateNormal];
        _tblChatMsgs.frame = CGRectMake(_tblChatMsgs.frame.origin.x, _tblChatMsgs.frame.origin.y, _tblChatMsgs.frame.size.width, _tblChatMsgs.frame.size.height-_viewSelectedContractor.frame.size.height);
        _tblContractors.frame = CGRectMake(_tblContractors.frame.origin.x, _tblContractors.frame.origin.y, _tblContractors.frame.size.width, _tblContractors.frame.size.height-_viewSelectedContractor.frame.size.height);
        
    }
    else
    {
        [_btnBidding setTitle:@"Closed" forState:UIControlStateNormal];
        isBidding = NO;
        _viewSelectedContractor.hidden = NO;
        _viewMsgContr.hidden=YES;
        _viewBiddingMessages.hidden=NO;
        _viewUnderLine.hidden=YES;
        _imgArrow.hidden = YES;
        _btnBidding.userInteractionEnabled = NO;
        _btnRate.hidden = YES;
        _imgCalendar.hidden = NO;
        _lblMeetingTimeSelected.hidden =NO;
        _viewStars.hidden = NO;
        
        
        
        self.imgSelected.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"ContractorDetails.profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        self.lblNameSelected.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.username"]];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Lng"]] floatValue]];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             _lblAddressSelected.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
         }];
        
        CGRect viewStarFrame = _viewStars.frame;
        
        viewStarFrame.origin.x = (IS_IPAD?CGRectGetMidX(_imgSelected.bounds):10 );
//        viewStarFrame.origin.x = ((_imgSelected.frame.size.width+_imgSelected.frame.origin.x)/2) - (_viewStars.frame.size.width / 2);
        
        viewStarFrame.origin.y = self.imgSelected.frame.origin.y + self.imgSelected.frame.size.height + 8;
        
        _viewStars.frame = viewStarFrame;
        
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"<null>"])
        {
            self.lblMeetingTimeSelected.text = @"Meeting not scheduled";
        }
        else
        {
            NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.meetingDate"]];
            __block NSDate *detectedDate;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                 [dateFormate setDateFormat:@"MM-dd-YYYY"];
                 self.lblMeetingTimeSelected.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[_arrSelected valueForKeyPath:@"ContractorDetails.meetingTime"]];
             }];
        }
        strPhone = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Phone"]];
        strEmail = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.Email"]];
        [_btnEmail setTitle:strEmail forState:UIControlStateNormal];
        [_btnCall setTitle:strPhone forState:UIControlStateNormal];
        
        _tblChatMsgs.frame = CGRectMake(_tblChatMsgs.frame.origin.x, _tblChatMsgs.frame.origin.y, _tblChatMsgs.frame.size.width, _tblChatMsgs.frame.size.height-_viewSelectedContractor.frame.size.height);
        _tblContractors.frame = CGRectMake(_tblContractors.frame.origin.x, _tblContractors.frame.origin.y, _tblContractors.frame.size.width, _tblContractors.frame.size.height-_viewSelectedContractor.frame.size.height);
        
        float totalRating = [[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"ContractorDetails.totalRating"]] floatValue];
        for (int i=1; i<=5; i++)
        {
            for (UIImageView *img in self.imgStars)
            {
                if (img.tag == 500+i)
                {
                    if (i<=totalRating)
                    {
                        img.image = [UIImage imageNamed:@"star-fill.png"];
                    }
                    else if ((i-0.5)==totalRating)
                    {
                        img.image = [UIImage imageNamed:@"half.png"];
                    }
                    else
                    {
                        img.image =[UIImage imageNamed:@"empty.png"];
                    }
                }
            }
        }
    }
    
    
    _lblHeader.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobtypeName"]];
    _lblJobTitle.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobtypeName"]];
    
    arrContractors = [[NSMutableArray alloc]init];
    arrContractors = [[_arrSelected valueForKeyPath:@"contractorlist"] mutableCopy];
    
    _lblContractors.text = [NSString stringWithFormat:@"Contractors (%lu)",(unsigned long)arrContractors.count];
    
    _lblJobPinCode.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"zipcode"]];
    NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"jobpostTime"]];
    
    __block NSDate *detectedDate;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
    [detector enumerateMatchesInString:dateString
                               options:kNilOptions
                                 range:NSMakeRange(0, [dateString length])
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         detectedDate = result.date;
         NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
         NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
         
         NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
         NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
         NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
         
         NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
         
         NSLog(@"detectedDate:%@",detectedDate);
         NSLog(@"currentDate:%@",[NSDate date]);
         self.lblCost.text = [self remaningTime:destinationDate endDate:[NSDate date]];
         NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
         
     }];
    
    
    
    
    
    
    NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
    arrAnswers = [_arrSelected valueForKeyPath:@"jobanwserlist"];
    NSMutableArray *arrTags=[[NSMutableArray alloc]init];
    NSMutableArray *arrWithoutTags=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrAnswers.count; i++)
    {
        if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
        {
            if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
            {
                [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
            }
            else
            {
                [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
            }
            
        }
        else
        {
            [arrWithoutTags addObject:[NSString stringWithFormat:@"%@: %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"quest_name"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
        }
        
    }
    
    
    
    NSString *str;
    if (arrWithoutTags.count!=0)
    {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *strWithoutTags =[NSString stringWithFormat:@"%@",arrWithoutTags];
        strWithoutTags = [[strWithoutTags componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedStringAt = [strWithoutTags stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@"\n"];
        
        str=[NSString stringWithFormat:@"%@\n%@",[_arrSelected valueForKeyPath:@"description"],trimmedStringAt];
    }
    else
    {
        str=[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"description"]];
    }
    
    
    if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"1"])
    {
        [arrTags addObject:[NSString stringWithFormat:@"Wait %@ to hire",[_arrSelected valueForKeyPath:@"Remaining_time"]]];
        self.tagList.isHighlightLast = YES;
    }
    
    [self.tagList setAutomaticResize:YES];
    [self.tagList setTags:arrTags];
    [self.tagList setTagDelegate:self];
    
    CGFloat origin = self.tagList.frame.origin.y+self.tagList.frame.size.height;
    
    _lblDescriptionl.text = str;
    //    [_lblDescriptionl sizeToFit];
    _lblDescriptionl.numberOfLines=0;
    
    
    
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?752:285, FLT_MAX);
    //    _lblDescriptionl.backgroundColor = [UIColor redColor];
    
    CGSize expectedLabelSize = [str sizeWithFont:_lblDescriptionl.font constrainedToSize:maximumLabelSize lineBreakMode:_lblDescriptionl.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = _lblDescriptionl.frame;
    newFrame.size.height = expectedLabelSize.height;
    newFrame.origin.y = origin+5;
    _lblDescriptionl.frame = newFrame;
    origin = self.lblDescriptionl.frame.origin.y+self.lblDescriptionl.frame.size.height;
    
    
    
    CGRect frameViewDetails = _viewDetails.frame;
    frameViewDetails.size.height = _lblDescriptionl.frame.origin.y+_lblDescriptionl.frame.size.height+5;
    _viewDetails.frame = frameViewDetails;
    
    
    CGRect frameviewFullContent = _viewFullContent.frame;
    frameviewFullContent.size.height = _viewDetails.frame.origin.y+_viewDetails.frame.size.height+5;
    _viewFullContent.frame = frameviewFullContent;
    
    _scrlViewFullContent.contentSize=CGSizeMake(IS_IPAD?768:320, _viewFullContent.frame.size.height);
    _scrlViewFullContent.scrollEnabled = YES;
    
    
    
    NSString *imageCacheKey = [[_arrSelected valueForKeyPath:@"jobicon"]  stringByDeletingPathExtension];
    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
    {
        self.imgJobIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                UIImage *img=[UIImage imageWithData:data];
                if (img)
                {
                    [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                    self.imgJobIcon.image=img;
                }
                else
                {
                    NSLog(@"Not Found");
                }
            });
            
            
        });
        
    }
    
    
    
    NSString *imageCacheKey1 = [[_arrSelected valueForKeyPath:@"job_image"]  stringByDeletingPathExtension];
    
    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey1])
    {
        self.imgBlur.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey1];
        [self.imgBlur assignBlur];
    }
    else
    {
       /* dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            
            NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"job_image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
            UIImage *img=[UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (img)
                {
                    [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey1];
                    self.imgBlur.image=img;
                    [self.imgBlur assignBlur];
                }
                else
                {
                    NSLog(@"Not Found");
                }
                
            });
        });*/
        
        self.imgBlur.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"job_image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        [self.imgBlur assignBlur];
        
//        self.imgBlur.AssignBlurToImage = @"YES";
//        
//        self.imgBlur.delegate  =  self;
        

        
        
        
    }
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    else
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
        
    }
    [self settingVideoorImage:0];
    
    
    if ([_strGoto isEqualToString:@"Message"])
    {
        [self btnDiscussion:self];
    }
    else if ([_strGoto isEqualToString:@"Contractor"])
    {
        [self btnContractorPressed:self];
    }
    
    
}

-(void)AssignBlurForTheImage
{
    [self.imgBlur assignBlur];
}

-(void)settingVideoorImage:(int)index
{
    if (index<[[_arrSelected valueForKeyPath:@"jobmedialist"] count])
    {
        self.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
        self.imgThumbNail.clipsToBounds = YES;
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imgThumbNail];
        selectedIndex=index;
        if ([[[arrMediaList objectAtIndex:index] valueForKey:@"mediaType"] isEqualToString:@"1"])
        {
            NSString *imageCacheKey = [[[arrMediaList objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                self.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    self.imgThumbNail.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:index] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.imgThumbNail.image)
                        {
                            [[ImageCache sharedImageCache] storeImage:self.imgThumbNail.image withKey:imageCacheKey];
                        }
                    });
                    
                    
                });
                
            }
            
            
            self.imgPlay.hidden=YES;
            
        }
        else if ([[[arrMediaList objectAtIndex:index] valueForKey:@"mediaType"] isEqualToString:@"2"])
        {
            self.imgPlay.hidden=NO;
            NSString *imageCacheKey = [[[arrMediaList objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                self.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:index] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                    NSString *imageCacheKey = [[[arrMediaList objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    generator.appliesPreferredTrackTransform = YES;
                    NSError *error;
                    CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                    if (!error)
                    {
                        UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            self.imgThumbNail.image=image;
                            if (self.imgThumbNail.image)
                            {
                                [[ImageCache sharedImageCache] storeImage:self.imgThumbNail.image withKey:imageCacheKey];
                            }
                            
                        });
                    }
                });
            }
        }
    }
    else
    {
        self.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
        self.imgThumbNail.clipsToBounds = YES;
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imgThumbNail];
        selectedIndex=index;
        if ([[arrMediaList objectAtIndex:index] isKindOfClass:[UIImage class]])
        {
            
            self.imgThumbNail.image=[arrMediaList objectAtIndex:index];
            self.imgPlay.hidden=YES;
            
        }
        else
        {
            self.imgPlay.hidden=NO;
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[arrMediaList objectAtIndex:index] options:nil];
            AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            generator.appliesPreferredTrackTransform = YES;
            NSError *error;
            CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
            if (!error)
            {
                UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.imgThumbNail.image=image;
                    
                });
            }
        }
    }
}
#pragma mark -UIDesign
-(void)UISetup
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        //lbl Font name and Size
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        
        self.lblHouseCleaning.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblOneStory.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblSqft.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblMaterials.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lbl2Bath.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lbl2Bed.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblCost.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblJobHeading.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblJobTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblJobPinCode.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblBiddingMemberCount.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblDiscussion.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblMesseges.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblContractors.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        
        self.lblDescriptionl.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        //Btn font name and Size
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        //BorderWidth
        self.lblSelectedDate.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        
        self.btnMeet.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        self.btnSkip.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        self.lblHeaderSelected.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblMeetingTimeSelected.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        _lblNameSelected.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        _lblAddressSelected.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        _btnEmail.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        _btnCall.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        _btnRate.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        self.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
        self.viewBidding.layer.cornerRadius=13.0;
        
        
    }
    else
    {
        //lbl Font name and Size
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHouseCleaning.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblOneStory.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblSqft.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblMaterials.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lbl2Bath.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lbl2Bed.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblCost.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblJobHeading.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblJobTitle.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblJobPinCode.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblBiddingMemberCount.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblDiscussion.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblMesseges.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblContractors.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblDescriptionl.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        
        self.lblSelectedDate.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        
        self.btnMeet.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:15.0f];
        
        self.btnSkip.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:15.0f];
        
        //Btn Font name and size
        
        
        
        
        
        
        self.lblHouseCleaning.layer.borderWidth=1.0;
        //Border Width
        self.lblOneStory.layer.borderWidth=1.0;
        self.lblSqft.layer.borderWidth=1.0;
        self.lblMaterials.layer.borderWidth=1.0;
        self.lbl2Bath.layer.borderWidth=1.0;
        self.lbl2Bed.layer.borderWidth=1.0;
        //Border Colour
        self.lblHouseCleaning.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblOneStory.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblSqft.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblMaterials.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bath.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner Radius
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        
        self.lblHouseCleaning.layer.cornerRadius=10.0;
        self.lblOneStory.layer.cornerRadius=10.0;
        self.lblSqft.layer.cornerRadius=10.0;
        self.lblMaterials.layer.cornerRadius=10.0;
        self.lbl2Bath.layer.cornerRadius=10.0;
        self.lbl2Bed.layer.cornerRadius=10.0;
        
        self.viewBidding.layer.cornerRadius=10.0;
        
        self.lblHeaderSelected.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblMeetingTimeSelected.font = [UIFont fontWithName:FONT_BOLD size:16.0f];
        
        _lblNameSelected.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        _lblAddressSelected.font = [UIFont fontWithName:FONT_THIN size:18.0f];
        _btnEmail.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        _btnCall.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        _btnRate.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        
        [_btnEmail.titleLabel setFont: [UIFont fontWithName:FONT_BOLD size:18.0f]];
        _btnEmail.titleLabel.adjustsFontSizeToFitWidth = TRUE;
        _btnEmail.titleLabel.minimumFontSize = 12;
    }
    
    _imgSelected.layer.cornerRadius = _imgSelected.frame.size.width/2;
    _imgSelected.layer.masksToBounds = YES;
    
    _btnEmail.layer.cornerRadius = 3.0f;
    _btnEmail.layer.masksToBounds = YES;
    
    _btnCall.layer.cornerRadius = 3.0f;
    _btnCall.layer.masksToBounds = YES;
    
    _scrollView.contentSize=CGSizeMake(IS_IPAD?768:320, _viewMessageContent.frame.origin.y+_viewMessageContent.frame.size.height);
    _scrollView.scrollEnabled=YES;
    _btnBidding.titleLabel.adjustsFontSizeToFitWidth = TRUE;
    _btnBidding.titleLabel.minimumFontSize= 12;
}
#pragma mark - set Button Actions
-(IBAction)btnDiscussion:(id)sender
{
    self.btnUp.enabled=YES;
    self.btnUpCon.enabled=YES;
    //Action for Discussion Button
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 830);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(0, 0, 376, 3);
                             
                         }
         
                         completion:nil];
        
         [_tblChatMsgs reloadData];
        
    }
    else if (IS_IPHONE5)
    {
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 464);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             _viewUnderLine.frame=CGRectMake(0, 0, 145, 2);
                         }
                         completion:nil];
        [_tblChatMsgs reloadData];
        
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 380);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             _viewUnderLine.frame=CGRectMake(0, 0, 145, 2);
                         }
                         completion:nil];
         [_tblChatMsgs reloadData];
        
    }
    if (isNotifyCalled!=YES)
    {
        [self NotifyToWeb];
    }
}
- (IBAction)btnUp:(id)sender
{
    CGPoint point = CGPointMake(0,0);
    [_scrollView setContentOffset:point animated:YES];
}

-(IBAction)btnBackBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark-ScrollView Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_scrollView)
    {
        CGFloat totalScroll = scrollView.contentSize.height - scrollView.bounds.size.height;
        CGFloat offset =  scrollView.contentOffset.y;
        CGFloat percentage = offset / totalScroll;
        NSLog(@"percentage:%f",percentage);
        self.btnUp.alpha = (0.f + percentage);
        self.btnUpCon.alpha = (0.f + percentage);
        if ((0.f + percentage)>=1)
        {
            if (isNotifyCalled!=YES)
            {
                [self NotifyToWeb];
            }
            
        }
    }
}

#pragma mark -UITableView Datasource and Delegates


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table)
    {
        return list.count;
    }
    else if(tableView == _tblContractors)
    {
        return arrContractors.count;
        if (arrContractors.count==0)
        {
            return 1;
        }
        else
        {
            return arrContractors.count;
        }
    }
    else
    {
        if (_resultJobDiscussionAry.count==0)
        {
            _tblChatMsgs.backgroundColor = [UIColor clearColor];
            return 1;
        }
        else
        {
            _tblChatMsgs.backgroundColor = [UIColor whiteColor];
            return _resultJobDiscussionAry.count;
            
        }
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (tableView==table)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        cell.textLabel.text = [list objectAtIndex:indexPath.row];
        
        
        cell.textLabel.textColor = [UIColor blackColor];
        
        return cell;
    }
    else if (tableView==_tblContractors)
    {
        if (arrContractors.count==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Contractors Found";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            static NSString *tblIdentifier=@"Cellname";
            OostaJobContractorsLIstTableViewCell *cell=(OostaJobContractorsLIstTableViewCell*)[self.tblContractors dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorsLIstTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    
                    cell.imgProfilePict.layer.cornerRadius=cell.imgProfilePict.frame.size.width/2;
                    cell.imgProfilePict.layer.masksToBounds=YES;
                    
                    cell.lblAddress.font=[UIFont fontWithName:FONT_THIN size:22];
                    cell.lblAmount.font=[UIFont fontWithName:FONT_BOLD size:20];
                    cell.lblNotExceed.font=[UIFont fontWithName:FONT_BOLD size:13];
                    cell.lblName.font=[UIFont fontWithName:FONT_BOLD size:30];
                    cell.lblHireMe.font=[UIFont fontWithName:FONT_BOLD size:16];
                    
                    cell.btnHireMe.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20];
                    cell.viewHireBtn.layer.cornerRadius=15;
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorsLIstTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    
                    cell.imgProfilePict.layer.cornerRadius=cell.imgProfilePict.frame.size.width/2;
                    cell.imgProfilePict.layer.masksToBounds=YES;
                    cell.lblAddress.font=[UIFont fontWithName:FONT_THIN size:15];
                    cell.lblAmount.font=[UIFont fontWithName:FONT_BOLD size:12];
                    cell.lblNotExceed.font=[UIFont fontWithName:FONT_BOLD size:7];
                    cell.lblName.font=[UIFont fontWithName:FONT_BOLD size:20];
                    cell.lblHireMe.font=[UIFont fontWithName:FONT_BOLD size:12];
                    
                    cell.btnHireMe.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10];
                    cell.viewHireBtn.layer.cornerRadius=12;
                }
                
            }
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.lblName.text = [NSString stringWithFormat:@"%@",[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"Name"]];
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"Lng"]] floatValue]];
            CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
            [geocoder reverseGeocodeLocation:location
                           completionHandler:^(NSArray *placemarks, NSError *error)
             {
                 if (error){
                     NSLog(@"Geocode failed with error: %@", error);
                     return;
                 }
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                 NSLog(@"locality %@",placemark.locality);
                 NSLog(@"postalCode %@",placemark.postalCode);
                 cell.lblAddress.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
             }];
            cell.lblAmount.text = [NSString stringWithFormat:@"$%@",[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            
            
            cell.btnHireMe.tag = indexPath.row;
            [cell.btnHireMe addTarget:self action:@selector(btnHireMePressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.imgProfilePict.contentMode = UIViewContentModeScaleAspectFill;
            cell.imgProfilePict.clipsToBounds = YES;
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProfilePict];
            cell.imgProfilePict.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            float totalRating = [[NSString stringWithFormat:@"%@",[[arrContractors objectAtIndex:indexPath.row] valueForKeyPath:@"totalRatingbycategory"]] floatValue];
            for (int i=1; i<=5; i++)
            {
                for (UIImageView *img in cell.stars)
                {
                    if (img.tag == 500+i)
                    {
                        if (i<=totalRating)
                        {
                            img.image = [UIImage imageNamed:@"star-fill.png"];
                        }
                        else if ((i-0.5)==totalRating)
                        {
                            img.image = [UIImage imageNamed:@"half.png"];
                        }
                        else
                        {
                            img.image =[UIImage imageNamed:@"empty.png"];
                        }
                    }
                }
            }
            
            
            
            return cell;
            
        }
    }
    else
    {
        if (_resultJobDiscussionAry.count==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Messages Found";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            static NSString *tblIdentifier=@"Cellname";
            
            OostaJobCustomerJobDetailsTableViewCell *cell=(OostaJobCustomerJobDetailsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tblIdentifier];
            
            if (cell==nil) {
                
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerJobDetailsTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.viewContentAll.layer.cornerRadius = 5;//Corner radius for View
                    cell.viewContentAll.layer.masksToBounds = YES;
                    //Font name for labels
                    cell.lblTblDispName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblDispTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblCustomerMsgTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblTblDispMsg.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    cell.lblCustomerMsg.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    //image description in table
                    
                    cell.imgTbl.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
                    cell.imgTbl.layer.masksToBounds=YES;
                    //temporary values for table
                    
                    cell.btnReply.layer.cornerRadius = 3.0f;
                    cell.btnReply.layer.masksToBounds=YES;
                    
                    cell.btnSend.layer.cornerRadius = 3.0f;
                    cell.btnSend.layer.masksToBounds=YES;
                    
                    cell.txtViewReply.layer.cornerRadius = 3.0f;
                    cell.txtViewReply.layer.masksToBounds=YES;
                    cell.txtViewReply.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    cell.txtViewReply.layer.borderWidth = 1.0f;
                    
                    cell.btnReply.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.btnSend.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.txtViewReply.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblReply.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    cell.txtViewReply.placeholder=@"Type your reply..";
                    cell.txtViewReply.placeholderColor = [UIColor darkGrayColor];
                    cell.txtViewReply.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
                    cell.txtViewReply.layer.cornerRadius=3.0;
                    
                    cell.txtViewReply.delegate = self;
                    
                    //                cell.viewCustomer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
                    
                    cell.txtViewReply.tag=1000;
                    cell.viewCustomer.tag=2000;
                    cell.viewContentAll.tag = 3000;
                    cell.viewSend.tag = 4000;
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerJobDetailsTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    
                    cell.viewContentAll.layer.cornerRadius = 5;//Corner radius for View
                    cell.viewContentAll.layer.masksToBounds = YES;
                    //Font name for labels
                    cell.lblTblDispName.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
                    cell.lblTblDispTime.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
                    cell.lblTblDispMsg.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblCustomerMsg.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblCustomerMsgTime.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
                    
                    //image description in table
                    
                    cell.imgTbl.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
                    cell.imgTbl.layer.masksToBounds=YES;
                    
                    cell.btnReply.layer.cornerRadius = 3.0f;
                    cell.btnReply.layer.masksToBounds=YES;
                    
                    cell.btnSend.layer.cornerRadius = 3.0f;
                    cell.btnSend.layer.masksToBounds=YES;
                    
                    cell.txtViewReply.layer.cornerRadius = 3.0f;
                    cell.txtViewReply.layer.masksToBounds=YES;
                    cell.txtViewReply.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    cell.txtViewReply.layer.borderWidth = 1.0f;
                    
                    cell.btnReply.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblReply.font = [UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.btnSend.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.txtViewReply.font = [UIFont fontWithName:FONT_THIN size:16.0f];
                    
                    cell.txtViewReply.placeholder=@"Type your reply..";
                    cell.txtViewReply.placeholderColor = [UIColor darkGrayColor];
                    cell.txtViewReply.delegate = self;
                    
                    //                cell.viewCustomer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
                    //temporary values for table
                    
                    cell.txtViewReply.tag=1000;
                    cell.viewCustomer.tag=2000;
                    cell.viewContentAll.tag = 3000;
                    cell.viewSend.tag = 4000;
                }
            }
            cell.lblTblDispMsg.text=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
            [cell.lblTblDispMsg sizeToFit];
            cell.lblTblDispMsg.numberOfLines = 0;
            
            
            NSString *dateString = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionTime"]];
            
            __block NSDate *detectedDate;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                 [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
                 cell.lblTblDispTime.text = [dateFormat stringFromDate:destinationDate];
                 
                 
             }];
            
            cell.lblTblDispName.text=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_name"]];
            cell.imgTbl.contentMode = UIViewContentModeScaleAspectFill;
            cell.imgTbl.clipsToBounds = YES;
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgTbl];
            /* NSString *imageCacheKey = [[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_profile"] stringByDeletingPathExtension];
             if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
             {
             cell.imgTbl.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
             }
             else
             {
             dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){*/
            cell.imgTbl.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_profile"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            /* dispatch_async(dispatch_get_main_queue(), ^{
             if (cell.imgTbl.image)
             {
             [[ImageCache sharedImageCache] storeImage:cell.imgTbl.image withKey:imageCacheKey];
             }
             });
             
             
             });
             
             }*/
            
            
            
            NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
            CGSize maximumLabelSize = CGSizeMake(IS_IPAD?605:229, FLT_MAX);
            CGSize expectedLabelSize = [str sizeWithFont:cell.lblTblDispMsg.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lblTblDispMsg.lineBreakMode];
            CGRect newFrame = cell.lblTblDispMsg.frame;
            newFrame.size.height = expectedLabelSize.height;
            cell.lblTblDispMsg.frame = newFrame;
            CGFloat origin = cell.lblTblDispMsg.frame.origin.y + cell.lblTblDispMsg.frame.size.height;
            
            CGRect viewCustomerFrame = cell.viewCustomer.frame;
            NSString *strAnswer=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]];
            if (strAnswer.length!=0)
            {
                cell.lblCustomerMsg.frame = CGRectMake(cell.lblCustomerMsg.frame.origin.x, origin+3, cell.lblCustomerMsg.frame.size.width, cell.lblCustomerMsg.frame.size.height);
                cell.imgReply.frame = CGRectMake(cell.imgReply.frame.origin.x, origin+3, cell.imgReply.frame.size.width, cell.imgReply.frame.size.height);
                
                
                CGSize maximumLabelSizeAnswer = CGSizeMake(IS_IPAD?586:210, FLT_MAX);
                
                CGSize expectedLabelSizeAnswer = [strAnswer sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:13.0f] constrainedToSize:maximumLabelSizeAnswer lineBreakMode:NSLineBreakByTruncatingTail];
                
                origin = origin + 3 + expectedLabelSizeAnswer.height + 3;
                cell.lblCustomerMsgTime.frame = CGRectMake(cell.lblCustomerMsgTime.frame.origin.x, origin, cell.lblCustomerMsgTime.frame.size.width, 27);
                origin = origin + 27 + 3;
                NSString *dateString = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"RespondTime"]];
                
                __block NSDate *detectedDate;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
                [detector enumerateMatchesInString:dateString
                                           options:kNilOptions
                                             range:NSMakeRange(0, [dateString length])
                                        usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                 {
                     detectedDate = result.date;
                     NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                     NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                     
                     NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                     NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                     NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                     
                     NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                     [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
                     cell.lblCustomerMsgTime.text = [dateFormat stringFromDate:destinationDate];
                     
                     
                 }];
                
                cell.lblCustomerMsg.text = strAnswer;
                [cell.lblCustomerMsg sizeToFit];
                cell.lblCustomerMsg.numberOfLines = 0;
                
                cell.lblCustomerMsg.hidden=NO;
                cell.imgReply.hidden=NO;
                cell.lblCustomerMsgTime.hidden = NO;
            }
            else
            {
                cell.lblCustomerMsgTime.hidden = YES;
                cell.lblCustomerMsg.hidden=YES;
                cell.imgReply.hidden=YES;
            }
            
            if (isBidding == YES)
            {
                viewCustomerFrame.origin.y = origin;
                if ([selectedReplyIndexPath isEqual: indexPath]&& isClickedReply == YES)
                {
                    if (msg.length!=0)
                    {
                        cell.txtViewReply.text = msg;
                    }
                    viewCustomerFrame.size.height = (IS_IPAD?180:85)+cell.txtViewReply.frame.size.height;
                    [cell.btnReply setBackgroundColor:[UIColor lightGrayColor]];
                    
                }
                else
                {
                    viewCustomerFrame.size.height = IS_IPAD?65:36;
                    [cell.btnReply setBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
                }
                cell.viewCustomer.frame = viewCustomerFrame;
                origin = viewCustomerFrame.origin.y+viewCustomerFrame.size.height;
            }
            else
            {
                cell.viewCustomer.hidden = YES;
                if (origin<120&&IS_IPAD==YES)
                {
                    origin = 117;
                }
            }
            
            
            CGFloat viewContentAllHeight = origin + 3;
            CGRect viewContentAllFrame = cell.viewContentAll.frame;
            viewContentAllFrame.size.height = viewContentAllHeight ;
            cell.viewContentAll.frame = viewContentAllFrame;
            
            
            
            CGRect viewSendFrame = cell.viewSend.frame;
            viewSendFrame.origin.y =cell.txtViewReply.frame.size.height+cell.txtViewReply.frame.origin.y+5;
            cell.viewSend.frame = viewSendFrame;
            
            cell.btnSend.tag = indexPath.row;
            [cell.btnSend addTarget:self action:@selector(SendButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.btnReply.tag = indexPath.row;
            [cell.btnReply addTarget:self action:@selector(ReplyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblContractors)
    {
        if (arrContractors.count!=0)
        {
            [self goToContractoDetails:arrContractors andSelectedInde:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        }
        
    }
    else if (tableView==table)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"If you cancel the job, the job will be removed from the OostaJob"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel Job"
                                              otherButtonTitles:@"Dismiss",nil];
        
        alert.tag = 200;
        [alert show];
        
        
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag != 1000)
    {
        if (buttonIndex == 0 && alertView.tag == 200)
        {
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus internetStatus = [reachability currentReachabilityStatus];
            if (internetStatus == NotReachable)
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=kMBProgressHUDLabelColor;
                hud.backgroundColor=kMBProgressHUDBackgroundColor;
                
                hud.labelText = INTERNET_ERR;
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
            else
            {
                [self getCanceltheJobForTheArray:_arrSelected andCompletionHandler:^(bool resultCanceltheJob) {
                    if (resultCanceltheJob == YES)
                    {
                        NSLog(@"Cancelled Job Called");
                    }
                }];
            }
        }
        if (buttonIndex == 1 && alertView.tag == 123) {
            if (val == 1) {
                [self CheckPayableAmountAPI:^(bool result){
                    if (result == YES) {
                        // [self showDropIn:clientToken];
                        [self getClientToken];

//                                       OostaJobContractorBillingViewController *Obj=[[OostaJobContractorBillingViewController alloc]init];
//                                       Obj.selecteddate1 = selectedDate;
//                                       Obj.selectedtime1 = selectedTime;
//                                       Obj.struserid1 =_strUserId;
//                                       Obj.strpostid1 =_strJobpostID;
//
//                                       [self.navigationController pushViewController:Obj animated:YES];
                        
                        
                    }
                }];
                

                
            }
            else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=kMBProgressHUDLabelColor;
                hud.backgroundColor=kMBProgressHUDBackgroundColor;
                
                hud.labelText = @"Please check the agreement";
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
            
            if (buttonIndex == 0 && alertView.tag == 123) {
                [alertinitialstat removeFromSuperview];
            }
                
                
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==table)
    {
        return 40;
    }
    else if(tableView==_tblContractors)
    {
        if (arrContractors.count==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            return IS_IPAD?120:80;
        }
        
    }
    else
    {
        if (_resultJobDiscussionAry.count==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"Cellname";
            
            OostaJobCustomerJobDetailsTableViewCell *cell=(OostaJobCustomerJobDetailsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tblIdentifier];
            
            if (cell==nil) {
                
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerJobDetailsTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerJobDetailsTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
            CGSize maximumLabelSize = CGSizeMake(IS_IPAD?605:229, FLT_MAX);
            
            CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0:13.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
            CGFloat viewContentAllHeight=0;
            CGRect viewCustomerFrame=cell.viewCustomer.frame;
            CGFloat origin = cell.lblTblDispMsg.frame.origin.y + expectedLabelSize.height;
            
            NSString *strAnswer=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]];
            if (strAnswer.length!=0)
            {
                CGSize maximumLabelSizeAnswer = CGSizeMake(IS_IPAD?586:210, FLT_MAX);
                
                CGSize expectedLabelSizeAnswer = [strAnswer sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:13.0f] constrainedToSize:maximumLabelSizeAnswer lineBreakMode:NSLineBreakByTruncatingTail];
                
                origin = origin + 3 + expectedLabelSizeAnswer.height + 3 + 30;
            }
            
            
            
            viewCustomerFrame.origin.y = origin;
            
            if (isBidding == YES)
            {
                if ([selectedReplyIndexPath isEqual: indexPath]&& isClickedReply == YES)
                {
                    if (msg.length!=0)
                    {
                        cell.txtViewReply.text = msg;
                    }
                    viewCustomerFrame.size.height = (IS_IPAD?180: 85)+cell.txtViewReply.frame.size.height;
                    origin = viewCustomerFrame.origin.y+viewCustomerFrame.size.height;
                    
                    viewContentAllHeight = origin + 3;
                    if (cell.contentView.frame.size.height>=viewContentAllHeight)
                    {
                        viewContentAllHeight = cell.frame.size.height;
                    }
                    
                }
                else
                {
                    viewCustomerFrame.size.height = IS_IPAD?65:36;
                    
                    origin = viewCustomerFrame.origin.y+viewCustomerFrame.size.height;
                    
                    viewContentAllHeight = origin + 3;
                    
                }
            }
            else
            {
                viewContentAllHeight = origin + 3;
                
                if (viewContentAllHeight<120&&IS_IPAD==YES)
                {
                    viewContentAllHeight = 120;
                }
            }
            return viewContentAllHeight;
        }
    }
}
-(void)btnHireMePressed:(UIButton *)sender
{
    
    _viewCalander.alpha = 0;
    _viewCalander.hidden = NO;
    _viewForCalendarSkip.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _viewCalander.alpha = 1;
        _viewForCalendarSkip.alpha =1;
        _strUserId = [[arrContractors objectAtIndex:sender.tag] valueForKeyPath:@"userID"];
        _strJobpostID = [_arrSelected valueForKeyPath:@"jobpostID"];
        _ContractorID = [_arrSelected valueForKeyPath:@"ContractorDetails.contractorID"];

        [self getCalendarDetailsndCompletionHandler:^(bool resultCalendarDetails) {
            
            NSLog(@"resultCalendarDetails called");
        }];
    }];
    
}

-(void)CheckPayableAmountAPI:(void (^)(bool result))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSLog(@"check array :%@",_resultJobDiscussionAry);
        NSLog(@"array selected :%@",_strUserId);
        NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
        
        NSString * key1 =@"jobId";
        NSString * obj1 = _strJobpostID;
        
        NSString * key2 =@"contractor_id";
        NSString * obj2 = _strUserId;
        
        NSString *key3 = @"braintreeID";
        NSString *obj3 = [brain objectForKey:@"BrainTreeid"];

        
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3]
                                       forKeys:@[key1,key2,key3]];
        
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@payableAmount",BaseURL]]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://52.41.134.73/payableAmount"]];
        NSLog(@"jobhired URL:%@",BaseURL);

        NSLog(@"jobhired URL:%@",request.URL);
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {
                     
                     
                     
                     NSUserDefaults *contractorid1 = [NSUserDefaults standardUserDefaults];
                     [contractorid1 setObject:_strUserId forKey:@"contractorid1"];
                     [contractorid1 synchronize];
                     
                     NSUserDefaults *amount1 = [NSUserDefaults standardUserDefaults];
                     [amount1 setObject:[result valueForKeyPath:@"Response.bidAmount"] forKey:@"amount1"];
                     [amount1 synchronize];
                     
                     NSUserDefaults *jobid1 = [NSUserDefaults standardUserDefaults];
                     [jobid1 setObject:[result valueForKeyPath:@"Response.jobID"] forKey:@"jobid1"];
                     [jobid1 synchronize];

                     
                     [hud hide:YES];
                     MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:hud];
                     
                     hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     hud.mode = MBProgressHUDModeCustomView;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     hud.delegate = self;
                     hud.labelText = @"Success";
                     [hud show:YES];
                     [hud hide:YES afterDelay:2];
                     // [self gotoJobs];
                     
                     
                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
                 
                 completionHandler(true);
             }
         }];
    }
}

-(void)JobHiringContractorandCompletionHandler:(void (^)(bool resultJobHirre))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Hiring...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"userID";
        NSString * obj1 =_strUserId;
        
        NSString * key2 =@"jobid";
        NSString * obj2 =_strJobpostID;
        
        NSString * key3 =@"meetingTime";
        NSString * obj3 =selectedTime;
        
        NSString * key4 =@"meetingDate";
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *date = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:selectedDate]];
        NSString * obj4 =date;
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@jobhired",BaseURL]]];
        NSLog(@"jobhired URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {
                     
                     if(_delegate && [_delegate respondsToSelector:@selector(getreloadto:)])
                     {
                         [_delegate getreloadto:@"Rate"];
                     }
                     [self.navigationController popViewControllerAnimated:YES];
                     [hud hide:YES];
                     MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:hud];
                     
                     hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     hud.mode = MBProgressHUDModeCustomView;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     hud.delegate = self;
                     hud.labelText = @"Hired Successfully";
                     [hud show:YES];
                     [hud hide:YES afterDelay:2];
                     
                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
                 
                 completionHandler(true);
             }
         }];
        
    }
    
}

-(void)ReplyButtonTapped:(UIButton *)sender
{
    if (isClickedReply == NO)
    {
        isClickedReply = YES;
        msg = nil;
        
    }
    else
    {
        isClickedReply = NO;
    }
    NSLog(@"Tag:%ld",(long)sender.tag);
    selectedReplyIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    [self.tblChatMsgs reloadData];
    
    
    
    
    
    OostaJobCustomerJobDetailsTableViewCell *theCell = (id)[self.tblChatMsgs cellForRowAtIndexPath:selectedReplyIndexPath];
    txtView= theCell.txtViewReply;
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    txtView.inputAccessoryView = numberToolbar;
    UITableViewCell *cell;
    cell = (UITableViewCell *) txtView.superview.superview.superview.superview;
    NSLog(@"cell rect height:%f",theCell.frame.size.height);
    //    [_tblChatMsgs scrollToRowAtIndexPath:[_tblChatMsgs indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [_tblChatMsgs scrollRectToVisible:cell.frame animated:YES];
    NSLog(@"txtView:%@",txtView);
}
-(void)doneWithNumberPad
{
    [txtView resignFirstResponder];
    
}
-(void)SendButtonTapped:(UIButton *)sender
{
    NSLog(@"send tag:%ld",(long)sender.tag);
    _strQuestionName=txtView.text;
    if (_strQuestionName.length!=0)
    {
        txtView.text=@"";
        _strJobpostID = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:sender.tag] valueForKeyPath:@"jobID"]];
       // _ContractorID = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:sender.tag] valueForKeyPath:@"ContractorDetails.contractorID"]];
        _strJobDiscID = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:sender.tag] valueForKeyPath:@"discussionID"]];
        
        [self resignTextView];
        [self JobDiscussionPostandCompletionHandler:^(bool resultJobDiscussionPost) {
            if (resultJobDiscussionPost==YES) {
                NSLog(@"Discussion Completed");
                
            }
        }];
    }
    
}
-(void)resignTextView
{
    [txtView resignFirstResponder];
}

#pragma mark - Textfield Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    isChatTextField = NO;
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    txtView= textView;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    txtView.inputAccessoryView = numberToolbar;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    /*UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
     self.tblChatMsgs.contentInset = contentInsets;
     self.tblChatMsgs.scrollIndicatorInsets = contentInsets;
     
     // If active text field is hidden by keyboard, scroll it so it's visible
     // Your application might not need or want this behavior.
     CGRect aRect = self.view.frame;
     aRect.size.height -= kbSize.height;
     if (CGRectContainsPoint(aRect, txtView.frame.origin) ) {
     CGPoint scrollPoint = CGPointMake(0.0, txtView.frame.origin.y-kbSize.height);
     [self.tblChatMsgs setContentOffset:scrollPoint animated:YES];
     }*/
    
    UITableViewCell *cell;
    cell = (UITableViewCell *) txtView.superview.superview.superview.superview;
    NSLog(@"cell rect height:%f",cell.frame.size.height);
    
    CGRect aRect = self.tblChatMsgs.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, cell.frame.origin) )
    {
        NSString *strAnswer=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:selectedReplyIndexPath.row] valueForKeyPath:@"anwser"]];
        CGFloat origin = 0.0;
        if (strAnswer.length!=0)
        {
            CGSize maximumLabelSizeAnswer = CGSizeMake(IS_IPAD?586:210, FLT_MAX);
            
            CGSize expectedLabelSizeAnswer = [strAnswer sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:13.0f] constrainedToSize:maximumLabelSizeAnswer lineBreakMode:NSLineBreakByTruncatingTail];
            
            origin = origin + 3 + expectedLabelSizeAnswer.height + 3 + 120;
        }
        
        CGPoint scrollPoint = CGPointMake(0.0, (cell.frame.origin.y+kbSize.height+origin)-cell.frame.size.height);
        [self.tblChatMsgs setContentOffset:scrollPoint animated:YES];
        
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tblChatMsgs.contentInset = contentInsets;
    self.tblChatMsgs.scrollIndicatorInsets = contentInsets;
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self.tblChatMsgs setContentOffset:scrollPoint animated:YES];
}

#pragma mark textview delegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString * string = [textView.text stringByReplacingCharactersInRange:range withString:text];
    msg = string;
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
    CGPoint point = CGPointMake(0, 0);
    [self.scrlViewFullContent setContentOffset:point animated:YES];
    
}

#pragma mark-JobDiscussionPostMethod
-(void)JobDiscussionPostandCompletionHandler:(void (^)(bool resultJobDiscussionPost))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        _strUserId=[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
        _strJobpostID=[_arrSelected valueForKeyPath:@"jobpostID"];
        
        NSString * key1 =@"userID";
        NSString * obj1 =_strUserId;
        
        NSString * key2 =@"jobpostID";
        NSString * obj2 =_strJobpostID;
        
        NSString * key3 =@"anwser";
        NSString * obj3 =_strQuestionName;
        
        NSString * key4 =@"discussionID";
        NSString * obj4 =_strJobDiscID;
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@replydiscusstion",BaseURL]]];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"replydiscussion URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 
                 NSLog(@"Server Error : %@", error);
                 
                 
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {
                     self.resultDiscussionAry=[result valueForKeyPath:@"DiscussionDetails"];
                     NSLog(@"Value is:%@",self.resultDiscussionAry);
                     msg = nil;
                     isClickedReply = NO;
                     
                     [self JobDiscussionandCompletionHandler:^(bool resultJobDiscussion) {
                         if (resultJobDiscussion==YES) {
                             NSLog(@"Completed Get Job Discussion Method");
                         }
                     }];
                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);
                     
                     
                     
                 }
                 
                 completionHandler(true);
             }
         }];
        
    }
    
}
#pragma mark-JobDiscussionMethod
-(void)JobDiscussionandCompletionHandler:(void (^)(bool resultJobDiscussion))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getforumlist/%@",BaseURL,[_arrSelected valueForKeyPath:@"jobpostID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
        NSLog(@"GetJob Discussion List URL:%@",request.URL);
        
        [request setHTTPMethod:@"GET"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 
                 NSLog(@"Server Error : %@", error);
                 
                 _lblDiscussion.text = [NSString stringWithFormat:@"Messages"];
                 self.lblMesseges.text=[NSString stringWithFormat:@"Messages"];
                 self.lblContractors.text=[NSString stringWithFormat:@"Contractors"];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {
                     self.resultJobDiscussionAry = [[NSMutableArray alloc]init];
                     self.resultJobDiscussionAry=[result valueForKeyPath:@"DiscustionDetails"];
                     NSLog(@"Value is:%@",self.resultDiscussionAry);
                     
                     for (int i=0; i<[[result valueForKeyPath:@"DiscustionDetails"] count]; i++)
                     {
                         
                         _lblDiscussion.text = [NSString stringWithFormat:@"Messages (%d)",i+1];
                         self.lblMesseges.text = [NSString stringWithFormat:@"Messages (%d)",i+1];
                     }
                     
                     
                     [_tblChatMsgs reloadData];
                     [self.tblChatMsgs scrollToRowAtIndexPath:selectedReplyIndexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
                     selectedReplyIndexPath = nil;
                     
                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);
                     _lblDiscussion.text = [NSString stringWithFormat:@"Message"];
                     
                     self.lblMesseges.text = [NSString stringWithFormat:@"Message"];
                     
                 }
                 
                 completionHandler(true);
             }
         }];
        
        
    }
}
#pragma mark Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"1"])
    {
        if ([arrMediaList count]!=25)
        {
            [arrMediaList addObject:@"Plus"];
        }
        if ([arrMediaList count]-1>[[_arrSelected valueForKeyPath:@"jobmedialist"] count])
        {
            [arrMediaList addObject:@"Upload"];
        }
    }
    
    
    return [arrMediaList count];
}


- (OostaJobMediaCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OostaJobMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgThumbNail.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgThumbNail];
    if (indexPath.row<[[_arrSelected valueForKeyPath:@"jobmedialist"] count])
    {
        if ([[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"1"])
        {
            NSString *imageCacheKey = [[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    // retrive image on global queue
                    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                        cell.imgThumbNail.image = img;
                    });
                });*/
                
                [NSURL_UIImage processImageDataWithURLString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] andBlock:^(NSData *imageData)
                 {
                     UIImage * img = [UIImage imageWithData:imageData];
                     
                     [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                     
                     cell.imgThumbNail.image = img;
                 }];
                
                
                
                
            }
            
            
            cell.imgPlay.hidden=YES;
            cell.btnDelete.hidden=YES;
        }
        else if ([[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"2"])
        {
            cell.imgPlay.hidden=NO;
            NSString *imageCacheKey = [[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                    NSString *imageCacheKey = [[[arrMediaList objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    generator.appliesPreferredTrackTransform = YES;
                    NSError *error;
                    CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                    if (!error)
                    {
                        UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            cell.imgThumbNail.image=image;
                            if (cell.imgThumbNail.image)
                            {
                                [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                            }
                            
                        });
                    }
                });
            }
            
            
            cell.btnDelete.hidden=YES;
        }
    }
    else
    {
        if ([[NSString stringWithFormat:@"%@",[arrMediaList objectAtIndex:indexPath.row]] isEqualToString:@"Upload"])
        {
            cell.imgThumbNail.image = [UIImage imageNamed:@"upload.png"];
            cell.imgPlay.hidden=YES;
            cell.btnDelete.hidden=YES;
            
        }else  if ([[NSString stringWithFormat:@"%@",[arrMediaList objectAtIndex:indexPath.row]] isEqualToString:@"Plus"])
        {
            cell.imgThumbNail.image = [UIImage imageNamed:@"plus_Build.png"];
            cell.imgPlay.hidden=YES;
            cell.btnDelete.hidden=YES;
        }
        else if ([[arrMediaList objectAtIndex:indexPath.row] isKindOfClass:[UIImage class]])
        {
            cell.imgThumbNail.image= [arrMediaList objectAtIndex:indexPath.row];
            cell.imgPlay.hidden=YES;
            cell.btnDelete.hidden=NO;
        }
        else
        {
            cell.imgPlay.hidden=NO;
            
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[arrMediaList objectAtIndex:indexPath.row] options:nil];
            AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            generator.appliesPreferredTrackTransform = YES;
            NSError *error;
            CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
            if (!error)
            {
                UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    cell.imgThumbNail.image=image;
                    
                });
            }
            
            cell.btnDelete.hidden=NO;
        }
        
        
    }
    
    
    
    cell.imgThumbNail.layer.cornerRadius=3.0;
    cell.imgThumbNail.layer.masksToBounds = YES;
    cell.layer.cornerRadius=3.0;
    cell.layer.masksToBounds = YES;
    
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteVideo:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnDidSelect.tag=indexPath.row;
    [cell.btnDidSelect addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)didSelect:(UIButton *)sender
{
    if ([[NSString stringWithFormat:@"%@",[arrMediaList objectAtIndex:sender.tag]] isEqualToString:@"Plus"])
    {
        [self openGalleryorCamera];
    }
    else if ([[NSString stringWithFormat:@"%@",[arrMediaList objectAtIndex:sender.tag]] isEqualToString:@"Upload"])
    {
        NSLog(@"Upload");
        [self UploadingImage];
    }
    else
    {
        [self settingVideoorImage:sender.tag];
    }
    
    
}

-(void)deleteVideo:(UIButton *)sender
{
    NSLog(@"tag:%ld",(long)sender.tag);
    [arrMediaList removeObject:@"Plus"];
    [arrMediaList removeObject:@"Upload"];
    NSString * key = [NSString stringWithFormat:@"%d",(sender.tag-[[_arrSelected valueForKeyPath:@"jobmedialist"] count])+1];
    if ([[arrMediaList objectAtIndex:sender.tag] isKindOfClass:[UIImage class]])
    {
        key = [NSString stringWithFormat:@"Image%@",key];
    }
    else
    {
        key = [NSString stringWithFormat:@"Video%@",key];
    }
    [dictMedia removeObjectForKey:key];
    [arrMediaList removeObjectAtIndex:sender.tag];
    [self.collectionView reloadData];
}


- (IBAction)btnPlayTapped:(id)sender
{
    [self hideDropDown:sender];
    [self rel];
    if (selectedIndex<[[_arrSelected valueForKeyPath:@"jobmedialist"] count])
    {
        if ([[[arrMediaList objectAtIndex:selectedIndex] valueForKey:@"mediaType"] isEqualToString:@"1"])
        {
            if (isClicked==NO)
            {
                isClicked=YES;
                _viewMedia.hidden = NO;
                _viewBidding.hidden = NO;
                _viewMedia.alpha = 0.1;
                _viewBidding.alpha = 0.1;
                [UIView animateWithDuration:0.25 animations:^{
                    _viewMedia.alpha = 1.0f;
                    _viewBidding.alpha = 1.0f;
                    if (IS_IPAD)
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-130, 768, 130);
                        _viewBidding.frame =  CGRectMake(-15, 15, 115, 30);
                    }
                    else
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-50, 320, 50);
                        _viewBidding.frame =  CGRectMake(-15, 15, 103, 22);
                    }
                    
                } completion:^(BOOL finished) {
                    
                }];
            }
            else
            {
                isClicked=NO;
                
                [UIView animateWithDuration:0.25 animations:^{
                    if (IS_IPAD)
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                        _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
                    }
                    else
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                        _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
                    }
                    
                    
                    [_viewBidding setAlpha:0.1f];
                    [_viewMedia setAlpha:0.1f];
                } completion:^(BOOL finished) {
                    _viewMedia.hidden = YES;
                    _viewBidding.hidden = YES;
                }];
                
            }
        }
        else if ([[[arrMediaList objectAtIndex:selectedIndex] valueForKey:@"mediaType"] isEqualToString:@"2"])
        {
            _imgPlay.hidden=YES;
            
            isClicked=NO;
            
            [UIView animateWithDuration:0.25 animations:^{
                if (IS_IPAD)
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
                }
                else
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
                }
                
                [_viewBidding setAlpha:0.1f];
                [_viewMedia setAlpha:0.1f];
            } completion:^(BOOL finished) {
                _viewMedia.hidden = YES;
                _viewBidding.hidden = YES;
            }];
            
            //create a player
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _imgThumbNail.frame.size.height)];
            }
            else if (IS_IPHONE5)
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
            }
            else
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
            }
            
            self.moviePlayer.view.alpha = 0.f;
            self.moviePlayer.delegate = self; //IMPORTANT!
            
            //create the controls
            ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleDefault];
            movieControls.barHeight=IS_IPAD?130.0f:50.0f;
            //    [movieControls setAdjustsFullscreenImage:YES];
            [movieControls setBarColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
            [movieControls setTimeRemainingDecrements:YES];
            //[movieControls setFadeDelay:2.0];
            //[movieControls setBarHeight:100.f];
            //[movieControls setSeekRate:2.f];
            
            //assign controls
            [self.moviePlayer setControls:movieControls];
            [self.viewMediaPreview addSubview:self.moviePlayer.view];
            
            //THEN set contentURL
            
            NSURL *videoURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrMediaList objectAtIndex:selectedIndex] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [self.moviePlayer setContentURL:videoURL];
            
            
            
            //delay initial load so statusBarOrientation returns correct value
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //        [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
                [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
                    self.moviePlayer.view.alpha = 1.f;
                } completion:^(BOOL finished) {
                    self.navigationItem.leftBarButtonItem.enabled = YES;
                    self.navigationItem.rightBarButtonItem.enabled = YES;
                }];
            });
            
            
            
        }
    }
    else
    {
        if ([[arrMediaList objectAtIndex:selectedIndex] isKindOfClass:[UIImage class]])
        {
            if (isClicked==NO)
            {
                isClicked=YES;
                _viewMedia.hidden = NO;
                _viewBidding.hidden = NO;
                _viewMedia.alpha = 0.1;
                _viewBidding.alpha = 0.1;
                [UIView animateWithDuration:0.25 animations:^{
                    _viewMedia.alpha = 1.0f;
                    _viewBidding.alpha = 1.0f;
                    if (IS_IPAD)
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-130, 768, 130);
                        _viewBidding.frame =  CGRectMake(-15, 15, 115, 30);
                    }
                    else
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-50, 320, 50);
                        _viewBidding.frame =  CGRectMake(-15, 15, 103, 22);
                    }
                    
                } completion:^(BOOL finished) {
                    
                }];
            }
            else
            {
                isClicked=NO;
                
                [UIView animateWithDuration:0.25 animations:^{
                    if (IS_IPAD)
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                        _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
                    }
                    else
                    {
                        _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                        _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
                    }
                    
                    
                    [_viewBidding setAlpha:0.1f];
                    [_viewMedia setAlpha:0.1f];
                } completion:^(BOOL finished) {
                    _viewMedia.hidden = YES;
                    _viewBidding.hidden = YES;
                }];
                
            }
        }
        else
        {
            _imgPlay.hidden=YES;
            
            isClicked=NO;
            
            [UIView animateWithDuration:0.25 animations:^{
                if (IS_IPAD)
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
                }
                else
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
                }
                
                [_viewBidding setAlpha:0.1f];
                [_viewMedia setAlpha:0.1f];
            } completion:^(BOOL finished) {
                _viewMedia.hidden = YES;
                _viewBidding.hidden = YES;
            }];
            
            //create a player
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _imgThumbNail.frame.size.height)];
            }
            else if (IS_IPHONE5)
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
            }
            else
            {
                self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
            }
            
            self.moviePlayer.view.alpha = 0.f;
            self.moviePlayer.delegate = self; //IMPORTANT!
            
            //create the controls
            ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleDefault];
            movieControls.barHeight=IS_IPAD?130.0f:50.0f;
            //    [movieControls setAdjustsFullscreenImage:YES];
            [movieControls setBarColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
            [movieControls setTimeRemainingDecrements:YES];
            //[movieControls setFadeDelay:2.0];
            //[movieControls setBarHeight:100.f];
            //[movieControls setSeekRate:2.f];
            
            //assign controls
            [self.moviePlayer setControls:movieControls];
            [self.viewMediaPreview addSubview:self.moviePlayer.view];
            
            //THEN set contentURL
            
            NSURL *videoURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@",[arrMediaList objectAtIndex:selectedIndex]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [self.moviePlayer setContentURL:videoURL];
            
            
            
            //delay initial load so statusBarOrientation returns correct value
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //        [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
                [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
                    self.moviePlayer.view.alpha = 1.f;
                } completion:^(BOOL finished) {
                    self.navigationItem.leftBarButtonItem.enabled = YES;
                    self.navigationItem.rightBarButtonItem.enabled = YES;
                }];
            });
            
            
            
        }
    }
}
- (void)configureViewForOrientation:(UIInterfaceOrientation)orientation {
    CGFloat videoWidth = 0;
    CGFloat videoHeight = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        videoWidth = 700.f;
        videoHeight = 535.f;
    } else {
        videoWidth = self.view.frame.size.width;
        videoHeight = _imgThumbNail.frame.size.height;
    }
    
    //calulate the frame on every rotation, so when we're returning from fullscreen mode we'll know where to position the movie plauyer
    self.defaultFrame = CGRectMake(self.view.frame.size.width/2 - videoWidth/2, self.view.frame.size.height/2 - videoHeight/2, videoWidth, videoHeight);
    
    //only manage the movie player frame when it's not in fullscreen. when in fullscreen, the frame is automatically managed
    if (self.moviePlayer.isFullscreen)
        return;
    
    //you MUST use [ALMoviePlayerController setFrame:] to adjust frame, NOT [ALMoviePlayerController.view setFrame:]
    [self.moviePlayer setFrame:self.defaultFrame];
}
//IMPORTANT!
- (void)moviePlayerWillMoveFromWindow {
    //movie player must be readded to this view upon exiting fullscreen mode.
    if (![self.view.subviews containsObject:self.moviePlayer.view])
        [self.view addSubview:self.moviePlayer.view];
    
    //you MUST use [ALMoviePlayerController setFrame:] to adjust frame, NOT [ALMoviePlayerController.view setFrame:]
    [self.moviePlayer setFrame:self.defaultFrame];
}

- (void)movieTimedOut {
    NSLog(@"MOVIE TIMED OUT");
}
- (BOOL)shouldAutorotate {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self configureViewForOrientation:toInterfaceOrientation];
}

- (void)movieFinishedNotification:(NSNotification *)note {
    [self.moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    isClicked=YES;
    _viewMedia.hidden = NO;
    _viewBidding.hidden = NO;
    _viewMedia.alpha = 0.1;
    _viewBidding.alpha = 0.1;
    [UIView animateWithDuration:0.25 animations:^{
        _viewMedia.alpha = 1.0f;
        _viewBidding.alpha = 1.0f;
        if (IS_IPAD)
        {
            _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-130, 768, 130);
            _viewBidding.frame =  CGRectMake(-15, 15, 115, 30);
        }
        else
        {
            _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-50, 320, 50);
            _viewBidding.frame =  CGRectMake(-15, 15, 103, 22);
        }
    } completion:^(BOOL finished) {
        [self settingVideoorImage:selectedIndex];
    }];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"Finished" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"NotificationReload" object:nil];
}
- (IBAction)btnHomeTapped:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"";
    
}

- (IBAction)btnBiddingTapped:(id)sender
{
    if(self.dropDown == nil)
    {
        CGFloat f = 40;
        self.dropDown = [[UIView alloc]init];
        NSArray *arr =[[NSArray alloc]initWithObjects:@"Cancel", nil];
        [self showDropDown:sender andHeight:&f theContentArr:arr theDiection:@"down"];
        //            [self.mapView bringSubviewToFront:_dropDown];
        
    }
    else {
        [self hideDropDown:sender];
        [self rel];
    }
}

#pragma mark DropDown

- (void)showDropDown:(UIButton *)b andHeight:(CGFloat *)height theContentArr:(NSArray *)arr theDiection:(NSString *)direction
{
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    
    // Initialization code
    CGRect btn = b.superview.frame;
    self.list = [NSArray arrayWithArray:arr];
    NSLog(@"arr:%@",arr);
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, -5);
    }else if ([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    self.dropDown.layer.masksToBounds = NO;
    self.dropDown.layer.cornerRadius = 8;
    self.dropDown.layer.shadowRadius = 5;
    self.dropDown.layer.shadowOpacity = 0.5;
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
    table.delegate = self;
    table.dataSource = self;
    table.layer.cornerRadius = 5;
    table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.separatorColor = [UIColor grayColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
    } else if([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, *height);
    [UIView commitAnimations];
    
    [self.dropDown addSubview:table];
    
    [self.viewMediaPreview addSubview:self.dropDown];
    [table reloadData];
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.superview.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}
-(void)rel
{
    self.dropDown=nil;
}

- (IBAction)btnContractorPressed:(id)sender
{
    self.btnUp.enabled=YES;
    self.btnUpCon.enabled=YES;
    //Action for Discussion Button
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 830);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(752, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(376, 0, 376, 3);
                             
                         }
         
                         completion:nil];
        
    }
    else if (IS_IPHONE5)
    {
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 464);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(300, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(145, 0, 155, 2);
                         }
                         completion:nil];
        
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 390);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(300, 0);
                             [_scrlMsgContractor setContentOffset:pointContent animated:YES];
                             
                             _viewUnderLine.frame=CGRectMake(145, 0, 155, 2);
                         }
                         completion:nil];
        
    }
}

#pragma mark Calendar
-(void)settingTheCalendarView
{
    arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
    
    [_pickerViewAvailableTimings reloadAllComponents];
    self.calendar.delegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.calendar.titleFont =[UIFont fontWithName:FONT_BOLD size:IS_IPAD?26.0f:18.0f];
    self.calendar.dateOfWeekFont =[UIFont fontWithName:FONT_BOLD size:IS_IPAD?22.0f:15.0f];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMMM YYYY"];
    _lblSelectedDate.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:60*60*24]]];
    selectedDate = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    self.calendar.onlyShowCurrentMonth = NO;
    self.calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    self.calendar.backgroundColor=[UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
}

- (void)localeDidChange
{
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    if ([date compare:[NSDate date]]== NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    // TODO: play with the coloring if we want to...
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    if ([self dateIsDisabled:date])
    {
        dateItem.backgroundColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor lightGrayColor];
        dateItem.disable = @"YES";
    }
    else if ([[dateFormat stringFromDate:date] isEqualToString:[dateFormat stringFromDate:selectedDate] ])
    {
        
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if (arr1.count==0&&[self containsKey:[dateFormat stringFromDate:date]]==YES)
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"SELECTED";
        
    }
    else
    {
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if (arr1.count==0&&[self containsKey:[dateFormat stringFromDate:date]]==YES)
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"NO";
    }
    
    
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    if([date compare:selectedDate]!=NSOrderedSame)
    {
        _lblSelectedDate.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
        selectedDate =date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        if ([self containsKey:[dateFormat stringFromDate:selectedDate]]==YES)
        {
            arrAvailableTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
            
        }
        else
        {
            arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
        }
        
        if(arrAvailableTimings.count>0)
        {
            selectedTime = [NSString stringWithFormat:@"%@",arrAvailableTimings[0]];
            [_pickerViewAvailableTimings reloadAllComponents];
            [self.pickerViewAvailableTimings selectRow:0 inComponent:0 animated:NO];
            [self pickerView:self.pickerViewAvailableTimings didSelectRow:0 inComponent:0];
        }
        else
        {
            [_pickerViewAvailableTimings reloadAllComponents];
        }
        
        
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date
{    
    return YES;
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
    CGRect frameOther = _viewOtherContents.frame;
    frameOther.origin.y = IS_IPAD?(self.calendar.frame.size.height+14):self.calendar.frame.size.height;
    _viewOtherContents.frame = frameOther;
    _viewContentCalendar.frame = CGRectMake(_viewContentCalendar.frame.origin.x, _viewContentCalendar.frame.origin.y, _viewContentCalendar.frame.size.width, frameOther.origin.y+frameOther.size.height);
    _scrlCalendar.scrollEnabled=YES;
    _scrlCalendar.contentSize =CGSizeMake(_scrlCalendar.frame.size.width, frameOther.origin.y+frameOther.size.height+55);
    
    NSLog(@"frame_other==>>%f",frameOther.origin.y+frameOther.size.height);
    
}

#pragma matk PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if (arrAvailableTimings.count==0)
    {
        return 1;
    }
    else
    {
        return arrAvailableTimings.count;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (arrAvailableTimings.count==0)
    {
        _btnMeet.hidden = YES;
        return [NSString stringWithFormat:@"%@",@"No Timing Available"];
    }
    else
    {
        _btnMeet.hidden = NO;
        return [NSString stringWithFormat:@"%@",arrAvailableTimings[row]];
    }
    
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (arrAvailableTimings.count!=0)
    {
        NSLog(@" Index of selected color: %li",  (long)row);
        selectedTime = [NSString stringWithFormat:@"%@",arrAvailableTimings[row]];
    }
}


- (IBAction)btnMeetUpPressed:(id)sender
{

    if (selectedDate!=nil&&selectedTime!=nil)
    {
        
               // [self showDropIn:clientToken];
        [self CheckPayableAmountAPI:^(bool result){
            if (result == YES) {
               [self loadcheckbox];
            }
        }];
               
       // [self getClientToken];
//               OostaJobContractorBillingViewController *Obj=[[OostaJobContractorBillingViewController alloc]init];
//               Obj.selecteddate1 = selectedDate;
//               Obj.selectedtime1 = selectedTime;
//               Obj.struserid1 =_strUserId;
//               Obj.strpostid1 =_strJobpostID;
//
//               [self.navigationController pushViewController:Obj animated:YES];
               
                
        
//        [UIView animateWithDuration:0.3 animations:^{
//            _viewCalander.alpha = 0;
//        } completion: ^(BOOL finished)
//         {
//             _viewCalander.hidden = finished;
//             [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
//                 if (resultJobHire == YES)
//                 {
//                     NSLog(@"Hired");
//                 }
//             }];
//         }];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = @"Select date & time";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}

-(void)loadcheckbox{
//    NSNumber *amount1;
//    NSUserDefaults *a = [NSUserDefaults standardUserDefaults];
//    amount1 = [a objectForKey:@"amount1"];
//    NSLog(@"amount :%@",amount1);
    if ([changetext isEqualToString:@"already added"]) {
        NSString *initialamount = @"We will hold your card in our vault, there will not be any charges on the card at this point";
        //    NSString *deduct = @"$ will be deducted";
        //    NSString *amountget = [initialamount stringByAppendingString:[amount1 stringValue]];
        //   NSString *gotcha = [amountget stringByAppendingString:deduct];
        alertinitialstat = [[UIAlertView alloc] initWithTitle:@"OOstajob"
                                                      message:initialamount
                                                     delegate:self cancelButtonTitle:
                            @"Cancel" otherButtonTitles:@"OK", nil];
        self.viewtermsnconditions.backgroundColor = [UIColor clearColor];
        alertinitialstat.delegate = self;
        UIAlertView *viewmain = [[UIAlertView alloc]initWithFrame:CGRectMake(10, 20, 100, 40)];
        [viewmain addSubview:self.viewtermsnconditions];
        /** adding the textfield to the alertView **/
        alertinitialstat.tag = 123;
        [alertinitialstat setValue:viewmain forKey:@"accessoryView"];
        
        [alertinitialstat show];
    }
    else
    {
    NSString *initialamount = @"We will be storing your card details in braintree vault for further payment";
//    NSString *deduct = @"$ will be deducted";
//    NSString *amountget = [initialamount stringByAppendingString:[amount1 stringValue]];
 //   NSString *gotcha = [amountget stringByAppendingString:deduct];
    alertinitialstat = [[UIAlertView alloc] initWithTitle:@"OOstajob"
                                                    message:initialamount
                                                   delegate:self cancelButtonTitle:
                          @"Cancel" otherButtonTitles:@"OK", nil];
    self.viewtermsnconditions.backgroundColor = [UIColor clearColor];
    alertinitialstat.delegate = self;
    UIAlertView *viewmain = [[UIAlertView alloc]initWithFrame:CGRectMake(10, 20, 100, 40)];
    [viewmain addSubview:self.viewtermsnconditions];
    /** adding the textfield to the alertView **/
    alertinitialstat.tag = 123;
    [alertinitialstat setValue:viewmain forKey:@"accessoryView"];

    [alertinitialstat show];
    }
}

- (IBAction)btnSkipPressed:(id)sender
{
    if (selectedDate!=nil&&selectedTime!=nil)
    {
        
        // [self showDropIn:clientToken];
        [self CheckPayableAmountAPI:^(bool result){
            if (result == YES) {
                [self loadcheckbox];
            }
        }];
        
        //               OostaJobContractorBillingViewController *Obj=[[OostaJobContractorBillingViewController alloc]init];
        //               Obj.selecteddate1 = selectedDate;
        //               Obj.selectedtime1 = selectedTime;
        //               Obj.struserid1 =_strUserId;
        //               Obj.strpostid1 =_strJobpostID;
        //
        //               [self.navigationController pushViewController:Obj animated:YES];
        
        
        
        //        [UIView animateWithDuration:0.3 animations:^{
        //            _viewCalander.alpha = 0;
        //        } completion: ^(BOOL finished)
        //         {
        //             _viewCalander.hidden = finished;
        //             [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
        //                 if (resultJobHire == YES)
        //                 {
        //                     NSLog(@"Hired");
        //                 }
        //             }];
        //         }];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = @"Select date & time";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    
}
- (IBAction)btnCalendarClosedTapped:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewCalander.alpha = 0;
    } completion: ^(BOOL finished)
     {
         _viewCalander.hidden = finished;
     }];
}

-(void)goToContractoDetails:(NSMutableArray *)array andSelectedInde:(NSString *)index
{
    
    OostaJobReviewViewController *DetObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        DetObj = [[OostaJobReviewViewController alloc]initWithNibName:@"OostaJobReviewViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        DetObj = [[OostaJobReviewViewController alloc]initWithNibName:@"OostaJobReviewViewController" bundle:nil];
    }
    else
    {
        DetObj = [[OostaJobReviewViewController alloc]initWithNibName:@"OostaJobReviewViewController" bundle:nil];
    }
    DetObj.imgBackGroundImage = _imgBlur.image;
    DetObj.arrSelected = [array mutableCopy];
    DetObj.indexofSelected = index;
    DetObj.strJobpostID = [_arrSelected valueForKeyPath:@"jobpostID"];
    DetObj.ContractorID = [_arrSelected valueForKeyPath:@"ContractorDetails.contractorID"];
    isConctractorClicked = YES;
    [self.navigationController pushViewController:DetObj animated:YES];
    
}
- (IBAction)btnEmailPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        //        [controller.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bg_iPhone.png"] forBarMetrics:UIBarMetricsDefault];
        //        controller.navigationBar.tintColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
        [controller setSubject:@""];
        [controller setMessageBody:@" " isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:strEmail,nil]];
        [self presentViewController:controller animated:YES completion:NULL];
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:@"Cant open mail composer" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
        [alert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    UIAlertView *MailAlert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:nil delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            MailAlert.message = @"Email Cancelled";
            break;
        case MFMailComposeResultSaved:
            MailAlert.message = @"Email Saved";
            break;
        case MFMailComposeResultSent:
            MailAlert.message = @"Email Sent";
            break;
        case MFMailComposeResultFailed:
            MailAlert.message = @"Email Failed";
            break;
        default:
            MailAlert.message = @"Email Not Sent";
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    MailAlert.tag = 1000;
    [MailAlert show];
}

- (IBAction)btnCallPressed:(id)sender
{
    NSString *phoneNumber = strPhone;
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
        [UIApplication.sharedApplication openURL:phoneUrl];
    } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl];
    } else {
        NSLog(@"Cant make call");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:@"Cant make call" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
        [alert show];
    }
}
- (IBAction)btnRatePressed:(id)sender
{
    OostaJobFeedbackViewController *DetObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController" bundle:nil];
    }
    else
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController" bundle:nil];
    }
    DetObj.strJobId = [_arrSelected valueForKeyPath:@"jobpostID"];
    DetObj.arrDetails = [_arrSelected valueForKeyPath:@"ContractorDetails"];
    DetObj.imageBackGround = _imgBlur.image;
    [self.navigationController pushViewController:DetObj animated:YES];
}

#pragma mark Get Calendar
-(void)getCalendarDetailsndCompletionHandler:(void (^)(bool resultCalendarDetails))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading Timings...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcalender/%@",BaseURL,_strUserId]]];
        NSLog(@"getcalender:%@",request.URL);
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     dictSelectedTimings = [[NSMutableDictionary alloc]init];
                     NSMutableArray *arrDate = [[NSMutableArray alloc]init];
                     NSMutableArray *arrTime = [[NSMutableArray alloc]init];
                     arrDate = [result valueForKeyPath:@"CalenderDetails.notAvailabledate"];
                     arrTime = [result valueForKeyPath:@"CalenderDetails.notAvailableTime"];
                     
                     NSMutableArray *arrCustomerDate = [[NSMutableArray alloc]init];
                     NSMutableArray *arrCustomerTime = [[NSMutableArray alloc]init];
                     arrCustomerDate = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailabledate"];
                     arrCustomerTime = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailableTime"];
                     
                     NSMutableArray *arrAvailable =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
                     
                     //Customer time enhancement
                     
                     NSMutableDictionary * dictCustomerSelectedTimings = [[NSMutableDictionary alloc]init];
                     for (int i=0; i<arrCustomerDate.count; i++)
                     {
                         NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                         NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrCustomerTime[i]] componentsSeparatedByString:@","];
                         for (int j=0; j<arrAvailable.count; j++)
                         {
                             if ([arrServiceTimings containsObject:arrAvailable[j]])
                             {
                                 [arrTimings addObject:arrAvailable[j]];
                             }
                             
                         }
                         [dictCustomerSelectedTimings setObject:arrTimings forKey:arrCustomerDate[i]];
                         
                         
                     }
                     
                     NSLog(@"dictCustomerSelectedTimings==>>%@",dictCustomerSelectedTimings);
                     
                     for (int i=0; i<arrDate.count; i++)
                     {
                         NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                         NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrTime[i]] componentsSeparatedByString:@","];
                        
                         
                         for (int j=0; j<arrAvailable.count; j++)
                         {
                             if (![arrServiceTimings containsObject:arrAvailable[j]])
                             {
                                 [arrTimings addObject:arrAvailable[j]];
                             }
                             
                         }
                         [dictSelectedTimings setObject:arrTimings forKey:arrDate[i]];
                         
                     }
                     
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                     [dateFormat setDateFormat:@"YYYY-MM-dd"];
                     
                     for (int i=0; i<[[dictCustomerSelectedTimings allKeys] count]; i++)
                     {
                         if ([self containsKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]]==YES)
                         {
                              NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                             arrTimings = [[dictSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] mutableCopy];
                             
                             for (int j=0; j<[[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] count]; j++)
                             {
                                 [arrTimings removeObject:[[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] objectAtIndex:j]];
                             }
                             
                             [dictSelectedTimings setObject:arrTimings forKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]];
                         }
                         else
                         {
                             NSMutableArray *arrTimings=[[NSMutableArray alloc]init];
                             NSMutableArray * arrServiceTimings = [[NSMutableArray alloc]init];
                             arrServiceTimings = [[dictCustomerSelectedTimings valueForKeyPath:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]] mutableCopy];
                             
                             
                             for (int j=0; j<arrAvailable.count; j++)
                             {
                                 if (![arrServiceTimings containsObject:arrAvailable[j]])
                                 {
                                     [arrTimings addObject:arrAvailable[j]];
                                 }
                                 
                             }
                             [dictSelectedTimings setObject:arrTimings forKey:[[dictCustomerSelectedTimings allKeys] objectAtIndex:i]];
                         }
                     }
                     
                     
                     
                     
                     if ([self containsKey:[dateFormat stringFromDate:selectedDate]]==YES)
                     {
                         arrAvailableTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
                         if(arrAvailableTimings.count>0)
                         {
                             selectedTime = arrAvailableTimings[0];
                         }
                         
                         
                         [_pickerViewAvailableTimings reloadAllComponents];
                     }
                     
                     
                     NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
                     [self.calendar reloadData];
                     completionHandler(true);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewCalander animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(true);
                 }
             }
         }];
    }
}
- (BOOL)containsKey: (NSString *)key {
    BOOL retVal = 0;
    NSArray *allKeys = [dictSelectedTimings allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

#pragma mark Cancel Job

-(void)getCanceltheJobForTheArray:(NSMutableArray *)theArray andCompletionHandler:(void (^)(bool resultCanceltheJob))completionHandler
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Cancelling...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@cancelcustomer/%@/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"],[NSString stringWithFormat:@"%@",[theArray valueForKeyPath:@"jobpostID"]]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"cancelcustomer:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = @"Job Cancelled Successfully";
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
                 if(_delegate && [_delegate respondsToSelector:@selector(getreloadto:)])
                 {
                     [_delegate getreloadto:@"Job bid"];
                 }
                 [self.navigationController popViewControllerAnimated:YES];
                 
                 completionHandler(true);
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
         }
     }];
    
}
-(void)NotifyToWeb
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getnotificationCunt/%@/%@",BaseURL,[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"jobpostID"]],[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getnotificationCunt:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 isNotifyCalled =YES;
                 [badgeView removeFromSuperview];
                 if(_delegate && [_delegate respondsToSelector:@selector(getreloadto:)])
                 {
                     if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"1"])
                     {
                         [_delegate getreloadto:@"Bid"];
                     }
                     else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"2"])
                     {
                         [_delegate getreloadto:@"Hire"];
                     }
                     else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"customerJobStatus"]] isEqualToString:@"3"])
                     {
                         [_delegate getreloadto:@"Rate"];
                     }
                     else
                     {
                         [_delegate getreloadto:@"Closed"];
                     }
                 }
                 
             }
             else
             {
                 
                 
             }
         }
     }];
    
}
- (void)NotificationReload:(NSNotification *)note
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getforumlist/%@",BaseURL,[note.object valueForKeyPath:@"jobID"]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    NSLog(@"GetJob Discussion List URL:%@",request.URL);
    
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             _lblDiscussion.text = [NSString stringWithFormat:@"Messages"];
             self.lblMesseges.text=[NSString stringWithFormat:@"Messages"];
             self.lblContractors.text=[NSString stringWithFormat:@"Contractors"];
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 self.resultJobDiscussionAry = [[NSMutableArray alloc]init];
                 self.resultJobDiscussionAry=[result valueForKeyPath:@"DiscustionDetails"];
                 NSLog(@"Value is:%@",self.resultDiscussionAry);
                 
                 for (int i=0; i<[[result valueForKeyPath:@"DiscustionDetails"] count]; i++)
                 {
                     
                     _lblDiscussion.text = [NSString stringWithFormat:@"Messages (%d)",i+1];
                     self.lblMesseges.text = [NSString stringWithFormat:@"Messages (%d)",i+1];
                 }
                 
                 
                 [_tblChatMsgs reloadData];
                 [self tableViewScrollToLastRow];
                 selectedReplyIndexPath = nil;
                 
                 
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 _lblDiscussion.text = [NSString stringWithFormat:@"Message"];
                 
                 self.lblMesseges.text = [NSString stringWithFormat:@"Message"];
                 
             }
             
             
         }
     }];
    
}
-(void)tableViewScrollToLastRow
{
    if (_tblChatMsgs.contentSize.height > _tblChatMsgs.frame.size.height)
    {
        CGPoint offset = CGPointMake(0, _tblChatMsgs.contentSize.height -     _tblChatMsgs.frame.size.height);
        [_tblChatMsgs setContentOffset:offset animated:YES];
    }
}

-(void)openGalleryorCamera
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Photo Album", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}
#pragma mark Camera
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)])
    {
        [self showCamera];
    }
}
- (void)showCamera
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
        [picker setVideoMaximumDuration:120.0f];
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    
}

- (void)openPhotoAlbum
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
        [controller setVideoMaximumDuration:120.0f];
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
    
}
- (ALAssetsLibrary *)assetsLibrary
{
    if (assetsLibrary_) {
        return assetsLibrary_;
    }
    assetsLibrary_ = [[ALAssetsLibrary alloc] init];
    return assetsLibrary_;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    mediaCount = mediaCount+1;
    int videoCount = 0;
    int ImageCount = 0;
    for (int i=0; i<[[_arrSelected valueForKeyPath:@"jobmedialist"] count]; i++)
    {
        if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKey:@"mediaType"] isEqualToString:@"1"])
        {
            ImageCount++;
        }
        else
        {
            videoCount++;
        }
    }
    
    // Manage the media (photo)
    if (dictMedia.count<25-[[_arrSelected valueForKeyPath:@"jobmedialist"] count])
    {
        
        NSString * mediaType = info[UIImagePickerControllerMediaType];
        
        
        if([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage])
        {
            NSLog(@"Image");
            int totalImage=0;
            for (int i=0; i<dictMedia.count; i++)
            {
                if([[[dictMedia allKeys] objectAtIndex:i] rangeOfString:@"Image"].length!=0)
                {
                    totalImage=totalImage+1;
                }
            }
            
            if (totalImage<20-ImageCount)
            {
                // Manage tasks in background thread
                if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
                {
                    
                    UIImage * editedImage = (UIImage *)info[UIImagePickerControllerEditedImage];
                    UIImage * imageToSave = (editedImage ?: (UIImage *)info[UIImagePickerControllerOriginalImage]);
                    
                    UIImage * finalImageToSave = nil;
                    /* Modify image's size before save it to photos album
                     *
                     *  CGSize sizeToSave = CGSizeMake(imageToSave.size.width, imageToSave.size.height);
                     *  UIGraphicsBeginImageContextWithOptions(sizeToSave, NO, 0.f);
                     *  [imageToSave drawInRect:CGRectMake(0.f, 0.f, sizeToSave.width, sizeToSave.height)];
                     *  finalImageToSave = UIGraphicsGetImageFromCurrentImageContext();
                     *  UIGraphicsEndImageContext();
                     */
                    finalImageToSave = imageToSave;
                    
                    
                    void (^completion)(NSURL *, NSError *) = ^(NSURL *assetURL, NSError *error) {
                        if (error) {
                            NSLog(@"%s: Write the image data to the assets library (camera roll): %@",
                                  __PRETTY_FUNCTION__, [error localizedDescription]);
                        }
                        
                        NSLog(@"%s: Save image with asset url %@ (absolute path: %@), type: %@", __PRETTY_FUNCTION__,
                              assetURL, [assetURL absoluteString], [assetURL class]);
                        
                        [dictMedia setObject:finalImageToSave forKey:[NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount]];
                        strLastKey = [NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount];
                        [self reloadtheCollectionView];
                        
                    };
                    
                    void (^failure)(NSError *) = ^(NSError *error) {
                        if (error) NSLog(@"%s: Failed to add the asset to the custom photo album: %@",
                                         __PRETTY_FUNCTION__, [error localizedDescription]);
                    };
                    
                    
                    [self.assetsLibrary saveImage:finalImageToSave
                                          toAlbum:@"OOstaJob"
                                       completion:completion
                                          failure:failure];
                    
                }
                else
                {
                    UIImage * editedImage = (UIImage *)info[UIImagePickerControllerEditedImage];
                    UIImage * imageToSave = (editedImage ?: (UIImage *)info[UIImagePickerControllerOriginalImage]);
                    [dictMedia setObject:imageToSave forKey:[NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount]];
                    strLastKey = [NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount];
                    [self reloadtheCollectionView];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                message:@"Image limit is 20"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            NSLog(@"Video");
            int totalVideo=0;
            for (int i=0; i<dictMedia.count; i++)
            {
                if([[[dictMedia allKeys] objectAtIndex:i] rangeOfString:@"Video"].length!=0)
                {
                    totalVideo=totalVideo+1;
                }
            }
            if (totalVideo<5-videoCount)
            {
                if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
                {
                    
                    NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
                    
                    
                    NSString *albumName=@"OOstaJob";
                    [self.assetsLibrary saveVideo:videoUrl toAlbum:albumName completion:^(NSURL *assetURL, NSError *error)
                     {
                         if (!error)
                         {
                             NSLog(@"Success");
                             [dictMedia setObject:videoUrl forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                             strLastKey = [NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount];
                             [self reloadtheCollectionView];
                         }
                         
                     } failure:^(NSError *error) {
                         NSLog(@"Failed");
                         
                     }];
                }
                else
                {
                    
                    NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
                    AVURLAsset *avUrl = [AVURLAsset assetWithURL:videoUrl];
                    CMTime time = [avUrl duration];
                    int seconds = ceil(time.value/time.timescale);
                    
                    if (seconds<=120&&videoUrl!=nil)
                    {
                        [dictMedia setObject:videoUrl forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                        strLastKey = [NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount];
                        [self reloadtheCollectionView];
                    }
                    else
                    {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                        message:@"Video length should less than 2.00 minute"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        /**** Trim ****/
                        
                        
                        /* AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
                         AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
                         
                         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                         NSString *outputURL = paths[0];
                         NSFileManager *manager = [NSFileManager defaultManager];
                         [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
                         NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                         [dateFormatter setDateFormat:@"dd-MM-YYYY"];
                         int random = arc4random() % 9999999;
                         NSString * randomName;
                         randomName=[NSString stringWithFormat:@"%@-%d-Video.MOV",[dateFormatter stringFromDate:[NSDate date]],random];
                         
                         outputURL = [outputURL stringByAppendingPathComponent:randomName];
                         // Remove Existing File
                         [manager removeItemAtPath:outputURL error:nil];
                         
                         
                         exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
                         exportSession.shouldOptimizeForNetworkUse = YES;
                         exportSession.outputFileType = AVFileTypeQuickTimeMovie;
                         CMTime start = CMTimeMakeWithSeconds(1.0, 600); // you will modify time range here
                         CMTime duration = CMTimeMakeWithSeconds(120.0, 600);
                         CMTimeRange range = CMTimeRangeMake(start, duration);
                         exportSession.timeRange = range;
                         [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
                         {
                         switch (exportSession.status) {
                         case AVAssetExportSessionStatusCompleted:
                         [dictMedia setObject:[NSURL fileURLWithPath:outputURL] forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                         [self reloadtheCollectionView];
                         NSLog(@"Export Complete %ld %@", (long)exportSession.status, exportSession.error);
                         break;
                         case AVAssetExportSessionStatusFailed:
                         NSLog(@"Failed:%@",exportSession.error);
                         break;
                         case AVAssetExportSessionStatusCancelled:
                         NSLog(@"Canceled:%@",exportSession.error);
                         break;
                         default:
                         break;
                         }
                         
                         //[exportSession release];
                         }];*/
                    }
                    
                    
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                message:@"Video limit is 5"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"Maximum limit reached"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)cropVideo:(NSURL*)videoToTrimURL
{
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoToTrimURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *outputURL = paths[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    int random = arc4random() % 9999999;
    NSString * randomName;
    randomName=[NSString stringWithFormat:@"%@-%d-Video.MOV",[dateFormatter stringFromDate:[NSDate date]],random];
    
    outputURL = [outputURL stringByAppendingPathComponent:randomName];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    
    exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    exportSession.shouldOptimizeForNetworkUse = YES;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    CMTime start = CMTimeMakeWithSeconds(1.0, 600); // you will modify time range here
    CMTime duration = CMTimeMakeWithSeconds(120.0, 600);
    CMTimeRange range = CMTimeRangeMake(start, duration);
    exportSession.timeRange = range;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCompleted:
                 [dictMedia setObject:[NSURL fileURLWithPath:outputURL] forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                 [self reloadtheCollectionView];
                 NSLog(@"Export Complete %ld %@", (long)exportSession.status, exportSession.error);
                 break;
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"Failed:%@",exportSession.error);
                 break;
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"Canceled:%@",exportSession.error);
                 break;
             default:
                 break;
         }
         
         //[exportSession release];
     }];
}


-(void)reloadtheCollectionView
{
    [arrMediaList removeObject:@"Plus"];
    [arrMediaList removeObject:@"Upload"];
    
    [arrMediaList addObject:[dictMedia valueForKey:strLastKey]];
    [self.collectionView reloadData];
    
    NSInteger section = [self.collectionView numberOfSections] - 1;
    NSInteger item = [self.collectionView numberOfItemsInSection:section] - 1;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionLeft) animated:YES];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)UploadingImage
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Uploading...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    NSString *URLString =[NSString stringWithFormat:@"%@postjobimgupload",BaseURL];
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [[dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrDictKey];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [sortedArray mutableCopy];
    for (int i=0; i<arrDictKey.count; i++)
    {
        if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-YYYY"];
            int random = arc4random() % 9999999;
            NSString * randomName;
            randomName=[NSString stringWithFormat:@"%@-%d-Photo.png",[dateFormatter stringFromDate:[NSDate date]],random];
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
            randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSLog(@" Name %@",randomName);
            
            [parameters setObject:randomName forKey:[NSString stringWithFormat:@"postjob%d",i+1]];
            
            
        }
        else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-YYYY"];
            int random = arc4random() % 9999999;
            NSString * randomName;
            randomName=[NSString stringWithFormat:@"%@-%d-Video.mp4",[dateFormatter stringFromDate:[NSDate date]],random];
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
            randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSLog(@" Name %@",randomName);
            
            [parameters setObject:randomName forKey:[NSString stringWithFormat:@"postjob%d",i+1]];
        }
    }
    
    NSLog(@"parameters:%@",parameters);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                                                          {
                                                                              for (int i=0; i<arrDictKey.count; i++)
                                                                              {
                                                                                  
                                                                                  if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
                                                                                  {
                                                                                      NSLog(@"fileName:%@",[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]);
                                                                                      [formData appendPartWithFileData:UIImagePNGRepresentation([UIImage compressImage:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]
                                                                                                                                                         compressRatio:0.7f]) name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"image/png"];
                                                                                  }
                                                                                  else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
                                                                                  {
                                                                                      NSURL *uploadURL = [NSURL fileURLWithPath:[[NSTemporaryDirectory() stringByAppendingPathComponent:[self createRandomName]] stringByAppendingString:@".mp4"]];
                                                                                      /*[self convertVideoToLowQuailtyWithInputURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]] outputURL:uploadURL andCompletionHandler:^(bool result) {
                                                                                       if (result==YES)
                                                                                       {
                                                                                       NSData *dataVideo=[NSData dataWithContentsOfURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]];
                                                                                       [formData appendPartWithFileData:dataVideo name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"video/mp4"];
                                                                                       }
                                                                                       }];*/
                                                                                      NSData *dataVideo=[NSData dataWithContentsOfURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]];
                                                                                      [formData appendPartWithFileData:dataVideo name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"video/mp4"];
                                                                                      
                                                                                      
                                                                                      
                                                                                  }
                                                                                  
                                                                              }
                                                                          }
                                                                               success:^(AFHTTPRequestOperation *operation, id responseObject)
                                                                          {
                                                                              NSLog(@"Success %@", responseObject);
                                                                              
                                                                              [hud hide:YES];
                                                                              [self callAddImageVideoPostjobFortheContent:parameters andCompletionHandler:^(bool result) {
                                                                                  
                                                                                  if (result==YES)
                                                                                  {
                                                                                      [hud hide:YES];
                                                                                      
                                                                                  }
                                                                              }];
                                                                              
                                                                              
                                                                          }
                                                                               failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                                                          {
                                                                              [hud hide:YES];
                                                                              MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                              
                                                                              hud.mode = MBProgressHUDModeText;
                                                                              hud.color=[UIColor whiteColor];
                                                                              hud.labelColor=kMBProgressHUDLabelColor;
                                                                              hud.backgroundColor=kMBProgressHUDBackgroundColor;
                                                                              
                                                                              hud.labelText = SERVER_ERR;
                                                                              hud.margin = 10.f;
                                                                              hud.yOffset = 20.f;
                                                                              hud.removeFromSuperViewOnHide = YES;
                                                                              [hud hide:YES afterDelay:2];
                                                                          }];
    
    
}

-(NSString *)createRandomName
{
    NSTimeInterval timeStamp = [ [ NSDate date ] timeIntervalSince1970 ];
    NSString *randomName = [ NSString stringWithFormat:@"M%f", timeStamp];
    randomName = [ randomName stringByReplacingOccurrencesOfString:@"." withString:@"" ];
    return randomName;
}

-(void)callAddImageVideoPostjobFortheContent:(NSMutableDictionary *)dictParameters andCompletionHandler:(void (^)(bool result))completionHandler
{
    
    NSString * key1 = @"userID";
    NSString * obj1 = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
    

    NSString * key2 = @"jobID";
    NSString * obj2 = [_arrSelected valueForKeyPath:@"jobpostID"];
    
    
    
    NSMutableArray *arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [[dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrDictKey];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [sortedArray mutableCopy];
    NSMutableArray *arrPostMedia=[[NSMutableArray alloc]init];
    for (int i=0; i<arrDictKey.count; i++)
    {
        NSMutableDictionary *dictM=[[NSMutableDictionary alloc]init];
        if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
        {
            [dictM setObject:@"1" forKey:@"mediaType"];
            [dictM setObject:[NSString stringWithFormat:@"%@",[dictParameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]] forKey:@"mediaContent"];
        }
        else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
        {
            [dictM setObject:@"2" forKey:@"mediaType"];
            [dictM setObject:[NSString stringWithFormat:@"%@",[dictParameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]] forKey:@"mediaContent"];
        }
        [arrPostMedia addObject:dictM];
    }
    
    
    NSData * JsonDataMedia =[NSJSONSerialization dataWithJSONObject:arrPostMedia options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonStringMedia= [[NSString alloc] initWithData:JsonDataMedia encoding:NSUTF8StringEncoding];
    NSData *dataMedia = [jsonStringMedia dataUsingEncoding:NSUTF8StringEncoding];
    id jsonMedia = [NSJSONSerialization JSONObjectWithData:dataMedia options:0 error:nil];
    
    NSString * key3 =@"media";
    NSString * obj3 =jsonMedia;
    
    
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3]
                                   forKeys:@[key1,key2,key3]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@AddImageVideoPostjob",BaseURL]]];
    NSLog(@"AddImageVideoPostjob URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Done";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 arrMediaList =[[NSMutableArray alloc]init];
                 dictMedia = [[NSMutableDictionary alloc]init];
                 mediaCount = 0;
                 //    arrMediaList = [_arrSelected valueForKeyPath:@"jobmedialist"];
                 NSMutableDictionary * dictTemp = [[NSMutableDictionary alloc]init];
                 dictTemp = (NSMutableDictionary *) _arrSelected;
                 [dictTemp setObject:[result valueForKeyPath:@"MediaDetails"] forKey:@"jobmedialist"];
                 _arrSelected = [[NSMutableArray alloc]init];
                 _arrSelected = (NSMutableArray *)dictTemp;
                 
                 for (int i=0; i<[[_arrSelected valueForKeyPath:@"jobmedialist"] count]; i++)
                 {
                     [arrMediaList addObject:[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:i]];
                 }
                 [self.collectionView reloadData];
                 if(_delegate && [_delegate respondsToSelector:@selector(getreloadto:)])
                 {
                     [_delegate getreloadto:@"Job bid"];
                 }
                 
                 completionHandler(true);
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
         }
     }];
}

- (IBAction)backButtonForCalendarSkip:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}

//- (void)showDropIn:(NSString *)clientToken {
//    BTDropInRequest *request = [[BTDropInRequest alloc] init];
//    request.paypalDisabled = YES;
//    request.applePayDisabled = YES;
//    request.venmoDisabled = YES;
//    request.threeDSecureVerification = YES;
//    request.amount = @"1.00";
//    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:clientToken request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
//        NSLog(@"result grop in :%@",result);
//        if (error != nil) {
//            NSLog(@"ERROR");
//        } else if (result.cancelled) {
//            NSLog(@"CANCELLED");
//            [self dismissViewControllerAnimated:YES completion:nil];
//        } else {
//            // Use the BTDropInResult properties to update your UI
//            //result.paymentOptionType
//            // result.paymentMethod
//            NSLog(@"Result payment method nonce :%@",result.paymentMethod.nonce);
//            // result.paymentIcon
//            // result.paymentDescription
//        }
//    }];
//    [self presentViewController:dropIn animated:YES completion:nil];
//}
//+ (void)fetchExistingPaymentMethod:(NSString *)clientToken {
//    [BTDropInResult fetchDropInResultForAuthorization:clientToken handler:^(BTDropInResult * _Nullable result, NSError * _Nullable error) {
//        if (error != nil) {
//            NSLog(@"ERROR");
//        } else {
//            // Use the BTDropInResult properties to update your UI
//            NSLog(@"Payment method%@", result.paymentMethod);
//            NSLog(@"Payment Description :%@", result.paymentDescription);
//            NSLog(@"Payment option type :%ld", (long)result.paymentOptionType);
//        }
//    }];
//}


- (IBAction)Onclickcheckbox:(id)sender {
    UIButton *btn = (UIButton *)sender;
    val = 0;
    if ([UIImagePNGRepresentation(btn.currentBackgroundImage) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"])]){
        val = 0;
        [btn setBackgroundImage:[UIImage imageNamed:@"uncheck-40.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        val = 1;
        
        
    }
}
- (IBAction)Onclickwrong:(id)sender {
    [popupTermsAndConditions dismissPresentingPopup];
    [alertinitialstat show];


}

-(IBAction)btnTermsandConditionTapped:(id)sender{
    [alertinitialstat dismissWithClickedButtonIndex:0 animated:YES];

    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    popupTermsAndConditions = [KLCPopup popupWithContentView:_viewTermsAndConditions
                                                    showType:KLCPopupShowTypeFadeIn
                                                 dismissType:KLCPopupDismissTypeFadeOut
                                                    maskType:KLCPopupMaskTypeDimmed
                                    dismissOnBackgroundTouch:NO
                                       dismissOnContentTouch:NO];
    [popupTermsAndConditions showWithLayout:layout];
    
    //HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.viewTermsAndConditions];
    [self.viewTermsAndConditions addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading...";
    
    //LOAD URL
    NSString* url = [[NSUserDefaults standardUserDefaults] objectForKey:@"Customerpaymentsterms"];
    NSLog(@"url:%@",url);
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    [_webViewTermsAndConditions loadRequest:request];
}

/// Brain tree card save

-(void)getClientToken
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Getting Client Token...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
        
        NSString *key1 = @"braintreeID";
        NSString *obj1 = [brain objectForKey:@"BrainTreeid"];
        
        NSLog(@"Brain tree :%@",[brain objectForKey:@"BrainTreeid"]);
        
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getclientaccesstoken",BaseURL]]];
        NSLog(@"getclientaccesstoken:%@",request.URL);
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"result"] isEqualToString:@"Success"])
                 {
                     
                     [hud hide:YES];
                     if (!TARGET_IPHONE_SIMULATOR)
                         self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]]];
                     strClientToken = [NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]];
                     
                     [self showDropIn:strClientToken];
                     //[self fetchExistingPaymentMethod:strClientToken];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
         }];
    }
}

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewController:(BTDropInViewController *)viewController
  didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce
{
    // Send payment method nonce to your server for processing
    NSLog(@"paymentMethodNonce:%@",paymentMethodNonce.nonce);
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showDropIn:(NSString *)clientTokenOrTokenizationKey {
    BTDropInRequest *request = [[BTDropInRequest alloc] init];
    request.applePayDisabled = YES;
    
    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:clientTokenOrTokenizationKey request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"ERROR");
        } else if (result.cancelled) {
            [self dismissViewControllerAnimated:YES completion:nil];
            NSLog(@"CANCELLED");
        } else {
            // Use the BTDropInResult properties to update your UI
             //result.paymentOptionType
             //result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
            NSLog(@"result in drop in :%@",result);
                         [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
                             if (resultJobHire == YES)
                             {
                                 NSLog(@"Hired");
                                
                             }
                         }];
            [self dismissViewControllerAnimated:YES completion:nil];

        }

    }];
    [self presentViewController:dropIn animated:YES completion:nil];
}


- (void)fetchExistingPaymentMethod:(NSString *)clientToken {
    [BTDropInResult fetchDropInResultForAuthorization:clientToken handler:^(BTDropInResult * _Nullable result, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"ERROR");
            
        } else {
            // Use the BTDropInResult properties to update your UI
            // result.paymentOptionType
            // result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
            NSLog(@"Already present%@",result.paymentMethod);
            NSLog(@"Already present%ld",(long)result.paymentOptionType);
           

            if(result.paymentMethod == NULL)
            {
                NSLog(@"enter null");
            }
            else
            {
                changetext = @"already added";

                NSLog(@"dont enter null");

            }
        }
    }];
}


-(void)getClientTokenalreadycheck
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
        
        NSString *key1 = @"braintreeID";
        NSString *obj1 = [brain objectForKey:@"BrainTreeid"];
        
        NSLog(@"Brain tree :%@",[brain objectForKey:@"BrainTreeid"]);
        
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getclientaccesstoken",BaseURL]]];
        NSLog(@"getclientaccesstoken:%@",request.URL);
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"result"] isEqualToString:@"Success"])
                 {
                     
                     [hud hide:YES];
                     if (!TARGET_IPHONE_SIMULATOR)
                         self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]]];
                     strClientToken = [NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]];
                     
                    // [self showDropIn:strClientToken];
                     [self fetchExistingPaymentMethod:strClientToken];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
         }];
    }
}




@end
