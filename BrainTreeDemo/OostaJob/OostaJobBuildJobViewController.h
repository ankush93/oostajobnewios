//
//  OostaJobBuildJobViewController.h
//  OostaJob
//
//  Created by Apple on 11/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OostaJobDescriptionViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "CustomerRearViewController.h"
@interface OostaJobBuildJobViewController : UIViewController<OostaJobDescriptionViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *JobSubTypeArr;
    NSMutableArray *arrCharacteristics;
}

@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;

@property (strong, nonatomic) IBOutlet UILabel *lblJobType;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrlView;
@property (strong, nonatomic) IBOutlet UIButton *PostBtn;
@property (strong, nonatomic) IBOutlet UIView *PopupView;
@property (strong, nonatomic) IBOutlet UIView *PopupContent;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblNotice;
@property (strong, nonatomic) IBOutlet UILabel *lblDetails;
@property (strong, nonatomic) IBOutlet UIButton *BtnWhatNext;

@property (strong, nonatomic) IBOutlet UILabel *lblViewCatagory;
@property (strong, nonatomic) IBOutlet UITableView *tblCaragoryList;
@property (strong, nonatomic) NSMutableArray *arrSelectedJob;
@property (strong, nonatomic) NSString *strOther;
@property (strong, nonatomic) NSString *strBuildJob;
//Actions

-(IBAction)PostJobBtnTouched:(id)sender;
-(IBAction)BackBtnTouched:(id)sender;
-(IBAction)HomeBtnTouched:(id)sender;
-(IBAction)CloseBtnTouched:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIView *viewLblHolder;
@property (strong, nonatomic) IBOutlet UIView *viewPopupContent;

@property (strong, atomic) ALAssetsLibrary* library;
@property (weak, nonatomic) IBOutlet UIView *viewRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)btnLoginTapped:(id)sender;
- (IBAction)btnRegisterTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlPopupContents;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@end
