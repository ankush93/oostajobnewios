//
//  OostaJobContractorBidNowTableViewCell.h
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface OostaJobContractorBidNowTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContentAll;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispName;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispTime;
@property(weak,nonatomic) IBOutlet UILabel *lblTblDispMsg;
@property(weak,nonatomic) IBOutlet AsyncImageView *imgTbl;
@property(weak,nonatomic) IBOutlet UIButton *imgBtn;
@property (weak, nonatomic) IBOutlet UIView *viewCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCstomerMsg;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgCustomer;
@end
