
//
//  OostaJobContractorJobsHomeViewController.m
//  OostaJob
//
//  Created by Armor on 20/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContractorJobsViewController.h"
#import "OostaJobContractorBiddingTableViewCell.h"
#import "OostaJobMediaCollectionViewCell.h"
#import "DWTagList.h"
#import <AVFoundation/AVFoundation.h>
#import "ImageCache.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "OostaJobCustomerJobDetailsViewController.h"
#import "OostaJobContractorTableViewCell.h"
#import "OostaJobContractorBidNowViewController.h"
#import "OostaJobMapViewController.h"
#import "ContractorRearViewController.h"
#import "OostaJobContractorJobDetailsViewController.h"
@interface OostaJobContractorJobsViewController ()<DWTagListDelegate,UICollectionViewDataSource,UICollectionViewDelegate,SWRevealViewControllerDelegate,UIActionSheetDelegate,OostaJobCustomerJobDetailsViewControllerDelegate,OostaJobContractorBidNowViewControllerDelegate,OostaJobContractorJobDetailsViewControllerDelegate>
{
    int page;
    BOOL isHidden;
    NSMutableDictionary *dictJobList;
    CGFloat height;
    NSString *jobcompleted;
    NSString *animationDirection;
    int currentIndex;
    NSString *custid;
    UITextField *textView;
}
@end

@implementation OostaJobContractorJobsViewController
@synthesize table;
@synthesize btnSender;
@synthesize list,strBids;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"UserId:%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = YES;
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    if ([strBids isEqualToString:@"Yes"])
    {
    
        page = 3;
    
        
    }
    else
    {
        page = 0;
    }
    [self setupUI];
    [self CreateBlurEffectOnBackgroundImage];
    [self settingTheBackgroudImages];
    [self getJobListandCompletionHandler:^(bool resultcompleted){
        if (resultcompleted == YES) {
            [self BtnFunction];
//            if ([strBids isEqualToString:@"Yes"]) {
//                [_tblClosed reloadData];
//            }
           // [self reloadTableViews];
            [table reloadData];
        }
    }];
    //[self getJobList];
   // [self BtnFunction];
    isHidden=YES;
    if (IS_IPAD )
    {
        _customTblPopover = [[UIPopoverController alloc]initWithContentViewController:_optionViewController];
    }
    
    _tblBidding.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblClosed.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblHire.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblRatenow.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    //For Dot Button Hide
    _imgArrow.hidden = YES;
    _btnList.hidden = YES;
    [self getJobListandCompletionHandler:^(bool resultcompleted){
        if (resultcompleted == YES) {
            [self BtnFunction];
            //            if ([strBids isEqualToString:@"Yes"]) {
            //                [_tblClosed reloadData];
            //            }
            // [self reloadTableViews];
            [table reloadData];
        }
    }];
    [self showListViewOnTop];
}


#pragma mark - Create the Blur Background
-(void)CreateBlurEffectOnBackgroundImage
{
    [self.imgBlur assignBlur];
}

#pragma mark-UIDesign

-(void)setupUI
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {//Border Width
        self.btnBidding.layer.borderWidth=1.0;
        self.btnHire.layer.borderWidth=1.0;
        self.btnRatenow.layer.borderWidth=1.0;
        self.btnClosed.layer.borderWidth=1.0;
        //Border colour
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner radius
        self.btnBidding.layer.cornerRadius=18.0;
        self.btnHire.layer.cornerRadius=18.0;
        self.btnRatenow.layer.cornerRadius=18.0;
        self.btnClosed.layer.cornerRadius=18.0;
        
        
        //Colour
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        
        self.btnBidding.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        
        //Font
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnHire.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnRatenow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnClosed.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHeading.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.btnRequestJob.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        
    }
    else
    {
        //Border Width
        self.btnBidding.layer.borderWidth=1.0;
        self.btnHire.layer.borderWidth=1.0;
        self.btnRatenow.layer.borderWidth=1.0;
        self.btnClosed.layer.borderWidth=1.0;
        //Border colour
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner radius
        self.btnBidding.layer.cornerRadius=10.0;
        self.btnHire.layer.cornerRadius=10.0;
        self.btnRatenow.layer.cornerRadius=10.0;
        self.btnClosed.layer.cornerRadius=10.0;
        
        //Colour
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        
        self.btnBidding.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        
        //Font
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnHire.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnRatenow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnClosed.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblHeading.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        
        self.btnRequestJob.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)settingTheBackgroudImages
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?840.0f:790.0f);
    }
    else if(IS_IPHONE5)
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?504.0f:475.0f);
    }
    else
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?420.0f:390.0f);
    }
    
}
//scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView==_scrollview)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
        else
        {
            //find the page number you are on
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
    }
}

//dragging ends, please switch off paging to listen for this event
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *) targetContentOffset
NS_AVAILABLE_IOS(5_0){
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView==_scrollview)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
        else
        {
            //find the page number you are on
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
    }
    
}
#pragma mark-Swipe Action page

-(IBAction)btnTapped:(id)sender
{
    if ([sender tag]==300)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0, 0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        
        
    }
    
    else if ([sender tag]==301)
    {
        
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(768,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(320,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
    }
    else if ([sender tag]==302)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(1536,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(640,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        
    }
    else if ([sender tag]==303)
    {
        
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(2304,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                                 
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(960,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        
    }
    
}

- (IBAction)btnMenuTapped:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
-(void)BtnFunction
{
    int values= 300+page;
    if (values==300)
    {
        self.btnBidding.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.btnBidding.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - Invites";
        
        [self.tblBidding reloadData];
        
    }
    else if (values==301) {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - Bids";
        [self.tblHire reloadData];
        
    }
    else if (values==302)
    {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - Hired";
        
        [self.tblRatenow reloadData];
        
    }
    else if (values==303) {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.lblHeading.text = @"Jobs - Closed";
        
        [self.tblClosed reloadData];
        
    }
}

#pragma mark -UitableView DataSource and Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobinvites"] count]==0)
        {
            return 1;
        }
        else
        {
            return [[dictJobList valueForKeyPath:@"Jobinvites"] count];
        }
    }
    else if (tableView==self.tblHire)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
            return 1;
        }
        else
        {
            return [[dictJobList valueForKeyPath:@"Jobbidding"] count];
        }
        
        
    }
    else if (tableView==self.tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
            return 1;
        }
        else
        {
            return [[dictJobList valueForKeyPath:@"Jobhired"] count];
        }
        
    }
    else if (tableView == table)
    {
        return list.count;
    }
    else
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
            return 1;
        }
        else
        {
            return [[dictJobList valueForKeyPath:@"Jobclosed"] count];
        }
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"CELL";
    tableView.backgroundColor=[UIColor clearColor];
    if (tableView==self.tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobinvites"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Invites";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            OostaJobContractorTableViewCell *cell=(OostaJobContractorTableViewCell*)[self.tblBidding dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
            
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.viewbids.layer.cornerRadius=15.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblJobClosedTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    
                    
                    cell.viewbids.layer.cornerRadius=14;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.lblJobClosedTime.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
            }
            NSString * strKey = @"Jobinvites";
            
            
           
            
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *milesRemining = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"Miles"]];
            
            if ([milesRemining integerValue]>1)
            {
                cell.lblTime.text = [NSString stringWithFormat:@"%@ miles from here",milesRemining];
            }
            else
            {
                cell.lblTime.text = cell.lblTime.text = [NSString stringWithFormat:@"< 1 mile from here"];
            }
            cell.lblJobClosedTime.text=[NSString stringWithFormat:@"Job closes in %@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
            
            
            cell.btnTblBidding.tag=indexPath.row;
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSLog(@"dictJobList==>>%@",dictJobList);
            
            if ([[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"] == (NSString *)[NSNull null])
            {
                [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",@"0"]];
                
                cell.tagList.isHighlightLast = YES;
            }
            else
            {
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
                
                cell.tagList.isHighlightLast = YES;
            }
            
            
            
            
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            
            
            
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *strRemining=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
          
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            cell.lblJobClosedTime.frame=CGRectMake(cell.lblJobClosedTime.frame.origin.x, origin+3, cell.lblJobClosedTime.frame.size.width, 20);
            
            if (strRemining.length!=0)
            {
                origin = origin+23;
            }
            else
            {
                cell.lblJobClosedTime.hidden=YES;
                origin=origin+0;
            }
            
            
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+3, cell.lblDesc.frame.size.width, IS_IPAD?72:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+3, cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+5, cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+5, cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            origin = origin +5;
            
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.collectionView.frame.origin.y+cell.collectionView.frame.size.height+7);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.collectionView.frame.origin.y+cell.collectionView.frame.size.height+7);
            
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"] stringByDeletingPathExtension];
            
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                       else
                                       {
                                           NSLog(@"Not Found");
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.btnTblBidding.userInteractionEnabled=YES;
            cell.tagList.userInteractionEnabled=NO;
            cell.imgArrow.hidden = NO;
            cell.backgroundColor = [UIColor clearColor];
            cell.btnTblBidding.tag=indexPath.row;
            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }
    }
    
    else if (tableView==self.tblHire)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Bids";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[self.tblHire dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.viewbids.layer.cornerRadius=15.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblLowestBidAmount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewbids.layer.cornerRadius=14;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewLowestBidAmount.layer.cornerRadius=5.0;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblLowestBidAmount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewLowestBidAmount.layer.cornerRadius=5.0;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            NSString * strKey = @"Jobbidding";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"dateofbid"]];
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSLog(@"dictJobList==>>%@",dictJobList);
            
            
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
            
            //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"Bids" forState:UIControlStateNormal];
            cell.tagList.isHighlightLast = YES;
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
//            [cell.tagList selectedTag:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]] tagIndex:arrTags.count-1];
            
            cell.tagList.userInteractionEnabled = NO;
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            if (arrContractor.count>0)
            {
                cell.lblContratorCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrContractor.count];
                cell.lblContratorCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
            }
            else
            {
                cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
                cell.lblContratorCount.textColor = [UIColor lightGrayColor];
            }
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+3, cell.lblDesc.frame.size.width, IS_IPAD?72:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+3, cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+5, cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+5, cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            cell.viewLowestBidAmount.frame =CGRectMake(cell.viewLowestBidAmount.frame.origin.x, origin+5, cell.viewLowestBidAmount.frame.size.width, cell.viewLowestBidAmount.frame.size.height);
            cell.viewLowestBidAmount.hidden = YES;
            origin = origin +5;
            
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.viewContractor.frame.origin.y+cell.viewContractor.frame.size.height+7);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+7);
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.lblContratorCount.text = [NSString stringWithFormat:@"$%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            cell.lblLowestBidAmount.text = [NSString stringWithFormat:@"$%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]];
            

            
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            
            if ([[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"notificationCont"]] intValue]>0)
            {
                JSBadgeView *badgeView = [[JSBadgeView alloc] initWithParentView:cell.viewMessage alignment:JSBadgeViewAlignmentTopRight];
                [badgeView setBadgeBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
                [badgeView setBadgeTextFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?16.0f:12.0f]];
                badgeView.badgeText = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"notificationCont"]];
                badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
                badgeView.layer.masksToBounds = YES;
            }
            
            if ([[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"] intValue]>0)
            {
                cell.imgMsg.image = [UIImage imageNamed:@"msgSel-120.png"];
                cell.lblMessageCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
            }
            else
            {
                cell.imgMsg.image = [UIImage imageNamed:@"mail-grey.png"];
                cell.lblMessageCount.textColor = [UIColor lightGrayColor];
            }
            cell.completedbtn.hidden=YES;

            cell.imgArrow.hidden = YES;
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            cell.btnTblBidding.userInteractionEnabled = NO;
            return cell;
        }
    }
    else if (tableView==self.tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Hired";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[self.tblRatenow dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.viewbids.layer.cornerRadius=15.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    cell.lblCongrdz.font=[UIFont fontWithName:FONT_BOLD size:26.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.completedbtn.layer.cornerRadius=5.0;
                    cell.viewbids.layer.cornerRadius=14;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                    
                }
                else
                {
//                    NSUserDefaults *customeruserid = [NSUserDefaults standardUserDefaults];
//                    [customeruserid setObject:[dictJobList valueForKeyPath:@"Jobhired.customerDetails.customerID"] forKey:@"customerid"];
//                    [customeruserid synchronize];
                    
                    custid = [dictJobList valueForKeyPath:@"Jobhired.customerDetails.customerID"];
                    NSLog(@"cust id :%@",custid);
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    cell.lblCongrdz.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            NSString * strKey = @"Jobhired";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"hiredTimed"]];
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"Hired" forState:UIControlStateNormal];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            NSString *contractorcomplete=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"is_contractor_completed"]];
            
            if ([contractorcomplete isEqualToString:@"0"])
            {
                cell.completedbtn.hidden=YES;
            }
            else
            {
                cell.completedbtn.hidden=YES;

            }
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+3, cell.lblDesc.frame.size.width, IS_IPAD?72:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+3, cell.collectionView.frame.size.width, (IS_IPAD?120:60));
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+5, cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+5, cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            
            
            cell.lblCongrdz.frame = CGRectMake(cell.lblCongrdz.frame.origin.x, cell.viewContractor.frame.size.height+origin+80, cell.lblCongrdz.frame.size.width, cell.lblCongrdz.frame.size.height);
            
            cell.completedbtn.frame = CGRectMake(cell.completedbtn.frame.origin.x, cell.viewContractor.frame.size.height+origin+30, cell.completedbtn.frame.size.width, cell.completedbtn.frame.size.height);
            
            NSLog(@"completed frame done :%f",cell.completedbtn.frame.size.width);
            [cell.completedbtn addTarget:self action:@selector(Completebutton:) forControlEvents:UIControlEventTouchUpInside];
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.lblCongrdz.frame.origin.y+cell.lblCongrdz.frame.size.height);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.lblCongrdz.frame.origin.y+cell.lblCongrdz.frame.size.height);
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.lblContratorCount.text = [NSString stringWithFormat:@"$%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            cell.imgArrow.hidden = YES;
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.lblCongrdz.hidden = NO;
            cell.backgroundColor=[UIColor clearColor];
            
            cell.lblMeetingTime.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:12.0f];
            cell.lblMeetingTime.frame=CGRectMake(cell.lblMeetingTime.frame.origin.x, cell.viewContractor.frame.origin.y+3, cell.lblMeetingTime.frame.size.width, cell.lblMeetingTime.frame.size.height);
            cell.imgCalendar.frame=CGRectMake(cell.imgCalendar.frame.origin.x, cell.viewContractor.frame.origin.y+3, cell.imgCalendar.frame.size.width, cell.imgCalendar.frame.size.height);
            cell.imgCalendar.hidden = NO;
            cell.lblMeetingTime.hidden = NO;
            
            if ([[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingDate"]] isEqualToString:@"<null>"])
            {
                cell.imgCalendar.hidden = YES;
                cell.lblMeetingTime.hidden = YES;
            }
            else
            {
                cell.lblMeetingTime.text = [NSString stringWithFormat:@"%@ %@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingDate"],[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingTime"]];
                
                NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingDate"]];
                __block NSDate *detectedDate;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
                [detector enumerateMatchesInString:dateString
                                           options:kNilOptions
                                             range:NSMakeRange(0, [dateString length])
                                        usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                 {
                     detectedDate = result.date;
                     NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                     [dateFormate setDateFormat:@"MM-dd-YYYY"];
                     cell.lblMeetingTime.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"meetingTime"]];
                 }];
            }
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            return cell;
        }
    }
    else if (tableView==self.tblClosed)
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Closed";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[self.tblClosed dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.viewbids.layer.cornerRadius=15.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    cell.lblCongrdz.font=[UIFont fontWithName:FONT_BOLD size:26.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewbids.layer.cornerRadius=14;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    cell.lblCongrdz.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            NSString * strKey = @"Jobclosed";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"closedTime"]];
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSArray *reasonarray= [[NSArray alloc]init];
            NSString *reasonstring = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"reason"];
            if ([reasonstring isEqual:[NSNull null]]) {
                
            }
            else
            {
                reasonarray = [reasonstring componentsSeparatedByString:@","];

                    
                
                

            }
            NSLog(@"reasons array :%@",reasonarray);
           

            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"Closed" forState:UIControlStateNormal];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
//            if (reasonarray.count == 0) {
//            }
//            else{
            
                    [cell.reasonslist setTags:reasonarray];
          //  }
            
            [cell.reasonslist setAutomaticResize:YES];
            [cell.reasonslist setTagDelegate:self];
            cell.reasonslist.userInteractionEnabled = NO;
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+3, cell.lblDesc.frame.size.width, (IS_IPAD?72:44));
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+3, cell.collectionView.frame.size.width, (IS_IPAD?120:60));
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+5, cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+5, cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);

            cell.lblCongrdz.frame = CGRectMake(cell.lblCongrdz.frame.origin.x, cell.viewContractor.frame.size.height+origin+8, cell.lblCongrdz.frame.size.width, cell.lblCongrdz.frame.size.height);
            
            if (reasonarray.count == 0) {
                cell.reasonslist.hidden = YES;
                cell.lblCongrdz.hidden = YES;

                cell.reasonslist.frame = CGRectMake(cell.lblCongrdz.frame.origin.x, cell.viewContractor.frame.size.height+origin+20, cell.reasonslist.frame.size.width, cell.reasonslist.frame.size.height);
            }
            else{
                cell.reasonslist.hidden = NO;
                cell.lblCongrdz.hidden = NO;
                
                cell.reasonslist.frame = CGRectMake(cell.lblCongrdz.frame.origin.x, cell.viewContractor.frame.size.height+origin+50, cell.reasonslist.frame.size.width, cell.reasonslist.frame.size.height);
            }
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height);
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.lblContratorCount.text = [NSString stringWithFormat:@"$%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            cell.imgArrow.hidden = YES;
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            cell.completedbtn.hidden=YES;
            cell.viewStars.hidden =NO;
            cell.viewStars.frame=CGRectMake(cell.viewStars.frame.origin.x, cell.viewContractor.frame.origin.y, cell.viewStars.frame.size.width, cell.viewStars.frame.size.height);
            float totalRating = [[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"customerDetails.totalRating"]] floatValue];
            for (int i=1; i<=5; i++)
            {
                for (UIImageView *img in cell.imgStars)
                {
                    if (img.tag == 500+i)
                    {
                        if (i<=totalRating)
                        {
                            img.image = [UIImage imageNamed:@"star-fill.png"];
                        }
                        else if ((i-0.5)==totalRating)
                        {
                            img.image = [UIImage imageNamed:@"half.png"];
                        }
                        else
                        {
                            img.image =[UIImage imageNamed:@"empty.png"];
                        }
                    }
                }
            }
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            [cell.lblCongrdz setText:@"Cancelled reasons"];
            return cell;
        }
    }
    else if (tableView==table)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        cell.textLabel.text = [list objectAtIndex:indexPath.row];
        
        
        cell.textLabel.textColor = [UIColor blackColor];
        
        return cell;
    }
    else
    {
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table)
    {
        return 40;
    }
    else if (tableView == _tblClosed)
    {
        NSLog(@"_tblView_height==>>%f",tableView.frame.size.height);
        NSLog(@"_tblClosed_height==>>%f",_tblClosed.frame.size.height);
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[_tblClosed dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSString *StrKey = @"Jobclosed";
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            NSArray *reasonarray= [[NSArray alloc]init];
            NSString *reasonstring = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"reason"];
            if ([reasonstring isEqual:[NSNull null]]) {
                
            }
            else
            {
                reasonarray = [reasonstring componentsSeparatedByString:@","];
                
                
                
                
                
            }
            NSLog(@"reasons array :%@",reasonarray);
            
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            
            [cell.reasonslist setAutomaticResize:YES];
            [cell.reasonslist setTags:reasonarray];
            [cell.reasonslist setTagDelegate:self];
            height = 0;
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+0;
                
            }
            else
            {
                
                
                height=cell.tagList.frame.size.height+cell.reasonslist.frame.size.height+250;
                
                
                if (str.length==0)
                {
                    height = height-47;
                }
                
                if (arr.count==0)
                {
                    height = height-63;
                }
            }
            return height+(IS_IPAD?54:24)+3;
        }
        
        
    }
    
    else if (tableView == _tblRatenow)
    {
        NSLog(@"_tblView_height==>>%f",tableView.frame.size.height);
        NSLog(@"_tblRatenow_height==>>%f",_tblRatenow.frame.size.height);
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[_tblRatenow dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSString *StrKey = @"Jobhired";
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSLog(@"dictJobList_first==>>%@",dictJobList);
            
           
            
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            height = 0;
            
            height=cell.tagList.frame.size.height+cell.tagList.frame.origin.y;
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+0;
                
            }
            else
            {
                height=cell.tagList.frame.size.height+80+cell.tagList.frame.origin.y+158;
                
                if (str.length==0)
                {
                    height = height-47;
                }
                
                if (arr.count==0)
                {
                    height = height-63;
                }
            }
            
            
            
            return height+(IS_IPAD?54:24)+3;
        }
        
    }
    else if (tableView == _tblHire)
    {
        NSLog(@"_tblView_height==>>%f",tableView.frame.size.height);
        NSLog(@"_tblHire_height==>>%f",_tblHire.frame.size.height);
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobContractorBiddingTableViewCell *cell=(OostaJobContractorBiddingTableViewCell*)[_tblHire dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSString *StrKey = @"Jobbidding";
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSLog(@"dictJobList_first==>>%@",dictJobList);
            
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            cell.tagList.isHighlightLast = YES;
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            height = 0;
            height=cell.tagList.frame.size.height+cell.tagList.frame.origin.y;
            
            if (str.length!=0)
            {
                height = height+(IS_IPAD?70:47);
            }
            
            if (arr.count!=0)
            {
                height = height+(IS_IPAD?123:63);
            }
            return height+(IS_IPAD?60:50);
        }
        
    }
    else if (tableView == _tblBidding)
    {
        //        tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, tableView.contentSize.height);
        NSLog(@"_tblView_height==>>%f",tableView.frame.size.height);
        NSLog(@"_tblBidding_height==>>%f",_tblBidding.frame.size.height);
        if ([[dictJobList valueForKeyPath:@"Jobinvites"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobContractorTableViewCell *cell=(OostaJobContractorTableViewCell*)[self.tblBidding dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            NSString *StrKey = @"Jobinvites";
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSString *strRemining=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            if ([[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"] == (NSString *)[NSNull null])
            {
                cell.tagList.isHighlightLast = YES;
                [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",@"0"]];
            }
            else
            {
                cell.tagList.isHighlightLast = YES;
                [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"minimumbidamount"]]];
            }
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            CGFloat heightCell = 0;
            
            heightCell=cell.tagList.frame.size.height+cell.tagList.frame.origin.y;
            if (strRemining.length!=0)
            {
                heightCell = heightCell+23;
            }
            
            if (str.length!=0)
            {
                heightCell = heightCell+(IS_IPAD?75:47);
            }
            
            if (arr.count!=0)
            {
                heightCell = heightCell+ (IS_IPAD?123:63);
            }
            NSLog(@"height:%f",heightCell);
            
            return heightCell+13;
        }
        
    }
    
    else
    {
        return 0;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobinvites"] count]==0)
        {
        }
        else
        {
            [self gotoJobBidNow:[[dictJobList valueForKeyPath:@"Jobinvites"] objectAtIndex:indexPath.row]];
        }
        
        
    }
    else if(tableView == _tblHire)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
//            [self gotoJobBidNow:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row]];
        }
        
        
    }
    else if(tableView == _tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
        
    }
    else if(tableView == _tblClosed)
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
    }
    else if (tableView==table)
    {
        if (indexPath.row == 0)
        {
            [self gotoJobBidNow:[[dictJobList valueForKeyPath:@"Jobinvites"] objectAtIndex:currentIndex]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                            message:@"If you skip this job, it will be removed from the list"
                                                           delegate:self
                                                  cancelButtonTitle:@"Skip Job"
                                                  otherButtonTitles:@"Dismiss",nil];
            alert.tag = currentIndex;
            [alert show];
        }
        
        
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([jobcompleted isEqualToString:@"jobcompleted"]) {
        jobcompleted = @"";
        if (buttonIndex == 0) {
            if ([textView.text  isEqual: @""]) {
                
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=kMBProgressHUDLabelColor;
                hud.backgroundColor=kMBProgressHUDBackgroundColor;
                hud.labelText = @"Please give a feedback";
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
            else
            {
            [self Contractorjobcompleted:alertView.tag andCompletionHandler:^(bool resultcompleted)
             {
                 if (resultcompleted == YES) {
                     [self getJobList];

                 }
             }];
            }
        }
        
    }
    else
    {
    if (buttonIndex == 0)
    {
        [self getSkiptheJobForTheIndex:alertView.tag  andCompletionHandler:^(bool resultSkiptheJob)
         {
             if (resultSkiptheJob == YES)
             {
                 NSLog(@"Skipped Called");
             }
         }];
    }
    }
}
-(void)GotoMessage:(UIButton *)sender
{
    
    
    if (page==0)
    {
        [self gotoJobBidNow:[[dictJobList valueForKeyPath:@"Jobinvites"] objectAtIndex:sender.tag]];
    }
    else if(page == 1)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    else if(page == 2)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    else if(page == 3)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    
}

-(void)biddingBtnTapped:(UIButton *)sender
{
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
//                                                    message:@"If you skip this job, it will be removed from the list"
//                                                   delegate:self
//                                          cancelButtonTitle:@"Skip Job"
//                                          otherButtonTitles:@"Dismiss",nil];
//    alert.tag = currentIndex;
//    [alert show];
    
    currentIndex = sender.tag;
    if(self.dropDown == nil)
    {
        CGFloat f = 40;
        self.dropDown = [[UIView alloc]init];
        NSArray *arr;
        if (page==0)
        {
            f = 80;
            arr =[[NSArray alloc]initWithObjects:@"Bid Now",@"Skip", nil];
        }
        else
        {
            arr =[[NSArray alloc]initWithObjects:@"Cancel", nil];
        }
        
        [self showDropDown:sender andHeight:&f theContentArr:arr theDiection:@"down"];
        //            [self.mapView bringSubviewToFront:_dropDown];
        
    }
    else {
        [self hideDropDown:sender];
        [self rel];
        
    }
    
}






-(void)showListViewOnTop
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (IS_IPAD)
                         {
                             _imgArrow.frame=CGRectMake(727, 75, 30, 30);
                             _viewListView.frame=CGRectMake(0, 100, 768, 50);
                             _scrollview.frame=CGRectMake(0, 150, 768, 790);
                             isHidden=NO;
                         }
                         else if (IS_IPHONE5)
                         {
                             _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                             _viewListView.frame=CGRectMake(0, 64, 320, 32);
                             _scrollview.frame=CGRectMake(0, 100, 320, 475);
                             isHidden=NO;
                         }
                         else
                         {
                             _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                             _viewListView.frame=CGRectMake(0, 64, 320, 32);
                             _scrollview.frame=CGRectMake(0, 100, 320, 390);
                             isHidden=NO;
                         }
                         
                     }
                     completion:nil];
}


#pragma mark-Button Action
-(IBAction)btnListTapped:(id)sender
{
    if (isHidden==YES)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             if (IS_IPAD)
                             {
                                 _imgArrow.frame=CGRectMake(727, 75, 30, 30);
                                 _viewListView.frame=CGRectMake(0, 100, 768, 50);
                                 _scrollview.frame=CGRectMake(0, 150, 768, 790);
                                 isHidden=NO;
                             }
                             else if (IS_IPHONE5)
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 32);
                                 _scrollview.frame=CGRectMake(0, 100, 320, 475);
                                 isHidden=NO;
                             }
                             else
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 32);
                                 _scrollview.frame=CGRectMake(0, 100, 320, 390);
                                 isHidden=NO;
                             }
                             
                         }
                         completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             if (IS_IPAD)
                             {
                                 _imgArrow.frame=CGRectMake(727, 75, 30, 0);
                                 _viewListView.frame=CGRectMake(0, 100, 768, 0);
                                 _scrollview.frame=CGRectMake(0, 100, 768, 840);
                                 isHidden=YES;
                             }
                             else if(IS_IPHONE5)
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 0);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 0);
                                 _scrollview.frame=CGRectMake(0, 64, 320, 504);
                                 isHidden=YES;
                             }
                             else
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 0);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 0);
                                 _scrollview.frame=CGRectMake(0, 64, 320, 430);
                                 isHidden=YES;
                             }
                             
                         }
                         completion:nil];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        NSLog(@"Cancel");
    }
    else if (buttonIndex==1)
    {
        
        NSLog(@"Close");
    }
}
#pragma mark getJobList
-(void)getJobList
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Job List...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcontractorjoblist/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    NSLog(@"getcontractorjoblist:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 dictJobList=[[NSMutableDictionary alloc]init];
                 [dictJobList setObject:[result valueForKeyPath:@"Jobinvites"] forKey:@"Jobinvites"];
                 [dictJobList setObject:[result valueForKeyPath:@"Jobhired"] forKey:@"Jobhired"];
                 [dictJobList setObject:[result valueForKeyPath:@"Jobbidding"] forKey:@"Jobbidding"];
                 [dictJobList setObject:[result valueForKeyPath:@"Jobclosed"] forKey:@"Jobclosed"];
                 NSLog(@"dictJobList %@",dictJobList);
                 NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                 [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobinvites"] count]]];
                 [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobhired"] count]]];
                 [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobbidding"] count]]];
                 [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobclosed"] count]]];
                 NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                 if ([[sortedArr lastObject] intValue]>=1)
                 {
                     [[NSUserDefaults standardUserDefaults]setObject:@"CONTRACTORJOBS" forKey:@"SCREEN"];
                 }
                 else
                 {
                     [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
                 }
                // [self.tblHire reloadData];
                 [self reloadTableViews];
                 [hud hide:YES];
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
             }
         }
     }];
    }
}
-(void)reloadTableViews
{
    if (page==1)
    {
        [self.tblHire reloadData];

    }
    else
    {
        [self.tblHire reloadData];
        [self.tblBidding reloadData];
    }
    
    
}

#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"less than min ago";
    
}

- (IBAction)requestaService:(id)sender
{
    [self gotoListOfJobs];
}

-(void)gotoListOfJobs
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobMapViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    
    ContractorRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (IBAction)btnCancelPressed:(id)sender {
}

- (IBAction)btnClosePressed:(id)sender {
}

-(void)gotoJobDetails:(NSMutableArray *)array andMoveTo:(NSString *)strPage
{
    OostaJobContractorJobDetailsViewController *DetObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        DetObj = [[OostaJobContractorJobDetailsViewController alloc]initWithNibName:@"OostaJobContractorJobDetailsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        DetObj = [[OostaJobContractorJobDetailsViewController alloc]initWithNibName:@"OostaJobContractorJobDetailsViewController" bundle:nil];
    }
    else
    {
        DetObj = [[OostaJobContractorJobDetailsViewController alloc]initWithNibName:@"OostaJobContractorJobDetailsViewController Small" bundle:nil];
    }
    DetObj.delegate = self;
    DetObj.arrSelected = [array mutableCopy];
    DetObj.strGoto = strPage;
    [self.navigationController pushViewController:DetObj animated:YES];
}
-(void)getreload
{
    [self getJobList];
    page = 0;
    [self BtnFunction];
    CGPoint point = CGPointMake(self.view.frame.size.width*page,0);
    [self.scrollview setContentOffset:point animated:YES];
}

#pragma mark DropDown

- (void)showDropDown:(UIButton *)b andHeight:(CGFloat *)height theContentArr:(NSArray *)arr theDiection:(NSString *)direction
{
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    
    // Initialization code
    CGRect btn = b.superview.frame;
    self.list = [NSArray arrayWithArray:arr];
    NSLog(@"arr:%@",arr);
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, -5);
    }else if ([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    self.dropDown.layer.masksToBounds = NO;
    self.dropDown.layer.cornerRadius = 8;
    self.dropDown.layer.shadowRadius = 5;
    self.dropDown.layer.shadowOpacity = 0.5;
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
    table.delegate = self;
    table.dataSource = self;
    table.layer.cornerRadius = 5;
    table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.separatorColor = [UIColor grayColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
    } else if([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, *height);
    [UIView commitAnimations];
    
    [self.dropDown addSubview:table];
    if (page==0)
    {
        UITableViewCell *cell = [_tblBidding cellForRowAtIndexPath:[NSIndexPath indexPathForRow:b.tag inSection:0]];
        UIView *view = (UIView *)[cell viewWithTag:100];
        [view.superview addSubview:self.dropDown];
        [table reloadData];
    }
    else if (page==1)
    {
        UITableViewCell *cell = [_tblHire cellForRowAtIndexPath:[NSIndexPath indexPathForRow:b.tag inSection:0]];
        UIView *view = (UIView *)[cell viewWithTag:100];
        [view.superview addSubview:self.dropDown];
        [table reloadData];
    }
    
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.superview.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}
-(void)rel
{
    self.dropDown=nil;
}

-(void)gotoJobBidNow:(NSMutableArray *)array
{
    OostaJobContractorBidNowViewController *BidObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
    }
    else
    {
        BidObj = [[OostaJobContractorBidNowViewController alloc]initWithNibName:@"OostaJobContractorBidNowViewController" bundle:nil];
    }
    BidObj.delegate =self;
    BidObj.arrSelected = [array mutableCopy];
    [self.navigationController pushViewController:BidObj animated:YES];
}

#pragma mark Skip Job

-(void)getSkiptheJobForTheIndex:(NSUInteger)theIndex andCompletionHandler:(void (^)(bool resultSkiptheJob))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Skipping...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    NSString * strKey = @"Jobinvites";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@skipcntractor/%@/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"],[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:theIndex] valueForKeyPath:@"jobpostID"]]]]];
    NSLog(@"skipcntractor:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = @"Job Skipped Successfully";
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
                 [self getJobList];
                 completionHandler(true);
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
         }
     }];
    }
}

-(void)Completebutton:(UIButton *)sender
{
//    jobcompleted = @"jobcompleted";
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
//                                                    message:@"Are you sure the Job is completed?"
//                                                   delegate:self
//                                          cancelButtonTitle:@"Confirm"
//                                          otherButtonTitles:@"Dismiss",nil];
//
//    alert.tag = [sender tag];
//    [alert show];
    jobcompleted = @"jobcompleted";
    UIAlertView *alertinitialstat = [[UIAlertView alloc] initWithTitle:@"OOstajob"
                                                  message:@"Are you sure the Job is completed?"
                                                 delegate:self cancelButtonTitle:
                        @"Confirm" otherButtonTitles:@"Dismiss", nil];
    alertinitialstat.delegate = self;
    textView = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, 300, 60)];
    CGRect frameRect = textView.frame;
    frameRect.size.height = 53;
    textView.frame = frameRect;
    [textView setPlaceholder:@"Please provide a feedback"];
    [textView setFont:[UIFont fontWithName:@"Helvetica Neue" size:12]];

    alertinitialstat.tag=sender.tag;
    /** adding the textfield to the alertView **/
    [alertinitialstat setValue:textView forKey:@"accessoryView"];
    
    [alertinitialstat show];
    
}


-(void)Contractorjobcompleted:(NSUInteger)theIndex andCompletionHandler:(void (^)(bool resultcompleted))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSString * strKey = @"Jobhired";
      
        NSString *custid;
        NSUserDefaults *customerid = [NSUserDefaults standardUserDefaults];
        custid = [customerid objectForKey:@"customerid"];
         NSString *key1 = @"jobId";
         NSString *obj1 = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:theIndex] valueForKeyPath:@"jobpostID"];
         
         NSString *key2 = @"loggedUserId";
         NSString *obj2 = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
        
        
         NSString *key3 = @"action_by";
         NSString *obj3 = @"contractor";
        
        NSString *key4 = @"userID";
        NSString *obj4 = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:theIndex]
                          valueForKeyPath:@"customerDetails.customerID"];
        
        NSString *key5 = @"contractor_remarks";
        NSString *obj5 = textView.text;
        NSLog(@"remarks :%@",textView.text);
        
         NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                        initWithObjects:@[obj1,obj2,obj3,obj4,obj5]
                                        forKeys:@[key1,key2,key3,key4,key5]];
         
         NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
         NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
         NSLog(@"DATA %@",jsonString);
         NSMutableData *body = [NSMutableData data];
         
         [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
         
         NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@makeJobCompleted",BaseURL]]];
         NSLog(@"PostJob URL:%@",request.URL);
         [request setHTTPBody:body];
         [request setHTTPMethod:@"POST"];
         request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
         [NSURLConnection sendAsynchronousRequest: request
                                            queue: [NSOperationQueue mainQueue]
                                completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"Job Successfully  Completed";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     [self getJobList];
                     completionHandler(true);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
}


-(void)getJobListandCompletionHandler:(void (^)(bool resultJobList))completionHandler
{Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading Job List...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcontractorjoblist/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
        NSLog(@"getcontractorjoblist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
                 
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     dictJobList=[[NSMutableDictionary alloc]init];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobinvites"] forKey:@"Jobinvites"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobhired"] forKey:@"Jobhired"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobbidding"] forKey:@"Jobbidding"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobclosed"] forKey:@"Jobclosed"];
                     NSLog(@"dictJobList %@",dictJobList);
                     NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobinvites"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobhired"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobbidding"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobclosed"] count]]];
                     NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                     if ([[sortedArr lastObject] intValue]>=1)
                     {
                         [[NSUserDefaults standardUserDefaults]setObject:@"CONTRACTORJOBS" forKey:@"SCREEN"];
                     }
                     else
                     {
                         [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
                     }
//                     [self BtnFunction];
//                     if ([strBids isEqualToString:@"YES"]) {
//                         [_tblClosed reloadData];
//                     }
//                     [self reloadTableViews];
                     completionHandler(true);

                     [hud hide:YES];
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
                     completionHandler(false);
                 }
             }
         }];
    }
}


@end
