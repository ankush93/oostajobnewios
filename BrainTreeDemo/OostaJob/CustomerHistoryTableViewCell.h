//
//  CustomerHistoryTableViewCell.h
//  OostaJob
//
//  Created by Ankush on 21/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "MXLMediaView.h"

@interface CustomerHistoryTableViewCell : UITableViewCell<MXLMediaViewDelegate>
{
    NSMutableArray *arrCollectionViewData;
}
@property (weak, nonatomic) IBOutlet UIView *dynamicview;

@property (weak, nonatomic) IBOutlet UILabel *labelname;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *paidamount;
@property (weak, nonatomic) IBOutlet UILabel *paidstatus;
@property (weak, nonatomic) IBOutlet UILabel *contractorname;
@property (weak, nonatomic) IBOutlet UILabel *phonenumber;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet DWTagList *cancelreasons;
@property (weak, nonatomic) IBOutlet UILabel *lbltime;
@property (weak, nonatomic) IBOutlet UILabel *pinlbl;
@property (weak, nonatomic) IBOutlet DWTagList *taglist;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *jobcancelheading;
@property (weak, nonatomic) IBOutlet UIView *cancelview;
-(void)CollectionData:(NSMutableArray *)arr andtheIndexPath:(NSIndexPath *)indexPath andTheTableView:(UITableView *)tableView;
@end
