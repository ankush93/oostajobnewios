//
//  OostaJobDescriptionViewController.m
//  OostaJob
//
//  Created by Apple on 05/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import "OostaJobDescriptionViewController.h"

@interface OostaJobDescriptionViewController ()

@end

@implementation OostaJobDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _btnDone.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:IS_IPAD?23.0f: 16.0f];
    _lblTitle.font=[UIFont fontWithName:FONT_BOLD size:IS_IPAD?23.0f:16.0f];
    _txtVIewDesc.font=[UIFont fontWithName:FONT_THIN size:IS_IPAD?23.0f:16.0f];
    
    [_txtVIewDesc becomeFirstResponder];
    if ([_currentTappedIndex intValue]!=_arrContent.count)
    {
       _lblTitle.text=[[_arrContent objectAtIndex:[_currentTappedIndex intValue]]valueForKeyPath:@"char_name"];
    }
    else
    {
        _lblTitle.text=@"Write down some notes about the job description, specifiation, size, etc.";
    }
    
    _txtVIewDesc.text=_currentValue;
    if ([_lblTitle.text isEqualToString:_txtVIewDesc.text])
    {
        _txtVIewDesc.text=@"";
    }
    [self setKeyBoardNotificationCenters];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark KeyBoard adjust Tableview

-(void) setKeyBoardNotificationCenters
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillDisappear:)
                                                 name: UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        
    } completion:^(BOOL finished) {
    }];
}

- (void) keyboardDidShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        CGSize kbSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        //change the frame of your talbleiview via kbsize.height
        CGRect frame=_txtVIewDesc.frame;
        frame.size.height=IS_IPAD?self.view.frame.size.height-kbSize.height-100:self.view.frame.size.height-kbSize.height-64;
        _txtVIewDesc.frame=frame;
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void) keyboardWillDisappear: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        //restore your tableview
        
        
    } completion:^(BOOL finished) {
    }];
}

- (NSTimeInterval) keyboardAnimationDurationForNotification:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = [info objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue: &duration];
    
    return duration;
}


- (IBAction)btnDonePressed:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(getDesc:andthecurrentTappedIndex:)])
    {
        [_delegate getDesc:_txtVIewDesc.text andthecurrentTappedIndex:_currentTappedIndex];
        NSLog(@"Pass");
    }
   [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
@end
