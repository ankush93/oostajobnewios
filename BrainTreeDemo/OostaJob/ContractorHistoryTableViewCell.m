//
//  ContractorHistoryTableViewCell.m
//  OostaJob
//
//  Created by Ankush on 22/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import "ContractorHistoryTableViewCell.h"
#import "OostaJobMediaCollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>

#import "ImageCache.h"
@implementation ContractorHistoryTableViewCell
@synthesize collectionView;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark collection view cell paddings
-(void)CollectionData:(NSMutableArray *)arr andtheIndexPath:(NSIndexPath *)indexPath andTheTableView:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    collectionView = (UICollectionView*)[cell viewWithTag:501];
    arrCollectionViewData = [[NSMutableArray alloc]init];
    arrCollectionViewData = [arr mutableCopy];
    [collectionView reloadData];
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return IS_IPAD?UIEdgeInsetsMake(10, 10, 0, 0):UIEdgeInsetsMake(5, 5, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return IS_IPAD?10.0:0.0;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return arrCollectionViewData.count;
}


- (OostaJobMediaCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OostaJobMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgThumbNail.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgThumbNail];
    
    if ([[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"1"])
    {
        NSString *imageCacheKey = [[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             // retrive image on global queue
             UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
             
             dispatch_async(dispatch_get_main_queue(), ^{
             
             [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
             cell.imgThumbNail.image = img;
             });
             });*/
            
            [NSURL_UIImage processImageDataWithURLString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] andBlock:^(NSData *imageData)
             {
                 UIImage * img = [UIImage imageWithData:imageData];
                 
                 [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                 cell.imgThumbNail.image = img;
             }];
            
        }
        
        
        cell.imgPlay.hidden=YES;
        cell.btnDelete.hidden=YES;
    }
    else if ([[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"2"])
    {
        cell.imgPlay.hidden=NO;
        NSString *imageCacheKey = [[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                NSString *imageCacheKey = [[[arrCollectionViewData objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                generator.appliesPreferredTrackTransform = YES;
                NSError *error;
                CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                if (!error)
                {
                    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.imgThumbNail.image=image;
                        if (cell.imgThumbNail.image)
                        {
                            [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                        }
                        
                    });
                }
            });
        }
        
        
        cell.btnDelete.hidden=YES;
    }
    cell.imgThumbNail.layer.cornerRadius=3.0;
    cell.imgThumbNail.layer.masksToBounds = YES;
    cell.layer.cornerRadius=3.0;
    cell.layer.masksToBounds = YES;
    cell.btnDidSelect.tag=indexPath.row;
    [cell.btnDidSelect addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)didSelect:(UIButton *)sender
{
    MXLMediaView *mediaView = [[MXLMediaView alloc] init];
    mediaView.arrMedia =  arrCollectionViewData;
    mediaView.selectedIndex = sender.tag;
    [mediaView setDelegate:self];
    [mediaView addingMediaInScrollViewinParentView:self.window completion:^{
        NSLog(@"Done showing MXLMediaView");
    }];
    
    /*if ([[[arrCollectionViewData objectAtIndex:sender.tag] valueForKey:@"mediaType"] isEqualToString:@"1"])
     {
     MXLMediaView *mediaView = [[MXLMediaView alloc] init];
     mediaView.arrMedia =  arrCollectionViewData;
     mediaView.selectedIndex = sender.tag;
     [mediaView setDelegate:self];
     NSString *imageCacheKey = [[[arrCollectionViewData objectAtIndex:sender.tag] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
     if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
     {
     [mediaView showImage:[[ImageCache sharedImageCache]imageForKey:imageCacheKey] inParentView:self.window completion:^{
     NSLog(@"Done showing MXLMediaView");
     }];
     }
     else
     {
     NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrCollectionViewData objectAtIndex:sender.tag] valueForKey:@"mediaContent"]]]];
     UIImage *img=[UIImage imageWithData:data];
     [mediaView showImage:img inParentView:self.window completion:^{
     NSLog(@"Done showing MXLMediaView");
     }];
     }
     
     
     }
     else if ([[[arrCollectionViewData objectAtIndex:sender.tag] valueForKey:@"mediaType"] isEqualToString:@"2"])
     {
     MXLMediaView *mediaView = [[MXLMediaView alloc] init];
     mediaView.arrMedia =  arrCollectionViewData;
     mediaView.selectedIndex = sender.tag;
     [mediaView setDelegate:self];
     
     // The best video on the Internet.
     NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrCollectionViewData objectAtIndex:sender.tag] valueForKey:@"mediaContent"]]];
     
     [mediaView showVideoWithURL:videoURL inParentView:self.window completion:^{
     NSLog(@"Complete");
     
     }];
     }
     */
}

#pragma mark MXLMediaView delegate
-(void)mediaView:(MXLMediaView *)mediaView didReceiveLongPressGesture:(id)gesture {
    NSLog(@"MXLMediaViewDelgate: Long pressed received");
}

-(void)mediaViewWillDismiss:(MXLMediaView *)mediaView {
    NSLog(@"MXLMediaViewDelgate: Will dismiss");
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
}

-(void)mediaViewDidDismiss:(MXLMediaView *)mediaView {
    NSLog(@"MXLMediaViewDelgate: Did dismiss");
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}


@end

