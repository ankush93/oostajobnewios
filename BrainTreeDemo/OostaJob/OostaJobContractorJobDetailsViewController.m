//
//  OostaJobContractorBidNowViewController.m
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//OostaJobContractorBidNowViewController

#import "OostaJobContractorJobDetailsViewController.h"
#import "OostaJobContractorBidNowTableViewCell.h"
#import "ALMoviePlayerController.h"
#import "OostaJobMediaCollectionViewCell.h"
#import "ImageCache.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
@interface OostaJobContractorJobDetailsViewController ()<ALMoviePlayerControllerDelegate,MFMailComposeViewControllerDelegate>
{
    int selectedIndex;
    BOOL isClicked;
    BOOL isChatTextField;
    BOOL isNotifyCalled;
    
    NSMutableArray *arrHeight;
    NSString *strPhone;
    NSString *strEmail;
    JSBadgeView *badgeView;
}
@end

@implementation OostaJobContractorJobDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedNotification:) name:@"Finished" object:nil];
    
    
    [super viewDidLoad];
    isNotifyCalled = NO;
    self.tblChatMsgs.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    [self UISetup];
    // Do any additional setup after loading the view from its nib.
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    _txtBidAmount.inputAccessoryView = numberToolbar;
    
    self.resultAry=[[NSMutableArray alloc]init];
    self.resultDiscussionAry=[[NSMutableArray alloc]init];
    //Call Job discussion Function
    
    selectedIndex = 0;
    isClicked = YES;
    isChatTextField = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReload:) name:@"NotificationReload" object:nil];
    [self registerForKeyboardNotifications];
    [self SetValueForSelectedArr];
    [self JobDiscussionandCompletionHandler:^(bool resultJobDiscussion) {
        if (resultJobDiscussion==YES) {
            NSLog(@"Completed Get Job Discussion Method");
        }
    }];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)SetValueForSelectedArr
{
    if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobstatus"]] isEqualToString:@"1"])
    {
        [_btnBidding setTitle:@"Bidding" forState:UIControlStateNormal];
        _lblJobDecisionTime.text = [NSString stringWithFormat:@"Job closes in %@",[_arrSelected valueForKeyPath:@"Remaining_time"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobstatus"]] isEqualToString:@"2"])
    {
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"notificationCont"]] intValue]>0)
        {
            badgeView = [[JSBadgeView alloc] initWithParentView:_viewNotification alignment:JSBadgeViewAlignmentTopCenter];
            [badgeView setBadgeBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
            [badgeView setBadgeTextFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?16.0f:12.0f]];
            badgeView.badgeText = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"notificationCont"]];
            badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
            badgeView.layer.masksToBounds = YES;
        }
        _viewLowestBid.hidden = YES;
        _btnRebid.hidden = NO;
        [_btnBidding setTitle:@"Bids" forState:UIControlStateNormal];
        
        NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"dateofbid"]];
        __block NSDate *detectedDate;
        //Detect.
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
        [detector enumerateMatchesInString:dateString
                                   options:kNilOptions
                                     range:NSMakeRange(0, [dateString length])
                                usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             detectedDate = result.date;
             NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
             NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
             
             NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
             NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
             NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
             
             NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
             
             NSLog(@"detectedDate:%@",detectedDate);
             NSLog(@"currentDate:%@",[NSDate date]);
             _lblJobDecisionTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
             NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
             
         }];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobstatus"]] isEqualToString:@"3"])
    {
        [_btnBidding setTitle:@"Hired" forState:UIControlStateNormal];
        _lblJobDecisionTime.hidden = YES;
        _viewSelectedContractor.hidden = NO;
        self.imgSelected.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"customerDetails.profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        self.lblNameSelected.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.username"]];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Lng"]] floatValue]];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             _lblAddressSelected.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
         }];
        
        
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]] isEqualToString:@"<null>"])
        {
            self.lblMeetingTimeSelected.text = @"Meeting not scheduled";
        }
        else
        {
            NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]];
            __block NSDate *detectedDate;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                 [dateFormate setDateFormat:@"MM-dd-YYYY"];
                 self.lblMeetingTimeSelected.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[_arrSelected valueForKeyPath:@"meetingTime"]];
             }];
        }
        
        strPhone = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Phone"]];
        strEmail = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Email"]];
        [_btnEmail setTitle:strEmail forState:UIControlStateNormal];
        [_btnCall setTitle:strPhone forState:UIControlStateNormal];
         _viewDiscussion.frame = CGRectMake(_viewDiscussion.frame.origin.x, _viewDiscussion.frame.origin.y, _viewDiscussion.frame.size.width, _viewDiscussion.frame.size.height-_viewSelectedContractor.frame.size.height);
        
        NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"hiredTimed"]];
        __block NSDate *detectedDate;
        //Detect.
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
        [detector enumerateMatchesInString:dateString
                                   options:kNilOptions
                                     range:NSMakeRange(0, [dateString length])
                                usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             detectedDate = result.date;
             NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
             NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
             
             NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
             NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
             NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
             
             NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
             
             NSLog(@"detectedDate:%@",detectedDate);
             NSLog(@"currentDate:%@",[NSDate date]);
             _lblJobDecisionTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
             NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
             
         }];
        _viewMsg.hidden = YES;
        _tblChatMsgs.frame = CGRectMake(_tblChatMsgs.frame.origin.x, _tblChatMsgs.frame.origin.y, _tblChatMsgs.frame.size.width, _tblChatMsgs.frame.size.height+_viewMsg.frame.size.height-5);
    }
    else
    {
        [_btnBidding setTitle:@"Closed" forState:UIControlStateNormal];
        _lblJobDecisionTime.hidden = YES;
        
        _imgCalendar.hidden = NO;
        _lblMeetingTimeSelected.hidden =NO;
        _viewStars.hidden = NO;

        _viewSelectedContractor.hidden = NO;
        
        
        
        
        
        
        self.imgSelected.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"customerDetails.profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        self.lblNameSelected.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.username"]];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Lng"]] floatValue]];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             _lblAddressSelected.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
         }];
        
        CGRect viewStarFrame = _viewStars.frame;
        
        viewStarFrame.origin.x = (IS_IPAD?CGRectGetMidX(_imgSelected.bounds):10 );
        //        viewStarFrame.origin.x = ((_imgSelected.frame.size.width+_imgSelected.frame.origin.x)/2) - (_viewStars.frame.size.width / 2);
        
        viewStarFrame.origin.y = self.imgSelected.frame.origin.y + self.imgSelected.frame.size.height + 8;
        
        _viewStars.frame = viewStarFrame;
        
        if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]] isEqualToString:@"<null>"])
        {
            self.lblMeetingTimeSelected.text = @"Meeting not scheduled";
        }
        else
        {
        
            
            NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"meetingDate"]];
            __block NSDate *detectedDate;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                 [dateFormate setDateFormat:@"MM-dd-YYYY"];
                 self.lblMeetingTimeSelected.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[_arrSelected valueForKeyPath:@"meetingTime"]];
             }];
        }

        
//        self.lblMeetingTimeSelected.text = [NSString stringWithFormat:@"%@ %@",[_arrSelected valueForKeyPath:@"meetingDate"],[_arrSelected valueForKeyPath:@"meetingTime"]];
        strPhone = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Phone"]];
        strEmail = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.Email"]];
        [_btnEmail setTitle:strEmail forState:UIControlStateNormal];
        [_btnCall setTitle:strPhone forState:UIControlStateNormal];
        _viewDiscussion.frame = CGRectMake(_viewDiscussion.frame.origin.x, _viewDiscussion.frame.origin.y, _viewDiscussion.frame.size.width, _viewDiscussion.frame.size.height-_viewSelectedContractor.frame.size.height);
        
        float totalRating = [[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"customerDetails.totalRating"]] floatValue];
       
        for (int i=1; i<=5; i++)
        {
            for (UIImageView *img in self.imgStars)
            {
                if (img.tag == 500+i)
                {
                    if (i<=totalRating)
                    {
                        img.image = [UIImage imageNamed:@"star-fill.png"];
                    }
                    else if ((i-0.5)==totalRating)
                    {
                        img.image = [UIImage imageNamed:@"half.png"];
                    }
                    else
                    {
                        img.image =[UIImage imageNamed:@"empty.png"];
                    }
                }
            }
        }

        NSString *dateString = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"closedTime"]];
        __block NSDate *detectedDate;
        //Detect.
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
        [detector enumerateMatchesInString:dateString
                                   options:kNilOptions
                                     range:NSMakeRange(0, [dateString length])
                                usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             detectedDate = result.date;
             NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
             NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
             
             NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
             NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
             NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
             
             NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
             
             NSLog(@"detectedDate:%@",detectedDate);
             NSLog(@"currentDate:%@",[NSDate date]);
             _lblJobDecisionTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
             NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
             
         }];
        _viewMsg.hidden = YES;
        _tblChatMsgs.frame = CGRectMake(_tblChatMsgs.frame.origin.x, _tblChatMsgs.frame.origin.y, _tblChatMsgs.frame.size.width, _tblChatMsgs.frame.size.height+_viewMsg.frame.size.height-5);
        [self.lblHeaderSelected setText:@"Congratulations you have been rated"];
    }
    
    _lblHeader.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobtypeName"]];
    _lblJobTitle.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobtypeName"]];
    
    _lblJobPinCode.text = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"zipcode"]];
    _lblNotToExceed.text = [NSString stringWithFormat:@"$%@",[_arrSelected valueForKeyPath:@"bidAmount"]];
    _lblLowestBid.text = [NSString stringWithFormat:@"$%@",[_arrSelected valueForKeyPath:@"minimumbidamount"]];
    _viewBidNow.layer.cornerRadius = 3.0;
    _viewBidNow.layer.masksToBounds = YES;
    _viewLowestBid.layer.cornerRadius = 3.0;
    _viewLowestBid.layer.masksToBounds = YES;
    _btnRebid.layer.cornerRadius = 3.0;
    _btnRebid.layer.masksToBounds = YES;
    NSString *milesRemining = [NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"Miles"]];
    if ([milesRemining integerValue]>1)
    {
        _lblCost.text = [NSString stringWithFormat:@"%@ miles from here",milesRemining];
    }
    else
    {
        _lblCost.text = [NSString stringWithFormat:@"< 1 mile from here"];
    }
    
    
    
    NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
    arrAnswers = [_arrSelected valueForKeyPath:@"jobanwserlist"];
    NSMutableArray *arrTags=[[NSMutableArray alloc]init];
    NSMutableArray *arrWithoutTags=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrAnswers.count; i++)
    {
        if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
        {
            if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
            {
                [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
            }
            else
            {
                [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
            }
            
        }
        else
        {
            [arrWithoutTags addObject:[NSString stringWithFormat:@"%@: %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"quest_name"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
        }
        
    }
    
    NSString *str;
    if (arrWithoutTags.count!=0)
    {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *strWithoutTags =[NSString stringWithFormat:@"%@",arrWithoutTags];
        strWithoutTags = [[strWithoutTags componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedStringAt = [strWithoutTags stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@"\n"];
        
        str=[NSString stringWithFormat:@"%@\n%@",[_arrSelected valueForKeyPath:@"description"],trimmedStringAt];
    }
    else
    {
        str=[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"description"]];
    }
    
    if ([[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobstatus"]] isEqualToString:@"2"]||[[NSString stringWithFormat:@"%@",[_arrSelected valueForKey:@"jobstatus"]] isEqualToString:@"1"])
    {
        if ([_arrSelected valueForKeyPath:@"minimumbidamount"] == (NSString *)[NSNull null])
        {
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",@"0"]];
            self.tagList.isHighlightLast = YES;
        }
        else
        {
            [arrTags addObject:[NSString stringWithFormat:@"Lowest Bid as of Now $%@",[_arrSelected valueForKeyPath:@"minimumbidamount"]]];
            self.tagList.isHighlightLast = YES;

        }
        
    }
    
    
    [self.tagList setAutomaticResize:YES];
    [self.tagList setTags:arrTags];
    [self.tagList setTagDelegate:self];
    
    CGFloat origin = self.tagList.frame.origin.y+self.tagList.frame.size.height;
    
    _lblDescriptionl.text = str;
//    [_lblDescriptionl sizeToFit];
    _lblDescriptionl.numberOfLines=0;
    
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?752:285, FLT_MAX);
    
    
    CGSize expectedLabelSize = [str sizeWithFont:_lblDescriptionl.font constrainedToSize:maximumLabelSize lineBreakMode:_lblDescriptionl.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = _lblDescriptionl.frame;
    newFrame.size.height = expectedLabelSize.height;
    newFrame.origin.y = origin+5;
    _lblDescriptionl.frame = newFrame;
    origin = self.lblDescriptionl.frame.origin.y+self.lblDescriptionl.frame.size.height;
    
    CGRect frame = _viewBidNow.frame;
    frame.origin.y = origin+5;
    _viewBidNow.frame = frame;
    
    CGRect frame1 = _viewLowestBid.frame;
    frame1.origin.y = origin+5;
    _viewLowestBid.frame = frame1;
    
    CGRect frame2 = _btnRebid.frame;
    frame2.origin.y = origin+5;
    _btnRebid.frame = frame2;
    
    CGRect frameViewDetails = _viewDetails.frame;
    frameViewDetails.size.height = _viewBidNow.frame.origin.y+_viewBidNow.frame.size.height+5;
    _viewDetails.frame = frameViewDetails;
    
    
    CGRect frameviewFullContent = _viewFullContent.frame;
    frameviewFullContent.size.height = _viewDetails.frame.origin.y+_viewDetails.frame.size.height+5;
    _viewFullContent.frame = frameviewFullContent;
    
    _scrlViewFullContent.contentSize=CGSizeMake(IS_IPAD?768:320, _viewFullContent.frame.size.height);
    _scrlViewFullContent.scrollEnabled = YES;
    
    
    
    NSString *imageCacheKey = [[_arrSelected valueForKeyPath:@"job_icon"]  stringByDeletingPathExtension];
    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
    {
        self.imgJobIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                UIImage *img=[UIImage imageWithData:data];
                if (img)
                {
                    [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                    self.imgJobIcon.image=img;
                }
                else
                {
                    NSLog(@"Not Found");
                }
            });
            
            
        });
        
    }
    
    
    
    NSString *imageCacheKey1 = [[_arrSelected valueForKeyPath:@"job_image"]  stringByDeletingPathExtension];
    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey1])
    {
        self.imgBlur.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey1];
        [self.imgBlur assignBlur];
    }
    else
    {
        self.imgBlur.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"job_image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        [self.imgBlur assignBlur];
        
//        self.imgBlur.AssignBlurToImage = @"YES";
//        
//        self.imgBlur.delegate = self;
        
        /*dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            
            NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelected valueForKeyPath:@"job_image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
            UIImage *img=[UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (img)
                {
                    [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey1];
                    self.imgBlur.image=img;
                    [self.imgBlur assignBlur];
                }
                else
                {
                    NSLog(@"Not Found");
                }
                
            });
        });*/
        
    }
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    else
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
        
    }
    [self settingVideoorImage:0];
    if ([_strGoto isEqualToString:@"Message"])
    {
        [self btnDiscussion:self];
    }
}

-(void)AssignBlurForTheImage
{
    [self.imgBlur assignBlur];
}

-(void)settingVideoorImage:(int)index
{
    self.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
    self.imgThumbNail.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imgThumbNail];
    selectedIndex=index;
    if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaType"] isEqualToString:@"1"])
    {
        NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            self.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                self.imgThumbNail.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.imgThumbNail.image)
                    {
                        [[ImageCache sharedImageCache] storeImage:self.imgThumbNail.image withKey:imageCacheKey];
                    }
                });
                
                
            });
            
        }
        
        
        self.imgPlay.hidden=YES;
        
    }
    else if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaType"] isEqualToString:@"2"])
    {
        self.imgPlay.hidden=NO;
        NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            self.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:index] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                generator.appliesPreferredTrackTransform = YES;
                NSError *error;
                CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                if (!error)
                {
                    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        self.imgThumbNail.image=image;
                        if (self.imgThumbNail.image)
                        {
                            [[ImageCache sharedImageCache] storeImage:self.imgThumbNail.image withKey:imageCacheKey];
                        }
                        
                    });
                }
            });
        }
    }
}
#pragma mark -UIDesign
-(void)UISetup
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        //lbl Font name and Size
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblHouseCleaning.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblOneStory.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblSqft.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblMaterials.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lbl2Bath.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lbl2Bed.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblCost.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblJobHeading.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblJobTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblJobPinCode.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        self.lblJobDecisionTime.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblDiscussion.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblNotToExceed.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblLowestBid.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblDescriptionl.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        //Btn font name and Size
        self.btnSend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.btnBids.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        //Textfiled font name and Size
        self.txtView.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtBidAmount.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.btnBidNow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        //BorderWidth
        self.lblHouseCleaning.layer.borderWidth=3.0;
        self.lblOneStory.layer.borderWidth=3.0;
        self.lblSqft.layer.borderWidth=3.0;
        self.lblMaterials.layer.borderWidth=3.0;
        self.lbl2Bath.layer.borderWidth=3.0;
        self.lbl2Bed.layer.borderWidth=3.0;
        //Border colour
        self.lblHouseCleaning.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblOneStory.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblSqft.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblMaterials.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bath.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner Radius
        
        self.lblHouseCleaning.layer.cornerRadius=15.0;
        self.lblOneStory.layer.cornerRadius=15.0;
        self.lblSqft.layer.cornerRadius=15.0;
        self.lblMaterials.layer.cornerRadius=15.0;
        self.lbl2Bath.layer.cornerRadius=15.0;
        self.lbl2Bed.layer.cornerRadius=15.0;
        self.btnSend.layer.cornerRadius=3.0;
        self.viewBids.layer.cornerRadius=15.0;
        self.btnBidNow.layer.cornerRadius=5.0;
        self.viewEnterAmount.layer.cornerRadius=5.0;
        _viewAmount.layer.borderColor=[[UIColor lightGrayColor] CGColor];
        _viewAmount.layer.cornerRadius = 3.0;
        _viewAmount.layer.borderWidth = 1.0;
        self.viewBidding.layer.cornerRadius=14.0;
        
        
        self.lblHeaderSelected.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblMeetingTimeSelected.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        _lblNameSelected.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        _lblAddressSelected.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        _btnEmail.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        _btnCall.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:25.0f];
        
        
        _btnRebid.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:30.0f];

        self.lblEnterYourZBid.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblNotToExceedAmtNew.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.tagList.font = [UIFont fontWithName:FONT_THIN size:20.0f];
    }
    else
    {
        //lbl Font name and Size
        self.lblHeader.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHouseCleaning.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblOneStory.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblSqft.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblMaterials.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lbl2Bath.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lbl2Bed.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblCost.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblJobHeading.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblJobTitle.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblJobPinCode.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblJobDecisionTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblDiscussion.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblDescriptionl.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblNotToExceed.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblLowestBid.font=[UIFont fontWithName:FONT_BOLD size:18.0f];

        //Btn Font name and size
        self.btnSend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.btnBids.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        
        self.txtView.font=[UIFont fontWithName:FONT_THIN size:16.0f];
        self.txtBidAmount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnBidNow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHouseCleaning.layer.borderWidth=1.0;
        //Border Width
        self.lblOneStory.layer.borderWidth=1.0;
        self.lblSqft.layer.borderWidth=1.0;
        self.lblMaterials.layer.borderWidth=1.0;
        self.lbl2Bath.layer.borderWidth=1.0;
        self.lbl2Bed.layer.borderWidth=1.0;
        //Border Colour
        self.lblHouseCleaning.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblOneStory.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblSqft.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lblMaterials.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bath.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.lbl2Bed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner Radius
        
        self.lblHouseCleaning.layer.cornerRadius=10.0;
        self.lblOneStory.layer.cornerRadius=10.0;
        self.lblSqft.layer.cornerRadius=10.0;
        self.lblMaterials.layer.cornerRadius=10.0;
        self.lbl2Bath.layer.cornerRadius=10.0;
        self.lbl2Bed.layer.cornerRadius=10.0;
        self.btnSend.layer.cornerRadius=3.0;
        self.viewBids.layer.cornerRadius=10.0;
        self.btnBidNow.layer.cornerRadius=5.0;
        self.viewEnterAmount.layer.cornerRadius=5.0;
        _viewAmount.layer.borderColor=[[UIColor lightGrayColor] CGColor];
        _viewAmount.layer.cornerRadius = 3.0;
        _viewAmount.layer.borderWidth = 1.0;
        self.viewBidding.layer.cornerRadius=10.0;
        
        _txtView.frame=CGRectMake(11, 5, 240, 0);
        
        
        
        self.lblHeaderSelected.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblMeetingTimeSelected.font = [UIFont fontWithName:FONT_BOLD size:16.0f];
        
        _lblNameSelected.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        _lblAddressSelected.font = [UIFont fontWithName:FONT_THIN size:18.0f];
        _btnEmail.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        _btnCall.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        
        
        [_btnEmail.titleLabel setFont: [UIFont fontWithName:FONT_BOLD size:18.0f]];
        _btnEmail.titleLabel.adjustsFontSizeToFitWidth = TRUE;
        _btnEmail.titleLabel.minimumFontSize = 12;
        
        _btnRebid.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:18.0f];
        
        self.lblEnterYourZBid.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblNotToExceedAmtNew.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        
    }
    
    _imgSelected.layer.cornerRadius = _imgSelected.frame.size.width/2;
    _imgSelected.layer.masksToBounds = YES;
    
    _btnEmail.layer.cornerRadius = 3.0f;
    _btnEmail.layer.masksToBounds = YES;
    
    _btnCall.layer.cornerRadius = 3.0f;
    _btnCall.layer.masksToBounds = YES;
    
    
    _txtView.placeholder=@"Ask for a clarification..";
    _txtView.placeholderColor = [UIColor darkGrayColor];
    _txtView.isScrollable = NO;
    _txtView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    _txtView.layer.cornerRadius=3.0;
    _txtView.minNumberOfLines = 1;
    _txtView.maxNumberOfLines = 3;
    _txtView.delegate = self;
    _txtView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    _viewMsg.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    _scrollView.contentSize=CGSizeMake(320, _viewDiscussion.frame.origin.y+_viewDiscussion.frame.size.height);
    _scrollView.scrollEnabled=YES;
    
    _btnBidding.titleLabel.adjustsFontSizeToFitWidth = TRUE;
    _btnBidding.titleLabel.minimumFontSize= 12;
}
#pragma mark - set Button Actions
-(IBAction)btnDiscussion:(id)sender
{
    self.btnUp.enabled=YES;
    
    //Action for Discussion Button
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGPoint point = CGPointMake(0, 830);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             //_viewUnderLine.frame=CGRectMake(0, 0, 360, 2);
                         }
         
                         completion:nil];
        
    }
    else if (IS_IPHONE5)
    {
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 464);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             //_viewUnderLine.frame=CGRectMake(0, 0, 146, 2);
                         }
                         completion:nil];
        
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGPoint point = CGPointMake(0, 380);
                             [_scrollView setContentOffset:point animated:YES];
                             CGPoint pointContent = CGPointMake(0, 0);
                             [_scrollViewContent setContentOffset:pointContent animated:YES];
                             //_viewUnderLine.frame=CGRectMake(0, 0, 146, 2);
                         }
                         completion:nil];
        
    }
    if (isNotifyCalled!=YES)
    {
        [self NotifyToWeb];
    }
}
- (IBAction)btnUp:(id)sender
{
    CGPoint point = CGPointMake(0,0);
    [_scrollView setContentOffset:point animated:YES];
}

-(IBAction)btnBackBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnBidNowTapped:(id)sender
{
    [self JobBiddingandCompletionHandler:^(bool resultJobBid) {
        if (resultJobBid==YES) {
            NSLog(@"COmpleted");
            
        }
    }];
}
-(IBAction)btnSendTapped:(id)sender
{
    _strQuestionName=_txtView.text;
    if (_strQuestionName.length!=0)
    {
        self.txtView.text=@"";
        [self resignTextView];
        [self JobDiscussionPostandCompletionHandler:^(bool resultJobDiscussionPost) {
            if (resultJobDiscussionPost==YES) {
                NSLog(@"Discussion Completed");
                
            }
        }];
    }
    
}
#pragma mark-ScrollView Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_scrollView)
    {
        CGFloat totalScroll = scrollView.contentSize.height - scrollView.bounds.size.height;
        CGFloat offset =  scrollView.contentOffset.y;
        CGFloat percentage = offset / totalScroll;
        NSLog(@"percentage:%f",percentage);
        self.btnUp.alpha = (0.f + percentage);
        if ((0.f + percentage)>=1)
        {
            if (isNotifyCalled!=YES)
            {
                [self NotifyToWeb];
            }
            
        }
    }
}

#pragma mark -UITableView Datasource and Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_resultJobDiscussionAry.count==0)
    {
        _tblChatMsgs.backgroundColor = [UIColor clearColor];
       return 1;
    }
    else
    {
         _tblChatMsgs.backgroundColor = [UIColor whiteColor];
       return _resultJobDiscussionAry.count;
       
    }
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (_resultJobDiscussionAry.count==0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.text = @"No Messages Found";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
        return cell;
    }
    else
    {
    
    static NSString *tblIdentifier=@"Cellname";
    
    OostaJobContractorBidNowTableViewCell *cell=(OostaJobContractorBidNowTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tblIdentifier];
    
    if (cell==nil) {
        
        NSArray *nib;
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBidNowTableViewCell IPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.viewContentAll.layer.cornerRadius = 5;//Corner radius for View
            cell.viewContentAll.layer.masksToBounds = YES;
            //Font name for labels
            cell.lblTblDispName.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
            cell.lblTblDispTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
            cell.lblTblDispMsg.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
            
            cell.lblCustomerName.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
            cell.lblCustomerTime.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
            cell.lblCstomerMsg.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
            
            //image description in table
            
            cell.imgTbl.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
            cell.imgTbl.layer.masksToBounds=YES;
            //temporary values for table
            
            cell.imgCustomer.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
            cell.imgCustomer.layer.masksToBounds=YES;
            
            
        }
        else
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBidNowTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            cell.viewContentAll.layer.cornerRadius = 5;//Corner radius for View
            cell.viewContentAll.layer.masksToBounds = YES;
            //Font name for labels
            cell.lblTblDispName.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
            cell.lblTblDispTime.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
            cell.lblTblDispMsg.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
            
            cell.lblCustomerName.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
            cell.lblCustomerTime.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
            cell.lblCstomerMsg.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
            
            //image description in table
            
            cell.imgTbl.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
            cell.imgTbl.layer.masksToBounds=YES;
            
            cell.imgCustomer.layer.cornerRadius=cell.imgTbl.frame.size.width/2.0;
            cell.imgCustomer.layer.masksToBounds=YES;
            
            //temporary values for table
            
            
        }
    }
    cell.lblTblDispMsg.text=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
    [cell.lblTblDispMsg sizeToFit];
    cell.lblTblDispMsg.numberOfLines = 0;
    NSString *dateString = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionTime"]];
    __block NSDate *detectedDate;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
    [detector enumerateMatchesInString:dateString
                               options:kNilOptions
                                 range:NSMakeRange(0, [dateString length])
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         detectedDate = result.date;
         NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
         NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
         
         NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
         NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
         NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
         
         NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
         NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
         [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
         cell.lblTblDispTime.text = [dateFormat stringFromDate:destinationDate];
         
         
     }];
    
    cell.lblTblDispName.text=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_name"]];
    cell.imgTbl.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgTbl.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgTbl];
//    NSString *imageCacheKey = [[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_profile"] stringByDeletingPathExtension];
//    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
//    {
//        cell.imgTbl.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    cell.imgTbl.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"contractor_profile"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (cell.imgTbl.image)
//                {
//                    [[ImageCache sharedImageCache] storeImage:cell.imgTbl.image withKey:imageCacheKey];
//                }
//            });
//            
//            
//        });
//        
//    }
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?667:229, FLT_MAX);
    CGSize expectedLabelSize = [str sizeWithFont:cell.lblTblDispMsg.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lblTblDispMsg.lineBreakMode];
    CGRect newFrame = cell.lblTblDispMsg.frame;
    newFrame.size.height = expectedLabelSize.height;
    cell.lblTblDispMsg.frame = newFrame;
    
    
    CGFloat origin = cell.lblTblDispMsg.frame.origin.y + cell.lblTblDispMsg.frame.size.height;
    CGFloat viewContentAllHeight = origin + 3;
    if ([NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]].length!=0)
    {
        cell.viewCustomer.hidden=NO;
        
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"customer_name"]];
        cell.lblCstomerMsg.text = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]];
        [cell.lblCstomerMsg sizeToFit];
        cell.lblCstomerMsg.numberOfLines = 0;
        
        NSString *dateString = [NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"RespondTime"]];
        __block NSDate *detectedDate;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
        [detector enumerateMatchesInString:dateString
                                   options:kNilOptions
                                     range:NSMakeRange(0, [dateString length])
                                usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             detectedDate = result.date;
             NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
             NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
             
             NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
             NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
             NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
             
             NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
             [dateFormat setDateFormat:@"MM-dd-YYYY hh:mm a"];
             cell.lblCustomerTime.text = [dateFormat stringFromDate:destinationDate];
             
             
         }];
        
        cell.imgCustomer.contentMode = UIViewContentModeScaleAspectFill;
        cell.imgCustomer.clipsToBounds = YES;
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgCustomer];
        NSString *imageCacheKey = [[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"customer_profile"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgCustomer.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                cell.imgCustomer.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"customer_profile"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell.imgCustomer.image)
                    {
                        [[ImageCache sharedImageCache] storeImage:cell.imgCustomer.image withKey:imageCacheKey];
                    }
                });
                
                
            });
            
        }
        CGRect customerFrame = cell.viewCustomer.frame;
        
        customerFrame.origin.y = origin + 5;
        
        NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]];
        CGSize maximumLabelSize = CGSizeMake(IS_IPAD?667:229, FLT_MAX);
        CGSize expectedLabelSize = [str sizeWithFont:cell.lblCstomerMsg.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lblCstomerMsg.lineBreakMode];
        CGRect newFrame = cell.lblCstomerMsg.frame;
        newFrame.size.height = expectedLabelSize.height;
        cell.lblCstomerMsg.frame = newFrame;
        CGFloat ht = cell.lblCstomerMsg.frame.origin.y + cell.lblCstomerMsg.frame.size.height+3;
        customerFrame.size.height = ht;
        cell.viewCustomer.frame = customerFrame;
        viewContentAllHeight = cell.viewCustomer.frame.origin.y+cell.viewCustomer.frame.size.height+3;
    }
    else
    {
        cell.viewCustomer.hidden=YES;
    }
    CGRect viewContentAllFrame = cell.viewContentAll.frame;
    viewContentAllFrame.size.height = viewContentAllHeight ;
    cell.viewContentAll.frame = viewContentAllFrame;
    return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_resultJobDiscussionAry.count==0)
    {
        return IS_IPAD?60:40;
    }
    else
    {
    
    static NSString *tblIdentifier=@"Cellname";
    
    OostaJobContractorBidNowTableViewCell *cell=(OostaJobContractorBidNowTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tblIdentifier];
    
    if (cell==nil) {
        
        NSArray *nib;
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBidNowTableViewCell IPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobContractorBidNowTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"questionName"]];
    CGSize maximumLabelSize = CGSizeMake(IS_IPAD?667:229, FLT_MAX);
    
    CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?23:13.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    
    
    CGFloat viewContentAllHeight = expectedLabelSize.height  + (IS_IPAD?70:41) + 3;
    if ([NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]].length!=0)
    {
        CGRect customerFrame = cell.viewCustomer.frame;
        customerFrame.origin.y =  expectedLabelSize.height+ (IS_IPAD?70:41)  + 3 + 5;
        NSString *str=[NSString stringWithFormat:@"%@",[[_resultJobDiscussionAry objectAtIndex:indexPath.row] valueForKeyPath:@"anwser"]];
        CGSize maximumLabelSize = CGSizeMake(IS_IPAD?667:229, FLT_MAX);
        CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?23.0:13.0f] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
        CGFloat ht = (IS_IPAD?70:41)  + expectedLabelSize.height+3;
        customerFrame.size.height = ht;
        viewContentAllHeight = customerFrame.origin.y+customerFrame.size.height+3;
    }
    
    
    
    [arrHeight replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%f",viewContentAllHeight]];
    return viewContentAllHeight;
        }
}
#pragma mark - Textfield Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    isChatTextField = NO;;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField==_txtBidAmount)
    {
        if (text.length>0)
        {
            [_btnBidNow setBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
            _btnBidNow.enabled=YES;
        }
        else
        {
            [_btnBidNow setBackgroundColor:[UIColor colorWithWhite:0.706 alpha:1.000]];
            _btnBidNow.enabled=NO;
        }
    }
    return YES;
}


-(void)doneWithNumberPad
{
    [self.txtBidAmount resignFirstResponder];
    CGPoint point = CGPointMake(0, 0);
    [self.scrlViewFullContent setContentOffset:point animated:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    if (isChatTextField == NO)
    {
        NSLog(@"isChatTextField:No");
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+14, 0.0);
        self.scrlViewFullContent.contentInset = contentInsets;
        self.scrlViewFullContent.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, _txtBidAmount.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, _txtBidAmount.frame.origin.y-kbSize.height);
            [self.scrlViewFullContent setContentOffset:scrollPoint animated:YES];
        }
    }
    else
    {
        NSLog(@"isChatTextField:YES");
        // get keyboard size and loctaion
        CGRect keyboardBounds;
        [[aNotification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
        NSNumber *duration = [aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSNumber *curve = [aNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
        
        // Need to translate the bounds to account for rotation.
        keyboardBounds = [self.viewDiscussion convertRect:keyboardBounds toView:nil];
        
        // get a rect for the textView frame
        CGRect containerFrame = _viewMsg.frame;
        containerFrame.origin.y = self.viewDiscussion.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
        // animations settings
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:[duration doubleValue]];
        [UIView setAnimationCurve:[curve intValue]];
        
        // set views with new info
        _viewMsg.frame = containerFrame;
        CGRect tblFrame = _tblChatMsgs.frame;
        tblFrame.size.height = _viewMsg.frame.origin.y;
        _tblChatMsgs.frame = tblFrame;
        
        // commit animations
        [self tableViewScrollToLastRow];
        [UIView commitAnimations];
        
        
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if (isChatTextField == NO)
    {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.scrlViewFullContent.contentInset = contentInsets;
        self.scrlViewFullContent.scrollIndicatorInsets = contentInsets;
    }
    else
    {
        NSNumber *duration = [aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSNumber *curve = [aNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
        
        // get a rect for the textView frame
        CGRect containerFrame = _viewMsg.frame;
        containerFrame.origin.y = self.viewDiscussion.bounds.size.height - containerFrame.size.height;
        
        // animations settings
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:[duration doubleValue]];
        [UIView setAnimationCurve:[curve intValue]];
        
        // set views with new info
        _viewMsg.frame = containerFrame;
        CGRect tblFrame = _tblChatMsgs.frame;
        tblFrame.size.height = _viewMsg.frame.origin.y;
        _tblChatMsgs.frame = tblFrame;
        // commit animations
        [self tableViewScrollToLastRow];
        [UIView commitAnimations];
    }
    
}

#pragma mark growingTextView
-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
    isChatTextField=YES;
}
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    CGRect r = _viewMsg.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    _viewMsg.frame = r;
    CGRect tblFrame = _tblChatMsgs.frame;
    tblFrame.size.height = _viewMsg.frame.origin.y;
    _tblChatMsgs.frame = tblFrame;
    [self tableViewScrollToLastRow];
}
-(void)resignTextView
{
    [_txtView resignFirstResponder];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
    CGPoint point = CGPointMake(0, 0);
    [self.scrlViewFullContent setContentOffset:point animated:YES];
}
#pragma mark-JobBiddingMethod
-(void)JobBiddingandCompletionHandler:(void (^)(bool resultJobBid))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Bidding...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    _strUserId=[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
    _strJobpostID=[_arrSelected valueForKeyPath:@"jobpostID"];
    NSString * key1 =@"userID";
    NSString * obj1 =_strUserId;
    
    NSString * key2 =@"jobpostID";
    NSString * obj2 =_strJobpostID;
    
    NSString * key3 =@"bidAmount";
    NSString * obj3 =_txtBidAmount.text;
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3]
                                   forKeys:@[key1,key2,key3]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@jobbidding",BaseURL]]];
    NSLog(@"jobbidding URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 self.resultAry=[result valueForKeyPath:@"JobDetails"];
                 NSLog(@"Value is:%@",self.resultAry);
                 if(_delegate && [_delegate respondsToSelector:@selector(getreload)])
                 {
                     [_delegate getreload];
                 }
                 [self.navigationController popViewControllerAnimated:YES];
                 [hud hide:YES];
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Bid Successfully";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
             }
             
             completionHandler(true);
         }
     }];
    
    }
    
}



#pragma mark-JobDiscussionPostMethod
-(void)JobDiscussionPostandCompletionHandler:(void (^)(bool resultJobDiscussionPost))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    _strUserId=[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
    _strJobpostID=[_arrSelected valueForKeyPath:@"jobpostID"];
    
    NSString * key1 =@"userID";
    NSString * obj1 =_strUserId;
    
    NSString * key2 =@"jobpostID";
    NSString * obj2 =_strJobpostID;
    
    NSString * key3 =@"questionName";
    NSString * obj3 =_strQuestionName;
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3]
                                   forKeys:@[key1,key2,key3]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@postdiscussion",BaseURL]]];
    NSLog(@"postdiscussion URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 self.resultDiscussionAry=[result valueForKeyPath:@"DiscussionDetails"];
                 NSLog(@"Value is:%@",self.resultDiscussionAry);
                 
                 [self JobDiscussionandCompletionHandler:^(bool resultJobDiscussion) {
                     if (resultJobDiscussion==YES) {
                         NSLog(@"Completed Get Job Discussion Method");
                     }
                 }];
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 
                 
                 
             }
             
             completionHandler(true);
         }
     }];
    
    }
    
}
#pragma mark-JobDiscussionMethod
-(void)JobDiscussionandCompletionHandler:(void (^)(bool resultJobDiscussion))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getforumlist/%@",BaseURL,[_arrSelected valueForKeyPath:@"jobpostID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    NSLog(@"GetJob Discussion List URL:%@",request.URL);
    
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             _lblDiscussion.text = [NSString stringWithFormat:@"Discussion"];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 self.resultJobDiscussionAry = [[NSMutableArray alloc]init];
                 self.resultJobDiscussionAry=[result valueForKeyPath:@"DiscustionDetails"];
                 NSLog(@"Value is:%@",self.resultDiscussionAry);
                 arrHeight = [[NSMutableArray alloc]init];
                 for (int i=0; i<[[result valueForKeyPath:@"DiscustionDetails"] count]; i++)
                 {
                     [arrHeight addObject:[NSString stringWithFormat:@"%d",0]];
                     _lblDiscussion.text = [NSString stringWithFormat:@"Discussion (%d)",i+1];
                 }
                 
                 
                 [_tblChatMsgs reloadData];
                 
                 [self tableViewScrollToLastRow];
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                  _lblDiscussion.text = [NSString stringWithFormat:@"Discussion"];
                 
             }
             
             completionHandler(true);
         }
     }];
    
    
    }
}
#pragma mark Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [[_arrSelected valueForKeyPath:@"jobmedialist"] count];
}


- (OostaJobMediaCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OostaJobMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgThumbNail.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgThumbNail];
    
    if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"1"])
    {
        NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                cell.imgThumbNail.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell.imgThumbNail.image)
                    {
                        [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                    }
                });
                
                
            });
            
        }
        
        
        cell.imgPlay.hidden=YES;
        cell.btnDelete.hidden=YES;
    }
    else if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaType"] isEqualToString:@"2"])
    {
        cell.imgPlay.hidden=NO;
        NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                NSString *imageCacheKey = [[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:indexPath.row] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                generator.appliesPreferredTrackTransform = YES;
                NSError *error;
                CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                if (!error)
                {
                    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.imgThumbNail.image=image;
                        if (cell.imgThumbNail.image)
                        {
                            [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                        }
                        
                    });
                }
            });
        }
        
        
        cell.btnDelete.hidden=YES;
    }
    
    cell.imgThumbNail.layer.cornerRadius=3.0;
    cell.imgThumbNail.layer.masksToBounds = YES;
    cell.layer.cornerRadius=3.0;
    cell.layer.masksToBounds = YES;
    cell.btnDidSelect.tag=indexPath.row;
    [cell.btnDidSelect addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)didSelect:(UIButton *)sender
{
    [self settingVideoorImage:sender.tag];
    
}

- (IBAction)btnPlayTapped:(id)sender
{
    
    if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:selectedIndex] valueForKey:@"mediaType"] isEqualToString:@"1"])
    {
        if (isClicked==NO)
        {
            isClicked=YES;
            _viewMedia.hidden = NO;
            _viewBidding.hidden = NO;
            _viewMedia.alpha = 0.1;
            _viewBidding.alpha = 0.1;
            [UIView animateWithDuration:0.25 animations:^{
                _viewMedia.alpha = 1.0f;
                _viewBidding.alpha = 1.0f;
                if (IS_IPAD)
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-130, 768, 130);
                    _viewBidding.frame =  CGRectMake(-15, 15, 115, 30);
                }
                else
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-50, 320, 50);
                    _viewBidding.frame =  CGRectMake(-15, 15, 103, 22);
                }
                
            } completion:^(BOOL finished) {
                
            }];
        }
        else
        {
            isClicked=NO;
            
            [UIView animateWithDuration:0.25 animations:^{
                if (IS_IPAD)
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
                }
                else
                {
                    _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                    _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
                }
                
                
                [_viewBidding setAlpha:0.1f];
                [_viewMedia setAlpha:0.1f];
            } completion:^(BOOL finished) {
                _viewMedia.hidden = YES;
                _viewBidding.hidden = YES;
            }];
            
        }
    }
    else if ([[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:selectedIndex] valueForKey:@"mediaType"] isEqualToString:@"2"])
    {
        _imgPlay.hidden=YES;
        
        isClicked=NO;
        
        [UIView animateWithDuration:0.25 animations:^{
            if (IS_IPAD)
            {
                _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 768, 0);
                _viewBidding.frame =  CGRectMake(-15, 15, 0, 30);
            }
            else
            {
                _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height, 320, 0);
                _viewBidding.frame =  CGRectMake(-15, 15, 0, 22);
            }
            
            [_viewBidding setAlpha:0.1f];
            [_viewMedia setAlpha:0.1f];
        } completion:^(BOOL finished) {
            _viewMedia.hidden = YES;
            _viewBidding.hidden = YES;
        }];
        
        //create a player
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _imgThumbNail.frame.size.height)];
        }
        else if (IS_IPHONE5)
        {
            self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
        }
        else
        {
            self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 320, _imgThumbNail.frame.size.height)];
        }
        
        self.moviePlayer.view.alpha = 0.f;
        self.moviePlayer.delegate = self; //IMPORTANT!
        
        //create the controls
        ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleDefault];
        movieControls.barHeight=IS_IPAD?130.0f:50.0f;
        //    [movieControls setAdjustsFullscreenImage:YES];
        [movieControls setBarColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
        [movieControls setTimeRemainingDecrements:YES];
        //[movieControls setFadeDelay:2.0];
        //[movieControls setBarHeight:100.f];
        //[movieControls setSeekRate:2.f];
        
        //assign controls
        [self.moviePlayer setControls:movieControls];
        [self.viewMediaPreview addSubview:self.moviePlayer.view];
        
        //THEN set contentURL
        
        NSURL *videoURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[_arrSelected valueForKeyPath:@"jobmedialist"] objectAtIndex:selectedIndex] valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self.moviePlayer setContentURL:videoURL];
        
        
        
        //delay initial load so statusBarOrientation returns correct value
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //        [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
            [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
                self.moviePlayer.view.alpha = 1.f;
            } completion:^(BOOL finished) {
                self.navigationItem.leftBarButtonItem.enabled = YES;
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }];
        });
        
        
        
    }
    
}
- (void)configureViewForOrientation:(UIInterfaceOrientation)orientation {
    CGFloat videoWidth = 0;
    CGFloat videoHeight = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        videoWidth = 700.f;
        videoHeight = 535.f;
    } else {
        videoWidth = self.view.frame.size.width;
        videoHeight = 220.f;
    }
    
    //calulate the frame on every rotation, so when we're returning from fullscreen mode we'll know where to position the movie plauyer
    self.defaultFrame = CGRectMake(self.view.frame.size.width/2 - videoWidth/2, self.view.frame.size.height/2 - videoHeight/2, videoWidth, videoHeight);
    
    //only manage the movie player frame when it's not in fullscreen. when in fullscreen, the frame is automatically managed
    if (self.moviePlayer.isFullscreen)
        return;
    
    //you MUST use [ALMoviePlayerController setFrame:] to adjust frame, NOT [ALMoviePlayerController.view setFrame:]
    [self.moviePlayer setFrame:self.defaultFrame];
}
//IMPORTANT!
- (void)moviePlayerWillMoveFromWindow {
    //movie player must be readded to this view upon exiting fullscreen mode.
    if (![self.view.subviews containsObject:self.moviePlayer.view])
        [self.view addSubview:self.moviePlayer.view];
    
    //you MUST use [ALMoviePlayerController setFrame:] to adjust frame, NOT [ALMoviePlayerController.view setFrame:]
    [self.moviePlayer setFrame:self.defaultFrame];
}

- (void)movieTimedOut {
    NSLog(@"MOVIE TIMED OUT");
}
- (BOOL)shouldAutorotate {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self configureViewForOrientation:toInterfaceOrientation];
}

- (void)movieFinishedNotification:(NSNotification *)note {
    [self.moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    
    isClicked=YES;
    _viewMedia.hidden = NO;
    _viewBidding.hidden = NO;
    _viewMedia.alpha = 0.1;
    _viewBidding.alpha = 0.1;
    [UIView animateWithDuration:0.25 animations:^{
        _viewMedia.alpha = 1.0f;
        _viewBidding.alpha = 1.0f;
        if (IS_IPAD)
        {
            _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-130, 768, 130);
            _viewBidding.frame =  CGRectMake(-15, 15, 115, 30);
        }
        else
        {
            _viewMedia.frame =  CGRectMake(0, _imgThumbNail.frame.size.height-50, 320, 50);
            _viewBidding.frame =  CGRectMake(-15, 15, 103, 22);
        }
        
    } completion:^(BOOL finished)
    {
        [self settingVideoorImage:selectedIndex];
    }];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"Finished" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"NotificationReload" object:nil];
    
}
- (IBAction)btnHomeTapped:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)tableViewScrollToLastRow
{
    if (_tblChatMsgs.contentSize.height > _tblChatMsgs.frame.size.height)
    {
        CGPoint offset = CGPointMake(0, _tblChatMsgs.contentSize.height -     _tblChatMsgs.frame.size.height);
        [_tblChatMsgs setContentOffset:offset animated:YES];
    }
}

- (IBAction)btnBiddingTapped:(id)sender
{
    
}

- (IBAction)btnEmailPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        //        [controller.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bg_iPhone.png"] forBarMetrics:UIBarMetricsDefault];
        //        controller.navigationBar.tintColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
        [controller setSubject:@""];
        [controller setMessageBody:@" " isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:strEmail,nil]];
        [self presentViewController:controller animated:YES completion:NULL];
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:@"Cant open mail composer" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
        [alert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    UIAlertView *MailAlert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:nil delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            MailAlert.message = @"Email Cancelled";
            break;
        case MFMailComposeResultSaved:
            MailAlert.message = @"Email Saved";
            break;
        case MFMailComposeResultSent:
            MailAlert.message = @"Email Sent";
            break;
        case MFMailComposeResultFailed:
            MailAlert.message = @"Email Failed";
            break;
        default:
            MailAlert.message = @"Email Not Sent";
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    MailAlert.tag = 1000;
    [MailAlert show];
}

- (IBAction)btnCallPressed:(id)sender
{
    NSString *phoneNumber = strPhone;
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
        [UIApplication.sharedApplication openURL:phoneUrl];
    } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl];
    } else {
        NSLog(@"Cant make call");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"OOstaJob" message:@"Cant make call" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] ;
        [alert show];
    }
}

#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"";
    
}

-(void)NotifyToWeb
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getnotificationCunt/%@/%@",BaseURL,[NSString stringWithFormat:@"%@",[_arrSelected valueForKeyPath:@"jobpostID"]],[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getnotificationCunt:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
            
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 isNotifyCalled =YES;
                 [badgeView removeFromSuperview];
                 if(_delegate && [_delegate respondsToSelector:@selector(getreload)])
                 {
                     [_delegate getreload];
                 }
             }
             else
             {
                 
                 
             }
         }
     }];
    
}

- (void)NotificationReload:(NSNotification *)note
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getforumlist/%@",BaseURL,[note.object valueForKeyPath:@"jobID"]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    NSLog(@"GetJob Discussion List URL:%@",request.URL);
    
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             _lblDiscussion.text = [NSString stringWithFormat:@"Discussion"];
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 self.resultJobDiscussionAry = [[NSMutableArray alloc]init];
                 self.resultJobDiscussionAry=[result valueForKeyPath:@"DiscustionDetails"];
                 NSLog(@"Value is:%@",self.resultDiscussionAry);
                 arrHeight = [[NSMutableArray alloc]init];
                 for (int i=0; i<[[result valueForKeyPath:@"DiscustionDetails"] count]; i++)
                 {
                     [arrHeight addObject:[NSString stringWithFormat:@"%d",0]];
                     _lblDiscussion.text = [NSString stringWithFormat:@"Discussion (%d)",i+1];
                 }
                 
                 
                 [_tblChatMsgs reloadData];
                 
                 [self tableViewScrollToLastRow];
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 _lblDiscussion.text = [NSString stringWithFormat:@"Discussion"];
                 
             }
             
             
         }
     }];
}

- (IBAction)btnRebidPressed:(id)sender
{
    _viewBidNowNew.hidden = NO;
}
- (IBAction)btnCloseTapped:(id)sender
{
    _viewBidNowNew.hidden = YES;
}

@end
