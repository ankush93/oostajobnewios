//
//  OostaJobListOfJobsTableViewCell.h
//  OostaJob
//
//  Created by Apple on 07/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobListOfJobsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblList;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgServices;

@property (strong, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UITextField *txtOthers;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *viewLine;

@end
