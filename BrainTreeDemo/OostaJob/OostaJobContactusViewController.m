//
//  OostaJobContactusViewController.m
//  OostaJob
//
//  Created by Armor on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContactusViewController.h"
#import "MVPopView.h"

@interface OostaJobContactusViewController ()<UITextViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong)MVPopView *popView;
@property NSArray *fruits;
@property CZPickerView *pickerWithImage;
@end

@implementation OostaJobContactusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fruits = @[@"Contractor didn’t show up.", @"Contractor didn’t complete the job.", @"Contractor didn’t honor the Not to Exceed price.", @"The scope of job changed after the on-site visit.", @"Contractor can not meet my deadline."];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"CZPicker";
    _popView = [[MVPopView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 400)];
    _popView.backgroundColor = [UIColor lightGrayColor];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 100, 40)];
    [btn addTarget:self action:@selector(hideBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"Done" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor redColor];
    [_popView addSubview:btn];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden=YES;
    _txtemail.text = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Email"]?[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Email"]:@"";
    [_imgBlur assignBlur];
    [self setupUI];
    [self addingToolBarToTextView];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    
   
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addingToolBarToTextView
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    _txtMessage.inputAccessoryView = numberToolbar;
}
-(void)doneWithNumberPad
{
    if (_txtMessage.text.length==0)
    {
        _txtMessage.text = @"Message";
    }
    [self.txtMessage resignFirstResponder];
    CGPoint point = CGPointMake(0, 0);
    [self.scrlContactus setContentOffset:point animated:YES];
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.btnsend.layer.cornerRadius=5.0;
        self.btnsend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        _txtname.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        _txtemail.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        _txtMessage.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        
        
        _viewName.layer.borderWidth = 1;
        _viewName.layer.borderColor = [[UIColor whiteColor] CGColor];
        _viewName.layer.cornerRadius=5.0f;
        _viewName.layer.masksToBounds=YES;
        
        _viewEmail.layer.borderWidth = 1;
        _viewEmail.layer.borderColor = [[UIColor whiteColor] CGColor];
        _viewEmail.layer.cornerRadius=5.0f;
        _viewEmail.layer.masksToBounds=YES;
        
        _txtMessage.layer.borderWidth = 1;
        _txtMessage.layer.borderColor = [[UIColor whiteColor] CGColor];
        _txtMessage.layer.cornerRadius=5.0f;
        _txtMessage.layer.masksToBounds=YES;
        
        
    }
    else
    {
        self.btnsend.layer.cornerRadius=5.0;
        self.btnsend.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        _txtname.font=[UIFont fontWithName:FONT_THIN size:14.0f];
        _txtemail.font=[UIFont fontWithName:FONT_THIN size:14.0f];
        _txtMessage.font=[UIFont fontWithName:FONT_THIN size:14.0f];
        
        
        _viewName.layer.borderWidth = 1;
        _viewName.layer.borderColor = [[UIColor whiteColor] CGColor];
        _viewName.layer.cornerRadius=3.0f;
        _viewName.layer.masksToBounds=YES;
        
        _viewEmail.layer.borderWidth = 1;
        _viewEmail.layer.borderColor = [[UIColor whiteColor] CGColor];
        _viewEmail.layer.cornerRadius=3.0f;
        _viewEmail.layer.masksToBounds=YES;
        
        _txtMessage.layer.borderWidth = 1;
        _txtMessage.layer.borderColor = [[UIColor whiteColor] CGColor];
        _txtMessage.layer.cornerRadius=3.0f;
        _txtMessage.layer.masksToBounds=YES;
    }
    
}
#pragma mark - TextView Delegate Methods
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_txtMessage.text isEqualToString:@"Message"])
    {
        _txtMessage.text = @"";
    }
    
    CGPoint scrollPoint = CGPointMake(0.0,textView.frame.origin.y);
    [self.scrlContactus setContentOffset:scrollPoint animated:YES];
    return YES;
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString* Str=@"";
    Str=[[textView text] stringByAppendingString:text];
    
    if(Str.length == 0)
    {
        _txtMessage.text = @"Message";
    }
    
    return YES;
}
- (IBAction)btnSend:(id)sender
{
    [self.scrlContactus endEditing:YES];
    CGPoint scrollPoint = CGPointMake(0.0,0.0);
    [self.scrlContactus setContentOffset:scrollPoint animated:YES];
    if ([_txtMessage.text isEqualToString:@""])
    {
        _txtMessage.text = @"Message";
    }
    
    [self PostContactusandCompletionHandler:^(bool resultPostContactus) {
        if (resultPostContactus == YES)
        {
            NSLog(@"PostContactus Completed");
        }
    }];
}
-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


-(void)PostContactusandCompletionHandler:(void (^)(bool resultPostContactus))completionHandler
{
    if ((_txtMessage.text.length==0||[_txtMessage.text isEqualToString:@"Message"])&&_txtname.text.length==0&&_txtemail.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Fill all details";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
        
    }
    else if (_txtname.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter name";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
    }
    else if (![self IsValidEmail:_txtemail.text])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter valid email";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
    }
    else if (_txtMessage.text.length==0||[_txtMessage.text isEqualToString:@"Message"])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter message";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Posting Message...";
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            
            
            NSString * key1 =@"userID";
            NSString * obj1 =[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]?[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]:@"";
            
            NSString * key2 =@"name";
            NSString * obj2 =_txtname.text;
            
            NSString * key3 =@"email_id";
            NSString * obj3 =_txtemail.text;
            
            NSString * key4 =@"msg";
            NSString * obj4 =_txtMessage.text;
            
            NSString * key5 =@"id";
            NSString * obj5 =@"";
            
            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3,obj4,obj5]
                                           forKeys:@[key1,key2,key3,key4,key5]];
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@contactusinsert",BaseURL]]];
            NSLog(@"contactusinsert URL:%@",request.URL);
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     
                     NSLog(@"Server Error : %@", error);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(true);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"result"]isEqualToString:@"Success"])
                     {
                         
                         
                         
                         [hud hide:YES];
                         MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         [self.navigationController.view addSubview:hud];
                         
                         hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         hud.mode = MBProgressHUDModeCustomView;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         hud.delegate = self;
                         hud.labelText = @"Posted Successfully";
                         [hud show:YES];
                         [hud hide:YES afterDelay:2];
                         
                         _txtemail.text = @"";
                         _txtname.text = @"";
                         _txtMessage.text = @"";
                         if (_txtMessage.text.length==0)
                         {
                             _txtMessage.text = @"Message";
                         }
                     }
                     else
                     {
                         NSLog(@"%@",SERVER_ERR);
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = SERVER_ERR;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                         
                     }
                     
                     completionHandler(true);
                 }
             }];
        }
    }
    
}
- (IBAction)btnMenu:(id)sender
{
    [self.scrlContactus endEditing:YES];
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

#pragma mark - Textfield Delegate Methods
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0.0,textField.frame.origin.y+0.8*textField.frame.size.height);
    [self.scrlContactus setContentOffset:scrollPoint animated:YES];
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtname)
    {
        [self.txtemail becomeFirstResponder];
    }
    else if (textField==_txtemail)
    {
        [self.txtMessage becomeFirstResponder];
    }
    
    return YES;
}
- (void) scrollViewAdaptToStartEditingTextField:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, textField.frame.origin.y +1 * textField.frame.size.height);
    [self.scrlContactus setContentOffset:point animated:YES];
}
- (void) scrollVievEditingFinished:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, 0);
    [self.scrlContactus setContentOffset:point animated:YES];
}
- (IBAction)Onclickpopup:(id)sender {
  //  Popupcontroller *popupObj = [[Popupcontroller alloc]init];
//   popupObj.view.frame = CGRectMake(5, 200, 350, 220);
//    [self.view addSubview:popupObj.view];
//    [self addChildViewController:popupObj];
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Fruits" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Confirm"];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    [picker show];
   // [_popView showInView:self.view];

}
- (void)hideBtnTapped {
    [_popView dismiss];
}


/* comment out this method to allow
 CZPickerView:titleForRow: to work.
 */
- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:self.fruits[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return self.fruits[row];
}

- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row {
    
    return nil;
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    return self.fruits.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row {
    NSLog(@"%@ is chosen!", self.fruits[row]);
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    for (NSNumber *n in rows) {
        NSInteger row = [n integerValue];
        NSLog(@"%@ is chosen!", self.fruits[row]);
    }
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    [self.navigationController setNavigationBarHidden:YES];
    NSLog(@"Canceled.");
}

- (void)czpickerViewWillDisplay:(CZPickerView *)pickerView {
    NSLog(@"Picker will display.");
}

- (void)czpickerViewDidDisplay:(CZPickerView *)pickerView {
    NSLog(@"Picker did display.");
}

- (void)czpickerViewWillDismiss:(CZPickerView *)pickerView {
    NSLog(@"Picker will dismiss.");
}

- (void)czpickerViewDidDismiss:(CZPickerView *)pickerView {
    NSLog(@"Picker did dismiss.");
}

@end
