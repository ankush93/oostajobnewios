//
//  ContractorHistoryController.h
//  OostaJob
//
//  Created by Ankush on 22/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContractorHistoryTableViewCell.h"
@interface ContractorHistoryController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *historytable;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *img;
- (IBAction)OnclickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (nonatomic) BOOL isPresented; 

@end
