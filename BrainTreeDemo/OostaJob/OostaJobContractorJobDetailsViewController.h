//
//  OostaJobPaintingsViewController.h
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALMoviePlayerController.h"
#import "DWTagList.h"
#import "HPTextViewInternal.h"
#import "HPGrowingTextView.h"
@protocol OostaJobContractorJobDetailsViewControllerDelegate <NSObject>
@optional
- (void)getreload;
@end


@interface OostaJobContractorJobDetailsViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate,DWTagListDelegate,HPGrowingTextViewDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UILabel *lblHouseCleaning;
@property (weak, nonatomic) IBOutlet UILabel *lblOneStory;
@property (weak, nonatomic) IBOutlet UILabel *lblSqft;
@property (weak, nonatomic) IBOutlet UILabel *lbl2Bed;
@property (weak, nonatomic) IBOutlet UILabel *lbl2Bath;
@property (weak, nonatomic) IBOutlet UILabel *lblMaterials;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblJobHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblJobDecisionTime;
@property (weak, nonatomic) IBOutlet UILabel *lblJobPinCode;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscussion;
@property (weak, nonatomic) IBOutlet UILabel *lblNotToExceed;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *viewEnterAmount;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIButton *btnUp;
@property (weak, nonatomic) IBOutlet UIButton *btnBids;
@property (weak, nonatomic) IBOutlet UIButton *btnBidNow;
@property (weak, nonatomic) IBOutlet UITextField *txtBidAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionl;
@property (weak, nonatomic) IBOutlet UITextField *txtClarifications;
@property (weak, nonatomic) IBOutlet UIView *viewBids;
@property (weak, nonatomic) IBOutlet UIView *viewKeybd;
@property (strong, nonatomic)NSString *strUserId;
@property (strong, nonatomic)NSString *strJobpostID;
@property (strong, nonatomic)NSString *strQuestionName;

@property (strong, nonatomic)NSMutableArray *resultAry;
@property (strong, nonatomic)NSMutableArray *resultDiscussionAry;
@property (strong, nonatomic)NSMutableArray *resultJobDiscussionAry;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic)NSMutableArray *arrSelected;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgThumbNail;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
- (IBAction)btnPlayTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *viewMedia;
@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;
@property (nonatomic) CGRect defaultFrame;

@property (weak, nonatomic) IBOutlet UIView *viewMediaPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgJobIcon;
@property (weak, nonatomic) IBOutlet DWTagList *tagList;
@property (weak, nonatomic) IBOutlet UIView *viewBidNow;
@property (weak, nonatomic) IBOutlet UIView *viewDetails;
@property (weak, nonatomic) IBOutlet UIView *viewFullContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlViewFullContent;
@property (weak, nonatomic) IBOutlet UIView *viewAmount;
@property (weak, nonatomic) IBOutlet UIView *viewDiscussion;
@property (weak, nonatomic) IBOutlet HPGrowingTextView *txtView;
@property (weak, nonatomic) IBOutlet UIView *viewMsg;
@property (weak, nonatomic) IBOutlet UITableView *tblChatMsgs;
@property (weak, nonatomic) IBOutlet UIButton *btnHomeTapped;
- (IBAction)btnHomeTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewBidding;
@property (weak, nonatomic) IBOutlet UIButton *btnBidding;
- (IBAction)btnBiddingTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *viewSelectedContractor;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSelected;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblNameSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingTimeSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;

@property (strong, nonatomic)NSString *strGoto;
- (IBAction)btnEmailPressed:(id)sender;
- (IBAction)btnCallPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCalendar;
@property (weak, nonatomic) IBOutlet UIView *viewStars;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UIView *viewNotification;
@property (weak, nonatomic) IBOutlet UIView *viewLowestBid;
@property (weak, nonatomic) IBOutlet UILabel *lblLowestBid;
@property (weak, nonatomic) IBOutlet UIButton *btnRebid;
- (IBAction)btnRebidPressed:(id)sender;

@property (nonatomic, assign)   id<OostaJobContractorJobDetailsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *viewBidNowNew;
@property (weak, nonatomic) IBOutlet UIView *viewBidNowContent;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterYourZBid;
@property (weak, nonatomic) IBOutlet UILabel *lblNotToExceedAmtNew;
- (IBAction)btnCloseTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBidNowNew;
- (IBAction)btnBidNowTapped:(id)sender;

@end
