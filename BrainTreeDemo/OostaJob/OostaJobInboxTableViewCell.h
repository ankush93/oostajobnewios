//
//  OostaJobInboxTableViewCell.h
//  OostaJob
//
//  Created by Armor on 23/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobInboxTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTblDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@end
