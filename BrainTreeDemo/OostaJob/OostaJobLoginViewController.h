//
//  OostaJobLoginViewController.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobLoginViewController : UIViewController<MBProgressHUDDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnForgetPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusPassword;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
- (IBAction)btnBackTapped:(id)sender;
- (IBAction)btnLoginTapped:(id)sender;
- (IBAction)btnForgetPasswordTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewForgetPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterYourRegisteredEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtForgetEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClosePopup;
- (IBAction)btnClosePopupTapped:(id)sender;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (strong, nonatomic) NSString *strPage;
@end
