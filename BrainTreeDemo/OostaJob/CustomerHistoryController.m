//
//  CustomerHistoryController.m
//  OostaJob
//
//  Created by Ankush on 21/02/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import "CustomerHistoryController.h"
#import "ImageCache.h"
#import <BraintreeCore/BraintreeCore.h>
#import <BraintreePayPal/BraintreePayPal.h>
#import <BraintreeCard/BraintreeCard.h>
@interface CustomerHistoryController () <BTAppSwitchDelegate, BTViewControllerPresentingDelegate,SWRevealViewControllerDelegate,BTDropInViewControllerDelegate,BTAppSwitchDelegate,MBProgressHUDDelegate,DWTagListDelegate>
{
    NSMutableDictionary *dictJobList;
    NSString * strClientToken;
    CGFloat origin1;
    NSMutableArray *tableViewCells;
}

@property (nonatomic, strong) BTAPIClient *braintreeClient;

@end

@implementation CustomerHistoryController
@synthesize scrollview,historytable;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = YES;
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self CreateBlurEffectOnBackgroundImage];
    [self CustomerPaymentHistory:^(bool resultJobList){
        NSLog(@"data fetched");
    }];
    

    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
 //   NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
//    if ([brain objectForKey:@"BrainTreeid"] == NULL || [brain objectForKey:@"BrainTreeid"] == nil)
//    {
//        NSLog(@"NULL in HiStory");
//    }
//    else
//    {
//        [self getClientToken];
//
//    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [UIViewController attemptRotationToDeviceOrientation];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Initialization code
       // [[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
        
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        
    }
    return self;
}
#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"less than min ago";
}

-(void)CustomerPaymentHistory:(void (^)(bool resultJobList))completionHandler{
#pragma mark getJobList
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading....";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@myPaymentHistory/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];
        
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getcustomerjoblist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     dictJobList=[[NSMutableDictionary alloc]init];
                     [dictJobList setObject:[result valueForKeyPath:@"response"] forKey:@"response"];
                     
                     

                     if ([[result valueForKeyPath:@"response"] count] == 0) {
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = @"No Transactions found";
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                     }
                     
                     NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"response"] count]]];
                     
                     
                     
                     //                         NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                     //
                     //                         if ([[sortedArr lastObject] intValue]>=1)
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"CUSTOMERJOBS" forKey:@"SCREEN"];
                     //                         }
                     //                         else
                     //                         {
                     //                             [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     //                         }
                     //                         [self reloadTableViews];
                     //
                     //                         if (isHireTab == YES){
                     //                             [self.tblHire reloadData];
                     //                         }
                     [historytable reloadData];
                     
                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Failure"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"No History found";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                   
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[dictJobList valueForKeyPath:@"response"] count]!=0)
    {
    return 1;
    }
    return 0;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

        if ([[dictJobList valueForKeyPath:@"response"] count]!=0)
        {
            return [[dictJobList valueForKeyPath:@"response"] count];
        }
        else
        {
            return 1;
        }

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"CELL";
    tableView.backgroundColor=[UIColor clearColor];



        if ([[dictJobList valueForKeyPath:@"response"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }

            cell.textLabel.text = @"No Jobs in Closed";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            CustomerHistoryTableViewCell *cell=(CustomerHistoryTableViewCell*)[historytable dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;



                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomerHistoryTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size


                [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];


            cell.pinlbl.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"contractordetails.Zipcode"]];

                cell.paidamount.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.amount"]];
                
                cell.paidstatus.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.transaction_for"]];
                
                cell.contractorname.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"contractordetails.Name"]];
                
                cell.phonenumber.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"contractordetails.Phone"]];
                cell.email.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"contractordetails.Email"]];
                
                NSLog(@"job list in history:%@",[NSString stringWithFormat:@"%@",[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row]]);
                
                NSLog(@"job list in history:%@",[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row]valueForKeyPath:@"contractordetails.Zipcode"]]);

            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.jobpostTime"]];




            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
            NSString *str1 =[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",dateString]];
            NSString* strName = [[str1 componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString *ct_title = [strName stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
            ct_title = [ct_title stringByReplacingOccurrencesOfString:@",    " withString:@", "];



            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];

                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;

                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];

                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);

                 cell.lbltime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);

             }];

            //            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];

           


            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"answer"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                }
            }
            //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];

                [cell.taglist setTags:arrTags];

                [cell.taglist setAutomaticResize:YES];
            [cell.taglist setTagDelegate:self];
            cell.taglist.userInteractionEnabled = NO;

                
                NSLog(@"cell.taglist in jobs:%@",cell.taglist);
          
//            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];

            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"media"] mutableCopy];
                origin1 = cell.taglist.frame.origin.y+cell.taglist.frame.size.height;
            CGFloat origin = cell.taglist.frame.origin.y+cell.taglist.frame.size.height;


            //        NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            //        arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            //        if (arrContractor.count>0)
            //        {

            //        }
            //        else
            //        {
            //            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
            //            cell.lblContratorCount.textColor = [UIColor lightGrayColor];
            //        }
cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
                [cell CollectionData:[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"media"] andtheIndexPath:indexPath andTheTableView:tableView];
                cell.labelname.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobtypeName"]];
                NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.job_icon"] stringByDeletingPathExtension];
                if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
                {
                    cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
                }
                else
                {
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                                   {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"job_icons.job_icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                           UIImage *img=[UIImage imageWithData:data];
                                           if (img)
                                           {
                                               [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                               cell.imgIcon.image=img;
                                           }
                                       });

                                   });

                }
                

                
                
                

            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            origin = origin+(IS_IPAD?0:0);

                
                cell.dynamicview.frame = CGRectMake(cell.dynamicview.frame.origin.x, origin+(IS_IPAD?5:3), cell.dynamicview.frame.size.width, cell.dynamicview.frame.size.height);


                NSArray *reasonarray= [[NSArray alloc]init];
                NSString *reasonstring = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.reason"]];
                NSLog(@"reasonstring.length :%lu",(unsigned long)reasonstring.length);
                if ([reasonstring  isEqual: @"<null>"]|| [reasonstring  isEqual:[NSNull null]] || [reasonstring  isEqual:nil]) {
                    cell.cancelreasons.hidden = YES;
                    cell.jobcancelheading.hidden = YES;
                    cell.cancelview.hidden=YES;
                    cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.dynamicview.frame.origin.y+50+cell.dynamicview.frame.size.height-cell.cancelreasons.frame.size.height);
                    
                    cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:10));
                }
                else
                {
                    reasonarray = [reasonstring componentsSeparatedByString:@","];
                    
                    cell.jobcancelheading.hidden = NO;
                    cell.cancelreasons.hidden = NO;
                    cell.cancelview.hidden=NO;
                    
                    cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.dynamicview.frame.origin.y+80+cell.dynamicview.frame.size.height+cell.cancelreasons.frame.size.height);
                    
                    cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:10));
                }
                NSLog(@"reasons array :%@",reasonarray);
                [cell.cancelreasons setTags:reasonarray];
                //  }
                
                [cell.cancelreasons setAutomaticResize:YES];
                [cell.cancelreasons setTagDelegate:self];
                cell.cancelreasons.userInteractionEnabled = NO;
                
                
               
//            [cell CollectionData:[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.media"] andtheIndexPath:indexPath andTheTableView:tableView];
//            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.jobicon"] stringByDeletingPathExtension];
//            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
//            {
//                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
//            }
//            else
//            {
//                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
//                               {
//                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//                                       UIImage *img=[UIImage imageWithData:data];
//                                       if (img)
//                                       {
//                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
//                                           cell.imgIcon.image=img;
//                                       }
//                                   });
//
//
//                               });
//
//            }
//            cell.btnMessage.tag = indexPath.row;
//            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
//            cell.btnContractor.tag = indexPath.row;
//            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
//            cell.backgroundColor=[UIColor clearColor];
//
//            cell.imgArrow.hidden = YES;
//            cell.btnTblBidding.userInteractionEnabled = NO;
//
//
//            cell.viewStars.frame=CGRectMake(cell.viewStars.frame.origin.x, cell.viewContractor.frame.origin.y, cell.viewStars.frame.size.width, cell.viewStars.frame.size.height);
//            float totalRating = [[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.totalRating"]] floatValue];
//            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
//
//            NSLog(@"check star rating :%f",totalRating);
//            if ( totalRating == 0) {
//                cell.viewStars.hidden =YES;
//                cell.ratenowbtn.hidden = NO;
//            }
//            else{
//                cell.viewStars.hidden =NO;
//                cell.ratenowbtn.hidden = YES;
//
//            }
//            for (int i=1; i<=5; i++)
//            {
//                for (UIImageView *img in cell.imgStars)
//                {
//                    if (img.tag == 500+i)
//                    {
//                        if (i<=totalRating)
//                        {
//                            img.image = [UIImage imageNamed:@"star-fill.png"];
//                        }
//                        else if ((i-0.5)==totalRating)
//                        {
//                            img.image = [UIImage imageNamed:@"half.png"];
//                        }
//                        else
//                        {
//                            img.image =[UIImage imageNamed:@"empty.png"];
//                        }
//                    }
//                }
//            }
//        }
            }

    return cell;

    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"CELL";

    CustomerHistoryTableViewCell *cell=(CustomerHistoryTableViewCell*)[historytable dequeueReusableCellWithIdentifier:tblIdentifier];
    
    NSLog(@"reasons cehck in height :%@",cell.cancelreasons);
    
    
        NSArray *nib;
        
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomerHistoryTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
     
    
    
    NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
    arrAnswers = [[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"answer"];
    NSMutableArray *arrTags=[[NSMutableArray alloc]init];
    for (int i=0; i<arrAnswers.count; i++)
    {
        if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
        {
            if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
            {
                [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
            }
            else
            {
                [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
            }
        }
    }
    
    NSLog(@"taglist check :%@",arrTags);
    
    NSArray *reasonarray= [[NSArray alloc]init];
    NSString *reasonstring = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"response"] objectAtIndex:indexPath.row] valueForKeyPath:@"response.reason"]];
    NSLog(@"reasonstring.length :%lu",(unsigned long)reasonstring.length);
    if ([reasonstring  isEqual: @"<null>"]|| [reasonstring  isEqual:[NSNull null]] || [reasonstring  isEqual:nil]) {
        cell.cancelreasons.hidden = YES;
        cell.jobcancelheading.hidden = YES;
        cell.cancelview.hidden=YES;
        
    }
    else
    {
        reasonarray = [reasonstring componentsSeparatedByString:@","];
        
        cell.jobcancelheading.hidden = NO;
        cell.cancelreasons.hidden = NO;
        cell.cancelview.hidden=NO;

    }
    NSLog(@"reasons array :%@",reasonarray);
    
    
   
   
    //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
    
    
    [cell.taglist setAutomaticResize:YES];
    [cell.taglist setTags:arrTags];
    [cell.taglist setTagDelegate:self];
    
    [cell.cancelreasons setAutomaticResize:YES];
    [cell.cancelreasons setTags:reasonarray];
    [cell.cancelreasons setTagDelegate:self];
    NSLog(@"cell.taglist :%@",cell.taglist);
    NSLog(@"check height done :%f",cell.taglist.frame.size.height+cell.taglist.frame.origin.y);
    
    
    int height = 0;
    if ([reasonstring  isEqual: @"<null>"]|| [reasonstring  isEqual:[NSNull null]] || [reasonstring  isEqual:nil]) {
        height=cell.taglist.frame.size.height+cell.taglist.frame.origin.y+(IS_IPAD?350:350);

        return height;
    }
    else
    {
        height=cell.taglist.frame.size.height+cell.cancelreasons.frame.size.height+450;

        return height;

    }
    
    

}

-(void)CreateBlurEffectOnBackgroundImage
{
    [self.img assignBlur];
}


- (IBAction)OnclickMenu:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}


- (IBAction)Onclickmenu:(id)sender {
}


-(void)getClientToken
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Getting Client Token...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
        
        NSString *key1 = @"braintreeID";
        NSString *obj1 = [brain objectForKey:@"BrainTreeid"];
        
        NSLog(@"Brain tree :%@",[brain objectForKey:@"BrainTreeid"]);
        
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getclientaccesstoken",BaseURL]]];
        NSLog(@"getclientaccesstoken:%@",request.URL);
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"result"] isEqualToString:@"Success"])
                 {
                     
                     [hud hide:YES];
                     if (!TARGET_IPHONE_SIMULATOR)
                         self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]]];
                     strClientToken = [NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]];
                     
                     [self showDropIn:strClientToken];
                     //[self fetchExistingPaymentMethod:strClientToken];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
         }];
    }
}

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewController:(BTDropInViewController *)viewController
  didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce
{
    // Send payment method nonce to your server for processing
    NSLog(@"paymentMethodNonce:%@",paymentMethodNonce.nonce);
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showDropIn:(NSString *)clientTokenOrTokenizationKey {
    BTDropInRequest *request = [[BTDropInRequest alloc] init];
    request.applePayDisabled = YES;

    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:clientTokenOrTokenizationKey request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"ERROR");
        } else if (result.cancelled) {
            [self dismissViewControllerAnimated:YES completion:nil];
            NSLog(@"CANCELLED");
        } else {
            // Use the BTDropInResult properties to update your UI
            // result.paymentOptionType
            // result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
        }
    }];
    [self presentViewController:dropIn animated:YES completion:nil];
}


- (void)fetchExistingPaymentMethod:(NSString *)clientToken {
    [BTDropInResult fetchDropInResultForAuthorization:clientToken handler:^(BTDropInResult * _Nullable result, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"ERROR");
        } else {
            // Use the BTDropInResult properties to update your UI
            // result.paymentOptionType
            // result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
        }
    }];
}



@end
