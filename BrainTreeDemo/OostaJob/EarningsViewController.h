//
//  EarningsViewController.h
//  OostaJob
//
//  Created by Ankush on 17/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OostaJobContractorJobsViewController.h"
#import "ContractorHistoryController.h"
@interface EarningsViewController : UIViewController <MBProgressHUDDelegate>
- (IBAction)OnclickJobHistory:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menureference;
@property (weak, nonatomic) IBOutlet UIButton *Onclickmenu;
- (IBAction)Onclickmenubtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *jobhistorybtn;
@property (weak, nonatomic) IBOutlet UILabel *amountlbl;
@property (weak, nonatomic) IBOutlet UILabel *monthlbl;
@property (weak, nonatomic) IBOutlet UILabel *totalEarningslbl;

@end
