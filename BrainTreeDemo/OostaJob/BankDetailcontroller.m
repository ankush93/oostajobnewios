//
//  BankDetailcontroller.m
//  OostaJob
//
//  Created by Ankush on 17/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import <BraintreeDropIn/BraintreeDropIn.h>
#import <BraintreeCore/BraintreeCore.h>

#import "BankDetailcontroller.h"

@interface BankDetailcontroller ()
{
    UIImageView *tick;
    UIImageView *tick1;
    UIImageView *tick2;
    UIImageView *tick3;
    KLCPopup* popup;
    NSString *clientToken;
}
@end

@implementation BankDetailcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self.imgBlur assignBlur];
    clientToken = @"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIxMmMxYmY0N2RkZTJkYTdhNjI1MmJmOThkMGJiMDQ1MmY5ZGViODhmZmJjMGVmNzU4ZjcyYzZlMWZkM2JhYzQ1fGNyZWF0ZWRfYXQ9MjAxOC0wMS0yMVQxNToxNDo1MC4xNjE2OTA5NDcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=";

    
    [self registerForKeyboardNotifications];
    self.scrollview.contentSize=CGSizeMake(320, 500);

    
    
    UIImageView *imgbankname=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [imgbankname setImage:[UIImage imageNamed:@"banks.png"]];
    [imgbankname setContentMode:UIViewContentModeCenter];
    self.bankname.leftView=imgbankname;
    self.bankname.leftViewMode=UITextFieldViewModeAlways;
    
    UIImageView *imgaccountname=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [imgaccountname setImage:[UIImage imageNamed:@"man-user.png"]];
    [imgaccountname setContentMode:UIViewContentModeCenter];
    self.accountname.leftView=imgaccountname;
    self.accountname.leftViewMode=UITextFieldViewModeAlways;
    
    UIImageView *imgaccountnumber=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [imgaccountnumber setImage:[UIImage imageNamed:@"Group10.png"]];
    [imgaccountnumber setContentMode:UIViewContentModeCenter];
    self.accountnumber.leftView=imgaccountnumber;
    self.accountnumber.leftViewMode=UITextFieldViewModeAlways;
    
    UIImageView *imgrouting=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [imgrouting setImage:[UIImage imageNamed:@"routing_number_icon.png"]];
    [imgrouting setContentMode:UIViewContentModeCenter];
    self.routing.leftView=imgrouting;
    self.routing.leftViewMode=UITextFieldViewModeAlways;
    
    
    self.accountnumber.delegate = self;
    self.accountname.delegate = self;
    self.bankname.delegate = self;
    self.routing.delegate = self;

   tick=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [tick setImage:[UIImage imageNamed:@"routing_number_icon.png"]];
    [tick setContentMode:UIViewContentModeScaleAspectFit];
    
    tick1=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [tick1 setImage:[UIImage imageNamed:@"routing_number_icon.png"]];
    [tick1 setContentMode:UIViewContentModeScaleAspectFit];
    
    tick2=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [tick2 setImage:[UIImage imageNamed:@"routing_number_icon.png"]];
    [tick2 setContentMode:UIViewContentModeScaleAspectFit];
    
    tick3=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [tick3 setImage:[UIImage imageNamed:@"routing_number_icon.png"]];
    [tick3 setContentMode:UIViewContentModeScaleAspectFit];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    self.navigationController.navigationBarHidden=YES;

//    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
//                                   initWithTitle:@"Flip"
//                                   style:UIBarButtonItemStyleBordered
//                                   target:self
//                                   action:@selector(flipView:)];
    
    
    [self.view addGestureRecognizer:tap];
    
    if([self.passage isEqualToString:@"fromregister"]){
        self.menureference.hidden = YES;

    }
    else
    {
        self.menureference.hidden = NO;

        SWRevealViewController *revealController = [self revealViewController];
        
        [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
        [self.view addGestureRecognizer:revealController.panGestureRecognizer];

        
        self.skipreference.hidden = YES;
    }
    
    
    [self Bankdetail:^(bool banklist){
        NSLog(@"data fetched");
        if (banklist == YES) {
            NSUserDefaults *accountname = [NSUserDefaults standardUserDefaults];
            NSLog(@"account name :%@",[accountname valueForKeyPath:@"bankfetch.account_holder_name"]);
            
            NSMutableString * status1 = [[NSMutableString alloc] init];

            for (NSObject * obj in [accountname valueForKeyPath:@"bankfetch.account_holder_name"])
            {
                [status1 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", status1);
            self.accountname.text = status1;
            
            
            NSMutableString * status2 = [[NSMutableString alloc] init];
            
            for (NSObject * obj in [accountname valueForKeyPath:@"bankfetch.account_number"])
            {
                [status2 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", status2);

            self.accountnumber.text = status2;
            
            NSMutableString * status3 = [[NSMutableString alloc] init];
            
            for (NSObject * obj in [accountname valueForKeyPath:@"bankfetch.bank_name"])
            {
                [status3 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", status3);

            
            self.bankname.text = status3;
            
            NSMutableString * status4 = [[NSMutableString alloc] init];
            
            for (NSObject * obj in [accountname valueForKeyPath:@"bankfetch.routing"])
            {
                [status4 appendString:[obj description]];
            }
            NSLog(@"The concatenated string is %@", status4);
            self.routing.text = status4;
            
        }
        
        
    }];
   
    
    
}



-(void)transparentbar
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}



-(IBAction)Menu
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}


-(void)viewDidLayoutSubviews
{
    self.bankname.layer.cornerRadius=5.0f;
    self.accountnumber.layer.cornerRadius=5.0f;
    self.routing.layer.cornerRadius=5.0f;
    self.accountname.layer.cornerRadius=5.0f;
    self.submit.layer.cornerRadius=5.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.bankname){
        if(text.length > 0){
            
            self.bankname.rightView=tick;
            self.bankname.rightViewMode=UITextFieldViewModeAlways;
            tick.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            tick.image=[UIImage imageNamed:@""];
        }
        
    }
    
    if (textField == self.accountname){
        if(text.length > 0){
            self.accountname.rightView=tick1;
            self.accountname.rightViewMode=UITextFieldViewModeAlways;
            tick1.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            tick1.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField == self.accountnumber){
        if(text.length > 0){
            self.accountnumber.rightView=tick2;
            self.accountnumber.rightViewMode=UITextFieldViewModeAlways;
            tick2.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            tick2.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField == self.routing){
        if(text.length > 0){
            self.routing.rightView=tick3;
            self.routing.rightViewMode=UITextFieldViewModeAlways;
            tick3.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            tick3.image=[UIImage imageNamed:@""];
        }
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)dismissKeyboard {
    [self.accountname resignFirstResponder];
    [self.bankname resignFirstResponder];
    [self.accountnumber resignFirstResponder];
    [self.routing resignFirstResponder];

}
-(void)gotoEmailVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController IPad" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    
    
}
- (IBAction)OnclickSkip:(id)sender {
    [self gotoContractorJobList];

}
- (IBAction)OnclickSubmit:(id)sender {
    if([self.bankname.text  isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your Bank name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];    }
    else if([self.accountname.text  isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your Account name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if([self.accountnumber.text  isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your Account number"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if([self.routing.text  isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your routing number"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        [self addBankdetails];
       // [self gotoEmailVerification];

    }
    
}

#pragma mark - Add Bank Details api
-(void)addBankdetails{
    NSString *userid;
    NSUserDefaults *userid1 = [NSUserDefaults standardUserDefaults];
    userid = [userid1 objectForKey:@"Userid"];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;

            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Sending...";
            hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
            hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];

            hud.margin = 10.f;
            hud.yOffset = 20.f;
            [hud show:YES];


            NSString * key1 =@"contractor_id";
            NSString * obj1 = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
            NSString * key2 =@"bank_name";
            NSString * obj2 = self.bankname.text;
            NSString * key3 =@"account_holder_name";
            NSString * obj3 = self.accountname.text;
            NSString * key4 =@"routing";
            NSString * obj4 = self.routing.text;
            NSString * key5 =@"account_number";
            NSString * obj5 = self.accountnumber.text;
            
            NSLog(@"cont id :%@",obj1);
            NSLog(@"cont id :%@",obj2);
            NSLog(@"cont id :%@",obj3);
            NSLog(@"cont id :%@",obj4);
            NSLog(@"cont id :%@",obj5);

            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3,obj4,obj5]
                                           forKeys:@[key1,key2,key3,key4,key5]];

            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];

            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];

            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@addBankDetails",BaseURL]]];
            NSLog(@"url of bank details :%@",request.URL);
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];

            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     NSLog(@"Server Error : %@", error);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKey:@"Result"] isEqualToString:@"Success"] || [[result valueForKey:@"Result"] isEqualToString:@"success"] )
                     {
                         [hud hide:YES];
                         MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         HUD.minSize = CGSizeMake(300, 100);
                         [self.navigationController.view addSubview:HUD];

                         HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         HUD.mode = MBProgressHUDModeCustomView;
                         HUD.color=[UIColor whiteColor];
                         HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         HUD.delegate = self;
                         HUD.mode = MBProgressHUDModeIndeterminate;

                         HUD.labelText = @"Bank account info updated successfully, please click on jobs on the main menu to continue bidding";
                         HUD.detailsLabelText = @"Wait 3 seconds";

                        
                         [HUD show:YES];
                         [HUD hide:YES afterDelay:2];
                         //**** Saving Bank Details ****//
                         NSUserDefaults *ac = [NSUserDefaults standardUserDefaults];
                         [ac setObject:_accountname.text forKey:@"accountholdername"];
                         [ac synchronize];
                         
                         NSUserDefaults *an = [NSUserDefaults standardUserDefaults];
                         [an setObject:_accountnumber.text forKey:@"accountnumber"];
                         [an synchronize];
                         
                         NSUserDefaults *bank = [NSUserDefaults standardUserDefaults];
                         [bank setObject:_bankname.text forKey:@"bankname"];
                         [bank synchronize];
                         
                         NSUserDefaults *ro = [NSUserDefaults standardUserDefaults];
                         [ro setObject:_routing.text forKey:@"routing"];
                         [ro synchronize];
                         
                         [popup dismissPresentingPopup];
                         
                         
                        // [self Gotoverification];
                         if([self.passage isEqualToString:@"fromregister"]){
                             [self gotoContractorJobList];
                         }
                         else
                         {
                             
                         }
                         
                     }
                     else
                     {
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];

                         hud.labelText = @"Account number already exist";
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                     }


                 }
             }];
        }
    }

-(void)gotoContractorJobList
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobMapViewController *ContractorfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    else
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    
    ContractorRearViewController *ContraRearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ContractorfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:ContraRearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}



-(void)Gotoverification{
    if([self.passage isEqualToString:@"fromregister"]){
        [self gotoEmailVerification];
    }
    else
    {
        
    }
}
- (IBAction)OnclickMenu:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
- (IBAction)Onclickbarbuttonmenu:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

- (IBAction)Onclickaddcard:(id)sender {
    [self showDropIn:clientToken];
}

- (void)showDropIn:(NSString *)clientTokenOrTokenizationKey {
    BTDropInRequest *request = [[BTDropInRequest alloc] init];
    request.paypalDisabled = YES;
    request.applePayDisabled = YES;
    request.venmoDisabled = YES;
    request.threeDSecureVerification = YES;
    request.amount = @"1.00";
    BTDropInController *dropIn = [[BTDropInController alloc] initWithAuthorization:clientTokenOrTokenizationKey request:request handler:^(BTDropInController * _Nonnull controller, BTDropInResult * _Nullable result, NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"ERROR");
        } else if (result.cancelled) {
            NSLog(@"CANCELLED");
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            // Use the BTDropInResult properties to update your UI
             //result.paymentOptionType
            // result.paymentMethod
            // result.paymentIcon
            // result.paymentDescription
        }
    }];
    [self presentViewController:dropIn animated:YES completion:nil];
}
+ (void)fetchExistingPaymentMethod:(NSString *)clientToken {
    [BTDropInResult fetchDropInResultForAuthorization:clientToken handler:^(BTDropInResult * _Nullable result, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"ERROR");
        } else {
            // Use the BTDropInResult properties to update your UI
            NSLog(@"Payment method%@", result.paymentMethod);
            NSLog(@"Payment Description :%@", result.paymentDescription);
            NSLog(@"Payment option type :%ld", (long)result.paymentOptionType);
        }
    }];
}


-(void)Bankdetail:(void (^)(bool banklist))completionHandler{
#pragma mark Getbankdetails
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading....";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getContractorBankDetails/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];
        
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getbankdetails:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     
                     
                     NSUserDefaults *acholder = [NSUserDefaults standardUserDefaults];
                     [acholder setObject:[result valueForKeyPath:@"Response"] forKey:@"bankfetch"];
                     [acholder synchronize];
                    
                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"Details"] isEqualToString:@"Contactor Bank Details not found"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"Please enter bank Details";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
    
}





- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+14, 0.0);
    self.scrollview.contentInset = contentInsets;
    self.scrollview.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _routing.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, _routing.frame.origin.y-kbSize.height);
        [self.scrollview setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollview.contentInset = contentInsets;
    self.scrollview.scrollIndicatorInsets = contentInsets;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}

#pragma mark - Textfield Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_bankname)
    {
        
        NSLog(@"Open Map");
        [_bankname resignFirstResponder];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    else if (textField==_accountname)
    {
        NSLog(@"Open Expertise");
        [_accountname resignFirstResponder];
        
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
        
    }
    else if (textField==_routing)
    {
        [_routing becomeFirstResponder];
    }
    else if (textField==_routing)
    {
        [_routing becomeFirstResponder];
    }
    //else if (textField==_txtHourlyRate)
    //{
    //    [_txtHourlyRate resignFirstResponder];
    //    [self btnLicensePressed:self];
    //}
    else
    {
        [self.view endEditing:YES];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    
    return YES;
}


@end
