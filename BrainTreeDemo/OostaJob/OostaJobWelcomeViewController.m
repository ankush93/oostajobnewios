//
//  OostaJobWelcomeViewController.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobWelcomeViewController.h"
#import "OostaJobLoginViewController.h"
#import "OostaJobRegisterViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "SWRevealViewController.h"
@interface OostaJobWelcomeViewController ()<SWRevealViewControllerDelegate>

@end

@implementation OostaJobWelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden=YES;
    NSArray *fontFamilies = [UIFont familyNames];
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
    
    [self setButtonStyles];
    [self settingTheBackgroudImages];
    [self setupUI];
    [self comeFromMenu];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"USERDETAILS"];
    [self removeWindows];
    
}
-(void)removeWindows
{
    
    for (int i =0; i<[[[UIApplication sharedApplication] windows] count]; i++)
    {
        if (i!=0)
        {
            UIWindow * currentWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:i];
            [currentWindow resignKeyWindow];
        }
        else
        {
            UIWindow * currentWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:i];
            currentWindow.backgroundColor = [UIColor clearColor];
            [currentWindow makeKeyAndVisible];
        }
    }
}
-(void)comeFromMenu
{
    if ([_strPage isEqualToString:@"Customer"])
    {
        CGRect frame = self.scrollview.frame;
        frame.origin.x = frame.size.width * 1;
        frame.origin.y = 0;
        [self.scrollview scrollRectToVisible:frame animated:YES];
        [self.pagecontrol setCurrentPage:1];
    }
    else if ([_strPage isEqualToString:@"Contractor"])
    {
        CGRect frame = self.scrollview.frame;
        frame.origin.x = frame.size.width * 2;
        frame.origin.y = 0;
        [self.scrollview scrollRectToVisible:frame animated:YES];
        [self.pagecontrol setCurrentPage:2];
    }
}
#pragma mark - Setup UI
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblMainTitle.font=[UIFont fontWithName:FONT_BOLD size:80.0f];
        self.lblContractorHeader1.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblContractorHeader2.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblContractorHeader3.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblContractorHeader4.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblContractorHeader5.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblCustomerHeader1.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblCustomerHeader2.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblCustomerHeader3.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblContractorSub1.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblContractorSub2.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblContractorSub3.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblContractorSub4.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblContractorSub5.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblCustomerSub1.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblCustomerSub2.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblCustomerSub3.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnSkipLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        
        self.btnLogin.layer.cornerRadius=5.0;
        self.btnRegister.layer.cornerRadius=5.0;
        self.btnLogin.layer.masksToBounds=YES;
        self.btnRegister.layer.masksToBounds=YES;
    }
    else
    {
        self.lblMainTitle.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorHeader1.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorHeader2.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorHeader3.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorHeader4.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorHeader5.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblCustomerHeader1.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblCustomerHeader2.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblCustomerHeader3.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblContractorSub1.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblContractorSub2.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblContractorSub3.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblContractorSub4.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblContractorSub5.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblCustomerSub1.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblCustomerSub2.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblCustomerSub3.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
    }
    
    self.lblCustomerHeader1.text = @"Submit a Job Request";
    self.lblCustomerHeader2.text = @"Compare quotes and ratings";
    self.lblCustomerHeader3.text = @"Hire the right Contractor";
    
    self.lblCustomerSub1.text = @"Take a video of your project and just answer few questions about the job detail, then post the job. We’ll send it to pre-approved contractors in your zip code. ";
    self.lblCustomerSub2.text = @"After 24 hours you’ll receive up to 5 job specific quotes. Compare pricing, rating, and contractor profiles.";
    self.lblCustomerSub3.text = @"Select the right contractor. At this point we will ask for your credit card info and hold it in our vault. There will be no charges on the account at this point. Make an appoint right through the app or just wait for the contractor’s call to meet and start the project.";
    
    self.lblContractorHeader1.text = @"Specify your field of work";
    self.lblContractorHeader2.text = @"Bid on Jobs";
    self.lblContractorHeader3.text = @"Get Hired";
    self.lblContractorHeader6.text = @"Get the job Done and Get paid";
    self.lblContractorHeader4.text = @"Update your calendar and have appointments made";
    self.lblContractorHeader5.text = @"Strive for high ratings and more jobs";
    
    self.lblContractorSub1.text = @"When you register, select your expertise. We’ll verify your account by verifying your identity or license if applicable within 24 hrs.  We also ask for your banking information just so we can pay you for the completed jobs. You can always enter the bank info later.";
    
    self.lblContractorSub2.text = @"You’ll automatically receive jobs in your field to bid on every day.  You can also search for jobs by zip code and the type of job. Bid on jobs and rebid within the first 24 hours. We show the lowest bid amount to all contractors. Every job also has a discussion board where you can ask for more details or pictures or videos on the job prior to bidding.";
    
    self.lblContractorSub3.text = @"Once you’re selected by the end user – with your competitive pricing- we will communicate the customer contact information to you. Make sure to call and make an appointment as soon as possible.";
    self.lblContractorSub4.text = @"Once you complete the job make sure you mark the job as completed on the app. We will pay you within 48 hours minus the transaction processing fee and commission of 10%.";
    self.lblContractorSub5.text = @"The customer will make an appointment for the job through the calendar. All you have to do is to update your calendar.";
    self.lblContractorSub6.text = @"Every job is rated by the end user. the higher the rating you get the mopre jobs you will be assigned to";
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set Button Styles
-(void)setButtonStyles
{
    [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    [self.btnSkipLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
    
    self.btnLogin.layer.cornerRadius=5.0;
    self.btnRegister.layer.cornerRadius=5.0;
    self.btnLogin.layer.masksToBounds=YES;
    self.btnRegister.layer.masksToBounds=YES;
    
    
    
}

#pragma mark - Background Walkthrough
-(void)settingTheBackgroudImages
{
    self.scrollview.pagingEnabled = YES;
    
    self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height);
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.scrlCustomer.contentSize=CGSizeMake(self.view.frame.size.width , 2100);
        self.scrlContractor.contentSize=CGSizeMake(self.view.frame.size.width , 3100);
    }
    else if(IS_IPHONE5)
    {
        self.scrlCustomer.contentSize=CGSizeMake(self.view.frame.size.width , 1500);
        self.scrlContractor.contentSize=CGSizeMake(self.view.frame.size.width , 2400);
    }
    else
    {
        self.scrlCustomer.contentSize=CGSizeMake(self.view.frame.size.width , 1800);
        self.scrlContractor.contentSize=CGSizeMake(self.view.frame.size.width , 2100);
    }
    //we set the origin to the 1rd page
    CGPoint scrollPoint = CGPointMake(self.view.frame.size.width * 0, 0);
    //change the scroll view offset the the 3rd page so it will start from there
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    
    
}
//scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    //find the page number you are on
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(page>=1)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    [self.pagecontrol setCurrentPage:page];
}

//dragging ends, please switch off paging to listen for this event
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *) targetContentOffset
NS_AVAILABLE_IOS(5_0){
    
    //find the page number you are on
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    NSLog(@"Dragging - You are now on page %i",page);
    
}

#pragma mark - set Button Actions
- (IBAction)btnLoginTapped:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController IPad" bundle:nil];
        [self.navigationController pushViewController:LoginObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController" bundle:nil];
        [self.navigationController pushViewController:LoginObj animated:YES];
    }
    else
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController Small" bundle:nil];
        [self.navigationController pushViewController:LoginObj animated:YES];
    }
    
}

- (IBAction)btnRegisterTapped:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController IPad" bundle:nil];
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController" bundle:nil];
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
    else
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController Small" bundle:nil];
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
}

- (IBAction)pageControlTapped:(id)sender
{
    CGRect frame = self.scrollview.frame;
    frame.origin.x = frame.size.width * _pagecontrol.currentPage;
    frame.origin.y = 0;
    [self.scrollview scrollRectToVisible:frame animated:YES];
}
- (IBAction)btnMenu:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

-(void)gotoListOfJobs
{
    
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (IBAction)btnSkipLoginPressed:(id)sender
{
    [self gotoListOfJobs];
}
@end
