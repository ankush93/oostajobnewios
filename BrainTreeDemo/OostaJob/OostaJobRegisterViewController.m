//
//  OostaJobRegisterViewController.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobRegisterViewController.h"
#import "OostaJobCustomerRegisterationViewController.h"
#import "OostaJobContractorRegisterViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "SWRevealViewController.h"
@interface OostaJobRegisterViewController ()<SWRevealViewControllerDelegate>

@end

@implementation OostaJobRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self.imgBlur assignBlur];
    [self setupUI];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI
-(void)setupUI
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        
        [self.btnIamContractor.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnINeedAService.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
    }
    else
    {
        [self.btnIamContractor.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnINeedAService.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    }
    self.viewCustomer.layer.cornerRadius=5.0;
    self.viewContractor.layer.cornerRadius=5.0;
    self.viewCustomer.layer.masksToBounds=YES;
    self.viewContractor.layer.masksToBounds=YES;
    self.btnIamContractor.layer.cornerRadius=5.0;
    self.btnIamContractor.layer.masksToBounds=YES;
    self.btnINeedAService.layer.cornerRadius=5.0;
    self.btnINeedAService.layer.masksToBounds=YES;
    
    
    
    
}

#pragma mark - Button Actions
- (IBAction)btnCustomerTapped:(id)sender
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {OostaJobCustomerRegisterationViewController *CustRegObj=[[OostaJobCustomerRegisterationViewController alloc]initWithNibName:@"OostaJobCustomerRegisterationViewController IPad" bundle:nil];
        [self.navigationController pushViewController:CustRegObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobCustomerRegisterationViewController *CustRegObj=[[OostaJobCustomerRegisterationViewController alloc]initWithNibName:@"OostaJobCustomerRegisterationViewController" bundle:nil];
        [self.navigationController pushViewController:CustRegObj animated:YES];
    }
    else
    {
        OostaJobCustomerRegisterationViewController *CustRegObj=[[OostaJobCustomerRegisterationViewController alloc]initWithNibName:@"OostaJobCustomerRegisterationViewController Small" bundle:nil];
        [self.navigationController pushViewController:CustRegObj animated:YES];
    }
    
}

- (IBAction)btnContractorTapped:(id)sender
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobContractorRegisterViewController *ConRegObj=[[OostaJobContractorRegisterViewController alloc]initWithNibName:@"OostaJobContractorRegisterViewController IPad" bundle:nil];
        [self.navigationController pushViewController:ConRegObj animated:YES];

    }
    else if (IS_IPHONE5)
    {
        OostaJobContractorRegisterViewController *ConRegObj=[[OostaJobContractorRegisterViewController alloc]initWithNibName:@"OostaJobContractorRegisterViewController" bundle:nil];
        [self.navigationController pushViewController:ConRegObj animated:YES];

//        BankDetailcontroller *bank = [[BankDetailcontroller alloc]initWithNibName:@"BankDetailcontroller" bundle:nil];
//        [self.navigationController pushViewController:bank animated:YES];


    }
    else
    {
        OostaJobContractorRegisterViewController *ConRegObj=[[OostaJobContractorRegisterViewController alloc]initWithNibName:@"OostaJobContractorRegisterViewController Small" bundle:nil];
        [self.navigationController pushViewController:ConRegObj animated:YES];
    }
    
   
}

- (IBAction)btnBackTapped:(id)sender
{
    if (![self.strPage isEqualToString:@"CUSTOMER"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self gotoListOfJobs];
    }
}
-(void)gotoListOfJobs
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    UINavigationController *viewControllerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.window.rootViewController = viewControllerNavigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"SKIPLOGIN"] isEqualToString:@"SKIPLOGIN"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SKIPLOGIN"];
        [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] goToBuildYourJob];
    }
    
}

@end
