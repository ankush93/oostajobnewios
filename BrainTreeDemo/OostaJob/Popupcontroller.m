//
//  Popupcontroller.m
//  OostaJob
//
//  Created by Ankush on 31/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import "Popupcontroller.h"

@interface Popupcontroller ()

@end

@implementation Popupcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHorizontalListWithImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createHorizontalListWithImage {
    TNImageRadioButtonData *reason1 = [TNImageRadioButtonData new];
    reason1.labelText = @"Contractor didn’t show up.";
    reason1.identifier = @"Contractor didn’t show up.";
    reason1.selected = YES;
    reason1.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason1.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason2 = [TNImageRadioButtonData new];
    reason2.labelText = @"Contractor didn’t complete the job.";
    reason2.identifier = @"Contractor didn’t complete the job.";
    reason2.selected = NO;
    reason2.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason2.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason3 = [TNImageRadioButtonData new];
    reason3.labelText = @"Contractor didn’t honor the Not to Exceed price.";
    reason3.identifier = @"Contractor didn’t honor the Not to Exceed price.";
    reason3.selected = NO;
    reason3.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason3.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason4 = [TNImageRadioButtonData new];
    reason4.labelText = @"The scope of job changed after the on-site visit.";
    reason4.identifier = @"The scope of job changed after the on-site visit.";
    reason4.selected = NO;
    reason4.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason4.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason5 = [TNImageRadioButtonData new];
    reason5.labelText = @"Contractor can not meet my deadline.";
    reason5.identifier = @"Contractor can not meet my deadline.";
    reason5.selected = NO;
    reason5.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason5.selectedImage = [UIImage imageNamed:@"checked"];
    self.temperatureGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[reason1, reason2, reason3, reason4, reason5] layout:TNRadioButtonGroupLayoutVertical];
    self.temperatureGroup.identifier = @"Temperature group";
    [self.temperatureGroup create];
    NSLog(@"height :%f",self.view.frame.size.height);
    //NSLog(@"2 height :%f",self.view.bounds.size.height/4);

    self.temperatureGroup.position = CGPointMake(5, self.view.frame.size.height/12);
    
  //  [self addSubview:self.temperatureGroup];
    [self.view addSubview:self.temperatureGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:self.temperatureGroup];
}
- (void)temperatureGroupUpdated:(NSNotification *)notification {
    NSLog(@"[MainView] Temperature group updated to %@", self.temperatureGroup.selectedRadioButton.data.identifier);
}
- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self name:SELECTED_RADIO_BUTTON_CHANGED object:self.temperatureGroup];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
