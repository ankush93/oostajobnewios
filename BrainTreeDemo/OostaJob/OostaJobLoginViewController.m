//
//  OostaJobLoginViewController.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobLoginViewController.h"
#import "OostaJobEmailVerificationViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "SWRevealViewController.h"
#import "OostaJobContractorStatusViewController.h"
#import "OostaJobMapViewController.h"
#import "ContractorRearViewController.h"
@interface OostaJobLoginViewController ()<SWRevealViewControllerDelegate>
{
    KLCPopup* popup;
}
@end

@implementation OostaJobLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self removePlusIcon];
    [self setupUI];
    [self CreateBlurEffectOnBackgroundImage];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view from its nib.
    
}
-(void)removePlusIcon
{
    _imgPlusEmail.image=[UIImage imageNamed:@""];
    _imgPlusPassword.image=[UIImage imageNamed:@""];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Setup UI
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.viewEmail.layer.cornerRadius=5.0;
        self.viewPassword.layer.cornerRadius=5.0;
        self.viewEmail.layer.masksToBounds=YES;
        self.viewPassword.layer.masksToBounds=YES;
        self.btnLogin.layer.cornerRadius=5.0;
        self.btnLogin.layer.masksToBounds=YES;
        self.btnSubmit.layer.cornerRadius=5.0;
        self.btnSubmit.layer.masksToBounds=YES;
        
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnSubmit.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnForgetPassword.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:25.0f]];
        
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtForgetEmail.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.lblEnterYourRegisteredEmail.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        
    }
    else
    {
        self.viewEmail.layer.cornerRadius=5.0;
        self.viewPassword.layer.cornerRadius=5.0;
        self.viewEmail.layer.masksToBounds=YES;
        self.viewPassword.layer.masksToBounds=YES;
        self.btnLogin.layer.cornerRadius=5.0;
        self.btnLogin.layer.masksToBounds=YES;
        self.btnSubmit.layer.cornerRadius=5.0;
        self.btnSubmit.layer.masksToBounds=YES;
        
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnForgetPassword.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
        [self.btnSubmit.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtForgetEmail.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.lblEnterYourRegisteredEmail.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
    }
}
#pragma mark - Create the Blur Background
-(void)CreateBlurEffectOnBackgroundImage
{
    [self.imgBlur assignBlur];
}
#pragma mark - Textfield Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtEmail)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else
    {
        [_txtForgetEmail resignFirstResponder];
        [_txtPassword resignFirstResponder];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField==_txtEmail)
    {
        if (text.length>0)
        {
            if ([self IsValidEmail:text])
            {
                _imgPlusEmail.image=[UIImage imageNamed:@"tick"];
            }
            else
            {
               _imgPlusEmail.image=[UIImage imageNamed:@""];
            }
            
        }
        else
        {
            _imgPlusEmail.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtPassword)
    {
        
        if (text.length>0&&(![string isEqualToString:@""]))
        {
            _imgPlusPassword.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusPassword.image=[UIImage imageNamed:@""];
        }
    }
    return YES;
}
- (void) scrollViewAdaptToStartEditingTextField:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, textField.frame.origin.y +1 * textField.frame.size.height);
    [self.scrollview setContentOffset:point animated:YES];
}
- (void) scrollVievEditingFinished:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField!=self.txtForgetEmail)
    {
        [self scrollViewAdaptToStartEditingTextField:textField];
    }
    
    return YES;
}



#pragma mark - set Button Actions
- (IBAction)btnBackTapped:(id)sender
{
    if (![self.strPage isEqualToString:@"CUSTOMER"])
    {
      [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([self.strPage isEqualToString:@"BUILD"])
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self gotoListOfJobs];
    }
    
}

- (IBAction)btnLoginTapped:(id)sender
{
    CAKeyframeAnimation * anim = [ CAKeyframeAnimation animationWithKeyPath:@"transform" ] ;
    anim.values = [ NSArray arrayWithObjects:
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f) ],
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f) ],
                   nil ] ;
    anim.autoreverses = YES ;
    anim.repeatCount = 2.0f ;
    anim.duration = 0.07f ;
    
    if (_txtEmail.text.length==0&&_txtPassword.text.length==0)
    {
        
        [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
        [ _viewPassword.layer addAnimation:anim forKey:nil ] ;
    }
    else if(![self IsValidEmail:_txtEmail.text])
    {
        [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
        
    }
    else if(_txtPassword.text.length==0)
    {
        [ _viewPassword.layer addAnimation:anim forKey:nil ] ;
    }
    else
    {
        NSLog(@"Login");
        [self.scrollview endEditing:YES];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
        [self LoginFunction];
    }
}
-(void)LoginFunction
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Log in...";
    hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
    hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
    hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
    hud.margin = 10.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSString * key1 =@"Email";
    NSString * obj1 =_txtEmail.text;
    NSString * key2 =@"Password";
    NSString * obj2 =_txtPassword.text;
    NSString *strToken=[[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TOKEN"];
    
    if (strToken.length==0)
    {
        strToken=@"28d1e4a814db45b0cdabf929106d5f9dd48d966900f1c591dcff70ecdd6b5e6d";
    }
    
    NSString * key3 =@"DeviceID";
    NSString * obj3 =strToken;
    NSString * key4 =@"DeviceType";
    NSString * obj4 =@"2";
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3,obj4]
                                   forKeys:@[key1,key2,key3,key4]];
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login",BaseURL]]];
    NSLog(@"login:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
//             NSString *userdetails = [result valueForKeyPath:@"UserDetails.account_holder_name"];
//             NSLog(@"userdetails :%@",userdetails);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
//                 NSString *accountholdername = [result valueForKeyPath:@"UserDetails.account_holder_name"];
//                 NSString *accountnumber = [result valueForKeyPath:@"UserDetails.account_number"];
//                 NSString *bankname = [result valueForKeyPath:@"UserDetails.bank_name"];
//                 NSString *routing = [result valueForKeyPath:@"UserDetails.routing"];
//                 NSLog(@"account name%@",accountholdername);
//                 NSLog(@"account number%@",accountnumber);
//                 NSLog(@"bank name%@",bankname);
//                 NSLog(@"routing%@",routing);
//
                 NSUserDefaults *userr = [NSUserDefaults standardUserDefaults];
                 [userr setObject:[result valueForKeyPath:@"UserDetails.userID"] forKey:@"useridlogin"];
                 [userr synchronize];
                 // *** Brain tree id ***
                 
                 NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];
                 [brain setObject:[result valueForKeyPath:@"UserDetails.braintreeID"] forKey:@"BrainTreeid"];
                 [brain synchronize];
                 
//                 //**** Saving Bank Details ****//
//                 NSUserDefaults *ac = [NSUserDefaults standardUserDefaults];
//                 [ac setObject:accountholdername forKey:@"accountholdername"];
//                 [ac synchronize];
//
//                 NSUserDefaults *an = [NSUserDefaults standardUserDefaults];
//                 [an setObject:accountnumber forKey:@"accountnumber"];
//                 [an synchronize];
//
//                 NSUserDefaults *bank = [NSUserDefaults standardUserDefaults];
//                 [bank setObject:bankname forKey:@"bankname"];
//                 [bank synchronize];
//
//                 NSUserDefaults *ro = [NSUserDefaults standardUserDefaults];
//                 [ro setObject:routing forKey:@"routing"];
//                 [ro synchronize];

                 [hud hide:YES];
                 MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:HUD];
                 
                 HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 HUD.mode = MBProgressHUDModeCustomView;
                 HUD.color=[UIColor whiteColor];
                 HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 HUD.delegate = self;
                 HUD.labelText = @"Done";
                 
                 [HUD show:YES];
                 [HUD hide:YES afterDelay:2];
                 
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"UserDetails"] forKey:@"USERDETAILS"];
                 NSString *userid = [result valueForKeyPath:@"UserDetails.userID"];
                 NSLog(@"User id :%@",userid);
                 NSString *contractorid = [result valueForKeyPath:@"UserDetails.contractorID"];

#pragma mark - saving userid for bank details

                 ///****saving userid for bank details///***
                 NSUserDefaults *saveid = [NSUserDefaults standardUserDefaults];
                 [saveid setObject:contractorid forKey:@"Userid"];
                 [saveid synchronize];

                 NSUserDefaults *saveid1 = [NSUserDefaults standardUserDefaults];
                 [saveid1 setObject:userid forKey:@"Useridcont"];
                 [saveid1 synchronize];
                 if ([[result valueForKeyPath:@"UserDetails.userType"] isEqualToString:@"1"])
                 {
                     [self gotoListOfJobs];
                     [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                 }
                 else
                 {
                     
                     [self Bankdetail:^(bool banklist){
                         NSLog(@"data fetched");
                         if (banklist == NO) {
                             [self Gotobankdetails];
                         }
                         else
                         {
                             [self gotoContractorJobList];
                             [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
                         }
                     }];
                     
                     
                 }
                 
             }
             else
             {
                 
                 [hud hide:YES];
                 if([[result valueForKeyPath:@"status"]isEqualToString:@"Customer email not yet activated"]||[[result valueForKeyPath:@"status"]isEqualToString:@"Contractor email not yet activated"])
                 {
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     HUD.delegate = self;
                     HUD.labelText = @"Done";
                     
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                     [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"UserDetails"] forKey:@"USERDETAILS"];
                     
                     [self gotoEmailVerification];
                 }
                
                 else if ([[result valueForKeyPath:@"status"]isEqualToString:@"Contractor profile not yet activated"])
                 {
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     HUD.delegate = self;
                     HUD.labelText = @"Done";
                     
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                     [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"UserDetails"] forKey:@"USERDETAILS"];
                     [self gotoProfileVerification];
                 }
                 else if ([[result valueForKeyPath:@"status"]isEqualToString:@"Login Failed"])
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     
                     hud.labelText = @"Invalid Credentials";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 else if ([[result valueForKeyPath:@"status"]isEqualToString:@"Customer has been blocked by admin"])
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     
                     hud.labelText = @"You have been blocked by admin";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 else if ([[result valueForKeyPath:@"status"]isEqualToString:@"Contractor has been blocked by admin"])
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     
                     hud.labelText = @"You have been blocked by admin";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 else
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 
             }
             
         }
         
     }];
    }
}

-(void)Gotobankdetails{
    BankDetailcontroller *emailVerificationObj=[[BankDetailcontroller alloc]initWithNibName:@"BankDetailcontroller" bundle:nil];
    emailVerificationObj.passage = @"fromregister";
    [self.navigationController pushViewController:emailVerificationObj animated:YES];
}
-(void)gotoProfileVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController IPad" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    else
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    
}
-(void)gotoEmailVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController IPad" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    
    
}
-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)btnForgetPasswordTapped:(id)sender
{
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    popup = [KLCPopup popupWithContentView:_viewForgetPassword
                                  showType:KLCPopupShowTypeBounceInFromTop
                               dismissType:KLCPopupDismissTypeBounceOutToBottom
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:NO
                     dismissOnContentTouch:NO];
    [popup showWithLayout:layout];
}
- (IBAction)btnSubmitTapped:(id)sender
{
    if ([self IsValidEmail:_txtForgetEmail.text])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_viewForgetPassword animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Sending...";
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
        
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"Email";
        NSString * obj1 =_txtForgetEmail.text;
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@forgetpassword",BaseURL]]];
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKey:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     HUD.delegate = self;
                     HUD.labelText = @"Password Recovery Email Sent";
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                     [popup dismissPresentingPopup];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_viewForgetPassword animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     
                     hud.labelText = @"Enter registered Email";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 
                 
             }
         }];
        }
    }
    else
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_viewForgetPassword animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        //        hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
        
        hud.labelText = @"Enter Valid Email";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    
}
- (IBAction)btnClosePopupTapped:(id)sender
{
    [popup dismissPresentingPopup];
}

-(void)gotoListOfJobs
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    UINavigationController *viewControllerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.window.rootViewController = viewControllerNavigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"SKIPLOGIN"] isEqualToString:@"SKIPLOGIN"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SKIPLOGIN"];
        [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] goToBuildYourJob];
    }
}
-(void)gotoContractorJobList
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobMapViewController *ContractorfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    else
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    
    ContractorRearViewController *ContraRearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ContractorfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:ContraRearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


-(void)Bankdetail:(void (^)(bool banklist))completionHandler{
#pragma mark Getbankdetails
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading....";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getContractorBankDetails/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];
        
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getbankdetails:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
//                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//                 hud.mode = MBProgressHUDModeText;
//                 hud.color=[UIColor whiteColor];
//                 hud.labelColor=kMBProgressHUDLabelColor;
//                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
//
//                 hud.labelText = SERVER_ERR;
//                 hud.margin = 10.f;
//                 hud.yOffset = 20.f;
//                 hud.removeFromSuperViewOnHide = YES;
//                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     
                     
                     NSUserDefaults *acholder = [NSUserDefaults standardUserDefaults];
                     [acholder setObject:[result valueForKeyPath:@"Response"] forKey:@"bankfetch"];
                     [acholder synchronize];
                     
                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"Details"] isEqualToString:@"Contactor Bank Details not found"])
                 {
                     [hud hide:YES];
//                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//                     hud.mode = MBProgressHUDModeText;
//                     hud.color=[UIColor whiteColor];
//                     hud.labelColor=kMBProgressHUDLabelColor;
//                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
//
//                     hud.labelText = @"Please enter bank Details";
//                     hud.margin = 10.f;
//                     hud.yOffset = 20.f;
//                     hud.removeFromSuperViewOnHide = YES;
//                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
//                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//                     hud.mode = MBProgressHUDModeText;
//                     hud.color=[UIColor whiteColor];
//                     hud.labelColor=kMBProgressHUDLabelColor;
//                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
//
//                     hud.labelText = SERVER_ERR;
//                     hud.margin = 10.f;
//                     hud.yOffset = 20.f;
//                     hud.removeFromSuperViewOnHide = YES;
//                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
    
}

@end
