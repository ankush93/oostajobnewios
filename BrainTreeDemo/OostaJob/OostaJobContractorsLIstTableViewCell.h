//
//  OostaJobChatViewTableViewCell.h
//  OostaJob
//
//  Created by Apple on 12/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface OostaJobContractorsLIstTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet AsyncImageView *imgProfilePict;
@property (strong, nonatomic) IBOutlet UILabel *lblNotExceed;
@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnHireMe;
@property (strong, nonatomic) IBOutlet UIView *viewHireBtn;
@property (weak, nonatomic) IBOutlet UIImageView *star1;
@property (weak, nonatomic) IBOutlet UIImageView *star2;
@property (weak, nonatomic) IBOutlet UIImageView *star3;
@property (weak, nonatomic) IBOutlet UIImageView *star4;
@property (weak, nonatomic) IBOutlet UIImageView *star5;
@property (weak, nonatomic) IBOutlet UIView *viewStar;
@property (weak, nonatomic) IBOutlet UILabel *lblHireMe;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *stars;

@end
