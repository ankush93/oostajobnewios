//
//  OostaJobCustomerStoriesViewController.m
//  OostaJob
//
//  Created by Apple on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobCustomerStoriesViewController.h"

@interface OostaJobCustomerStoriesViewController ()
{
    MBProgressHUD *HUD;
}
@end

@implementation OostaJobCustomerStoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden=YES;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
    }
    else
    {
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
    [_imgBg assignBlur];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        //HUD
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.delegate = self;
        HUD.labelText = @"Loading...";
        
        //LOAD URL
        NSString* url = [[NSUserDefaults standardUserDefaults] objectForKey:@"CustomerStories"];
        NSLog(@"url:%@",url);
        NSURL* nsUrl = [NSURL URLWithString:url];
        NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];

        [_webviewCustomerStories loadRequest:request];
    }
}

#pragma mark - Webview Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //SHOW HUD
    [HUD show:YES];
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //KILL HUD
    
    [HUD hide:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if(!webView.loading)
    {
        //KILL HUD
        [HUD hide:YES];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnMenu:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}
@end
