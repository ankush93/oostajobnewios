//
//  OostaJobMediaCollectionViewCell.h
//  OostaJob
//
//  Created by Apple on 05/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobMediaCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *imgThumbNail;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnDidSelect;

@end
