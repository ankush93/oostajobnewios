//  OostaJobContractorJobsHomeViewController.m
//  OostaJob
//
//  Created by Armor on 20/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobCustomerJobsViewController.h"
#import "OostaJobCustomerBiddingTableViewCell.h"
#import "OostaJobMediaCollectionViewCell.h"
#import "DWTagList.h"
#import <AVFoundation/AVFoundation.h>
#import "ImageCache.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "OostaJobCustomerJobDetailsViewController.h"
#import "CZPicker.h"
@interface OostaJobCustomerJobsViewController ()<DWTagListDelegate,UICollectionViewDataSource,UICollectionViewDelegate,SWRevealViewControllerDelegate,UIActionSheetDelegate,OostaJobCustomerJobDetailsViewControllerDelegate,MBProgressHUDDelegate,CZPickerViewDataSource, CZPickerViewDelegate>
{
    NSMutableString *reason;
     NSArray *fruits;
     CZPickerView *pickerWithImage;
    int page;
    BOOL isHidden;
    NSMutableDictionary *dictJobList;
    CGFloat height;
    
    NSString *animationDirection;
    int currentIndex;
    BOOL isHireTab;
    NSMutableArray *repostarray;
    KLCPopup *popupViewGreat;
    
    NSInteger indexer1;
    NSString *newbie;
    UIAlertView *alertinitialstat;
    int val;
    MBProgressHUD *HUD;
    KLCPopup *popupTermsAndConditions;

}

@end

@implementation OostaJobCustomerJobsViewController
@synthesize table;
@synthesize btnSender;
@synthesize list;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    repostarray = [[NSMutableArray alloc]init];
    
    fruits = @[@"Contractor didn’t show up.", @"Contractor didn’t complete the job.", @"Contractor didn’t honor the Not to Exceed price.", @"The scope of job changed after the on-site visit.", @"Contractor can not meet my deadline."];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"CZPicker";
    NSLog(@"UserId:%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = YES;
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self setupUI];
    [self CreateBlurEffectOnBackgroundImage];
    [self settingTheBackgroudImages];
    //    [self getJobList];
    //
    //    page = 0;
    //    [self BtnFunction];
    [self getreloadto:_redirectPage];
    isHidden=YES;
    if (IS_IPAD )
    {
        _customTblPopover = [[UIPopoverController alloc]initWithContentViewController:_optionViewController];
    }
    
    _tblBidding.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblClosed.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblHire.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblRatenow.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _tblBidding.backgroundColor = [UIColor clearColor];
    _tblClosed.backgroundColor = [UIColor clearColor];
    _tblHire.backgroundColor = [UIColor clearColor];
    _tblRatenow.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view from its nib.
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    //For Dot Button Hide
    _imgArrow.hidden = YES;
    _btnList.hidden = YES;
    [self showListViewOnTop];
}


-(void)showListViewOnTop
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (IS_IPAD)
                         {
                             _imgArrow.frame=CGRectMake(727, 75, 30, 30);
                             _viewListView.frame=CGRectMake(0, 100, 768, 50);
                             _scrollview.frame=CGRectMake(0, 150, 768, 790);
                             isHidden=NO;
                         }
                         else if (IS_IPHONE5)
                         {
                             _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                             _viewListView.frame=CGRectMake(0, 64, 320, 32);
                             _scrollview.frame=CGRectMake(0, 100, 320, 475);
                             isHidden=NO;
                         }
                         else
                         {
                             _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                             _viewListView.frame=CGRectMake(0, 64, 320, 32);
                             _scrollview.frame=CGRectMake(0, 100, 320, 390);
                             isHidden=NO;
                         }
                         
                     }
                     completion:nil];
}


#pragma mark - Create the Blur Background
-(void)CreateBlurEffectOnBackgroundImage
{
    [self.imgBlur assignBlur];
}

#pragma mark-UIDesign

-(void)setupUI
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {//Border Width
        self.btnBidding.layer.borderWidth=1.0;
        self.btnHire.layer.borderWidth=1.0;
        self.btnRatenow.layer.borderWidth=1.0;
        self.btnClosed.layer.borderWidth=1.0;
        //Border colour
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner radius
        self.btnBidding.layer.cornerRadius=18.0;
        self.btnHire.layer.cornerRadius=18.0;
        self.btnRatenow.layer.cornerRadius=18.0;
        self.btnClosed.layer.cornerRadius=18.0;
        
        //Colour
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        
        self.btnBidding.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        
        //Font
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnHire.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnRatenow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.btnClosed.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblHeading.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.btnRequestJob.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        
    }
    else
    {
        //Border Width
        self.btnBidding.layer.borderWidth=1.0;
        self.btnHire.layer.borderWidth=1.0;
        self.btnRatenow.layer.borderWidth=1.0;
        self.btnClosed.layer.borderWidth=1.0;
        //Border colour
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //Corner radius
        self.btnBidding.layer.cornerRadius=10.0;
        self.btnHire.layer.cornerRadius=10.0;
        self.btnRatenow.layer.cornerRadius=10.0;
        self.btnClosed.layer.cornerRadius=10.0;
        
        //Colour
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        
        self.btnBidding.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.highlightedTextColor = [UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        
        //Font
        self.btnBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnHire.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnRatenow.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.btnClosed.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:10.0f];
        self.lblHeading.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        
        self.btnRequestJob.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)settingTheBackgroudImages
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?840.0f:790.0f);
    }
    else if(IS_IPHONE5)
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?504.0f:475.0f);
    }
    else
    {
        self.scrollview.pagingEnabled = YES;
        self.scrollview.scrollEnabled=YES;
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 4, isHidden?420.0f:390.0f);
    }
    
    
}
//scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView==_scrollview)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
        else
        {
            //find the page number you are on
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
    }
}

//dragging ends, please switch off paging to listen for this event
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *) targetContentOffset
NS_AVAILABLE_IOS(5_0){
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView==_scrollview)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
        else
        {
            //find the page number you are on
            CGFloat pageWidth = scrollView.frame.size.width;
            page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"Dragging - You are now on page %i",page);
            [self BtnFunction];
        }
    }
    
}
#pragma mark-Swipe Action page

-(IBAction)btnTapped:(id)sender
{
    if ([sender tag]==300)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0, 0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(0,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        
        
    }
    
    else if ([sender tag]==301)
    {
        
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(768,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(320,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
    }
    else if ([sender tag]==302)
    {
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(1536,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(640,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        
    }
    else
    {
        
        if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(2304,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                                 
                             }
                             completion:nil];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 CGPoint point = CGPointMake(960,0);
                                 [self.scrollview setContentOffset:point animated:YES];
                             }
                             completion:nil];
        }
        
    }
    
}

- (IBAction)btnMenuTapped:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}
-(void)BtnFunction
{
    int values= 300+page;
    if (values==300)
    {
        self.btnBidding.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnBidding.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - Bidding";
        
        [self.tblBidding reloadData];
        
    }
    else if (values==301)
    {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - Hire";
        
        [self.tblHire reloadData];
        
    }
    else if (values==302)
    {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnClosed.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.btnClosed.titleLabel.textColor=[UIColor grayColor];
        self.lblHeading.text = @"Jobs - In Progress";
        
        [self.tblRatenow reloadData];
        
    }
    else if (values==303)
    {
        self.btnBidding.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnHire.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnRatenow.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.btnClosed.layer.borderColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000].CGColor;
        self.btnBidding.titleLabel.textColor=[UIColor grayColor];
        self.btnHire.titleLabel.textColor=[UIColor grayColor];
        self.btnRatenow.titleLabel.textColor=[UIColor grayColor];
        self.btnClosed.titleLabel.textColor=[UIColor colorWithRed:0.059 green:0.455 blue:0.996 alpha:1.000];
        self.lblHeading.text = @"Jobs - Closed";
        
        [self.tblClosed reloadData];
        
    }
}






#pragma mark -UitableView DataSource and Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]!=0)
        {
            return [[dictJobList valueForKeyPath:@"Jobbidding"] count];
        }
        else
        {
            return 1;
        }
        
    }
    else if (tableView==self.tblHire)
    {
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]!=0)
        {
            return [[dictJobList valueForKeyPath:@"Jobhired"] count];
        }
        else
        {
            return 1;
        }
        
    }
    else if (tableView==self.tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobrated"] count]!=0)
        {
            return [[dictJobList valueForKeyPath:@"Jobrated"] count];
        }
        else
        {
            return 1;
        }
        
    }
    else if (tableView == table)
    {
        return list.count;
    }
    else
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]!=0)
        {
            return [[dictJobList valueForKeyPath:@"Jobclosed"] count];
        }
        else
        {
            return 1;
        }
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tblIdentifier=@"CELL";
    tableView.backgroundColor=[UIColor clearColor];
    
    if (tableView==self.tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Bidding";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[self.tblBidding dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    cell.viewbids.layer.cornerRadius=14.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewamount.layer.cornerRadius=5.0;
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    cell.cancelButton.layer.cornerRadius =5.0;
                    cell.cancelButton.tag = indexPath.row;
                    cell.completedbtn.layer.cornerRadius = 5.0;
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;
                    cell.imgArrow.hidden = YES;
                    
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    
                    cell.viewamount.layer.cornerRadius=5.0;
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    
                    
                    cell.cancelButton.layer.cornerRadius =5.0;
                    cell.cancelButton.tag = indexPath.row;
                    cell.completedbtn.layer.cornerRadius = 5.0;
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;

                    cell.imgArrow.hidden = YES;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobpostTime"]];
            
            cell.viewamount.hidden = YES;
            
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            //            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            
            
            [arrTags addObject:[NSString stringWithFormat:@"Wait %@ to hire",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]]];
            
            cell.tagList.isHighlightLast = YES;
            
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+(IS_IPAD?5:3), cell.lblDesc.frame.size.width, IS_IPAD?70:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            
            NSLog(@"x==>>%f,y==>>%f,wid==>>%f,hei==>>%f",cell.viewContractor.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            
            //Enhance cancel Button
            cell.cancelButton.frame =CGRectMake(cell.cancelButton.frame.origin.x, origin+(IS_IPAD?8:5), cell.cancelButton.frame.size.width, cell.cancelButton.frame.size.height);
            [cell.cancelButton addTarget:self action:@selector(biddingBtnTappedmain:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"x==>>%f,y==>>%f,wid==>>%f,hei==>>%f",cell.cancelButton.frame.origin.x, origin+(IS_IPAD?8:5), cell.cancelButton.frame.size.width, cell.cancelButton.frame.size.height);
            
            
            origin = origin+(IS_IPAD?8:5);
            cell.reasonslist.hidden = YES;

            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.viewContractor.frame.origin.y+cell.viewContractor.frame.size.height+(IS_IPAD?10:7));
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:7));
            
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                               });
                
            }
            
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            if ([[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"] intValue]>0)
            {
                cell.imgMsg.image = [UIImage imageNamed:@"msgSel-120.png"];
                cell.lblMessageCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
            }
            else
            {
                cell.imgMsg.image = [UIImage imageNamed:@"mail-grey.png"];
                cell.lblMessageCount.textColor = [UIColor lightGrayColor];
            }
            NSLog(@"%@",[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"Contractorcnt"]]);
            if ([[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"Contractorcnt"] ] intValue]>0)
            {
                cell.lblContratorCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"Contractorcnt"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"notificationCount"]] intValue]>0)
            {
                JSBadgeView *badgeView = [[JSBadgeView alloc] initWithParentView:cell.viewMessage alignment:JSBadgeViewAlignmentTopRight];
                [badgeView setBadgeBackgroundColor:[UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000]];
                [badgeView setBadgeTextFont:[UIFont fontWithName:FONT_BOLD size:IS_IPAD?16.0f:12.0f]];
                badgeView.badgeText = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"notificationCount"]];
                badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
                badgeView.layer.masksToBounds = YES;
            }
            
            
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor clearColor];
            return cell;
        }
    }
    else if (tableView==self.tblHire)
    {
        
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Hire";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            
            return cell;
        }
        else
        {
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[self.tblHire dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    cell.viewbids.layer.cornerRadius=14.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewamount.layer.cornerRadius=5.0;
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    cell.cancelButton.layer.cornerRadius = 5.0;
                    cell.cancelButton.backgroundColor = [UIColor grayColor];
                    cell.cancelButton.tag = indexPath.row;
                    cell.cancelButton.hidden = YES;
                    cell.completedbtn.hidden = YES;

                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    //cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    
                    cell.viewamount.layer.cornerRadius=5.0;
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    
                    cell.cancelButton.layer.cornerRadius = 5.0;
                    cell.cancelButton.backgroundColor = [UIColor grayColor];
                    cell.cancelButton.tag = indexPath.row;
                    cell.cancelButton.hidden = YES;
                    
                    cell.completedbtn.layer.cornerRadius = 5.0;
                    cell.completedbtn.backgroundColor = [UIColor grayColor];
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    //cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            NSString * strKey = @"Jobhired";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobpostTime"]];
            
            NSLog(@"date check :%@",dateString);
            
            cell.viewamount.hidden = YES;
            
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            [cell.btnTblBidding addTarget:self action:@selector(HireBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"Hire" forState:UIControlStateNormal];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            if (arrContractor.count>0)
            {
                cell.lblContratorCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrContractor.count];
                cell.lblContratorCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
                cell.imgMem.image = [UIImage imageNamed:@"contr-120.png"];
            }
            else
            {
                cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
                cell.lblContratorCount.textColor = [UIColor lightGrayColor];
                cell.imgMem.image = [UIImage imageNamed:@"mem-grey.png"];
            }
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+(IS_IPAD?5:3), cell.lblDesc.frame.size.width, IS_IPAD?70:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            cell.reasonslist.hidden = YES;

            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            origin = origin+(IS_IPAD?8:5);
            
            NSLog(@"%f",cell.viewContractor.frame.origin.y);
            NSLog(@"%f",cell.viewContractor.frame.size.height);
            
            //            cell.cancelButton.frame =CGRectMake(cell.cancelButton.frame.origin.x, origin+(IS_IPAD?8:5), cell.cancelButton.frame.size.width, cell.cancelButton.frame.size.height);
            //            origin = origin+(IS_IPAD?8:5);
            //
            //            NSLog(@"cancel_origin==>>%f",cell.cancelButton.frame.origin.y);
            //            NSLog(@"cancel_height==>>%f",cell.cancelButton.frame.size.height);
            //
            //            [cell.cancelButton addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.viewContractor.frame.origin.y+cell.viewContractor.frame.size.height+(IS_IPAD?10:7));
            NSLog(@"%f",cell.viewContent.frame.origin.y);
            NSLog(@"%f",cell.viewContent.frame.size.height);
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:7));
            NSLog(@"%f",cell.contentView.frame.origin.y);
            NSLog(@"%f",cell.contentView.frame.size.height);
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            
            
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            return cell;
        }
    }
    else if (tableView==self.tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobrated"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Rate Now";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[self.tblRatenow dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    cell.viewbids.layer.cornerRadius=14.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblamount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewamount.layer.cornerRadius = 5.0;
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    cell.cancelButton.layer.cornerRadius =5.0;
                    cell.cancelButton.tag = indexPath.row;
                    cell.cancelButton.hidden = YES;
                    cell.completedbtn.layer.cornerRadius =5.0;
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblamount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    cell.viewamount.layer.cornerRadius = 5.0;
                    
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.cancelButton.tag = indexPath.row;
                    cell.completedbtn.tag = indexPath.row;

                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    cell.cancelButton.hidden = NO;
                    cell.completedbtn.hidden = NO;

                    cell.viewContent.layer.cornerRadius=5;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            
            
            NSString * strKey = @"Jobrated";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobpostTime"]];
            
            NSString * strKey1 = @"Jobrated";
            
            NSString *dateString1=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey1] objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
            NSString *str1 =[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",dateString1]];
            NSString* strName = [[str1 componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString *ct_title = [strName stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
            ct_title = [ct_title stringByReplacingOccurrencesOfString:@",    " withString:@", "];
            cell.lblamount.text = [NSString stringWithFormat:@"%@",ct_title];
            
            cell.viewamount.hidden = NO;
            
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            //            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"In Progress" forState:UIControlStateNormal];
            cell.btnTblBidding.titleLabel.adjustsFontSizeToFitWidth=true;
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            //        NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            //        arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            //        if (arrContractor.count>0)
            //        {
            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",1];
            cell.lblContratorCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
            cell.imgMem.image = [UIImage imageNamed:@"contr-120.png"];
            
            //        }
            //        else
            //        {
            //            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
            //            cell.lblContratorCount.textColor = [UIColor lightGrayColor];
            //        }
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+(IS_IPAD?5:3), cell.lblDesc.frame.size.width, IS_IPAD?70:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            origin = origin+(IS_IPAD?8:5);
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            cell.reasonslist.hidden = YES;

            
            cell.cancelButton.frame =CGRectMake(cell.cancelButton.frame.origin.x, origin+(IS_IPAD?70:50), cell.cancelButton.frame.size.width, cell.cancelButton.frame.size.height);
            cell.completedbtn.frame =CGRectMake(cell.completedbtn.frame.origin.x, origin+(IS_IPAD?70:50), cell.completedbtn.frame.size.width, cell.completedbtn.frame.size.height);
            
            cell.viewamount.frame =CGRectMake(cell.viewamount.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewamount.frame.size.width, cell.viewamount.frame.size.height);
            origin = origin+(IS_IPAD?8:5);
            
            NSLog(@"completed frame==>>%f",cell.completedbtn.frame.size.width);

            NSLog(@"cancel_origin==>>%f",cell.viewamount.frame.origin.y);
            NSLog(@"cancel_height==>>%f",cell.viewamount.frame.size.height);
            
            [cell.cancelButton addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.completedbtn addTarget:self action:@selector(Completedtapped:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.viewContractor.frame.origin.y+cell.viewContractor.frame.size.height+(IS_IPAD?10:50));
            
            cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:30));
            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            cell.lblMeetingTime.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?18.0f:12.0f];
            cell.lblMeetingTime.frame=CGRectMake(cell.lblMeetingTime.frame.origin.x, cell.viewContractor.frame.origin.y+3, cell.lblMeetingTime.frame.size.width, cell.lblMeetingTime.frame.size.height);
            cell.imgCalendar.frame=CGRectMake(cell.imgCalendar.frame.origin.x, cell.viewContractor.frame.origin.y+3, cell.imgCalendar.frame.size.width, cell.imgCalendar.frame.size.height);
            cell.imgCalendar.hidden = NO;
            cell.lblMeetingTime.hidden = NO;
            cell.lblMeetingTime.text = [NSString stringWithFormat:@"%@ %@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingDate"],[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingTime"]];
            if ([[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"0000-00-00"]||[[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingDate"]] isEqualToString:@"<null>"])
            {
                cell.imgCalendar.hidden = YES;
                cell.lblMeetingTime.hidden = YES;
            }
            else
            {
                NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingDate"]];
                __block NSDate *detectedDate;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
                [detector enumerateMatchesInString:dateString
                                           options:kNilOptions
                                             range:NSMakeRange(0, [dateString length])
                                        usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                 {
                     detectedDate = result.date;
                     NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
                     [dateFormate setDateFormat:@"MM-dd-YYYY"];
                     cell.lblMeetingTime.text = [NSString stringWithFormat:@"%@ %@",[dateFormate stringFromDate:detectedDate],[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.meetingTime"]];
                 }];
            }
            cell.imgArrow.hidden = YES;
            cell.btnTblBidding.userInteractionEnabled = NO;
            return cell;
        }
    }
    else if (tableView==self.tblClosed)
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"No Jobs in Closed";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
            return cell;
        }
        else
        {
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[self.tblClosed dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(120, 120);
                cell.collectionView.collectionViewLayout = flowLayout;
                cell.collectionView.backgroundColor=[UIColor clearColor];
                cell.collectionView.tag=501;
                cell.collectionView.hidden=YES;
                cell.collectionView.delegate=self;
                cell.collectionView.dataSource=self;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    
                    cell.btnTblBidding.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                    cell.viewbids.layer.cornerRadius=14.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:20.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                    cell.viewamount.layer.cornerRadius = 5.0;
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    
                    cell.cancelButton.layer.cornerRadius = 5.0;
                    cell.cancelButton.backgroundColor = [UIColor grayColor];
                    cell.cancelButton.tag = indexPath.row;
                    cell.cancelButton.hidden = YES;
                    
                    
                    cell.completedbtn.layer.cornerRadius = 5.0;
                    cell.completedbtn.backgroundColor = [UIColor grayColor];
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;
                    cell.viewContent.layer.cornerRadius=5;
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    // Disable Cell highlight
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    //Font name annd size
                    
                    cell.lblTblJobName.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
                    cell.lblTblPin.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
                    cell.lblDesc.font=[UIFont fontWithName:FONT_THIN size:14.0f];
                    cell.lblContratorCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.lblMessageCount.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    cell.viewbids.layer.cornerRadius=10.0;
                    cell.txtDescription.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
                    [cell.btnTblBidding.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
                    cell.viewamount.layer.cornerRadius=5.0;
                    
                    
                    cell.viewMessage.layer.cornerRadius=5.0;
                    cell.viewContractor.layer.cornerRadius=5.0;
                    cell.viewContent.layer.cornerRadius=5;
                    
                    cell.cancelButton.layer.cornerRadius = 5.0;
                    cell.cancelButton.backgroundColor = [UIColor grayColor];
                    cell.cancelButton.tag = indexPath.row;
                    cell.cancelButton.hidden = YES;
                    
                    cell.completedbtn.layer.cornerRadius = 5.0;
                    cell.completedbtn.backgroundColor = [UIColor grayColor];
                    cell.completedbtn.tag = indexPath.row;
                    cell.completedbtn.hidden = YES;
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    cell.viewbids.tag=100;
                    cell.btnTblBidding.tag=200;
                }
                cell.dropdown.tag = 1000;
            }
            
            NSString * strKey = @"Jobclosed";
            cell.lblTblJobName.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobtypeName"]];
            cell.lblTblPin.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"zipcode"]];
            cell.lblDesc.text=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            NSString *dateString = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobpostTime"]];
            
            NSString * strKey1 = @"Jobclosed";
            
            NSString *dateString1=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey1] objectAtIndex:indexPath.row] valueForKeyPath:@"bidAmount"]];
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
            NSString *str1 =[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",dateString1]];
            NSString* strName = [[str1 componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString *ct_title = [strName stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
            ct_title = [ct_title stringByReplacingOccurrencesOfString:@",    " withString:@", "];
            cell.lblamount.text = [NSString stringWithFormat:@"%@",ct_title];
            
            cell.viewamount.hidden = NO;
            
            
            __block NSDate *detectedDate;
            //Detect.
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:dateString
                                       options:kNilOptions
                                         range:NSMakeRange(0, [dateString length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             {
                 detectedDate = result.date;
                 NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
                 NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                 
                 NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:detectedDate];
                 NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:detectedDate];
                 NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
                 
                 NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:detectedDate];
                 
                 NSLog(@"detectedDate:%@",detectedDate);
                 NSLog(@"currentDate:%@",[NSDate date]);
                 cell.lblTime.text = [self remaningTime:destinationDate endDate:[NSDate date]];
                 NSLog(@"remaini time:%@",[self remaningTime:destinationDate endDate:[NSDate date]]);
                 
             }];
            
            cell.btnTblBidding.tag=indexPath.row;
            //            [cell.btnTblBidding addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            NSArray *reasonarray= [[NSArray alloc]init];
            NSString *reasonstring = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"reason"];
            if ([reasonstring isEqual:[NSNull null]]) {
                
            }
            else
            {
                reasonarray = [reasonstring componentsSeparatedByString:@","];
                
                
                
                
                
            }
            NSLog(@"reasons array :%@",reasonarray);
            
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            //        cell.viewbids.backgroundColor= [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            [cell.btnTblBidding setTitle:@"Closed" forState:UIControlStateNormal];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            cell.tagList.userInteractionEnabled = NO;
            
            [cell.reasonslist setTags:reasonarray];
            //  }
            
            [cell.reasonslist setAutomaticResize:YES];
            [cell.reasonslist setTagDelegate:self];
            cell.reasonslist.userInteractionEnabled = NO;
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            CGFloat origin = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
            
            
            //        NSMutableArray *arrContractor=[[NSMutableArray alloc]init];
            //        arrContractor = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"contractorlist"] mutableCopy];
            //        if (arrContractor.count>0)
            //        {
            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",1];
            cell.lblContratorCount.textColor = [UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1.000];
            cell.imgMem.image = [UIImage imageNamed:@"contr-120.png"];
            //        }
            //        else
            //        {
            //            cell.lblContratorCount.text = [NSString stringWithFormat:@"%d",0];
            //            cell.lblContratorCount.textColor = [UIColor lightGrayColor];
            //        }
            cell.lblDesc.frame=CGRectMake(cell.lblDesc.frame.origin.x, origin+(IS_IPAD?5:3), cell.lblDesc.frame.size.width, IS_IPAD?70:44);
            
            if (str.length!=0)
            {
                
                origin=origin+(IS_IPAD?75:47);
            }
            else
            {
                cell.lblDesc.hidden=YES;
                origin=origin+0;
            }
            
            cell.collectionView.frame = CGRectMake(cell.collectionView.frame.origin.x, origin+(IS_IPAD?5:3), cell.collectionView.frame.size.width, IS_IPAD?120:60);
            
            if (arr.count!=0)
            {
                origin = origin + (IS_IPAD?123:63);
            }
            else
            {
                cell.collectionView.hidden=YES;
                origin=origin+0;
            }
            origin = origin+(IS_IPAD?8:5);
            
            cell.viewMessage.frame =CGRectMake(cell.viewMessage.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewMessage.frame.size.width, cell.viewMessage.frame.size.height);
            
            cell.viewContractor.frame =CGRectMake(cell.viewContractor.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewContractor.frame.size.width, cell.viewContractor.frame.size.height);
            //  origin = origin+(IS_IPAD?8:5);
            
            
            cell.ratenowbtn.frame = CGRectMake(cell.ratenowbtn.frame.origin.x, cell.viewContractor.frame.size.height+origin+14, cell.ratenowbtn.frame.size.width, cell.ratenowbtn.frame.size.height);
            
            
            cell.cancelButton.frame =CGRectMake(cell.cancelButton.frame.origin.x, origin+(IS_IPAD?8:5), cell.cancelButton.frame.size.width, cell.cancelButton.frame.size.height);
            
            cell.viewamount.frame =CGRectMake(cell.viewamount.frame.origin.x, origin+(IS_IPAD?8:5), cell.viewamount.frame.size.width, cell.viewamount.frame.size.height);
            origin = origin+(IS_IPAD?8:5);
            
            NSLog(@"cancel_origin==>>%f",cell.cancelButton.frame.origin.y);
            NSLog(@"cancel_height==>>%f",cell.cancelButton.frame.size.height);
            
            [cell.cancelButton addTarget:self action:@selector(biddingBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            cell.ratenowbtn.tag=indexPath.row;
            [cell.ratenowbtn addTarget:self action:@selector(rate:) forControlEvents:UIControlEventTouchUpInside];
            
            if (reasonarray.count == 0) {
                cell.congrdzlbl.hidden = YES;
                cell.reasonslist.hidden = YES;
                
                
                cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.ratenowbtn.frame.origin.y+cell.ratenowbtn.frame.size.height+(IS_IPAD?10:10));
                
                cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.viewContent.frame.origin.y+cell.viewContent.frame.size.height+(IS_IPAD?10:10));
            }
            else{
                cell.congrdzlbl.hidden = NO;
                cell.reasonslist.hidden = NO;
                NSLog(@"cell.ratenowbtn.hidden %hhd",cell.ratenowbtn.hidden);
                if (cell.ratenowbtn.hidden) {
                    NSLog(@"enter true");
                    cell.congrdzlbl.frame = CGRectMake(cell.congrdzlbl.frame.origin.x, cell.viewContractor.frame.size.height+origin+40, cell.congrdzlbl.frame.size.width, cell.congrdzlbl.frame.size.height);
                    
                    cell.reasonslist.frame = CGRectMake(cell.viewMessage.frame.origin.x, cell.congrdzlbl.frame.size.height+origin+80, cell.reasonslist.frame.size.width, cell.reasonslist.frame.size.height);
                    
                    cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height+(IS_IPAD?10:10));
                    
                    cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height+(IS_IPAD?10:10));
                }
                else
                {                    NSLog(@"enter false");

               
                cell.congrdzlbl.frame = CGRectMake(cell.congrdzlbl.frame.origin.x, cell.ratenowbtn.frame.size.height+origin+40, cell.congrdzlbl.frame.size.width, cell.congrdzlbl.frame.size.height);
                
                cell.reasonslist.frame = CGRectMake(cell.viewMessage.frame.origin.x, cell.congrdzlbl.frame.size.height+origin+80, cell.reasonslist.frame.size.width, cell.reasonslist.frame.size.height);
                
                cell.viewContent.frame = CGRectMake(cell.viewContent.frame.origin.x, cell.viewContent.frame.origin.y, cell.viewContent.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height+(IS_IPAD?10:10));
                
                cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.reasonslist.frame.origin.y+cell.reasonslist.frame.size.height+(IS_IPAD?10:10));
                }
                }
            

            
            [cell CollectionData:[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] andtheIndexPath:indexPath andTheTableView:tableView];
            NSString *imageCacheKey = [[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"] stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
            {
                cell.imgIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"jobicon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                       UIImage *img=[UIImage imageWithData:data];
                                       if (img)
                                       {
                                           [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey];
                                           cell.imgIcon.image=img;
                                       }
                                   });
                                   
                                   
                               });
                
            }
            cell.btnMessage.tag = indexPath.row;
            [cell.btnMessage addTarget:self action:@selector(GotoMessage:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnContractor.tag = indexPath.row;
            [cell.btnContractor addTarget:self action:@selector(GotoContractors:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            
            cell.imgArrow.hidden = YES;
            cell.btnTblBidding.userInteractionEnabled = NO;
            
            
            cell.viewStars.frame=CGRectMake(cell.viewStars.frame.origin.x, cell.viewContractor.frame.origin.y, cell.viewStars.frame.size.width, cell.viewStars.frame.size.height);
            float totalRating = [[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.totalRating"]] floatValue];
            NSString *nullvalue = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"ContractorDetails.totalRating"]];
            cell.lblMessageCount.text = [NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexPath.row] valueForKeyPath:@"messageCount"]];
            
            NSLog(@"check star rating :%@",nullvalue);
            
            if ([nullvalue  isEqual: @"<null>"] || [nullvalue  isEqual:[NSNull null]] || [nullvalue  isEqual:nil]) {
                cell.viewStars.hidden =YES;
                cell.ratenowbtn.hidden = NO;
            }
            else
            {
                cell.viewStars.hidden =NO;
                cell.ratenowbtn.hidden = YES;

            }
            for (int i=1; i<=5; i++)
            {
                for (UIImageView *img in cell.imgStars)
                {
                    if (img.tag == 500+i)
                    {
                        if (i<=totalRating)
                        {
                            img.image = [UIImage imageNamed:@"star-fill.png"];
                        }
                        else if ((i-0.5)==totalRating)
                        {
                            img.image = [UIImage imageNamed:@"half.png"];
                        }
                        else
                        {
                            img.image =[UIImage imageNamed:@"empty.png"];
                        }
                    }
                }
            }
            return cell;
        }
    }
    else if (tableView==table)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        cell.textLabel.text = [list objectAtIndex:indexPath.row];
        
        
        cell.textLabel.textColor = [UIColor blackColor];
        
        return cell;
    }
    else
    {
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table)
    {
        return 40;
    }
    else if (tableView == _tblClosed)
    {
        
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[_tblClosed dequeueReusableCellWithIdentifier:tblIdentifier];
            
            NSLog(@"check please in taglist in height for row jobs :%@",cell.tagList);
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            
            NSString *StrKey = @"Jobclosed";

            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            NSArray *reasonarray= [[NSArray alloc]init];
            NSString *reasonstring = [[[dictJobList valueForKeyPath:StrKey] objectAtIndex:indexPath.row] valueForKeyPath:@"reason"];
            if ([reasonstring isEqual:[NSNull null]]) {
                
            }
            else
            {
                reasonarray = [reasonstring componentsSeparatedByString:@","];
                
                
                
                
                
            }
            NSLog(@"reasons array :%@",reasonarray);
            
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            NSLog(@"cell.taglist in checker place 1 :%@",cell.tagList);
            NSLog(@"cell.taglist in checker place 1:%f",cell.tagList.frame.size.height);
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            
            NSLog(@"cell.taglist in checker place :%@",cell.tagList);
            NSLog(@"cell.taglist in checker place :%f",cell.tagList.frame.size.height);

            [cell.reasonslist setAutomaticResize:YES];
            [cell.reasonslist setTags:reasonarray];
            [cell.reasonslist setTagDelegate:self];
            
            height = 0;
            
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+17;
                
            }
            else
            {
                if (reasonarray.count == 0) {
                    height=cell.tagList.frame.size.height+cell.tagList.frame.origin.y+(IS_IPAD?350:200);
                }
                else
                {
          height=cell.tagList.frame.size.height+cell.reasonslist.frame.size.height+320;
                }
                if (str.length==0)
                {
                    height = height-(IS_IPAD?70:47);
                }
                if (arr.count==0)
                {
                    height = height-(IS_IPAD?135:63);
                }
            }
            return height;
        }
        
    }
    else if (tableView == _tblRatenow)
    {
        
        if ([[dictJobList valueForKeyPath:@"Jobrated"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[_tblRatenow dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            height = 0;
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+17;
                
            }
            else
            {
                height=cell.tagList.frame.size.height+50+cell.tagList.frame.origin.y+(IS_IPAD?350:158);
                
                if (str.length==0)
                {
                    height = height-(IS_IPAD?70:47);
                }
                
                if (arr.count==0)
                {
                    height = height-(IS_IPAD?135:63);
                }
            }
            return height;
        }
        
    }
    else if (tableView == _tblHire)
    {
        
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
            return  IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[_tblHire dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            height = 0;
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+17;
                
            }
            else
            {
                height=cell.tagList.frame.size.height+cell.tagList.frame.origin.y+(IS_IPAD?350:158);
                
                if (str.length==0)
                {
                    height = height-(IS_IPAD?70:47);
                }
                
                if (arr.count==0)
                {
                    height = height-(IS_IPAD?135:63);
                }
            }
            return height;
        }
        
    }
    else if (tableView == _tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
            return IS_IPAD?60:40;
        }
        else
        {
            static NSString *tblIdentifier=@"CELL";
            OostaJobCustomerBiddingTableViewCell *cell=(OostaJobCustomerBiddingTableViewCell*)[self.tblBidding dequeueReusableCellWithIdentifier:tblIdentifier];
            if (cell==nil)
            {
                NSArray *nib;
                if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    
                    
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell IPad" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.tagList.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
                    
                }
                else
                {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"OostaJobCustomerBiddingTableViewCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
            }
            
            NSMutableArray *arrAnswers=[[NSMutableArray alloc]init];
            arrAnswers = [[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobanwserlist"];
            NSMutableArray *arrTags=[[NSMutableArray alloc]init];
            for (int i=0; i<arrAnswers.count; i++)
            {
                if ([[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_key"] isEqualToString:@"1"] )
                {
                    if ([NSString stringWithFormat:@"%@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"]].length == 0 )
                    {
                        [arrTags addObject:[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]];
                    }
                    else
                    {
                        [arrTags addObject:[NSString stringWithFormat:@"%@ %@",[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"tag_keyword"],[[arrAnswers  objectAtIndex:i] valueForKeyPath:@"answer"]]];
                    }
                    
                }
            }
            
            NSString *str=[NSString stringWithFormat:@"%@",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"description"]];
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"jobmedialist"] mutableCopy];
            
            [arrTags addObject:[NSString stringWithFormat:@"Wait %@ to hire",[[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] valueForKeyPath:@"Remaining_time"]]];
            
            cell.tagList.isHighlightLast = YES;
            
            [cell.tagList setAutomaticResize:YES];
            [cell.tagList setTags:arrTags];
            [cell.tagList setTagDelegate:self];
            
            height = 0;
            
            if (IS_IPAD)
            {
                height = 0;
                height = cell.tagList.frame.origin.y+cell.tagList.frame.size.height;
                if (str.length!=0)
                {
                    
                    height=height+75;
                }
                else
                {
                    height=height+0;
                }
                if (arr.count!=0)
                {
                    height = height + 123;
                }
                else
                {
                    
                    height=height+0;
                }
                height = height+5+35+10+10+17;
                
            }
            else
            {
                height=cell.tagList.frame.size.height+cell.tagList.frame.origin.y+(IS_IPAD?350:158);
                
                if (str.length==0)
                {
                    height = height-(IS_IPAD?70:47);
                }
                
                if (arr.count==0)
                {
                    height = height-(IS_IPAD?135:63);
                }
            }
            
            
            return height;
        }
        
    }
    else
    {
        return 0;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _tblBidding)
    {
        if ([[dictJobList valueForKeyPath:@"Jobbidding"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
        
    }
    else if(tableView == _tblHire)
    {
        if ([[dictJobList valueForKeyPath:@"Jobhired"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
        
    }
    else if(tableView == _tblRatenow)
    {
        if ([[dictJobList valueForKeyPath:@"Jobrated"] count]==0)
        {
            
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
    }
    else if(tableView == _tblClosed)
    {
        if ([[dictJobList valueForKeyPath:@"Jobclosed"] count]==0)
        {
        }
        else
        {
            [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:indexPath.row] andMoveTo:@"DidSelect"];
        }
        
    }
    else if (tableView==table)
    {
        if (page == 1)
        {
            if (indexPath.row == 0)
            {
                [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:currentIndex] andMoveTo:@"Contractor"];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                message:@"If you cancel the job, the job will be removed from the OostaJob"
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel Job"
                                                      otherButtonTitles:@"Dismiss",nil];
                alert.tag = currentIndex;
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                            message:@"If you cancel the job, the job will be removed from the OostaJob"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel Job"
                                                  otherButtonTitles:@"Dismiss",nil];
            alert.tag = currentIndex;
            [alert show];
        }
        [self hideDropDown:btnSender];
        [self rel];
        
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if([newbie isEqualToString:@"from2nd"]){
        if (buttonIndex == 1 ) {
            if (val == 1) {
                NSLog(@"alertinitialstat.tag%ld",(long)alertinitialstat.tag);
                [self Secondpayment:alertinitialstat.tag andCompletionHandler:^(bool result){
                    if (result == YES) {
                        
                    }
                }];
                
                
            }
            else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=kMBProgressHUDLabelColor;
                hud.backgroundColor=kMBProgressHUDBackgroundColor;
                
                hud.labelText = @"Please check the agreement";
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
        }
    }

    if ([newbie isEqualToString:@"cancelo"]) {
        
    if (buttonIndex == 0)
    {
        NSString *strKey;
        if (page==0)
        {
            strKey = @"Jobbidding";
        }
        else if (page == 1)
        {
            isHireTab = YES;
            strKey = @"Jobhired";
        }
        else if (page == 2)
        {
            strKey = @"Jobrated";
        }
        else
        {
            strKey = @"Jobclosed";
        }
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            [self getCanceltheJobForTheArray:[[dictJobList valueForKeyPath:strKey] objectAtIndex:alertView.tag] andCompletionHandler:^(bool resultCanceltheJob)
             {
                 if (resultCanceltheJob == YES)
                 {
                     [self getJobListandCompletionHandler:^(bool resultJobList) {
                         if (resultJobList == YES)
                         {
                             NSLog(@"job list Call3d");
                         }
                     }];
                     NSLog(@"Job Cancelled Called");
                 }

             }];
        }
    }
    }
    else{
        
    }
}
    

-(void)GotoMessage:(UIButton *)sender
{
    if (page==0)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    else if (page == 1)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    else if (page == 2)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    else
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:sender.tag] andMoveTo:@"Message"];
    }
    
}
-(void)GotoContractors:(UIButton *)sender
{
    if (page==0)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobbidding"] objectAtIndex:sender.tag] andMoveTo:@"Contractor"];
    }
    else if (page == 1)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobhired"] objectAtIndex:sender.tag] andMoveTo:@"Contractor"];
    }
    else if (page == 2)
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:sender.tag] andMoveTo:@"Contractor"];
    }
    else
    {
        [self gotoJobDetails:[[dictJobList valueForKeyPath:@"Jobclosed"] objectAtIndex:sender.tag] andMoveTo:@"Contractor"];
    }
    
}

-(void)HireBtnTapped:(UIButton *)sender
{
    if(self.dropDown == nil)
    {
        CGFloat f = 80;
        self.dropDown = [[UIView alloc]init];
        NSArray *arr =[[NSArray alloc]initWithObjects:@"Hire",@"Cancel", nil];
        [self showDropDown:sender andHeight:&f theContentArr:arr theDiection:@"down"];
        //            [self.mapView bringSubviewToFront:_dropDown];
        currentIndex = sender.tag;
    }
    else {
        [self hideDropDown:sender];
        [self rel];
        currentIndex = 0;
    }
}
-(void)Completedtapped:(UIButton *)sender{
    NSLog(@"button tag :%ld",(long)sender.tag);
    
    [self CheckPayableAmountAPI:sender.tag andCompletionHandler:^(bool result){
        if (result == YES) {
            [self loadcheckbox:sender.tag];

            
            
            
        }
    }];
    
//    [self Secondpayment:sender.tag andCompletionHandler:^(bool result){
//        if (result == YES) {
//
//        }
//    }];
    
    
    }


-(void)loadcheckbox:(NSInteger)index{
        NSNumber *amount1;
        NSUserDefaults *a = [NSUserDefaults standardUserDefaults];
        amount1 = [a objectForKey:@"amount1"];
    NSLog(@"final amount :%@",amount1);
        NSString *initialamount = @"Final installment of amount ";
        NSString *deduct = @"$ will be deducted";
        NSString *amountget = [initialamount stringByAppendingString:[amount1 stringValue]];
        NSString *gotcha = [amountget stringByAppendingString:deduct];
    newbie = @"from2nd";
    alertinitialstat = [[UIAlertView alloc] initWithTitle:@"OOstajob"
                                                  message:gotcha
                                                 delegate:self cancelButtonTitle:
                        @"Cancel" otherButtonTitles:@"OK", nil];
    self.termsview.backgroundColor = [UIColor clearColor];
    alertinitialstat.delegate = self;
    UIAlertView *viewmain = [[UIAlertView alloc]initWithFrame:CGRectMake(10, 20, 100, 40)];
    [viewmain addSubview:self.termsview];
    /** adding the textfield to the alertView **/
    alertinitialstat.tag = index;
    [alertinitialstat setValue:viewmain forKey:@"accessoryView"];

    [alertinitialstat show];

}

-(void)Secondpayment:(NSInteger)index andCompletionHandler:(void (^)(bool result))completionHandler
{
    NSLog(@"index in second payment :%ld",(long)index);
    
        [self CheckPayableAmountAPI:index andCompletionHandler:^(bool result){
            if (result == YES) {
                // [self showDropIn:clientToken];
              //  [self CustomerjobCompleted:index andCompletionHandler:^(bool resultcompleted){
                    OostaJobContractorBillingViewController *Obj=[[OostaJobContractorBillingViewController alloc]init];
                    
                    
                    Obj.struserid1 =[[dictJobList valueForKeyPath:@"Jobrated.customerID"] objectAtIndex:index];
                    Obj.strpostid1 =[[dictJobList valueForKeyPath:@"Jobrated.jobpostID"] objectAtIndex:index];
                    Obj.arraydetails = [[dictJobList valueForKeyPath:@"Jobrated.ContractorDetails"] objectAtIndex:index];
                    Obj.fromcomplete = @"fromcomplete";
                    [self.navigationController pushViewController:Obj animated:YES];
               // }];
                
                
                
            }
        }];
}

         
-(void)CheckPayableAmountAPI:(NSInteger)indexfind andCompletionHandler:(void (^)(bool result))completionHandler
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"";
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
           
            
            
            
            
            NSString * key1 =@"jobId";
            NSString * obj1 = [[dictJobList valueForKeyPath:@"Jobrated.jobpostID"] objectAtIndex:indexfind];
            
            NSLog(@"object checker 1:%@",obj1);
            

            NSString * key2 =@"contractor_id";
            NSString * obj2 = [[dictJobList valueForKeyPath:@"Jobrated.ContractorDetails.contractorID"] objectAtIndex:indexfind];
            
            NSLog(@"object checker 2:%@",obj2);
            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2]
                                           forKeys:@[key1,key2]];
            
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            //        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@payableAmount",BaseURL]]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://52.41.134.73/payableAmount"]];
            NSLog(@"jobhired URL:%@",BaseURL);
            
            NSLog(@"jobhired URL:%@",request.URL);
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     
                     NSLog(@"Server Error : %@", error);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                     {
                         
                         
                         
                         NSUserDefaults *contractorid1 = [NSUserDefaults standardUserDefaults];
                         [contractorid1 setObject:[[dictJobList valueForKeyPath:@"Jobrated.ContractorDetails.contractorID"] objectAtIndex:indexfind] forKey:@"contractorid1"];
                         [contractorid1 synchronize];
                         
                         NSUserDefaults *amount1 = [NSUserDefaults standardUserDefaults];
                         [amount1 setObject:[result valueForKeyPath:@"Response.bidAmount"] forKey:@"amount1"];
                         [amount1 synchronize];
                         
                         NSUserDefaults *jobid1 = [NSUserDefaults standardUserDefaults];
                         [jobid1 setObject:[result valueForKeyPath:@"Response.jobID"] forKey:@"jobid1"];
                         [jobid1 synchronize];
                         
                         
                         [hud hide:YES];
                         MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         [self.navigationController.view addSubview:hud];
                         
                         hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         hud.mode = MBProgressHUDModeCustomView;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         hud.delegate = self;
                         hud.labelText = @"Success";
                         [hud show:YES];
                         [hud hide:YES afterDelay:2];
                         // [self gotoJobs];
                         
                         
                     }
                     else
                     {
                         NSLog(@"%@",SERVER_ERR);
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = SERVER_ERR;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                         
                     }
                     
                     completionHandler(true);
                 }
             }];
        }
    }

         
-(void)biddingBtnTappedmain:(UIButton *)sender
{
    newbie = @"cancelo";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"If you cancel the job, the job will be removed from the OostaJob"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel Job"
                                              otherButtonTitles:@"Dismiss",nil];
    
        alert.tag = sender.tag;
        [alert show];
}
-(void)biddingBtnTapped:(UIButton *)sender
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"OOstaJob" cancelButtonTitle:@"Dismiss" confirmButtonTitle:@"Cancel Job"];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.tag=sender.tag;
    
    indexer1 = sender.tag;
    [picker show];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
//                                                    message:@"If you cancel the job, the job will be removed from the OostaJob"
//                                                   delegate:self
//                                          cancelButtonTitle:@"Cancel Job"
//                                          otherButtonTitles:@"Dismiss",nil];
//
//    alert.tag = sender.tag;
//    [alert show];
    
    //    currentIndex = sender.tag;
    //    if(self.dropDown == nil)
    //    {
    //        CGFloat f = 40;
    //        self.dropDown = [[UIView alloc]init];
    //        NSArray *arr;
    //        if (page==1)
    //        {
    //            f = 80;
    //           arr =[[NSArray alloc]initWithObjects:@"Hire",@"Cancel", nil];
    //        }
    //        else
    //        {
    //           arr =[[NSArray alloc]initWithObjects:@"Cancel", nil];
    //        }
    //
    //        [self showDropDown:sender andHeight:&f theContentArr:arr theDiection:@"down"];
    //        //            [self.mapView bringSubviewToFront:_dropDown];
    //
    //    }
    //    else {
    //        [self hideDropDown:sender];
    //        [self rel];
    //    }
    
}
#pragma mark-Button Action
-(IBAction)btnListTapped:(id)sender
{
    if (isHidden==YES)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             if (IS_IPAD)
                             {
                                 _imgArrow.frame=CGRectMake(727, 75, 30, 30);
                                 _viewListView.frame=CGRectMake(0, 100, 768, 50);
                                 _scrollview.frame=CGRectMake(0, 150, 768, 790);
                                 isHidden=NO;
                             }
                             else if(IS_IPHONE5)
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 32);
                                 _scrollview.frame=CGRectMake(0, 100, 320, 475);
                                 isHidden=NO;
                             }
                             else
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 20);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 32);
                                 _scrollview.frame=CGRectMake(0, 100, 320, 390);
                                 isHidden=NO;
                             }
                             
                         }
                         completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             if (IS_IPAD)
                             {
                                 _imgArrow.frame=CGRectMake(727, 75, 30, 0);
                                 _viewListView.frame=CGRectMake(0, 100, 768, 0);
                                 _scrollview.frame=CGRectMake(0, 100, 768, 840);
                                 isHidden=YES;
                             }
                             else if(IS_IPHONE5)
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 0);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 0);
                                 _scrollview.frame=CGRectMake(0, 64, 320, 504);
                                 isHidden=YES;
                             }
                             else
                             {
                                 _imgArrow.frame=CGRectMake(297, 49, 20, 0);
                                 _viewListView.frame=CGRectMake(0, 64, 320, 0);
                                 _scrollview.frame=CGRectMake(0, 64, 320, 430);
                                 isHidden=YES;
                             }
                             
                             
                         }
                         completion:nil];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        NSLog(@"Cancel");
    }
    else if (buttonIndex==1)
    {
        
        NSLog(@"Close");
    }
}


#pragma mark getJobList
-(void)getJobListandCompletionHandler:(void (^)(bool resultJobList))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading Job List...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcustomerjoblist/%@",BaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]]]];
        [request setTimeoutInterval:500];

        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSLog(@"getcustomerjoblist:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 isHireTab = NO;
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     dictJobList=[[NSMutableDictionary alloc]init];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobbidding"] forKey:@"Jobbidding"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobhired"] forKey:@"Jobhired"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobrated"] forKey:@"Jobrated"];
                     [dictJobList setObject:[result valueForKeyPath:@"Jobclosed"] forKey:@"Jobclosed"];
                     NSLog(@"dictJobList %@",dictJobList);
                     
                     NSMutableArray *arrCounts = [[NSMutableArray alloc]init];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobbidding"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobhired"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobrated"] count]]];
                     [arrCounts addObject:[NSNumber numberWithInteger:[[result valueForKeyPath:@"Jobclosed"] count]]];
                     NSArray *sortedArr = [arrCounts sortedArrayUsingSelector:@selector(compare:)];
                     
                     if ([[sortedArr lastObject] intValue]>=1)
                     {
                         [[NSUserDefaults standardUserDefaults]setObject:@"CUSTOMERJOBS" forKey:@"SCREEN"];
                     }
                     else
                     {
                         [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     }
                     [self reloadTableViews];
                     
                     if (isHireTab == YES){
                         [self.tblHire reloadData];
                     }
                     [self.tblRatenow reloadData];

                     [hud hide:YES];
                     completionHandler(true);
                     
                 }
                 else if ([[result valueForKeyPath:@"FaildStatus"] isEqualToString:@"No Jobs Found"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"No jobs found";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     completionHandler(false);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     isHireTab = NO;
                     completionHandler(false);
                 }
             }
         }];
    }
}
-(void)reloadTableViews
{
    [self.tblBidding reloadData];
    
}

#pragma mark day difference
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate{
    
    NSDateComponents *components;
    NSInteger seconds;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSInteger Years;
    NSString *durationString;
    
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitSecond
                                                 fromDate: startDate toDate: endDate options: 0];
    days = [components day];
    hour=[components hour];
    minutes=[components minute];
    months=[components month];
    Years=[components year];
    seconds=[components second];
    
    if(Years>0){
        
        if(Years>1){
            durationString=[NSString stringWithFormat:@"%ld years ago",(long)Years];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld year ago",(long)Years];
            
        }
        return durationString;
    }
    
    if(months>0){
        
        if(months>1){
            durationString=[NSString stringWithFormat:@"%ld months ago",(long)months];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld month ago",(long)months];
            
        }
        return durationString;
    }
    
    if(days>0){
        
        if(days>1){
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld day ago",(long)days];
            
        }
        return durationString;
    }
    
    if(hour>0){
        
        if(hour>1){
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld hr ago",(long)hour];
            
        }
        return durationString;
    }
    
    if(minutes>0){
        
        if(minutes>1){
            durationString=[NSString stringWithFormat:@"%ld mins ago",(long)minutes];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld min ago",(long)minutes];
            
        }
        return durationString;
    }
    
    if(seconds>0)
    {
        if(seconds>1){
            durationString=[NSString stringWithFormat:@"%ld secs ago",(long)seconds];
        }
        else{
            durationString=[NSString stringWithFormat:@"%ld sec ago",(long)seconds];
            
        }
        return durationString;
    }
    return @"less than min ago";
    
}

- (IBAction)requestaService:(id)sender
{
    [self gotoListOfJobs];
}

-(void)gotoListOfJobs
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (IBAction)btnCancelPressed:(id)sender {
}

- (IBAction)btnTermsAndConditionsClosePressed:(id)sender {
    [popupTermsAndConditions dismissPresentingPopup];
    [alertinitialstat show];
    
}

-(void)gotoJobDetails:(NSMutableArray *)array andMoveTo:(NSString *)strPage
{
    OostaJobCustomerJobDetailsViewController *DetObj;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        DetObj = [[OostaJobCustomerJobDetailsViewController alloc]initWithNibName:@"OostaJobCustomerJobDetailsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        DetObj = [[OostaJobCustomerJobDetailsViewController alloc]initWithNibName:@"OostaJobCustomerJobDetailsViewController" bundle:nil];
    }
    else
    {
        DetObj = [[OostaJobCustomerJobDetailsViewController alloc]initWithNibName:@"OostaJobCustomerJobDetailsViewController Small" bundle:nil];
    }
    DetObj.delegate = self;
    DetObj.arrSelected = [array mutableCopy];
    DetObj.strGoto = strPage;
    [self.navigationController pushViewController:DetObj animated:YES];
}

-(void)getreloadto:(NSString *)strPage
{
    [self getJobListandCompletionHandler:^(bool resultJobList) {
        if (resultJobList == YES)
        {
            if ([strPage isEqualToString:@"Rate"])
            {
                page = 2;
            }
            else if ([strPage isEqualToString:@"Closed"])
            {
                page = 3;
            }
            else if ([strPage isEqualToString:@"Hire"])
            {
                page = 1;
            }
            else
            {
                page = 0;
            }
            [self BtnFunction];
            CGPoint point = CGPointMake(self.view.frame.size.width*page,0);
            [self.scrollview setContentOffset:point animated:YES];
        }
    }];
    
}
#pragma mark DropDown

- (void)showDropDown:(UIButton *)b andHeight:(CGFloat *)height theContentArr:(NSArray *)arr theDiection:(NSString *)direction
{
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    
    // Initialization code
    CGRect btn = b.superview.frame;
    self.list = [NSArray arrayWithArray:arr];
    NSLog(@"arr:%@",arr);
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, -5);
    }else if ([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
        self.dropDown.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    self.dropDown.layer.masksToBounds = NO;
    self.dropDown.layer.cornerRadius = 8;
    self.dropDown.layer.shadowRadius = 5;
    self.dropDown.layer.shadowOpacity = 0.5;
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
    table.delegate = self;
    table.dataSource = self;
    table.layer.cornerRadius = 5;
    table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.separatorColor = [UIColor grayColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([direction isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
    } else if([direction isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, *height);
    [UIView commitAnimations];
    
    [self.dropDown addSubview:table];
    if (page==0)
    {
        UITableViewCell *cell = [_tblBidding cellForRowAtIndexPath:[NSIndexPath indexPathForRow:b.tag inSection:0]];
        UIView *view = (UIView *)[cell viewWithTag:100];
        [view.superview addSubview:self.dropDown];
        [table reloadData];
    }
    else if (page==1)
    {
        UITableViewCell *cell = [_tblHire cellForRowAtIndexPath:[NSIndexPath indexPathForRow:b.tag inSection:0]];
        UIView *view = (UIView *)[cell viewWithTag:100];
        [view.superview addSubview:self.dropDown];
        [table reloadData];
    }
    
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.superview.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.dropDown.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}
-(void)rel
{
    self.dropDown=nil;
}

#pragma mark Cancel Job

-(void)getCanceltheJobForTheArray:(NSMutableArray *)theArray andCompletionHandler:(void (^)(bool resultCanceltheJob))completionHandler
{
    repostarray = theArray;
//    [self callPostJobFortheContent:theArray andCompletionHandler:^(bool calljoblist)
//     {
//         if (calljoblist == YES) {
//             [self getJobListandCompletionHandler:^(bool resultJobList){
//                 NSLog(@"job list Call3d");
//
//             }];
//         }
//     }];

    ///
    
    ////
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Cancelling...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSLog(@"Userdetails.userid :%@",[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]);
    NSLog(@"Userdetails.jobid :%@",[NSString stringWithFormat:@"%@",[theArray valueForKeyPath:@"jobpostID"]]);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@cancelcustomer/%@/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"],[NSString stringWithFormat:@"%@",[theArray valueForKeyPath:@"jobpostID"]]]]];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"cancelcustomer:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             isHireTab = NO;
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = @"Job Cancelled Successfully";
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
//                 [self callPostJobFortheContent:theArray andCompletionHandler:^(bool calljoblist)
//                  {
//                      if (calljoblist == YES) {
//                          [self getJobListandCompletionHandler:^(bool resultJobList){
//                              NSLog(@"job list Call3d");
//
//                          }];
//                      }
//                  }];
                
                 completionHandler(true);
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 isHireTab = NO;
                 completionHandler(false);
             }
         }
     }];
}

# pragma mark RepostApi
-(void)callPostJobFortheContent:(NSMutableArray *)Repostarray andCompletionHandler:(void (^)(bool result))completionHandler
{


    NSString * key1 =@"userID";
    NSString * obj1 =[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];

    NSString * key2 =@"jobtypeID";
    NSString * obj2 =[repostarray valueForKeyPath:@"jobtypeID"];

    NSString * key3 =@"jobsubtypeID";
    NSString * obj3 =[repostarray valueForKeyPath:@"jobsubtypeID"];

    NSString * key4 =@"Address";
    NSString * obj4 =[repostarray valueForKeyPath:@"Address"];

    NSString * key5 =@"zipcode";
    NSString * obj5 =[repostarray valueForKeyPath:@"zipcode"];

    NSString * key6 =@"lat";
    NSString * obj6 =[repostarray valueForKeyPath:@"lat"];

    NSString * key7 =@"lng";
    NSString * obj7 =[repostarray valueForKeyPath:@"lng"];
    int i;
    NSString *mediacontent;
    NSMutableArray *mediaarray = [[NSMutableArray alloc]init];
        NSLog(@"media check :%@",[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:0] valueForKeyPath:@"mediaType"]);
        NSLog(@"media check :%@",[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:0] valueForKeyPath:@"mediaContent"]);
    NSLog(@"repost count :%@",repostarray);
    for (i=0; i<[repostarray count] ;i++) {
        NSMutableDictionary *dictM=[[NSMutableDictionary alloc]init];
        NSLog(@"check index :%lu",[[repostarray valueForKeyPath:@"jobmedialist"] count]);
        
     
        if(i >= [[repostarray valueForKeyPath:@"jobmedialist"] count]){
        }
        else{
        if ([[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i]!=0)
        {
            if ([[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaType"]  isEqual: @"1"]) {
                [dictM setObject:@"1" forKey:@"mediaType"];
                mediacontent =[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaContent"];
                
                mediacontent  = [mediacontent stringByReplacingOccurrencesOfString:@"public/assets/uploads/jobpost/" withString:@""];
                
                [dictM setObject:[NSString stringWithFormat:@"%@",mediacontent] forKey:@"mediaContent"];
            }   
         
            if ([[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaType"]  isEqual: @"2"]) {
                [dictM setObject:@"2" forKey:@"mediaType"];
                mediacontent =[[[repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaContent"];
                
                mediacontent  = [mediacontent stringByReplacingOccurrencesOfString:@"public/assets/uploads/jobpost/" withString:@""];
                
                [dictM setObject:[NSString stringWithFormat:@"%@",mediacontent] forKey:@"mediaContent"];
            }
        }
            [mediaarray addObject:dictM];

        }
        
    }
        NSLog(@"media check :%@",mediaarray);
        NSData * JsonDataMedia =[NSJSONSerialization dataWithJSONObject:[mediaarray valueForKeyPath:@"jobmedialist"] options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonStringMedia= [[NSString alloc] initWithData:JsonDataMedia encoding:NSUTF8StringEncoding];
        NSData *dataMedia = [jsonStringMedia dataUsingEncoding:NSUTF8StringEncoding];
       id jsonMedia = [NSJSONSerialization JSONObjectWithData:dataMedia options:0 error:nil];
   // NSString *myString = @"public/assets/uploads/jobpost/";


           NSString *  key8 =@"media";
           NSString *  obj8 =mediaarray;


   
    NSString * key9 =@"answerkey";
    NSString * obj9 =[repostarray valueForKeyPath:@"jobanwserlist"];

    NSString * key10 =@"description";
    //    NSString * obj10 =[dictSelected valueForKeyPath:[NSString stringWithFormat:@"%u",(arrCharacteristics.count-1)]];
    NSString * obj10 = [repostarray valueForKeyPath:@"description"];

    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10]
                                   forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10]];

    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];

    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@postjob",BaseURL]]];
    NSLog(@"PostJob URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);

             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];

             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {

                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];

                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Done";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];

//                 KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
//                 popupViewGreat = [KLCPopup popupWithContentView:_PopupContent
//                                                        showType:KLCPopupShowTypeBounceInFromTop
//                                                     dismissType:KLCPopupDismissTypeBounceOutToBottom
//                                                        maskType:KLCPopupMaskTypeDimmed
//                                        dismissOnBackgroundTouch:NO
//                                           dismissOnContentTouch:NO];
              //   [popupViewGreat showWithLayout:layout];

                 completionHandler(true);
             }
             else
             {

                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];

                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
         }
     }];
}



- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:fruits[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont systemFontOfSize:10]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return self->fruits[row];
}

- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row {
    return nil;
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    return fruits.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row {
   // NSLog(@"%@ is chosen!", fruits[row]);
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    reason = [[NSMutableString alloc]init];
    for (NSNumber *n in rows) {
        NSInteger row = [n integerValue];
        NSLog(@"before data :%@",reason);
        NSLog(@"%@ is chosen!", fruits[row]);
        [reason appendString:[NSString stringWithFormat:@"%@, ",fruits[row]]];

        }
    
    if (reason.length != 0) {

    NSLog(@"reason selected :%@",reason);
    NSLog(@"indexer in czpicker :%ld",(long)indexer1);
    NSString *strKey;
    if (page==0)
    {
        strKey = @"Jobbidding";
    }
    else if (page == 1)
    {
        isHireTab = YES;
        strKey = @"Jobhired";
    }
    else if (page == 2)
    {
        strKey = @"Jobrated";
    }
    else
    {
        strKey = @"Jobclosed";
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        
        [self getCanceltheJobForTheArray1:[[dictJobList valueForKeyPath:strKey] objectAtIndex:indexer1] :reason andCompletionHandler:^(bool resultCanceltheJob)
         
         
         {
             if (resultCanceltheJob == YES)
             {
                 [self gotoFeedback:indexer1];
                 
             }
             
         }];
    }
    }
    else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                            message:@"Please select a Reason to continue"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil,nil];
        
            //alert.tag = sender.tag;
            [alert show];
    }

    
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    [self.navigationController setNavigationBarHidden:YES];
    NSLog(@"Canceled.");
}

- (void)czpickerViewWillDisplay:(CZPickerView *)pickerView {
    NSLog(@"Picker will display.");
}

- (void)czpickerViewDidDisplay:(CZPickerView *)pickerView {
    NSLog(@"Picker did display.");
}

- (void)czpickerViewWillDismiss:(CZPickerView *)pickerView {
    NSLog(@"Picker will dismiss.");
}

- (void)czpickerViewDidDismiss:(CZPickerView *)pickerView {
    NSLog(@"Picker did dismiss.");
}



-(void)getCanceltheJobForTheArray1:(NSMutableArray *)theArray :(NSMutableString *)reason andCompletionHandler:(void (^)(bool resultCanceltheJob))completionHandler
{
    repostarray = theArray;
    //    [self callPostJobFortheContent:theArray andCompletionHandler:^(bool calljoblist)
    //     {
    //         if (calljoblist == YES) {
    //             [self getJobListandCompletionHandler:^(bool resultJobList){
    //                 NSLog(@"job list Call3d");
    //
    //             }];
    //         }
    //     }];
    
    ///
    
    ////
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Cancelling...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSLog(@"Userdetails.userid :%@",[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]);
    NSLog(@"Userdetails.jobid :%@",[NSString stringWithFormat:@"%@",[theArray valueForKeyPath:@"jobpostID"]]);
    NSString *key1 = @"jobId";
    NSString *obj1 = [theArray valueForKeyPath:@"jobpostID"];
    
    NSString *key2 = @"reason";
    NSString *obj2 = reason;
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2]
                                   forKeys:@[key1,key2]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@cancelAJob",BaseURL]]];
    NSLog(@"PostJob URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             isHireTab = NO;
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = @"Job Cancelled Successfully";
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
                 //                 [self getJobListandCompletionHandler:^(bool resultJobList) {
                 //                     if (resultJobList == YES)
                 //                     {
                 //                         NSLog(@"job list Call3d");
                 //                     }
                 //                 }];
                 
                 completionHandler(true);
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 isHireTab = NO;
                 completionHandler(false);
             }
         }
     }];
}


-(void)gotoFeedback:(NSInteger )indexer
{
    OostaJobFeedbackViewController *Obj=[[OostaJobFeedbackViewController alloc]init];
    Obj.strJobId =[[dictJobList valueForKeyPath:@"Jobrated.jobpostID"] objectAtIndex:indexer];
    Obj.arrDetails = [[dictJobList valueForKeyPath:@"Jobrated.ContractorDetails"] objectAtIndex:indexer];
    Obj.ratedarray = [[dictJobList valueForKeyPath:@"Jobrated"] objectAtIndex:indexer];
    Obj.check = @"from cancel";
    [self.navigationController pushViewController:Obj animated:YES];
}


-(void)CustomerjobCompleted:(NSUInteger)theIndex andCompletionHandler:(void (^)(bool resultcompleted))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSString * strKey = @"Jobrated";
        
        
        NSString *key1 = @"jobId";
        NSString *obj1 = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:theIndex] valueForKeyPath:@"jobpostID"];
        
        NSString *key2 = @"loggedUserId";
        NSString *obj2 = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
        
        
        NSString *key3 = @"action_by";
        NSString *obj3 = @"customer";
        
        NSString *key4 = @"userID";
        NSString *obj4 = [[[dictJobList valueForKeyPath:strKey] objectAtIndex:theIndex] valueForKeyPath:@"ContractorDetails.contractorID"];
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@makeJobCompleted",BaseURL]]];
        NSLog(@"PostJob URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"Job Successfully  Completed";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(true);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
}

-(void)rate:(UIButton *)sender
{
    OostaJobFeedbackViewController *DetObj;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController" bundle:nil];
    }
    else
    {
        DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController Small" bundle:nil];
    }
    DetObj.strJobId=[[dictJobList valueForKeyPath:@"Jobclosed.jobpostID"] objectAtIndex:sender.tag];
    DetObj.arrDetails= [[dictJobList valueForKeyPath:@"Jobclosed.ContractorDetails"] objectAtIndex:sender.tag];
    
    NSLog(@"arr details before :%@",DetObj.arrDetails);
    NSLog(@"sender tag :%ld",(long)sender.tag);

    [self.navigationController pushViewController:DetObj animated:YES];
}

- (IBAction)Onclickcheckbox:(id)sender {
    UIButton *btn = (UIButton *)sender;
    val = 0;
    if ([UIImagePNGRepresentation(btn.currentBackgroundImage) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"])]){
        val = 0;
        [btn setBackgroundImage:[UIImage imageNamed:@"uncheck-40.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        val = 1;
        
        
    }
}

-(IBAction)btnTermsandConditionTapped:(id)sender{
[alertinitialstat dismissWithClickedButtonIndex:0 animated:YES];

KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);

popupTermsAndConditions = [KLCPopup popupWithContentView:_viewTermsAndConditions
                                                showType:KLCPopupShowTypeFadeIn
                                             dismissType:KLCPopupDismissTypeFadeOut
                                                maskType:KLCPopupMaskTypeDimmed
                                dismissOnBackgroundTouch:NO
                                   dismissOnContentTouch:NO];
[popupTermsAndConditions showWithLayout:layout];

//HUD
HUD = [[MBProgressHUD alloc] initWithView:self.viewTermsAndConditions];
[self.viewTermsAndConditions addSubview:HUD];
HUD.delegate = self;
HUD.labelText = @"Loading...";

//LOAD URL
NSString* url = [[NSUserDefaults standardUserDefaults] objectForKey:@"Customerpaymentsterms"];
NSLog(@"url:%@",url);
NSURL* nsUrl = [NSURL URLWithString:url];
NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

[_webViewTermsAndConditions loadRequest:request];
}

@end
